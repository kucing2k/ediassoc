
CREATE TABLE  "edia_ads" (
  "ad_id" int PRIMARY KEY IDENTITY,
  "ad_client_id" int NOT NULL,
  "ad_title" varchar(200) NOT NULL,
  "ad_url" text NOT NULL,
  "ad_path" text NOT NULL,
  "status" int NOT NULL,
  "deleted" int NOT NULL,
  "deleted_dt" datetime DEFAULT NULL,
  "created_dt" datetime NOT NULL,
  "modified_dt" datetime DEFAULT NULL
);




CREATE TABLE  "edia_ads_activity" (
  "ad_activity_id" int PRIMARY KEY IDENTITY,
  "ad_id" int NOT NULL,
  "ad_view_cnt" int NOT NULL,
  "ad_click_cnt" int NOT NULL,
  "ad_activity_month" int NOT NULL,
  "ad_activity_year" int NOT NULL
);



CREATE TABLE  "edia_ads_clients" (
  "ad_client_id" int PRIMARY KEY IDENTITY,
  "ad_client_name" varchar(100) NOT NULL,
  "ad_client_contact" varchar(100) NOT NULL,
  "ad_client_email" varchar(100) NOT NULL,
  "ad_client_phone" varchar(20) DEFAULT NULL,
  "status" int NOT NULL,
  "deleted" int NOT NULL,
  "deleted_dt" datetime DEFAULT NULL,
  "created_dt" datetime NOT NULL,
  "modified_dt" datetime DEFAULT NULL
);



CREATE TABLE  "edia_box_status_cfg" (
  "stat_id" int PRIMARY KEY IDENTITY,
  "stat_path" text NOT NULL,
  "stat_desc" varchar(40) DEFAULT NULL
);




CREATE TABLE  "edia_ftp_access" (
  "DataKey" bigint PRIMARY KEY IDENTITY,
  "USERID" varchar(50) DEFAULT NULL,
  "PASSWORD" varchar(50) DEFAULT NULL,
  "FULLNAME" varchar(50) DEFAULT NULL,
  "FTPMAXSPACE" int DEFAULT NULL,
  "FTPMAXFILES" int DEFAULT NULL,
  "FTPFLAGS" int DEFAULT NULL,
  "created_dt" datetime DEFAULT NULL
) ;




CREATE TABLE  "edia_members" (
  "member_id" bigint PRIMARY KEY IDENTITY,
  "member_company_name" varchar(50) NOT NULL,
  "member_gateway" bigint DEFAULT NULL,
  "member_status" int DEFAULT NULL,
  "member_deleted" int DEFAULT NULL,
  "member_deleted_dt" datetime DEFAULT NULL,
  "member_created_dt" datetime DEFAULT NULL,
  "member_modified_dt" datetime DEFAULT NULL,
  "member_perm" int DEFAULT NULL,
  "member_login" varchar(20) NOT NULL,
  "billing_address_1" varchar(80) DEFAULT NULL,
  "billing_address_2" varchar(80) DEFAULT NULL,
  "billing_address_3" varchar(80) DEFAULT NULL,
  "billing_address_4" varchar(80) DEFAULT NULL,
  "billing_address_city" varchar(80) DEFAULT NULL,
  "billing_address_state" varchar(50) DEFAULT NULL,
  "billing_address_zip" varchar(50) DEFAULT NULL,
  "billing_address_country" varchar(50) DEFAULT NULL,
  "billing_address_contactName" varchar(50) DEFAULT NULL,
  "billing_address_telephone" varchar(50) DEFAULT NULL,
  "billing_email" text,
  "billing_amount" decimal(18,2) DEFAULT '0.01',
  "last_bill_dt" datetime DEFAULT NULL
) ;

CREATE TABLE  "edia_members_billing_options" (
  "billing_option_id" int PRIMARY KEY IDENTITY,
  "billing_option" varchar(50) NOT NULL,
  "billing_option_desc" text,
  "is_childable" tinyint DEFAULT NULL
) ;




CREATE TABLE  "edia_member_ids" (
  "DataKey" bigint PRIMARY KEY IDENTITY,
  "member_id" bigint DEFAULT NULL,
  "member_qual" varchar(2) DEFAULT NULL,
  "member_receiver_id" varchar(50) DEFAULT NULL
) ;



CREATE TABLE  "edia_member_perms" (
  "key_id" int PRIMARY KEY IDENTITY,
  "member_id" int NOT NULL,
  "user_app_id" int NOT NULL,
  "member_perm" int DEFAULT NULL
) ;




CREATE TABLE  "edia_member_users" (
  "user_id" int PRIMARY KEY IDENTITY,
  "user_member_id" int NOT NULL,
  "user_login" varchar(50) NOT NULL,
  "user_fname" varchar(50) NOT NULL,
  "user_lname" varchar(50) NOT NULL,
  "user_pass" varchar(50) NOT NULL,
  "user_remind" varchar(100) NOT NULL,
  "user_email" varchar(100) NOT NULL,
  "user_last_login_ip" varchar(15) DEFAULT NULL,
  "user_last_login_host" varchar(100) DEFAULT NULL,
  "user_last_login_dt" datetime DEFAULT NULL,
  "user_status" int DEFAULT NULL,
  "user_deleted" int DEFAULT NULL,
  "user_deleted_dt" datetime DEFAULT NULL,
  "user_created_dt" datetime DEFAULT NULL,
  "user_modified_dt" datetime DEFAULT NULL,
  "activation_key" varchar(50) DEFAULT NULL
) ;




CREATE TABLE  "edia_misc_lookups" (
  "id" bigint PRIMARY KEY IDENTITY,
  "lookup_id" bigint NOT NULL,
  "lookup_note" text NOT NULL
) ;




CREATE TABLE  "edia_partners" (
  "tp_id" int PRIMARY KEY IDENTITY,
  "tp_member_id" int NOT NULL,
  "tp_app_code" varchar(20) DEFAULT NULL,
  "tp_edi_code" varchar(20) DEFAULT NULL,
  "tp_name" varchar(100) DEFAULT NULL,
  "tp_contact" varchar(100) DEFAULT NULL,
  "tp_email" varchar(100) DEFAULT NULL,
  "tp_phone" varchar(30) DEFAULT NULL,
  "tp_status" int DEFAULT NULL,
  "tp_catalog" int DEFAULT NULL,
  "tp_image" text,
  "tp_deleted" int DEFAULT NULL,
  "tp_deleted_dt" datetime DEFAULT NULL,
  "tp_created_dt" datetime DEFAULT NULL,
  "tp_modified_dt" datetime DEFAULT NULL
) ;




CREATE TABLE  "edia_partner_profiles" (
  "tpp_id" int PRIMARY KEY IDENTITY,
  "tpp_member_id" int NOT NULL,
  "tpp_partner_id" int NOT NULL,
  "tpp_name" varchar(100) NOT NULL,
  "tpp_standard" char(3) NOT NULL,
  "tpp_version" varchar(10) NOT NULL,
  "tpp_transaction" varchar(20) DEFAULT NULL,
  "tpp_release" char(3) DEFAULT NULL,
  "tpp_testmode" char(1) DEFAULT NULL,
  "tpp_Direction" int NOT NULL,
  "tpp_Description" varchar(40) DEFAULT NULL,
  "tpp_ImportTemplateKEY" varchar(40) DEFAULT NULL,
  "tpp_DataEntryTemplateKEY" varchar(40) DEFAULT NULL,
  "tpp_TurnAroundTemplateKEY" varchar(40) DEFAULT NULL,
  "tpp_PrintTemplateKEY" varchar(40) DEFAULT NULL,
  "tpp_RelationshipTemplateKEY" varchar(40) DEFAULT NULL,
  "tpp_ExportTemplateKEY" varchar(40) DEFAULT NULL,
  "tpp_ComplianceCheckTemplateKEY" varchar(40) DEFAULT NULL,
  "tpp_ExportFileName" varchar(130) DEFAULT NULL,
  "tpp_ExportToFlatFileNow" int NOT NULL,
  "tpp_TurnAroundNow" int NOT NULL,
  "tpp_SequenceCheckType" int NOT NULL,
  "tpp_SkipComplianceCheck" int NOT NULL,
  "tpp_AckErrors" int NOT NULL,
  "tpp_AckExpected" int NOT NULL,
  "tpp_HoursOverdue" int NOT NULL,
  "tpp_AckTemplateKEY" varchar(40) DEFAULT NULL,
  "tpp_AckTransactionSetID" varchar(150) DEFAULT NULL,
  "tpp_ApplicationKEY" varchar(150) DEFAULT NULL,
  "tpp_Alias" varchar(150) DEFAULT NULL,
  "tpp_FunctionalGroupControlKEY" varchar(20) DEFAULT NULL,
  "tpp_InterchangeControlKEY" varchar(40) DEFAULT NULL,
  "tpp_ControlNumber" text,
  "tpp_ImmediateAckProcessing" int NOT NULL,
  "tpp_IsAcknowledgement" int NOT NULL,
  "tpp_status" int NOT NULL,
  "tpp_deleted" int NOT NULL,
  "tpp_deleted_dt" datetime DEFAULT NULL,
  "tpp_created_dt" datetime DEFAULT NULL,
  "tpp_modified_dt" datetime DEFAULT NULL
) ;




CREATE TABLE  "edia_support" (
  "id" bigint PRIMARY KEY IDENTITY,
  "to_member_id" bigint NOT NULL DEFAULT '1',
  "from_member_id" bigint NOT NULL,
  "type_id" smallint DEFAULT '0',
  "support_title" varchar(250) NOT NULL,
  "support_status" tinyint DEFAULT '0'
) ;




CREATE TABLE  "edia_support_log" (
  "id" int PRIMARY KEY IDENTITY,
  "support_id" bigint NOT NULL,
  "note_id" bigint NOT NULL,
  "user_id" bigint NOT NULL,
  "log_note_id" smallint DEFAULT '0',
  "log_date" datetime NOT NULL
) ;




CREATE TABLE  "edia_support_notes" (
  "id" bigint  PRIMARY KEY IDENTITY,
  "support_id" bigint NOT NULL,
  "notes" text NOT NULL
) ;




CREATE TABLE  "edia_tracker_issues" (
  "issue_id" int PRIMARY KEY IDENTITY,
  "user_id" int NOT NULL,
  "subject" varchar(127) NOT NULL,
  "status" int NOT NULL DEFAULT 0,
  "created" datetime NOT NULL,
  "updated" datetime DEFAULT NULL,
  "closed" datetime DEFAULT NULL
) ;



CREATE TABLE  "edia_tracker_issues_comments" (
  "comment_id" int PRIMARY KEY IDENTITY,
  "user_id" int NOT NULL,
  "issue_id" varchar(45) NOT NULL,
  "comment" text NOT NULL,
  "attachment" varchar(255) DEFAULT NULL,
  "status_change" int DEFAULT NULL,
  "internal" tinyint NOT NULL DEFAULT '0',
  "date" datetime NOT NULL
) ;



CREATE TABLE  "edia_user_apps" (
  "user_app_id" int PRIMARY KEY IDENTITY,
  "user_app_name" varchar(50) NOT NULL,
  "user_app_path" varchar(100) NOT NULL,
  "is_menu" char(1) DEFAULT NULL,
  "user_app_icon" text NOT NULL,
  "is_childable" tinyint DEFAULT NULL
) ;



CREATE TABLE  "edia_user_perms" (
  "user_perm_id" int PRIMARY KEY IDENTITY,
  "user_app_id" int NOT NULL,
  "user_user_id" int NOT NULL,
  "user_perm" int NOT NULL
) ;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
