<?php

// File Location: /site/news/index.php

require_once("tpl_unsecure.php");

// generate the header info
setHeader();

// start a new page with a banner ad
openPage(true);

?>
<!-- main content -->

<table border="0" width="724" cellpadding="0" cellspacing="0">
    <tr>
        <td width="723" height="3" colspan="6"></td>
        <td width="1" height="3"></td>
    </tr>
    <tr>
        <td width="3" height="479" rowspan="3"></td>
        <td width="488" height="470" rowspan="2">
            <hr>
            <p>
            <font color="#D3960E" size="5"><b>Privacy Policy</b></font></p>
            
            <p>Thank you for visiting our web site. This privacy policy tells you how we use personal information collected at this site. Please read this privacy policy before using the site or submitting any personal information. By using the site, you are accepting the practices described in this privacy policy. These practices may be changed, but any changes will be posted and changes will only apply to activities and information on a going forward, not retroactive basis. You are encouraged to review the privacy policy whenever you visit the site to make sure that you understand how any personal information you provide will be used.

            <p><b>Collection of Information</b>
            <p>We collect personally identifiable information, like names, postal addresses, email addresses, etc., when voluntarily submitted by our customer. The information you provide is only used to aid EDI Associates in billing for services sold and to help you get the most from your Van-on-Demand experience. Your information will never be divulged unless you give us permission to use it in another manner, for example to add you to one of our mailing lists.
            <p>
            <p><b>Cookie/Tracking Technology</b>
            <p>The Site may use cookie and tracking technology depending on the features offered. Cookie and tracking technology are useful for gathering information such as browser type and operating system, tracking the number of visitors to the Site, and understanding how visitors use the Site. Cookies can also help customize the Site for visitors. Personal information cannot be collected via cookies and other tracking technology, however, if you previously provided personally identifiable information, cookies may be tied to such information. Aggregate cookie and tracking information may be shared with third parties.
            <p>
            <p><b>Distribution of Information</b>
            <p>We may share information with governmental agencies or other companies assisting us in fraud prevention or investigation. We may do so when: (1) permitted or required by law; or, (2) trying to protect against or prevent actual or potential fraud or unauthorized transactions; or, (3) investigating fraud which has already taken place. The information is not provided to these companies for marketing purposes. 
            <p>
            <p><b>Commitment to Data Security</b>
            <p>Your personally identifiable information is kept secure. Only authorized employees, agents and contractors (who have agreed to keep information secure and confidential) have access to this information. All emails and newsletters from this site allow you to opt out of further mailings. 
            <p>
            <p><b>Privacy Contact Information</b>
            <p>If you have any questions, concerns, or comments about our privacy policy you may contact us using the information below:
            <p>
            <p>We reserve the right to make changes to this policy. Any changes to this policy will be posted.

        <td width="1" height="479" rowspan="3"></td>
        <td width="1" height="330"></td>
    </tr>
</table>

<p align="center"> &nbsp;

<?php

// print out footer information
closePage();

?>
