<?php

// File Location: /site/news/index.php

require_once("tpl_unsecure.php");

// generate the header info
setHeader();

// start a new page with a banner ad
openPage(true);

?>
<!-- main content -->

<table border="0" width="724" cellpadding="0" cellspacing="0">
    <tr>
        <td width="723" height="3" colspan="6"></td>
        <td width="1" height="3"></td>
    </tr>
    <tr>
        <td width="3" height="0" rowspan="3"></td>
        <td width="488" height="0" rowspan="2">
	<hr>

<p><font color="#D3960E" size="5"><b>Welcome to Van-On-Demand</b></font></p>
<P>A service brought to you by EDI Associates, Inc.</P>
<P>Van-On-Demand delivers a no thrills, no contracts, no minimum usages Store and Forward Value Added Network type of service that simply works. Use it as you need it (development, testing or even production) and only be billed for what you use. If you don't use it there is no bill and no salesman will ever call.</P>
<P>Van-On-Demand is not meant to replace your Value Added Networks and does not offer any interconnect services to other Value Added Networks. But to serve as an alternative to those relationships that may have been put on hold or all together cancelled due to cost. </P>
<P>Looking for a quick way to get 20 of your smaller vendors on line quickly? New accounts can be setup and able to start routing X12 transactions to your account in less than 5 minutes.</P>
<P>Lastly, we realize that no new service is without some faults and we are working to make improvements as fast as we can, so please let us know if there is anything we can do to make your Van-On-Demand experience better.</p>


</td>
        <td width="1" height="0" rowspan="3"></td>
        <td width="1" height="0"></td>
    </tr>
</table>

<p align="center"> &nbsp;</body>

<?php

// print out footer information
closePage();

?>
