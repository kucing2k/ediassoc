<?php

// File Location: /site/news/index.php

require_once("tpl_unsecure.php");

// generate the header info
setHeader();

// start a new page with a banner ad
openPage(true);

?>
<!-- main content -->

<table border="0" width="724" cellpadding="0" cellspacing="0">
    <tr>
        <td width="723" height="3" colspan="6"></td>
        <td width="1" height="3"></td>
    </tr>
    <tr>
        <td width="3" height="0" rowspan="3"></td>
        <td width="488" height="0" rowspan="2">
	<hr>

<p><font color="#D3960E" size="5"><b>Frequently Asked Questions</b></font></p>

<P>* <b>Does Van-On-Demand support Alias Sender/Receiver IDs?</b> This feature is currently not supported but will be coming soon.</p>
<P>* <b>How do I send data to my trading partners?</b> If your trading partner has a Van-On-Demand account you do not have to do anything, simply put their EDIA id into the Receiver ID of your X12 interchange and post the interchange to your Van-On-Demand Outbound directory.</p>
<P>* <b>What if my partner does not have a Van-On-Demand account?</b> Van-On-Demand at this time does not support interconnects with any other Value Added Network Services</p>
<P>* <b>What Communication Protocols are supported?</b> FTP (AS2 and SFTP are coming soon).</p>
<P>* <b>Which data formats are supported?</b> For now Van-On-Demand only has support for the X12 structure as covered by the Accredited Standards Committee (ASC) at www.x12.org.</p>



</td>
        <td width="1" height="0" rowspan="3"></td>
        <td width="1" height="0"></td>
    </tr>
</table>

<p align="center"> &nbsp;</body>

<?php

// print out footer information
closePage();

?>
