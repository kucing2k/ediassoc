<?php

// File Location: /site/news/index.php

require_once("tpl_unsecure.php");

// generate the header info
setHeader();

// start a new page with a banner ad
openPage(true);

?>
<!-- main content -->

<p align="center"><font size="6"><strong>EDI and E-Commerce Resources</strong></font></p>

<p align="center"><font size="5"><u>What is EDI - An Overview</u></font></p>

<p><font size="4">Electronic Data Interchange (EDI) is an electronic means for companies to exchange business documents, such as purchase orders and invoices, over telephone lines, IntraNet, and InterNet. Because EDI eliminates the need for sending paper documents through traditional means -- including mail, faxes, or telexes -- EDI is a major contributor to creating a &quot;paperless&quot; office environment. What sets EDI apart from other electronic tools is that EDI documents are standardized. </font></p>

<p><font size="4">Companies that exchange documents via EDI are
called trading partners. The implementation of EDI requires
trading partners to evaluate business procedures and to invest in
and learn about special software and hardware as well as
communications, standards, audit issues, and legal support
necessities. Although EDI implementation requires an investment
up front, the benefits generally outweigh the costs. </font></p>

<p><font size="4">The concept and technology of EDI have been
around for a long time. Many large corporations have long been
using EDI, some for as many as 25 years. In recent years, EDI has
caught the attention of the world, as governments and private
industries strive to become more competitive internationally and
increase profitability. Because EDI is a necessary component of
such cutting-edge manufacturing and supply strategies as Just In
Time (JIT), demand for EDI solutions has been growing rapidly.
Worldwide, companies are realizing that EDI as a technology can
help reduce administrative costs, accelerate information
processing, and ensure data accuracy. And EDI as a strategic tool
can eliminate certain business transactions, streamline business
procedures, re-engineer the way business is done, and tighten
vendor/customer relations. </font></p>

<p><font size="4">Today, most companies have implemented EDI to
some extent. Many companies are faced with the challenges
involved in further leveraging their existing EDI investment. One
of the biggest challenges EDI managers face is convincing
noncompliant trading partners to use EDI. Many middle-market
companies have initiated EDI activity because of pressure from a
larger &quot;hub&quot; company. The hub companies often start
with an aggressive trading partner penetration plan, but realize
quickly that expanding EDI to trading partners is not as easy as
they may have thought. </font></p>

<p><font size="4">Although the major hubs are using EDI with
their first-tier suppliers, they are struggling to extend EDI to
the second tier. Most middle-market organizations cannot provide
financial support for EDI projects to their smaller trading
partners, but many hubs do. Hubs have been known to penalize
those who are not using EDI, but many also provide assistance to
their trading partner based. Remember, leveraging your investment
in EDI requires a long-term perspective and commitment. For
example, you can extend your EDI investment by using it with
other electronic commerce technologies. </font></p>

<p><font size="4">At the outset, many believe that EDI will be
easy to implement. However, once you start implementing, you
realize how much more you can do with EDI. EDI involves computer
hardware and software and requires re-definition of typical
business tasks, ranging from keeping paper records in files to
accommodating audit procedures. EDI is a strategic tool that
reduces expenses, streamlines business procedures, and creates a
competitive advantage. Because EDI crosses many boundaries within
an organization, you need a sound EDI education for confident
decision making. Integration of EDI with other business
procedures in a company requires evaluation of current procedures
in an effort to define and prioritize EDI applications. It is
important to develop a long-term EDI strategy to ensure a
long-term successful program. </font></p>

<p><font size="4">EDI has been around since the late 1960s. The
transportation industry was primarily responsible for
establishing EDI in an effort to remain competitive in the world
market. As EDI gained popularity, it spread to other industries
and is now a part of business in all industries around the world.
The question is no longer whether your company should use EDI;
the question is whether you can afford not to. If you are not
using EDI, you will be soon. And if you are using EDI, you may
need to implement it more extensively or move to advanced forms
of EDI. </font></p>

<p><a href="Edi.php#top"><img src="../../images/top.gif" border="0" width="21"
height="22"></a></p>


<p align="center"> &nbsp;</body>

<?php

// print out footer information
closePage();

?>
