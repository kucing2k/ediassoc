<?php

// File Location: /site/news/index.php

require_once("tpl_unsecure.php");

// generate the header info
setHeader();

// start a new page with a banner ad
openPage(true);

?>
<!-- main content -->

<h3 align="center"><font size="6" face="Arial">EDI Costs to Benefits</font></h3>

<p><a href="edi_costs.php#intro"><font size="3" face="Arial"><strong>Introduction</strong></font></a></p>

<p><a href="edi_costs.php#benefits"><font size="3" face="Arial"><strong>Benefits
of Electronic Data Interchange</strong></font></a></p>

<ul>
    <li><font size="3" face="Arial"><strong>Improved Customer
        Service</strong></font></li>
    <li><font size="3" face="Arial"><strong>Lower Inventory
        Carrying Costs</strong></font></li>
    <li><font size="3" face="Arial"><strong>Decreased Procurement
        Costs</strong></font></li>
</ul>

<p><a href="edi_costs.php#costs"><font size="3" face="Arial"><strong>Costs
of Electronic Data Interchange</strong></font></a></p>

<ul>
    <li><font size="3" face="Arial"><strong>Personnel</strong></font></li>
    <li><font size="3" face="Arial"><strong>Training</strong></font></li>
    <li><font size="3" face="Arial"><strong>Software</strong></font></li>
    <li><font size="3" face="Arial"><strong>Communications</strong></font></li>
</ul>

<p><a href="edi_costs.php#intangible_b"><font size="3"
face="Arial"><strong>Intangible Benefits of Electronic Data
Interchange</strong></font></a></p>

<p><a href="edi_costs.php#intangible_c"><font size="3"
face="Arial"><strong>Intangible Costs of Electronic Data
Interchange</strong></font></a></p>

<p><a href="edi_costs.php#summary"><font size="3" face="Arial"><strong>Summary</strong></font></a></p>

<p><font size="5" face="Arial"><b></b></font>&nbsp;</p>

<p><a name="intro"><font size="5" face="Arial"><b></b></font></a><font
size="5" face="Arial"><b>Introduction</b></font></p>

<blockquote>
    <blockquote>
        <p><font size="3">Over the past few years, the EDI
        landscape has changed. The knowledge and skills needed to
        implement EDI have increased along with the number of EDI
        transaction sets and their complexity. However, the costs
        of implementing EDI have decreased. The
        telecommunications industry has developed Value Added
        Networks (VANs) to assist with EDI communications.
        Software vendors have created comprehensive,
        user-friendly EDI software translation packages and many
        consulting firms offer EDI development, maintenance, and
        training services.</font></p>
        <p><a href="edi_costs.php#top"><font size="3"><img
        src="../../images/top.gif" border="0" width="21" height="22"></font></a></p>
    </blockquote>
</blockquote>

<p><a href="edi_costs.php#intro" name="benefits"><font size="5"
face="Arial"><b></b></font></a><font size="5" face="Arial"><b>Benefits
of Electronic Data Interchange</b></font></p>

<blockquote>
    <blockquote>
        <p><font size="3">The major benefits of implementing EDI
        falls into three broad categories: improved customer
        service, lower inventory carrying costs, and decreased
        procurement costs.</font></p>
        <p><font size="4" face="Arial"><b>Improved Customer
        Service</b></font></p>
        <p><font size="3">Customer service is the package of
        activities provided by a supplier, which creates time,
        place, and form utility for a customer. Three components
        of customer service affected by EDI are pre-sale
        information, product availability, and data entry. </font></p>
        <p><font size="3">Pre-Sale Information. EDI partners at
        the beginning of the EDI relationship usually agree upon
        products, specifications, and prices. EDI orders are
        usually for standard frequently ordered products. In many
        businesses standard products, with volume business
        partners, account for 75 percent of total sales. Since
        the EDI purchase process is automated, the
        specifications, item prices, and estimated order volume
        for the orders transmitted over EDI are often agreed upon
        as part of the relationship. </font></p>
        <p><font size="3">Product Availability. Product
        availability is the supplier's ability to ship a finished
        product in a timely manner required by the purchaser. The
        benefits in product availability created by EDI are
        measured by the reduction in customer service staff time,
        lost sales, lost customer good-will, back orders, partial
        shipments, and expedites.</font></p>
        <p><font size="3">An EDI partnership is a close binding
        of two companies. EDI creates a &quot;virtual link&quot;
        between the customer's Retail Requirements System for a
        wholesaler or retailer or Material Requirement Planning
        (MRP) System for a manufacturer, and the supplier's
        Master Production Schedule (MPS) System. Often this
        linkage is formalized between two EDI trading partners by
        a blanket order to purchase a large number of goods in a
        year. In addition, EDI customers often share sales and
        forecasting information with the supplier to streamline
        the manufacturing process and to improve manufacturing
        capacity planning decisions.</font></p>
        <p><font size="3">This virtual linkage decreases the
        amount of time needed to process an order. No longer are
        requirements generated, vendors evaluated, purchase
        orders created, mailed, entered, and then processed. The
        unnecessary steps are left out and the remaining steps
        are automated through the EDI relationship. This fast
        reaction time, and shared sales and forecasting
        information gives the supplier a longer manufacturing
        lead-time. With a longer lead-time, the supplier can
        better determine manufacturing requirements and allocate
        manufacturing capacity to meet these requirements. When
        more manufacturing requirements can be covered, lost
        sales and related lost customer goodwill will be reduced.</font></p>
        <p><font size="3">While increasing the lead-time of the
        supplier, EDI can also decrease the order cycle-time of
        the customer. Order cycle-time is the time period from
        when a customer releases an order until the customer
        receives a shipment on that order from the supplier.</font></p>
        <p><font size="3">Companies try to shorten the order
        cycle time to decrease inventory-carrying costs,
        shrinkage costs, and improve cash flow. In a Just-In-Time
        (JIT) environment the requested receiving date by the
        customer is often no longer than the manufacturing time
        of the supplier and product shipment time. By helping
        manage a customer's inventory through decreased cycle
        time, a supplier can greatly improve customer goodwill
        and achieve volume business from that customer.</font></p>
        <p><font size="3">Back Orders, partial shipments, and
        expedites are often caused by an error in ordering
        process. A study by the Gartner Group (Evans-Correia, K.,
        &quot;EDI: the Future Frontier&quot;, Purchasing,
        February 1989, pp. 44-49) found that correcting an
        ordering mistake costs between 10 to 15 times as much as
        processing an accurate order. Since there will be few
        human errors in an automated EDI process there will be
        fewer costs of back-ordering, partial shipments, or
        expedites due to ordering mistakes.</font></p>
        <p><font size="3">Data Entry. Since EDI is the direct
        transmittal of business documents between computers, the
        printing of orders and invoices by customers and the
        subsequent entry of these documents by the suppliers are
        eliminated. While not all business partners, especially
        small ones, will become EDI ready, a company can
        drastically decrease the amount of data entry when large
        customers send documents through EDI. </font></p>
        <p><font size="4" face="Arial"><b>Customer Service
        Benefit</b></font></p>
    </blockquote>
</blockquote>

<blockquote>
    <blockquote>
        <ul>
            <li><font size="3">Decreased lost sales</font></li>
            <li><font size="3">Decreased back orders, partial
                shipments, &amp; expedites</font></li>
            <li><font size="3">Decreased overtime</font></li>
            <li><font size="3">Increased customer good will</font></li>
            <li><font size="3">Decreased data entry costs</font></li>
        </ul>
    </blockquote>
</blockquote>

<blockquote>
    <blockquote>
        <p><font size="4" face="Arial"><b>Lower Inventory
        Carrying Costs</b></font></p>
        <p><font size="3">Inventory carrying costs result from
        storing or holding goods for a period of time. The
        components of carrying costs are capital, space,
        inventory service, and inventory risk. Inventory carrying
        costs are proportional to the amount of inventory on
        hand.</font></p>
        <p><font size="3">The amount of inventory on hand is a
        function of product demand during order cycle time and
        the variability of that demand. EDI decreases the order
        cycle-time and its variability, which decreases the
        required amount of inventory on hand. This decreased
        inventory level decreases the costs of carrying
        inventory. The reduction of inventory by EDI is the
        number of days, which the orders cycle-time is decreased
        multiplied by the amount of inventory on hand. Major
        types of inventory costs include: capital costs, space
        costs, service costs, and risk costs.</font></p>
        <p><font size="3">Capital Costs. Capital costs are the
        costs incurred by keeping money tied up in inventory.
        Capital costs are reduced by using EDI because less
        inventory on hand is required due to shorter order cycle
        times.</font></p>
        <p><font size="3">Space Costs. Space costs include
        physical space, materials moving equipment, and inventory
        personnel time. Physical space costs are reduced by EDI
        with decreased inventory on hand. The savings in material
        moving equipment results from the reduction of material
        moving equipment required to handle the storage and
        retrieval of the inventory. The savings in inventory
        personnel time results from the reduction of time to move
        and administer the inventory. </font></p>
        <p><font size="3">Inventory Service Costs. Inventory
        service costs are the costs associated with insurance and
        taxes. With a decrease in the level of inventory, there
        is a corresponding decrease in the cost of insurance and
        taxes. </font></p>
        <p><font size="3">Inventory Risk Costs. Inventory risk
        costs are the costs associated with deterioration,
        damage, or obsolescence of inventory. These costs
        decrease in a direct relationship with a decrease in the
        quantity of inventory on hand.</font></p>
        <p><font size="4" face="Arial"><b>Inventory Carrying
        Benefit</b></font></p>
    </blockquote>
</blockquote>

<blockquote>
    <blockquote>
        <ul>
            <li><font size="3">Decreased capital costs</font></li>
            <li><font size="3">Decreased space costs</font></li>
            <li><font size="3">Decreased inventory service costs</font></li>
            <li><font size="3">Decreased inventory risk costs</font></li>
        </ul>
    </blockquote>
    <p><a href="edi_costs.php#top"><font size="3"><img
    src="../../images/top.gif" border="0" width="21" height="22"></font></a></p>
</blockquote>

<blockquote>
    <blockquote>
        <p><font size="4" face="Arial"><b>Decreased Procurement
        Costs</b></font></p>
        <p><font size="3">Procurement costs are the costs
        associated with the acquisition of goods for the
        replenishment of raw materials inventories. The elements
        of procurement costs are processing, transmitting,
        purchase, and receiving.</font></p>
        <p><font size="3">Processing, Transmitting, and
        Purchasing. EDI improves the procurement cost elements of
        processing, transmitting, and purchasing by providing an
        automated method for standardized procurement. In many
        organizations, standardized procurement is 75 percent of
        total procurement. Since the majority of total
        procurement can be automatically performed by EDI, the
        purchasing staff will need to spend less time on creating
        and administering purchase orders. The purchasing staff
        will be able to concentrate more on vendor searches and
        evaluations. Better vendors can help to improve the
        quality and costs of all procured products.</font></p>
        <p><font size="3">Receiving. EDI improves receiving by
        refilling the scheduling of resources in a customer's
        receiving area. When the suppliers sends a customer's
        order, it also sends an EDI Advance Ship Notice (ASN)
        which includes the items shipped, the carrier name, and
        expected delivery date. The customer receives this
        information before the shipment and uses it to schedule
        people, equipment, and a receiving dock for that order.
        This improves the scheduling of the receiving area
        resources, which decreases the need for excess people and
        equipment.</font></p>
        <p><font size="4" face="Arial"><b>Procurement Benefit</b></font></p>
    </blockquote>
</blockquote>

<blockquote>
    <blockquote>
        <ul>
            <li><font size="3">Decreased procurement
                administration</font></li>
            <li><font size="3">Increased time for vendor
                evaluations</font></li>
            <li><font size="3">Decreased receiving personnel and
                equipment</font></li>
        </ul>
    </blockquote>
    <p><a href="edi_costs.php#top"><font size="3"><img
    src="../../images/top.gif" border="0" width="21" height="22"></font></a></p>
</blockquote>

<p><a href="edi_costs.php#benefits" name="costs"><font size="5"
face="Arial"><b></b></font></a><font size="5" face="Arial"><b>Costs
of Electronic Data Interchange</b></font></p>

<blockquote>
    <blockquote>
        <p><font size="3">The costs of implementing Electronic
        Data Interchange fall into the broad classifications of
        personnel, training, software, and communication.</font></p>
        <p><font size="4" face="Arial"><b>Personnel</b></font></p>
        <p><font size="3">Implementing EDI in an organization
        requires support and training staff. At least one
        full-time EDI Coordinator is needed to answer user
        questions and perform maintenance on the EDI system. At
        least one temporary trainer is needed to initially train
        management and users about the theory of EDI and how to
        use the EDI system.</font></p>
        <p><font size="3">Estimates of the cost of the EDI
        Coordinator can be obtained from human resources or by
        speaking with recruiting firms. The trainer could be an
        internal person who either understands EDI or is willing
        to learn it, or an external consultant who specializes in
        training about Electronic Data Interchange.</font></p>
        <p><font size="4" face="Arial"><b>Personnel Costs</b></font></p>
    </blockquote>
</blockquote>

<blockquote>
    <blockquote>
        <ul>
            <li><font size="3">EDI Coordinator</font></li>
            <li><font size="3">EDI Trainer</font></li>
        </ul>
    </blockquote>
    <p><a href="edi_costs.php#top"><font size="3"><img
    src="../../images/top.gif" border="0" width="21" height="22"></font></a></p>
</blockquote>

<blockquote>
    <blockquote>
        <p><font size="4" face="Arial"><b>Training </b></font></p>
        <p><font size="3">Training costs are the costs associated
        with the lost work hours of the personnel being trained,
        training materials, and the training environment.
        Training is probably the most important aspect of EDI.
        Often problems with Electronic Data Interchange are less
        hardware and software related and more system usage
        related. People need to become comfortable with a new
        system. Training is a good way to make new systems and
        procedures seem less intimidating and to make users more
        productive when utilizing the new EDI systems.</font></p>
        <p><font size="4" face="Arial"><b>Training Costs</b></font></p>
    </blockquote>
</blockquote>

<blockquote>
    <blockquote>
        <ul>
            <li><font size="3">Lost work hours</font></li>
            <li><font size="3">Training materials</font></li>
            <li><font size="3">Training environment</font></li>
        </ul>
    </blockquote>
    <p><a href="edi_costs.php#top"><font size="3"><img
    src="../../images/top.gif" border="0" width="21" height="22"></font></a></p>
</blockquote>

<blockquote>
    <blockquote>
        <p><font size="4" face="Arial"><b>Software</b></font></p>
        <p><font size="3">When implementing EDI, new software is
        purchased or developed and existing business application
        software is changed. The software components of
        implementing EDI are communication, translation, and
        application interface. </font></p>
        <p><font size="3">Communication Software. Communication
        software handles the transfer of information between the
        organizations using EDI. The information may be
        transferred through a third party Value Added Network
        (VAN) or directly to a trading partner's computer through
        just about any standard communications link. The
        functions of communication software are standardized. The
        communication software is often included with the EDI
        translation software or can be purchased from the EDI
        translation software vendor for a nominal charge.</font></p>
        <p><font size="3">Translation Software. The translation
        software converts Business Application information into
        an EDI information standard. The most common standard in
        the United States is the ANSI X.12 EDI format. Usually
        translation software is purchased from an EDI software
        vendor. A Popular EDI communication and translation
        software vendor for the Mainframe, AS/400, Unix and
        Windows platform is the Gentran Server by </font><a
        href="http://www.stercomm.com"><font size="3">Sterling
        Commerce</font></a><font size="3">.</font></p>
        <p><font size="3">The application interface software is
        purchased, or developed internally by information systems
        staff or by EDI consultants. Usually packaged interface
        software cannot be purchased and when it can be purchased
        it will often need modification to fit the information
        needs of the company and its EDI trading partners.
        Estimates of the costs for development varies depending
        on the scope of the EDI project. An examination of
        similar systems, request of bids from consulting firms
        and an approximation of the costs from the EDI software
        vendor can be used to estimate the cost of the
        application interface.</font></p>
        <p><font size="3">The total software costs vary widely
        and can range from $2000 for a basic software with
        limited functionality, to the $100,000 plus software
        package that runs on a Mainframe and will completely
        automate your Just in Time (JIT) procurement process.</font></p>
        <p><font size="4" face="Arial"><b>Software Costs</b></font></p>
    </blockquote>
</blockquote>

<blockquote>
    <blockquote>
        <ul>
            <li><font size="3">Communication</font></li>
            <li><font size="3">Translation</font></li>
            <li><font size="3">Application Interface</font></li>
        </ul>
    </blockquote>
    <p><a href="edi_costs.php#top"><font size="3"><img
    src="../../images/top.gif" border="0" width="21" height="22"></font></a></p>
</blockquote>

<blockquote>
    <blockquote>
        <p><font size="4" face="Arial"><b>Communication</b></font></p>
        <p><font size="3">The communication costs are the costs
        associated with the installation and maintenance of a
        connection between the organization and its EDI trading
        partners. The components of communication costs are
        installation, monthly service, and communication
        equipment.</font></p>
        <p><font size="3">Installation. The communication links
        can be a dial-up or leased line between the organization
        and a Value Added Network (VAN) or the EDI trading
        partners. In the past it was normally a dial-up line to a
        VAN, but in today's market leased line seem to be the
        preferred mode of communications for many business
        processes. For a Value Added Network connection, there is
        an initial EDI electronic mailbox setup charge and an
        initial charge for links to each trading partner's
        mailbox.</font></p>
        <p><font size="3">Monthly Service. Monthly service costs
        are charged by your local telephone service company or
        leased line provider. The value added network normally
        charges by the number of lines or characters in each EDI
        communication.</font></p>
        <p><font size="3">Communication Equipment. The
        communication equipment translates data from a format
        which is understood by the computer into a format which
        can be transmitted to the VAN or EDI trading partner.
        Communication Equipment costs will range widely based on
        the mode of transmission, a modem may cost less than $100
        as where the hardware for a leased line may cost upwards
        of a few thousand dollars.</font></p>
        <p><font size="4" face="Arial"><b>Communication Costs</b></font></p>
    </blockquote>
</blockquote>

<blockquote>
    <blockquote>
        <ul>
            <li><font size="3">Installation</font></li>
            <li><font size="3">Monthly telephone and/or leased
                line connection charges</font></li>
            <li><font size="3">Monthly VAN connection</font></li>
            <li><font size="3">Modem and/or leased line hardware</font></li>
        </ul>
    </blockquote>
    <p><a href="edi_costs.php#top"><font size="3"><img
    src="../../images/top.gif" border="0" width="21" height="22"></font></a></p>
</blockquote>

<p><a name="intangible_b"><font size="4" face="Arial"><b></b></font></a><font
size="4" face="Arial"><b>INTANGIBLE BENEFITS OF ELECTRONIC DATA
INTERCHANGE</b></font></p>

<blockquote>
    <blockquote>
        <p><font size="3">There are many intangible benefits to
        implementing Electronic Data Interchange including:
        faster response to market needs, faster customer response
        time, improved trading partner relationships through
        mutual commitment, gained or maintained competitive
        position, facilitating Just In Time (JIT) production,
        improved audit trails, superior service, and improved
        cash flow.</font></p>
        <p><font size="3">Another benefit of implementing EDI is
        the ability to retain and expand business. Many
        organizations are decreasing the number of suppliers that
        they work with. The business environment is changing from
        competitive priced procurement where bids are requested
        from many organizations, to target pricing where costs
        and acceptable profit are analyzed with one or two
        suppliers who are long term business partners. As the
        number of suppliers' decrease, sales volume often
        increases for the remaining suppliers. Fully functional
        EDI capabilities are often a prerequisite for suppliers
        who become these business partners.</font></p>
    </blockquote>
    <p><a href="edi_costs.php#top"><font size="3"><img
    src="../../images/top.gif" border="0" width="21" height="22"></font></a></p>
</blockquote>

<p><a name="intangible_c"><font size="4" face="Arial"><b></b></font></a><font
size="4" face="Arial"><b>INTANGIBLE COSTS OF ELECTRONIC DATA
INTERCHANGE</b></font></p>

<blockquote>
    <blockquote>
        <p><font size="3">There are also intangible costs of
        implementing EDI. EDI causes a re-design of the
        organizational structure and its work processes. These
        changes create costly behavioral and organizational
        transformations. Instead of increasing productivity,
        after initial EDI implementation productivity usually
        decreases until personnel in the organization become
        accustomed to the EDI system and how to use it
        effectively.</font></p>
    </blockquote>
    <p><a href="edi_costs.php#top"><font size="3"><img
    src="../../images/top.gif" border="0" width="21" height="22"></font></a></p>
</blockquote>

<p><a name="summary"><font size="4" face="Arial"><b></b></font></a><font
size="4" face="Arial"><b>SUMMARY</b></font></p>

<blockquote>
    <blockquote>
        <p><font size="3">In the next ten years the number of
        companies using Electronic Data Interchange (EDI) will
        dramatically increase. It's even been noted by some
        experts that estimate 95% of all business transactions
        will be completely computerized, from negotiations to
        payments. While this percentage may be aggressive, I
        think it is safe to say that many business transactions
        are moving to automation through EDI.</font></p>
        <p><font size="3">The knowledge, software, and
        communication cost requirements of implementing EDI are
        decreasing. Mid-size and small organizations are
        beginning to develop EDI systems. Many large
        organizations are requiring their business partners, even
        the small organizations, to use EDI. Every manufacturing,
        distribution, and retail-related organization should
        consider implementing EDI.</font></p>
        <p><font size="3">Since the EDI X.12 standards keep
        changing, the EDI software system will need periodic
        maintenance. However, if properly implemented a company
        can probably go one to two years between EDI software
        upgrades. </font></p>
        <p><font size="3">This article created a framework for
        determining the measurable costs and benefits of
        implementing EDI. An EDI project is a major cost
        expenditure. In some organizations the costs of
        implementing EDI might outweigh the financial benefits.
        The costs and benefits of implementing electronic data
        interchange should be carefully weighed before starting
        an EDI project.</font></p>
    </blockquote>
    <p><a href="edi_costs.php#top"><font size="3"><img
    src="../../images/top.gif" border="0" width="21" height="22"></font></a></p>
</blockquote>

<p><font size="4"><strong>Special Note:</strong></font></p>

<blockquote>
    <blockquote>
        <p><font size="3">EDI Associates believe in giving credit
        were credit is due and Richard Hornback deserves the
        credit for this fantastic case study which he posted on
        ecworld.com's web site in 1995. Outside of a few
        modifications to reflect the changes in technology,
        Richard Hornbacks case study is as true today as it was
        at the time of its writing.</font></p>
    </blockquote>
    <p><a href="edi_costs.php#top"><font size="3"><img
    src="../../images/top.gif" border="0" width="21" height="22"></font></a></p>
</blockquote>

<blockquote>
    <blockquote>
        <p>&nbsp;</p>
    </blockquote>
</blockquote>


<p align="center"> &nbsp;</body>

<?php

// print out footer information
closePage();

?>
