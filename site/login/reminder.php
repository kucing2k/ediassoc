<?php

// File Location: /site/login/reminder.php

require_once("tpl_unsecure.php");
require_once("class.users.php");
require_once("handlers.php");

if ($_POST) { // check for http post vars
    
    // assign post vars
    $sLogin = $_POST["login"];
    $sComp = $_POST["comp"];
    
    // validate user name
    if (!validUser($sLogin)) {
        
        catchErr("Enter a login name");
        $FORMOK = false;
    }
    
    // validate company
    if (!validUser($sComp)) {
        
        catchErr("Enter a company id");
        $FORMOK = false;
    }
    
    // if forms variables validated
    if ($FORMOK) {
        
        // assign array values
        $aArgs["Login"] = $sLogin;
        $aArgs["Company"] = $sComp;
        
        $oUsers = new users;
        
        // try login and redirect
        if ($oUsers->sendReminder($aArgs)) {
            
            header("Location: index.php");
        }
    }
    
} else { // post vars not sent
    
    // initialize page vars
    $sUser = null;
    $sEmail = null;
}

setHeader();
openPage();

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header"><?php print ENTITY ?> Administrator Password 
        Reminder</div></td>
    </tr>
    <tr>
        <td><div class="copy">Please complete the form below to have your password
        reminder emailed to you. If your account information is correct you will be
        redirected to the login form.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<form action="<?php print SELF ?>" method="post" name="apressform">
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="formlabel">Login Name:</div></td>
        <td><input type="text" name="login" value="<?php print clean($sLogin) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Company ID:</div></td>
        <td><input type="text" name="comp" value="<?php print clean($sComp) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td align="right" colspan="2"><input type="image" src="../../_img/buttons/btn_submit.gif" width="58" height="15" alt="" border="0" onfocus="this.blur();" /></td>
    </tr>
    <tr>
        <td colspan="2"><a href="index.php">Return to login?</a></td>
    </tr>
</table>
</form>

<?php closePage(); ?>
