<?php

// File Location: /site/news/index.php

require_once("tpl_unsecure.php");

// generate the header info
setHeader();

// start a new page with a banner ad
openPage(true);

?>
<!-- main content -->

<table border="0" width="724" cellpadding="0" cellspacing="0" namo_layoutbox="10,1,1,10,1,1">
    <tr>
        <td width="723" height="3" colspan="6"></td>
        <td width="1" height="3"></td>
    </tr>
    <tr>
        <td width="3" height="1276" rowspan="5"></td>
        <td width="214" height="217" namo_textbox="0,0,0,0,0,0,0,0,0,0" rowspan="2">
            <p align="center">&nbsp;<img src="../../images/Two-Men_Blue-Homepage.gif" width="202" height="135" border="0"></p>
        </td>
        <td width="392" height="7" colspan="3"></td>
        <td width="214" height="917" rowspan="3" namo_textbox="0,0,0,0,0,0,0,0,0,0">
            <p>&nbsp;</p>
        </td>
        <td width="1" height="7"></td>
    </tr>
    <tr>
        <td width="3" height="1269" rowspan="4"></td>
        <td width="388" height="1260" rowspan="3" namo_textbox="0,0,0,0,0,0,0,0,0,0">
            <hr>
            <p><font color="#D3960E" size="5"><b>Professional Services</b></font></p>
            <p>EDI Associates offers a comprehensive range of services to help 
            customers fully implement and benefit from our connectivity and 
            collaboration solutions. EDI Associates Professional Services and 
            Customer Support organizations help customers quickly connect their 
            partner communities and ensure maximum value from their EDI Associates 
            solution.</p>
            <p>EDI Associates Professional Services team members are more than 
            technology specialists. They're business people. They understand 
            the dynamics of trading community relationships and how e-business 
            integration plays a key role in your success. </p>
            <p>Our Professional Services include: </p>
            <p><B>Project Planning</B></p>
            <p>We will analyze your business requirements to help you define 
            and develop your e-business. We consider all factors: platform, 
            operating system, network environment, integration with ERP and 
            mission-critical applications and more. Your EDI Associates Project 
            Manager develops a timeline and ensures that schedules and milestones 
            are met. </p>
            <p><B>Implementation</B></p>
            <p>We will determine your data and mapping requirements, manage 
            the installation of your system, integrate your enterprise and connect 
            your operations to your chosen communications platform. </p>
            <p><B>Migration Services</B></p>
            <p>We have the expertise to efficiently manage the migration of 
            an entire trading community. We will smoothly direct the migration 
            of your partners from a paper-based or VAN system to the Internet, 
            with no disruption to your current EDI environment. </p>
            <p><B>Community Development</B></p>
            <p>We will work with you to connect your trading partners and increase 
            the return on your investment. We'll develop a strategy to meet 
            your trading partner connectivity objectives, taking into account 
            the number and size of your trading partners, transaction sets, 
            and the level of involvement required from your internal company 
            resources. </p>
            <p><B>Trading Community Management</B></p>
            <p>Once your partners are connected, we'll assist you with the management 
            of your trading community. Our team works with you to manage partner 
            relationships, add new vendors and customers and handle partner 
            contracts. </p>
            <p><font size="4" color="#D3960E">Customer Support</font></p>
            <p>When a problem appears in your B2B connectivity infrastructure, 
            you need more than technical expertise. You need the assistance 
            of support professionals who understand e-business. EDI Associates 
            Customer Support is your single source for problem resolution. </p>
            <p>Staffed by a team of experienced software engineers and network 
            specialists, EDI Associates Customer Support team responds to questions 
            quickly and delivers timely, effective solutions. Our automation 
            and tracking system allows our customer support staff to quickly 
            access detailed information on each customer's implementation. </p>
            <p>For more information on EDI Associates services, please contact 
            your EDI Associates Sales Representative.</p>
<P><SPAN class="heading1"><font color="#D3960E" size="4">How Can EDI Associates 
            Benefit Your Company</font><font color="#D3960E">?</font></SPAN><SPAN 
class=bodytext><BR>Contact us at <A 
href="mailto:info@ediassociates.com?subject=EDIAnywhere">info@ediassociates.com</A> or (503) 297-5188 and 
connect today! Or, if you prefer, allow us to contact you to learn more about 
your specific needs. Let us know today.</SPAN> </P></td>
        <td width="1" height="1269" rowspan="4"></td>
        <td width="1" height="210"></td>
    </tr>
    <tr>
        <td width="214" height="1059" rowspan="3"></td>
        <td width="1" height="700"></td>
    </tr>
    <tr>
        <td width="214" height="359" rowspan="2"></td>
        <td width="1" height="350"></td>
    </tr>
    <tr>
        <td width="388" height="9"></td>
        <td width="1" height="9"></td>
    </tr>
    <tr>
        <td width="3" height="1"></td>
        <td width="214" height="1"></td>
        <td width="3" height="1"></td>
        <td width="388" height="1"></td>
        <td width="1" height="1"></td>
        <td width="214" height="1"></td>
        <td width="1" height="1"></td>
    </tr>
</table>


<p align="center"> &nbsp;</body>

<?php

// print out footer information
closePage();

?>
