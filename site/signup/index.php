<?php

// File Location: /site/news/index.php

require_once("tpl_unsecure.php");
require_once("class.signup.php");

$oSignup = new signup();

// generate the header info
setHeader();

// start a new page with a banner ad
openPage(true);

if ($_POST) { // check for http post vars

?>
<!-- main content -->
<?php


    $aMember["primary_name"] = $_POST["primary_name"];
    $aMember["contact_first"] = $_POST["contact_first"];
    $aMember["contact_middle"] = $_POST["contact_middle"];
    $aMember["contact_last"] = $_POST["contact_last"];
    $aMember["a_telephone"] = $_POST["a_telephone"];
    $aMember["a_email"] = $_POST["a_email"];
    $aMember["contact"] = $_POST["contact"];
    $aMember["telephone1"] = $_POST["telephone1"];
    $aMember["email"] = $_POST["email"];
    $aMember["address1"] = $_POST["address1"];
    $aMember["address2"] = $_POST["address2"];
    $aMember["city_town"] = $_POST["city_town"];
    $aMember["state_province"] = $_POST["state_province"];
    $aMember["postal_code"] = $_POST["postal_code"];
    
    $tmpId = $oSignup->createNewMemberAccount($aMember);
    
    // print $_POST["primary_name"];
    print $tmpId;



} else {

?>
<script type="text/javascript">
function validate_email(field,alerttxt)
{
with (field)
  {
  apos=value.indexOf("@");
  dotpos=value.lastIndexOf(".");
  if (apos<1||dotpos-apos<2)
    {alert(alerttxt);return false;}
  else {return true;}
  }
}

function validate_form(thisform)
{
with (thisform)
  {
  if (primary_name.value == "")
    {alert("Please fill in the Organizations legal name.");primary_name.focus();return false;}
  if (contact_first.value == "")
    {alert("Please fill in the contact first name.");contact_first.focus();return false;}
  if (contact_last.value == "")
    {alert("Please fill in the contact last name.");contact_last.focus();return false;}
  if (telephone1.value == "")
    {alert("Please fill in the contacts phone number.");telephone1.focus();return false;}
  if (address1.value == "" || city_town.value == "" || state_province.value == "" || postal_code.value == "")
    {alert("Please fill in a valid Address.");address1.focus();return false;}
  if (validate_email(a_email,"Not a valid e-mail address!")==false)
    {a_email.focus();return false;}
  }
}
</script>
<table border="0" width="724" cellpadding="0" cellspacing="0">
    <tr>
        <td width="723" height="3" colspan="6"></td>
        <td width="1" height="3"></td>
    </tr>
    <tr>
        <td width="3" rowspan="3"></td>
        <td width="490" rowspan="2">
            <hr>
            <p>
            <font color="#D3960E" size="5"><b>Signup today?</b></font></p>
            <p>
            <p>Get the benefits of a secure value added network account, plus much more.
            <p>
            <form action="index.php" method="post" onsubmit="return validate_form(this);">
            <p>
            <hr>
            <p><b>1 Company</b>
            <p>What is the Company or Organizations legal name?
            <p><font color="red">* </font>Company Name: <input type="text" name="primary_name" size="32">
            <p>
            <hr>
            <p><b>2 Administrators Information</b>
            <p>Who will be administering users for this Company or Organization?
            <p><font color="red">* </font>First: <input type="text" name="contact_first" size="25">
            Middle: <input type="text" name="contact_middle" size="20">
            <font color="red">* </font>Last: <input type="text" name="contact_last" size="25">
            <p>Phone: <input type="text" name="a_telephone" size="20">
            <font color="red">* </font>email: <input type="text" name="a_email" size="32">
            <hr>
            <p><b>3 Accounts Payable Information</b>
            <p>Who do we contact for accounts payable issues?
            <p>Attention: <input type="text" name="contact" size="32">
            <p><font color="red">* </font>Phone: <input type="text" name="telephone1" size="20">
            email: <input type="text" name="email" size="32">
            <p><font color="red">* </font>Address 1: <input type="text" name="address1" size="32">
            <p>Address 2: <input type="text" name="address2" size="32">
            <p><font color="red">* </font>City: <input type="text" name="city_town" size="24">
            <font color="red">* </font>State/Prov: <input type="text" name="state_province" size="24">
            <font color="red">* </font>ZIP: <input type="text" name="postal_code" size="10">
            <p>
            <hr>
            <p><b>Please Note.</b>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;- Fields with a Red Asterisks (<font color="red">*</font>) are required.
            <p>&nbsp;&nbsp;&nbsp;&nbsp;- An activation email will be sent to the administrators email account to complete the activation process. 
            <p>
            <br />
            <hr />
            <p>
            <input type="submit" value="submit" name="submit">
            </form>

        </td>
        <td width="1" rowspan="1"></td>
        <td width="1" ></td>
    </tr>
</table>

<p align="center"> &nbsp;

<?php

}

// print out footer information
closePage();

?>
