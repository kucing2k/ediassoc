<?php

// File Location: /core/partners/index.php

require_once("tpl_secure.php");
require_once("class.partners.php");

// instantiate news class
$oParts = new part();

// the session class is instantiated in the tpl_secure.php file

// get users and user count
$aPart = $oParts->getPartners($iComId, $iCursor);

// check for users
if (count($aPart)) {
    
    // build page data array
    $i = 0;
    while ($i < count($aPart)) {
        $aData[$i]["Id"] = $aPart[$i]["tp Id"];
        $aData[$i]["Name"] = $aPart[$i]["tp Name"];
        $aData[$i]["Status"] = $aPart[$i]["Status"];
        $aData[$i]["Created"] =$aPart[$i]["Created Date"];
        ++$i;
    }
}

// check for id
if ($id) {
    
    // assign user id
    $oParts->setPartId($id);
    
    // check operation type
    if (!strcmp($op, "del")) {
        
        // try delete user and redirect
        $oParts->deletePartner();
        header("Location: ".SELF);
        
    } elseif (!strcmp($op, "act")) {
        
        // try activate user and redirect
        $oParts->activatePartner();
        header("Location: ".SELF);
        
    } elseif (!strcmp($op, "deact")) {
        
        // try deactivate user and redirect
        $oParts->deactivatePartner();
        header("Location: ".SELF);
    }
}

setHeader();
openPage();
?>

<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header">Trading Partner Administration</div></td>
    </tr>
    <tr>
        <td colspan="2"><div class="copy">To manage trading partners, select a trading partner action from the list below.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
        <td align="right" valign="top"><?php if ($iPerm > 1) { ?><a href="form.php?op=add"><img src="../../_img/buttons/btn_additem.gif" width="58" height="15" alt="" border="0" /></a><?php } ?></td>
    </tr>
</table>

<?php renderList($aData) ?>

<?php closePage(); ?>
