<?php

// File Location: /core/partners/form.php

require_once("tpl_secure.php");
require_once("handlers.php");
require_once("class.partners.php");

// instantiate news class
$oParts = new part();

// check for id
if ($id) {
    
    // assign user id
    $oParts->setPartId($id);
}

if ($_POST) { // check for http post vars
    
    // assign post vars
    $sName = $_POST["name"];
    $sEdic = $_POST["edicode"];
    $sAppc = $_POST["appcode"];
    $sCont = $_POST["contact"];
    $sEmai = $_POST["email"];
    $sPhon = $_POST["phone"];
    $aPerms = $_POST["perms"];
    
    // validate user name
    if (!$sName) {
        
        catchErr("Enter a Company name");
        $FORMOK = false;
    }
    
    // if forms variables validated
    if ($FORMOK) {
        
        // assign array values
        $aArgs["id"] = $id;
        $aArgs["Compa"] = $iComId;
        $aArgs["CName"] = $sName;
        $aArgs["EdiCo"] = $sEdic;
        $aArgs["AppCo"] = $sAppc;
        $aArgs["Email"] = $sEmai;
        $aArgs["Conta"] = $sCont;
        $aArgs["Phone"] = $sPhon;
        $aArgs["Perms"] = $aPerms;
        
        
        // check operation type
        if (!strcmp("edit", $op)) {
            
            // try edit user
            $FORMOK = $oParts->editPartner($aArgs, $id, $iComId);
            
        } elseif (!strcmp("add", $op)) {
            
            // try add user
            $FORMOK = $oParts->addPartner($iComId, $aArgs);
            // header("Location: index.php");
        }
        
        // redirect if successful
        if ($FORMOK) {
            
            header("Location: index.php");
        }
    }
    
} else { // post vars not sent
    
    // initialize page vars
        $sName = null;
	$sEDICode = null;
	$sAPPCode = null;
	$sContact = null;
	$sEmail = null;
	$sPhone = null;
	$sStatus = 0;
	$sCatalog = null;
	$sImage = '';
    
    if (!strcmp("edit", $op)) {
        
        $aPart = $oParts->getPartner($id, $iComId);
        $sName = $aPart["Name"];
	$sEDICode = $aPart["EDICode"];
	$sAPPCode = $aPart["APPCode"];
	$sContact = $aPart["Contact"];
	$sEmail = $aPart["Email"];
	$sPhone = $aPart["Phone"];
	$sStatus = $aPart["Status"];
	$sCatalog = $aPart["catalog"];
	$sImage = $aPart["Image"];
	$aPerms = $aPart["Perms"];


    }
   
}

$aProfiles = $oParts->getProfiles($id, $iComId);
$aCats = $oParts->getCatalogs($iComId);

setHeader();
openPage();
// print_r($aPerms);
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header">Trading Partner Administration</div></td>
    </tr>
    <tr>
        <td><div class="copy">To <?php print $op ?> this item in the system, please complete the form below.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<form action="<?php print SELF ?>?op=<?php print $op ?>&id=<?php print $id ?>" method="post" name="ediaform">
	<!-- Note that LEGEND.DIV's are "replicated" in all the "FieldSets" -->
	<!-- FIRST FS -->
	<FIELDSET ID=dv1 class="menu" style="display:inline">
	<LEGEND>
	<DIV class=outer><A href="javascript:show(1);" onclick="this.blur()"><SPAN>General</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(2);" onclick="this.blur()"><SPAN>Catalogs</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(3);" onclick="this.blur()"><SPAN>Tables</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(4);" onclick="this.blur()"><SPAN>Locations</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(5);" onclick="this.blur()"><SPAN>Future Use</SPAN></A></DIV>
	</LEGEND>
	
	<h5>General Partner Information for <?php print clean($sName) ?></h5>
	To <?php print $op ?> this item in the system, please complete the form below.<br>
	<pre>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
            
                <tr>
                    <td><div class="formlabel">Partner Name:</div></td>
                    <td><input type="text" name="name" value="<?php print clean($sName) ?>" class="textfield" /></td>
                </tr>
                <tr>
                    <td><div class="formlabel">EDI Code:</div></td>
                    <td><input type="text" name="edicode" value="<?php print clean($sEDICode) ?>" class="textfield" /></td>
                </tr>
                <tr>
                    <td><div class="formlabel">Application Code:</div></td>
                    <td><input type="text" name="appcode" value="<?php print clean($sAPPCode) ?>" class="textfield" /></td>
                </tr>
                <tr>
                    <td><div class="formlabel">Contact:</div></td>
                    <td><input type="text" name="contact" value="<?php print clean($sContact) ?>" class="textfield" /></td>
                </tr>
                <tr>
                    <td><div class="formlabel">Email:</div></td>
                    <td><input type="text" name="email" value="<?php print clean($sEmail) ?>" class="textfield" /></td>
                </tr>
                <tr>
                    <td><div class="formlabel">Phone:</div></td>
                    <td><input type="text" name="phone" value="<?php print clean($sPhone) ?>" class="textfield" /></td>
                </tr>
                <?php if (!strcmp("edit", $op)) { ?>
                <tr>
                    <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
                </tr>
                <tr>
                    <td><div class="formlabel">Created:</div></td>
                    <td><?php print date("Y-m-d H:i:s", $aPart["Created Date"]) ?></td>
                </tr>
                <tr>
                    <td><div class="formlabel">Modified:</div></td>
                    <td><?php print date("Y-m-d H:i:s", $aPart["Modified Date"]) ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
                </tr>
                <tr>
                    <td align="right" colspan="2"><input type="image" src="../../_img/buttons/btn_submit.gif" width="58" height="15" alt="" border="0" onfocus="this.blur();" /></td>
                </tr>

            </table>
	</pre>
	<br>
	<br>
	</FIELDSET>
	
	
	
	<!-- SECOND FS -->
	<FIELDSET ID=dv2 class="menu" style="display:none">
	<LEGEND>
	<DIV class=outer><A href="javascript:show(1);" onclick="this.blur()"><SPAN>General</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(2);" onclick="this.blur()"><SPAN>Catalogs</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(3);" onclick="this.blur()"><SPAN>Tables</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(4);" onclick="this.blur()"><SPAN>Locations</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(5);" onclick="this.blur()"><SPAN>Future Use</SPAN></A></DIV>
	</LEGEND>
	
	<h5>Configure Catalog Access for <?php print clean($sName) ?></h5>
	<p>

	To assign a catalog to this partner, select one from the list below.
	<br>
	<br>
	
	<pre>
            <table width="450" border="0" cellpadding="0" cellspacing="0">
    <?php
    if (count($aCats)) {
        $i = 0;
        while ($i < count($aCats)) {
            $iCatId = $aCats[$i]["val"];
    ?>
    <tr>
        <td><div><?php print clean($aCats[$i]["des"]) ?>:</div></td>
        <td>
        
        <table border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td><input type="radio" name="perms[<?php print $iCatId ?>]<?php print $iCatId ?>" value="1"<?php !strcmp(1, $aPerms[$iCatId]) ? print "checked" : print ""; ?>>View&nbsp;</td>
                <td><input type="radio" name="perms[<?php print $iCatId ?>]<?php print $iCatId ?>" value="0"<?php !$aPerms[$iCatId] ? print "checked" : print ""; ?>>No Access&nbsp;</td>
            </tr>
        </table>
        
        </td>
    </tr>
    <?php
            ++$i;
        }
    }
    ?>

            </table>
	</pre>
	</p>
	<br>
	<br>
	</FIELDSET>
	
	
	
	<!-- THIRD FS -->
	<FIELDSET ID=dv3 class="menu" style="display:none">
	<LEGEND>
	<DIV class=outer><A href="javascript:show(1);" onclick="this.blur()"><SPAN>General</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(2);" onclick="this.blur()"><SPAN>Catalogs</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(3);" onclick="this.blur()"><SPAN>Tables</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(4);" onclick="this.blur()"><SPAN>Locations</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(5);" onclick="this.blur()"><SPAN>Future Use</SPAN></A></DIV>
	</LEGEND>
	
	<h5>Setup Cross Reference Tables for <?php print clean($sName) ?></h5>
	<p>
	<strong>Future Option</strong>
	<br>
	<br>
	
	<pre>
	</pre>
	</p>
	<br>
	<br>
	</FIELDSET>
	
	
	
	<!-- THIRD FS -->
	<FIELDSET ID=dv4 class="menu" style="display:none">
	<LEGEND>
	<DIV class=outer><A href="javascript:show(1);" onclick="this.blur()"><SPAN>General</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(2);" onclick="this.blur()"><SPAN>Catalogs</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(3);" onclick="this.blur()"><SPAN>Tables</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(4);" onclick="this.blur()"><SPAN>Locations</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(5);" onclick="this.blur()"><SPAN>Future Use</SPAN></A></DIV>
	</LEGEND>
	
	<h5>Setup Address Locations for <?php print clean($sName) ?></h5>
	<p>
	<strong>Future Option</strong>
	<br>
	<br>
	
	<pre>
	</pre>
	</p>
	<br>
	<br>
	</FIELDSET>

	<!-- THIRD FS -->
	<FIELDSET ID=dv5 class="menu" style="display:none">
	<LEGEND>
	<DIV class=outer><A href="javascript:show(1);" onclick="this.blur()"><SPAN>General</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(2);" onclick="this.blur()"><SPAN>Catalogs</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(3);" onclick="this.blur()"><SPAN>Tables</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(4);" onclick="this.blur()"><SPAN>Locations</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(5);" onclick="this.blur()"><SPAN>Future Use</SPAN></A></DIV>
	</LEGEND>
	
	<h5>Setup Address Locations for <?php print clean($sName) ?></h5>
	<p>
	<strong>Future Option</strong>
	<br>
	<br>
	
	<pre>
	</pre>
	</p>
	<br>
	<br>
	</FIELDSET>

        <?php renderProfiles($aProfiles) ?>
</form>

<?php closePage(); ?>
