<?php

// File Location: /core/partners/index.php

require_once("tpl_secure.php");
require_once("class.inbox.php");

$oInbox = new inbox;

// delete any topics if that was the requested action
if ($_POST["delete"]) {
	$aDeleteTopic = $_POST["favorite"];
	foreach($aDeleteTopic as $iTopicId) {
		$oInbox->deleteTopic($iTopicId);
	}
}
if ($_POST["archive"]) {
	$aDeleteTopic = $_POST["favorite"];
	foreach($aDeleteTopic as $iTopicId) {
		$oInbox->archiveTopic($iTopicId);
	}
}

// the session class is instantiated in the tpl_secure.php file

    // check operation type
    if (!strcmp($op, "new")) {
        
        // Get Only new transactions
	$sql = "doc.EDIAStatus=1
	and doc.GSSenderID=par.tp_edi_code
	and doc.MemberNum=".$iComId;

        $aTrans = $oEdia->getTransHeaders($sql, $iCursor);
        $iCnt = $oEdia->getTransHeaderCount($sql);
    } else {
        
        // Get All Transactions
	$sql = "doc.EDIAStatus<4
	and doc.GSSenderID=par.tp_edi_code
	and doc.MemberNum=".$iComId;

        $aTrans = $oEdia->getTransHeaders($sql, $iCursor);
        $iCnt = $oEdia->getTransHeaderCount($sql);
    } 

// check for Transactions
if (count($aTrans)) {
    
    // build page data array
    $i = 0;
    $aList = '';
    while ($i < count($aTrans)) {

        $aData[$i]["Usr"] = $iUserId;
        $aData[$i]["Id"] = $aTrans[$i]["Key ID"];
        $aList[$i] = $aTrans[$i]["Key ID"];
        $aData[$i]["Typ"] = $aTrans[$i]["Doc Type"];
        $aData[$i]["SndI"] = $aTrans[$i]["Send ID"];
        $aData[$i]["TrnN"] = $aTrans[$i]["Trans Num"];
        $aData[$i]["SndN"] = $aTrans[$i]["Send Name"];
        $aData[$i]["Stat"] = $aTrans[$i]["Status"];
        $aData[$i]["LodD"] = $aTrans[$i]["Log DT"];
        ++$i;
    }
    $_SESSION["aList"] = $aList;
}

setHeader();
openPage();

?>
<form name="deletethread" method="post" action="<?php if (!strcmp($op, "new")) { echo SELF."?op=new"; } else { echo SELF; } ?>">
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header">Inbound Transaction</div></td>
    </tr>
    <tr>
        <td colspan="2"><div class="copy">To view a transaction, select a trading partner action from the list below.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
    <tr>
    <td align="left">
	<input type='submit' name='delete' value='delete'>
	<input type='submit' name='archive' value='archive'>
    </td>
    </tr>
</table>

<?php renderTransHeader($iCnt, $aData, $iUserId) ?>

</form>

<?php closePage(); ?>
