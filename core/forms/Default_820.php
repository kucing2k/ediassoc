<?php


function display820($aData='', $aAdd='', $aHDet='', $aDet='') {
$intAddWidth=0;
$strAddHdr='';
$strRefHdr='';
$strAdd='';
$strAddType='';

$i = 0;

if (count($aRef)) {
    while ($i < count($aRef)) {
    
        $strRefHdr = $strRefHdr . "<tr>\n";
        $strRefHdr = $strRefHdr . "<td  colspan='2' width='100%' valign=top style='width:100.0%;border:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>\n";
        $strRefHdr = $strRefHdr . "<p class=MsoNormal><B>Reference Numbers</B></p>\n";
        $strRefHdr = $strRefHdr . "</td>\n";
        $strRefHdr = $strRefHdr . "<tr>\n";
        $strRefHdr = $strRefHdr . "<td width='50%' valign=top style='width:50.0%;border:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>\n";
        $strRefHdr = $strRefHdr . "<p class=MsoNormal>" . $aRef[$i]["dr_qual"] . "</p>\n";
        $strRefHdr = $strRefHdr . "</td>\n";
        $strRefHdr = $strRefHdr . "<td width='50%' valign=top style='width:50.0%;border-left: none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>\n";
        $strRefHdr = $strRefHdr . "<p class=MsoNormal>" . $aRef[$i]["dr_id"] . "</p>\n";
        $strRefHdr = $strRefHdr . "</td>\n";
        $strRefHdr = $strRefHdr . "</tr>\n";

        ++$i;
    }
}


?>
<style>
 /* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@page Section1
	{size:8.5in 11.0in;
	margin:1.0in 1.25in 1.0in 1.25in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
</style>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header">View <?php echo $aData["EDIADocTypeID"] ?> - Remittance Advice</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
   
</table>
&nbsp;

<div class=Section1>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>
<table border=1 cellspacing=0 cellpadding=0 width="60%" style='width:60.0%; border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt; mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr>
  <td width="33%" valign=top style='width:33.0%;border:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><B>Check Number #</B></p>
  </td>
  <td width="33%" valign=top style='width:33.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><B>Amount</B></p>
  </td>
  <td width="33%" valign=top style='width:33.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><B>Date</B></p>
  </td>
 </tr>
 <tr>
  <td width="33%" valign=top style='width:33.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><?php Print $aData["TransNumber"] ?></p>
  </td>
  <td width="33%" valign=top style='width:33.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><?php Print currency($aHDet["monetary_amount"]) ?></p>
  </td>
  <td width="33%" valign=top style='width:33.0%;border-top:none;border-left: none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><?php print date("Y-m-d", strtotime($aHDet["trans_date"])) ?></p>
  </td>
 </tr>
</table>
&nbsp;
<table border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;
 border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr>
    <?php echo displayAddresses($aAdd) ?>
 </tr>
</table>
<table border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%; border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt; mso-padding-alt:0in 5.4pt 0in 5.4pt'>
   <tr>
      <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>Payer ABA Routing Number</p>
      </td>
      <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>Payer Account Number</p>
      </td>
      <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>Payee ABA Routing Number</p>
      </td>
      <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>Payee Account Number</p>
      </td>
   </tr>
   <tr>
      <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aHDet["sender_dfi_id_number"] ?></p>
      </td>
      <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aHDet["sender_account_number"] ?></p>
      </td>
      <td width="25%" valign=top style='width:25.0%;border-top:none;border-left: none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php print $aHDet["receiver_dfi_id_number"] ?></p>
      </td>
      <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aHDet["receiver_account_number"] ?></p>
      </td>
   </tr>
</table>
&nbsp;

<table border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;
 border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-padding-alt:0in 5.4pt 0in 5.4pt'>
   <tr>
      <td colspan="4" valign=top style='width:50.0%;border:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><B>Payments</B></p>
      </td>
      <td  colspan="2" valign=top style='width:50.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><B>Adjustments</B></p>
      </td>
   </tr>
   <tr>
      <td width="16%" valign=top style='width:16.0%;border:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><B>Document No.</B></p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><B>Action</B></p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><B>Amount Invoiced</B></p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><B>Amount Paid</B></p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
          <p class=MsoNormal><B>Discount</B></p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><B>Reason</B></p>
      </td>
   </tr>

<?php

   // loop through data and conditionally display functionality and content
   $i = 0;
   while ($i < count($aDet)) {

?>


   <tr>
      <td width="16%" valign=top style='width:16.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_ref_doc_number"] ?></p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_pay_action"] ?></p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_invoice_amount"] ?></p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_paid_amount"] ?></p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_cash_discount"] ?></p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_cash_discount_reason"] ?></p>
      </td>
   </tr>

      <tr>
         <td colspan="1" valign=top style='width:16.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
            <p class=MsoNormal><?php Print date("Y-m-d", strtotime($aDet[$i]["det_ref_doc_date"])) ?></p>
         </td>
         <td colspan="5" valign=top style='width:100%;border-top:none;border-left:solid windowtext .5pt;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>

   <?php  
       $j = 0; 
       $aDetRef = $aDet[$i]["det_ref_rec"];


       while ($j < count($aDetRef)) { 
   ?>
            <p class=MsoNormal><?php Print $aDetRef[$j]["dr_qual"]."-".$aDetRef[$j]["dr_id"] ; ?></p>
   <?php 
   ++$j; 
   }
   ?>

         </td>

      </tr>


<?php
 
++$i;
}

?>


</table>
<table border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%; border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt; mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr>
  <td width="60%" valign=top style='width:60.0%;border:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>Comments: </p>
  </td>
  <td width="40%" valign=top style='width:40.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>Net Freight: $<?php Print round($aData["NetFreightDue"], 2) ?></p>
  <p class=MsoNormal>Net Due After <?php print date("Y-m-d", strtotime($aData["TermsNetDueDate"])) ?>: $<?php Print round($aData["NetAmountDue"], 2) ?></p>
  <p class=MsoNormal>Net Due Before <?php print date("Y-m-d", strtotime($aData["TermsNetDueDate"])) ?>: $<?php Print round($aData["NetAmountDueTerms"], 2) ?></p>
  </td>
 </tr>


</table>

</div>



<?php

}

?>