<?php


function display812($aData='', $aAdd='', $aHDet='', $aDet='', $aRef='') {
$intAddWidth=0;
$strAddHdr='';
$strRefHdr='';
$strAdd='';
$strAddType='';

$i = 0;

if (count($aRef)) {
    while ($i < count($aRef)) {
    
        $strRefHdr = $strRefHdr . "<tr>\n";
        $strRefHdr = $strRefHdr . "<td  colspan='2' width='100%' valign=top style='width:100.0%;border:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>\n";
        $strRefHdr = $strRefHdr . "<p class=MsoNormal><B>Reference Numbers</B></p>\n";
        $strRefHdr = $strRefHdr . "</td>\n";
        $strRefHdr = $strRefHdr . "<tr>\n";
        $strRefHdr = $strRefHdr . "<td width='50%' valign=top style='width:50.0%;border:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>\n";
        $strRefHdr = $strRefHdr . "<p class=MsoNormal>" . $aRef[$i]["dr_qual"] . "</p>\n";
        $strRefHdr = $strRefHdr . "</td>\n";
        $strRefHdr = $strRefHdr . "<td width='50%' valign=top style='width:50.0%;border-left: none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>\n";
        $strRefHdr = $strRefHdr . "<p class=MsoNormal>" . $aRef[$i]["dr_id"] . "</p>\n";
        $strRefHdr = $strRefHdr . "</td>\n";
        $strRefHdr = $strRefHdr . "</tr>\n";

        ++$i;
    }
}


?>
<style>
 /* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@page Section1
	{size:8.5in 11.0in;
	margin:1.0in 1.25in 1.0in 1.25in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
</style>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header">View <?php echo $aData["EDIADocTypeID"] ?> - Credit/Debit Adjustment</div></td>
    </tr>
    <tr>
        <td><div class="header">Handling Code - <?php echo $aHDet["ra_trans_handling_code"] ?></div></td>
    </tr>
    <tr>
        <td><div class="header">Credit/Debit Flag - <?php echo $aHDet["creditdebit_flag_code"] ?></div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
   
</table>
&nbsp;

<div class=Section1>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>
<table border=1 cellspacing=0 cellpadding=0 width="60%" style='width:60.0%; border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt; mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr>
  <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><B>Adjustment Number</B></p>
  </td>
  <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><B>Date</B></p>
  </td>
  <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><B>Amount</B></p>
  </td>
  <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><B>Purchase Order Number</B></p>
  </td>
 </tr>
 <tr>
  <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><?php Print $aData["TransNumber"] ?></p>
  </td>
  <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><?php print date("Y-m-d", strtotime($aData["TransDate"])) ?></p>
  </td>
  <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><?php Print currency($aHDet["monetary_amount"]) ?></p>
  </td>
  <td width="25%" valign=top style='width:25.0%;border-top:none;border-left: none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><?php Print $aHDet["trace_reference_id"] ?></p>
  </td>
 </tr>
</table>
&nbsp;
<table border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;
 border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-padding-alt:0in 5.4pt 0in 5.4pt'>


 <tr>
    <?php echo $strRefHdr ?>
 </tr>
 <tr>
    <?php echo displayAddresses($aAdd) ?>
 </tr>
</table>
&nbsp;

<table border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;
 border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-padding-alt:0in 5.4pt 0in 5.4pt'>
   <tr>
      <td colspan="3" valign=top style='width:50.0%;border:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><B>Debit/Credits Adjustments</B></p>
      </td>
   </tr>
   <tr>
      <td width="33%" valign=top style='width:33.0%;border:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><B>Identification No.</B></p>
      </td>
      <td width="33%" valign=top style='width:33.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><B>Reason</B></p>
      </td>
      <td width="33%" valign=top style='width:33.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><B>Adjustment Amount</B></p>
      </td>
   </tr>

<?php

   // loop through data and conditionally display functionality and content
   $i = 0;
   while ($i < count($aDet)) {

?>

   <tr>
      <td width="33%" valign=top style='width:33.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_ref_doc_number"] ?></p>
      </td>
      <td width="33%" valign=top style='width:33.0%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_cash_discount_reason"] ?></p>
      </td>
      <td width="33%" valign=top style='width:33.0%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_paid_amount"] ?></p>
      </td>
   </tr>

<?php
 
   ++$i;
   }

?>


</table>

</div>



<?php

}

?>