<?php


function display824($aData='', $aAdd='', $aDetTxt='') {
$intAddWidth=0;
$strAddHdr='';
$strRefHdr='';
$strAdd='';
$strAddType='';

$i = 0;

?>
<style>
 /* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@page Section1
	{size:8.5in 11.0in;
	margin:1.0in 1.25in 1.0in 1.25in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
</style>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header">View <?php echo $aData["EDIADocTypeID"] ?> Transaction</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
    <tr>
        <td><div class="MsoNormal"><B>Transaction Set Purpose : <?php echo $aData["TransPurpose"] ?></B></div></td>
    </tr>
</table>
&nbsp;

<div class=Section1>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>
<table border=1 cellspacing=0 cellpadding=0 width="40%" style='width:40.0%; border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt; mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr>
  <td width="50%" valign=top style='width:50.0%;border:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><B>Invoice #</B></p>
  </td>
  <td width="50%" valign=top style='width:50.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><B>Date</B></p>
  </td>
 </tr>
 <tr>
  <td width="50%" valign=top style='width:50.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><?php Print $aData["TransNumber"] ?></p>
  </td>
  <td width="50%" valign=top style='width:50.0%;border-top:none;border-left: none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><?php print date("Y-m-d", strtotime($aData["TransDate"])) ?></p>
  </td>
 </tr>
</table>
&nbsp;
<table border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;
 border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr>
    <?php echo displayAddresses($aAdd) ?>
 </tr>
</table>
&nbsp;



<table border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%; border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt; mso-padding-alt:0in 5.4pt 0in 5.4pt'>
     <tr>
         <td colspan="7" width="100%" valign=top style='width:100%;border-top:solid windowtext .5pt;border-left:solid windowtext .5pt;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>


<?php

   // loop through data and conditionally display functionality and content
   $i = 0;
   while ($i < count($aDetTxt)) {

?>

            <p class=MsoNormal><?php Print $aDetTxt[$i]["dt_det_text"]; ?></p>

<?php
 
    ++$i;
    }

?>

        </td>
    </tr>
</table>

</div>
<?php

}

?>