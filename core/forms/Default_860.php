<?php


function display860($aData='', $aAdd='', $aRef='', $aDet='') {
$intAddWidth=0;
$strAddHdr='';
$strRefHdr='';
$strAdd='';
$strAddType='';

$i = 0;

if (count($aRef)) {
    while ($i < count($aRef)) {
    
        $strRefHdr = $strRefHdr . "<tr>\n";
        $strRefHdr = $strRefHdr . "<td  colspan='2' width='100%' valign=top style='width:100.0%;border:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>\n";
        $strRefHdr = $strRefHdr . "<p class=MsoNormal><B>Reference Numbers</B></p>\n";
        $strRefHdr = $strRefHdr . "</td>\n";
        $strRefHdr = $strRefHdr . "<tr>\n";
        $strRefHdr = $strRefHdr . "<td width='50%' valign=top style='width:50.0%;border:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>\n";
        $strRefHdr = $strRefHdr . "<p class=MsoNormal>" . $aRef[$i]["dr_qual"] . "</p>\n";
        $strRefHdr = $strRefHdr . "</td>\n";
        $strRefHdr = $strRefHdr . "<td width='50%' valign=top style='width:50.0%;border-left: none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>\n";
        $strRefHdr = $strRefHdr . "<p class=MsoNormal>" . $aRef[$i]["dr_id"] . "</p>\n";
        $strRefHdr = $strRefHdr . "</td>\n";
        $strRefHdr = $strRefHdr . "</tr>\n";

        ++$i;
    }
}


?>
<style>
 /* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";}
@page Section1
	{size:8.5in 11.0in;
	margin:1.0in 1.25in 1.0in 1.25in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.Section1
	{page:Section1;}
</style>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header">View <?php echo $aData["EDIADocTypeID"] ?> Transaction</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
    <tr>
        <td><div class="MsoNormal"><B>Transaction Set Purpose : <?php echo $aData["TransPurpose"] ?></B></div></td>
    </tr>
    <tr>
        <td><div class="MsoNormal"><B>Transaction Type : <?php echo $aData["TransType"] ?></B></div></td>
    </tr>
    
</table>
&nbsp;

<div class=Section1>

<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>
<table border=1 cellspacing=0 cellpadding=0 width="40%" style='width:40.0%; border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt; mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr>
  <td width="50%" valign=top style='width:50.0%;border:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><B>Purchase Order #</B></p>
  </td>
  <td width="50%" valign=top style='width:50.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><B>Date</B></p>
  </td>
 </tr>
 <tr>
  <td width="50%" valign=top style='width:50.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><?php Print $aData["TransNumber"] ?></p>
  </td>
  <td width="50%" valign=top style='width:50.0%;border-top:none;border-left: none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
   <p class=MsoNormal><?php print date("Y-m-d", strtotime($aData["TransDate"])) ?></p>
  </td>
 </tr>
</table>
&nbsp;
<table border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;
 border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr>
    <?php echo displayAddresses($aAdd) ?>
 </tr>
</table>
&nbsp;

<table border=1 cellspacing=0 cellpadding=0 width="40%" style='width:40.0%; border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt; mso-padding-alt:0in 5.4pt 0in 5.4pt'>
    <?php echo $strRefHdr ?>
</table>


<p class=MsoNormal><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></p>
<table border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%; border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt; mso-padding-alt:0in 5.4pt 0in 5.4pt'>
   <tr>
      <td width="16%" valign=top style='width:16.0%;border:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>P.O. Number</p>
      </td>
      <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>Terms</p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>Term Date</p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>Weight</p>
      </td>
      <td width="8%" valign=top style='width:8.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>Via</p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>FOB</p>
      </td>
   </tr>
   <tr>
      <td width="16%" valign=top style='width:16.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print Clean($aData["RefTransNumber"]) ?></p>
      </td>
      <td width="25%" valign=top style='width:25.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print Clean($aData["TermsDescription"]) ?></p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border-top:none;border-left: none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php print date("Y-m-d", strtotime($aData["TermsNetDueDate"])) ?></p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aData["ShipWeight"] ?></p>
      </td>
      <td width="8%" valign=top style='width:8.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aData["ShipperName"] ?></p>
      </td>
      <td width="16%" valign=top style='width:16.0%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php // Print $aData["TransNumber"] ?></p>
      </td>
   </tr>
</table>

<table border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;
 border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-padding-alt:0in 5.4pt 0in 5.4pt'>
   <tr>
      <td width="7%" valign=top style='width:7.4%;border:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>Line No.</p>
      </td>
      <td width="12%" valign=top style='width:12.1%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>QTY Ordered</p>
      </td>
      <td width="12%" valign=top style='width:12.1%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>QTY to Receive</p>
      </td>
      <td width="12%" valign=top style='width:12.18%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>UOM</p>
      </td>
      <td width="12%" valign=top style='width:12.18%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
          <p class=MsoNormal>Price</p>
      </td>
      <td width="18%" valign=top style='width:18.26%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>Item</p>
      </td>
      <td width="18%" valign=top style='width:18.26%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal>SKU</p>
      </td>
   </tr>

<?php

   // loop through data and conditionally display functionality and content
   $i = 0;
   while ($i < count($aDet)) {

?>


   <tr>
      <td width="7%" valign=top style='width:7.4%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print trim($aDet[$i]["det_itemlinenum"]) ?></p>
      </td>
      <td width="12%" valign=top style='width:12.1%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_item_qty_Order"] ?></p>
      </td>
      <td width="12%" valign=top style='width:12.1%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_item_qty_ToReceive"] ?></p>
      </td>
      <td width="12%" valign=top style='width:12.18%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_Item_uom"] ?></p>
      </td>
      <td width="12%" valign=top style='width:12.18%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_itemunitcost"] ?></p>
      </td>
      <td width="18%" valign=top style='width:18.26%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_item_upc"] ?></p>
      </td>
      <td width="18%" valign=top style='width:18.26%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
         <p class=MsoNormal><?php Print $aDet[$i]["det_item_sku"] ?></p>
      </td>
   </tr>

      <tr>
         <td colspan="7" width="100%" valign=top style='width:100%;border-top:none;border-left:solid windowtext .5pt;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>

   <?php  
       $j = 0; 
       $aDetTxt = $aDet[$i]["det_msg_txt"];


       while ($j < count($aDetTxt)) { 
   ?>
            <p class=MsoNormal><?php Print $aDetTxt[$j]["dt_det_text"]; ?></p>
   <?php 
   ++$j; 
   }
   ?>

         </td>

      </tr>
      <tr>
<table border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;
 border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr>
    <?php
    $aDetSch = $aDet[$i]["det_lin_sch"];
    echo displayScheduled($aDetSch); 
    ?>
 </tr>
</table>
<table border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%;
 border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr>
    <?php
    $aDetAdd = $aDet[$i]["det_add_rec"];
    echo displayAddresses($aDetAdd); 
    ?>
 </tr>
</table>
      </tr>


<?php
 
++$i;
}

?>


</table>
<table border=1 cellspacing=0 cellpadding=0 width="100%" style='width:100.0%; border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt; mso-padding-alt:0in 5.4pt 0in 5.4pt'>
 <tr>
  <td width="60%" valign=top style='width:60.0%;border:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>Comments: </p>
  </td>
  <td width="40%" valign=top style='width:40.0%;border:solid windowtext .5pt; border-left:none;mso-border-left-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>Net Freight: $<?php Print round($aData["NetFreightDue"], 2) ?></p>
  <p class=MsoNormal>Net Due After <?php print date("Y-m-d", strtotime($aData["TermsNetDueDate"])) ?>: $<?php Print round($aData["NetAmountDue"], 2) ?></p>
  <p class=MsoNormal>Net Due Before <?php print date("Y-m-d", strtotime($aData["TermsNetDueDate"])) ?>: $<?php Print round($aData["NetAmountDueTerms"], 2) ?></p>
  </td>
 </tr>


</table>

</div>



<?php

}


function create860($aData,$it) {

$tid = $aData["EDIATransID"];
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header">Edit Transaction <?php echo $it; ?></div></td>
    </tr>
    <tr>
        <td><div class="copy">To create a new <?php echo $it; ?> to <?php print $aData[tp_name]." for Inbound Transaction ".$aData[TransNumber] ?>, please complete the form below.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<form action="<?php print SELF ?>?op=add&it=<?php print $it ?>&tid=<?php print $tid ?>" method="post" name="ediaform">
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="formlabel">Tracking Number:</div></td>
        <td><input type="text" name="name" value="<?php print clean($sName) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td>
        <table border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td><input type="radio" name="ack" value="1" <?php if ($sAck == 1) { print "checked"; } else { print ""; }  ?>>Accept&nbsp;</td>
                <td><input type="radio" name="ack" value="0" <?php if ($sAck == 0) { print "checked"; } else { print ""; }  ?>>Decline&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td align="right" colspan="2"><input type="image" src="../../_img/buttons/btn_submit.gif" width="58" height="15" alt="" border="0" onfocus="this.blur();" /></td>
    </tr>
</table>
</form>


<?php

}

?>