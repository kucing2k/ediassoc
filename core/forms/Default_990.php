<?php

function display990($aData) {

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header">View <?php echo $aData["EDIADocTypeID"] ?> Transaction</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

    <table style="width:100%;">
        <tr>
            <td width="55%"><img src="<?php echo $aData["tp_image"] ?>" border="0" /><br>
            </td>
	    <td width="45%"><font SIZE="2">
		<b>Partner : </b><?php echo $aData["tp_name"] ?><br>
		<b>Ack Number : </b><?php echo $aData["TransNumber"] ?><br>
		<b>Ack Status : </b><?php if (!strcmp("Y", $aData["Ack"])) { print "Accepted"; } else { Print "Rejected"; } ?><br>
		<b>Ack Date : </b><?php echo $aData["TransDate"] ?><br>
		<b>Manifest Number : </b><?php echo $aData["RefTransNumber"] ?><br>
		<b>Manifest Date : </b><?php echo $aData["RefTransDate"] ?><br>
		<b>Status : </b><?php if (!strcmp("0", $aData["EDIAStatus"])) { print "Received - Error"; } elseif (!strcmp("1", $aData["EDIAStatus"])) { Print "Received - Unread"; } elseif (!strcmp("2", $aData["EDIAStatus"])) { Print "Received - Read"; } elseif (!strcmp("3", $aData["EDIAStatus"])) { Print "Received - Acknowledged"; } elseif (!strcmp("4", $aData["EDIAStatus"])) { Print "Received - Archived"; } elseif (!strcmp("5", $aData["EDIAStatus"])) { Print "Received - Deleted"; } elseif (!strcmp("6", $aData["EDIAStatus"])) { Print "Sending - Draft"; } elseif (!strcmp("7", $aData["EDIAStatus"])) { Print "Sending - Queued"; } elseif (!strcmp("8", $aData["EDIAStatus"])) { Print "Sending - Sent"; } elseif (!strcmp("9", $aData["EDIAStatus"])) { Print "Sending - Acknowledged"; } elseif (!strcmp("10", $aData["EDIAStatus"])) { Print "Sending - Archived"; } elseif (!strcmp("11", $aData["EDIAStatus"])) { Print "Sending - Deleted"; } ?><br>
          </b>
		</font>
	    </td>
	
        </tr>
    </table>



<?php

}
function create990($aData,$it) {

$tid = $aData["EDIATransID"];
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header">Edit Transaction <?php echo $it; ?></div></td>
    </tr>
    <tr>
        <td><div class="copy">To create a new <?php echo $it; ?> to <?php print $aData[tp_name]." for Inbound Transaction ".$aData[TransNumber] ?>, please complete the form below.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<form action="<?php print SELF ?>?op=add&it=<?php print $it ?>&tid=<?php print $tid ?>" method="post" name="ediaform">
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="formlabel">Tracking Number:</div></td>
        <td><input type="text" name="name" value="<?php print clean($sName) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td>
        <table border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td><input type="radio" name="ack" value="1" <?php if ($sAck == 1) { print "checked"; } else { print ""; }  ?>>Accept&nbsp;</td>
                <td><input type="radio" name="ack" value="0" <?php if ($sAck == 0) { print "checked"; } else { print ""; }  ?>>Decline&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td align="right" colspan="2"><input type="image" src="../../_img/buttons/btn_submit.gif" width="58" height="15" alt="" border="0" onfocus="this.blur();" /></td>
    </tr>
</table>
</form>


<?php

}

?>