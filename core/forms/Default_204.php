<?php

function display204($aData,$aDet,$aRef,$aAdd,$aTxt,$aResp) {

?>

<FORM method='post' action='$PHP_SELF' NAME='duplist' ID='duplist'>
    <table id="tblBody" border="0" style="border-style:None;width:100%;">
        <tr id="Tablerow2" valign="Top">
            <td valign="Middle"><span></span><table border="0" style="width:100%;">
                           <tr>
                               <td>
                                   <table style="width:100%;">
                                       <tr>
                                           <td width="65%"><img src="../../_img/logos/ls_logo.jpg" border="0" /><br>
                                           </td>
                           	           <td width="35%"><font SIZE="2">
                           	              <b>Partner : <?php print $aData["tp_name"]; ?><br>
                           	              Manifest Number : <?php print $aData["TransNumber"]; ?><br>
                           	              Transaction Purpose : <?php if (!strcmp("00", $aData["TransPurpose"])) { print "Original"; } elseif (!strcmp("01", $aData["TransPurpose"])) { Print "Cancellation"; } elseif (!strcmp("04", $aData["TransPurpose"])) { Print "Change"; } ?><br>
                           	              Date : <?php print $aData["EDIATransDateTime"]; ?><br>
                           	              Status : <?php if (!strcmp("0", $aData["EDIAStatus"])) { print "Received - Error"; } elseif (!strcmp("1", $aData["EDIAStatus"])) { Print "Received - Unread"; } elseif (!strcmp("2", $aData["EDIAStatus"])) { Print "Received - Read"; } elseif (!strcmp("3", $aData["EDIAStatus"])) { Print "Received - Acknowledged"; } elseif (!strcmp("4", $aData["EDIAStatus"])) { Print "Received - Archived"; } elseif (!strcmp("5", $aData["EDIAStatus"])) { Print "Received - Deleted"; } elseif (!strcmp("6", $aData["EDIAStatus"])) { Print "Sending - Draft"; } elseif (!strcmp("7", $aData["EDIAStatus"])) { Print "Sending - Queued"; } elseif (!strcmp("8", $aData["EDIAStatus"])) { Print "Sending - Sent"; } elseif (!strcmp("9", $aData["EDIAStatus"])) { Print "Sending - Acknowledged"; } elseif (!strcmp("10", $aData["EDIAStatus"])) { Print "Sending - Archived"; } elseif (!strcmp("11", $aData["EDIAStatus"])) { Print "Sending - Deleted"; } ?></b>
                           	              </font>
                           	           </td>
                                       </tr>
                                   </table>
                               </td>
                           </tr>
                           <tr align="Center" style="width:100%;">
                               <td>
                                   <table class="T" border="0" style="width:100%;">
                                       <tr>
                                           <td width="13%" class="header"><center><b><u>Customer ID</u></b></center></td>
                                           <td width="13%" class="header"><center><b><u>ISA Control Num</u></b></center></td>
                                           <td width="13%" class="header"><center><b><u>GS Control Num</u></b></center></td>
                                           <td width="13%" class="header"><center><b><u>ST Control Num</u></b></center></td>
                                           <td width="13%" class="header"><center><b><u>Contact Name</u></b></center></td>
                                           <td width="13%" class="header"><center><b><u>Contact Number</u></b></center></td>
                                       </tr>
                                       <tr>
                                           <td width="13%" bgcolor="#FFF7BE"><center><?php print $aData["GSSenderID"]; ?></center></td>
                                           <td width="13%" bgcolor="#FFF7BE"><center><?php print $aData["ISAControlNum"]; ?></center></td>
                                           <td width="13%" bgcolor="#FFF7BE"><center><?php print $aData["GSControlNum"]; ?></center></td>
                                           <td width="13%" bgcolor="#FFF7BE"><center><?php print $aData["STControlNum"]; ?></center></td>
                                           <td width="13%" bgcolor="#FFF7BE"><center><?php print $aData["ContactName"]; ?></center></td>
                                           <td width="13%" bgcolor="#FFF7BE"><center><?php print $aData["ContactPhone"]; ?></center></td>
                                       </tr>
                                   </table>
                               </td>
                           </tr>
                           <tr align="Center" style="width:100%;">
                               <table style="width:100%;">

<?php

   // loop through data and conditionally display functionality and content
   $i = 0;
   while ($i < count($aDet)) {
   $bg = "FFFFFF";
?>
                                   <table colspan="6" width="100%" class="T">
                                       <tr>
                                           <td width="13%" class="header"><center><b><u>Stop #</u></b></center></td>
                                           <td width="13%" class="header"><center><b><u>Reason</u></b></center></td>
                                           <td width="13%" class="header"><center><b><u>Date</u></b></center></td>
                                           <td width="13%" class="header"><center><b><u>Time</u></b></center></td>
                                           <td width="13%" class="header"><center><b><u>Weight</u></b></center></td>
                                           <td width="13%" class="header"><center><b><u>Wt.Unit</u></b></center></td>
                                       </tr>
                                   </table>
                                   <table colspan="6" width="100%">
                                       <tr>
                                           <td width="13%" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $aDet[$i]["det_itemlinenum"]; ?></div></center></td>
                                           <td width="13%" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php if (!strcmp("CL", $aDet[$i]["det_reasoncode"])) { print "Complete Load"; } elseif (!strcmp("PU", $aDet[$i]["det_reasoncode"])) { Print "Partial Unload"; } elseif (!strcmp("CU", $aDet[$i]["det_reasoncode"])) { Print "Complete Unload"; } ?></div></center></td>
                                           <td width="13%" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $aDet[$i]["det_stop_dt"]; ?></div></center></td>
                                           <td width="13%" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $aDet[$i]["det_stop_tm"]; ?></div></center></td>
                                           <td width="13%" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $aDet[$i]["det_lineweight"]; ?></div></center></td>
                                           <td width="13%" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $aDet[$i]["det_lineweightuom"]; ?></div></center></td>
                                       </tr>
                                   </table>
                                   <table width="100%" bgcolor="#4791c5">
                                       <tr>
                                           <td WIDTH="33%" class="header"><center><b><u>Reference Numbers</u></b></center></td>
                                           <td WIDTH="33%" class="header"><center><b><u>Address</u></b></center></td>
                                           <td WIDTH="33%" class="header"><center><b><u>Notes</u></b></center></td>
                                       </tr>
                                   </table>
                                   <table BORDER=0 WIDTH="100%" id="tblDocIL" style="width:100%;" >
                                       <tr>
                                           <td VALIGN=TOP WIDTH="33%">
                                               <table BORDER=0 WIDTH="100%">

<?php

$j = 0;
while ($j < count($aRef)) { // Loop to add Reference Numbers
   $bg = "FFFFFF";
   if (!strcmp($aRef[$j]["dr_det_key"], $aDet[$i]["det_key"])) {

?>

                                                   <tr>
                                                     <td><center><?php if (!strcmp("CO", $aRef[$j]["dr_qual"])) { print "Customer Order"; } elseif (!strcmp("BM", $aRef[$j]["dr_qual"])) { Print "Bill Of Lading"; } else { Print "Unknown"; } ?></center></td>
                                                     <td><center><?php Print $aRef[$j]["dr_id"]; ?></center></td>
                                                   </tr>

<?php

   }
   ++$j;
} // end loop

?>

                                               </table>
                                           </td>
                                           <td VALIGN=TOP WIDTH="33%">
                                               <table BORDER=0 WIDTH="100%" BGCOLOR="#FFFFFF" >
                                       
<?php

$j = 0;
while ($j < count($aAdd)) { // Loop to add Reference Numbers
$bg = "FFFFFF";
   if (!strcmp($aAdd[$j]["da_det_key"], $aDet[$i]["det_key"])) {

?>

                                                   <tr>
                                                       <td><?php Print $aAdd[$j]["da_name"]." - ".$aAdd[$j]["da_duns_id"]; ?></td>
                                                   </tr>

<?php

if ($aAdd[$j]["da_address1"]) {

?>

                                                   <tr>
                                                       <td><?php Print $aAdd[$j]["da_address1"]; ?></td>
                                                   </tr>

<?php
}
if ($aAdd[$j]["da_address2"]) {

?>

                                                   <tr>
                                                       <td><?php Print $aAdd[$j]["da_address2"]; ?></td>
                                                   </tr>

<?php

}
if ($aAdd[$j]["da_address3"]) {

?>

                                                   <tr>
                                                       <td><?php Print $aAdd[$j]["da_address3"]; ?></td>
                                                   </tr>

<?php

}
if ($aAdd[$j]["da_address4"]) {

?>

                                                   <tr>
                                                       <td><?php Print $aAdd[$j]["da_address4"]; ?></td>
                                                   </tr>

<?php

}

?>

                                                   <tr>
                                                       <td><?php Print $aAdd[$j]["da_city"].", ".$aAdd[$j]["da_state"]." ".$aAdd[$j]["da_postalcode"] ; ?></td>
                                                   </tr>
                                                   
                                                   <tr>
                                                       <td></td>
                                                   </tr>
<?php

if ($aAdd[$j]["da_contactname"]) {

?>
                                                   <tr>
                                                       <td>Contact:<?php Print $aAdd[$j]["da_contactname"]; ?></td>
                                                   </tr>
<?php

}
if ($aAdd[$j]["da_ContactPhone"]) {

?>
                                                   <tr>
                                                       <td>Phone:<?php Print $aAdd[$j]["da_ContactPhone"]; ?></td>
                                                   </tr>

<?php
}
}
++$j;
} // end loop

?>

                                               </table>
                                           </td>
                                           <td VALIGN=TOP WIDTH="33%">
                                               <table BORDER=0 WIDTH="100%" BGCOLOR="#FFF7BE">
<?php

$j = 0;
while ($j < count($aTxt)) { // Loop to add Reference Numbers
$bg = "FFFFFF";
   if (!strcmp($aTxt[$j]["dt_det_key"], $aDet[$i]["det_key"])) {

?>
                                                   <tr>
                                                       <td><?php Print $aTxt[$j]["dt_det_text"]; ?></td>
                                                   </tr>
<?php
   }
++$j;
} // end loop

?>
                                               </table>
                                           </td>
                                       </tr>
                                       <tr>
                                           <td colspan="6"><hr SIZE=1 WIDTH="100%"></td>
                                       </tr>
                               
<?php

++$i;
} // end loop

?>
                                       <tr>
                                           <td colspan="6">
                                               <CENTER>
                                                   <table BORDER=0 WIDTH="70%" BGCOLOR="#FFF7BE">
                                                       <tr>
                                                           <td colspan="3"><CENTER><b><u>Replies to this Message</u></b></CENTER></td>
                                                       </tr>

<?php

$j = 0;
while ($j < count($aResp)) { // Loop to add Reference Numbers
$bg = "FFFFFF";
?>
                                                       <tr>
                                                           <td><CENTER><?php Print $aResp[$j]["EDIADocTypeID"]; ?></CENTER></td>
                                                           <td><CENTER><?php Print $aResp[$j]["TransNumber"]; ?></CENTER></td>
                                                           <td><CENTER><?php Print $aResp[$j]["EDIATransDateTime"]; ?></CENTER></td>
                                                           <td><CENTER><?php Print $aResp[$j]["EDIAStatus"]; ?></CENTER></td>
                                                       </tr>
<?php
++$j;
} // end loop

?>
                                                       </table>
                                                   </CENTER>
                                               </td>
                                           </tr>
                                       </table>
                        
                        
                                    </table>
                                </td>
                            </tr>
            </td>
        </tr>
    </table>
</FORM>



<?php

}

function create204($aData,$it) {

$tid = $aData["EDIATransID"];
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header">Edit Transaction <?php echo $it; ?></div></td>
    </tr>
    <tr>
        <td><div class="copy">To create a new <?php echo $it; ?> to <?php print $aData[tp_name]." for Inbound Transaction ".$aData[TransNumber] ?>, please complete the form below.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<form action="<?php print SELF ?>?op=add&it=<?php print $it ?>&tid=<?php print $tid ?>" method="post" name="ediaform">
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="formlabel">Tracking Number:</div></td>
        <td><input type="text" name="name" value="<?php print clean($sName) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td>
        <table border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td><input type="radio" name="ack" value="1" <?php if ($sAck == 1) { print "checked"; } else { print ""; }  ?>>Accept&nbsp;</td>
                <td><input type="radio" name="ack" value="0" <?php if ($sAck == 0) { print "checked"; } else { print ""; }  ?>>Decline&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td align="right" colspan="2"><input type="image" src="../../_img/buttons/btn_submit.gif" width="58" height="15" alt="" border="0" onfocus="this.blur();" /></td>
    </tr>
</table>
</form>


<?php

}

?>