<?php

// File Location: /core/support/index.php

require_once("tpl_secure.php");
require_once("class.support.php");

// instantiate news class
$oSupport = new support();

// the session class is instantiated in the tpl_secure.php file

// get users and user count
$aSupport = $oSupport->getSupportList($iComId, $iCursor);
// check for users
if (count($aSupport)) {
    
    // build page data array
    $i = 0;
    while ($i < count($aSupport)) {
        $aData[$i]["Id"] = $aSupport[$i]["sid"];
        $aData[$i]["support_title"] = $aSupport[$i]["support_title"];
        $aData[$i]["lookup_note"] = $aSupport[$i]["lookup_note"];
        $aData[$i]["user_login"] = $aSupport[$i]["user_login"];
        $aData[$i]["log_date"] =$aSupport[$i]["log_date"];
        ++$i;
    }
}

setHeader();
openPage();
?>

<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header">Support Administrator</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
        <td align="right" valign="top"><?php if ($iPerm > 1) { ?><a href="form.php?op=add"><img src="../../_img/buttons/btn_additem.gif" width="58" height="15" alt="" border="0" /></a><?php } ?></td>
    </tr>
</table>

<?php renderSupportList($aData) ?>

<?php closePage(); ?>
