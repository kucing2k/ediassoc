<?php

// File Location: /core/order/index.php

require_once("tpl_secure.php");
require_once("class.silorder.php");


setHeader();
openPage();

$order = new SilOrder;

$aData = $order->getAllShipments();
?>


<script type="text/javascript" language="javascript" src="/_lib/_js/datatables/jquery.js"></script>
<script type="text/javascript" language="javascript" src="/_lib/_js/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="/_lib/_js/datatables/datatables.mailbox.js"></script>
<style type="text/css">
    @import "/_lib/edia.mailbox.datatables.css";
    @import "/_lib/edia.mailbox.searchform.css";
</style>



<form name="deletethread" method="post" action="/core/order/index.php">
<table border="0" cellpadding="0" cellspacing="0" width="758">
    <tbody>
    <tr>
        <td colspan="2">
            <div id="searchContainer">
                <div id="searchICNDiv">
                    3B11 XML Search Options

                </div>
                <div id="advSearchDivSilOrder">
                    <table>
                        <tr>
                            <td class="tableLabel">
                                Search Type:
                            </td>
                            <td colspan="2" style="text-align:left">
                                <select name="searchType">
                        <option>Shipment #</option>
                        <option>Delivery numbers</option>
                        <option>Msg ID</option>
                        <option>Customer Name</option>
                        
                    </select>
                                 <input type="text" name="searchICN" id="searchICN" style="width: 240px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="tableLabel">
                                Start Date:
                            </td>
                            <td>
                                <input id="searchStartDate" name="searchStartDate" type="text" value="<?php echo date('Y-m-d', $t);?>"> (yyyy-mm-dd)
                            </td>
                            <td>
                                <input id="searchStartTime" name="searchStartTime" type="text" value="<?php echo date('H:i:s', $t);?>"> (hh:mm:ss)
                            </td>
                        </tr>
                        <tr>
                            <td class="tableLabel">
                                End Date:
                            </td>
                            <td>
                                <input id="searchEndDate" name="searchEndDate" type="text"> (yyyy-mm-dd)
                            </td>
                            <td>
                                <input id="searchEndTime" name="searchEndTime" type="text"> (hh:mm:ss)
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <table>
                        <tr>
                            <td style="text-align: left">
                                <input type='submit' name='action' value='Start Over'/> <input type='submit' name='action' value='Go'/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2"><div class="header">TLR - Siltronic 3B11 - 3B13 Order Processing</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<?php renderSiltronicTrans($iCnt, $aData, $iUserId) ?>

</form>


<?php
closePage();