<?php

// File Location: /core/users/form.php

require_once("elements.php");
require_once("class.mailbox.php");
require_once("DB.php");
$oMailbox = new mailbox;


$aRec = $oMailbox->getRecordInfo($id);

?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>View Transaction Information</title>
<style type="text/css"><!--
body,table,td { font-family:Arial; font-size:10px; color: #000000;
}
input { cursor: hand; font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; text-decoration: none; background-color: #ffffff; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; border: #000000; border-style: solid; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px
}
textarea { color: #000000; font-size: 10px; font-family: Arial, Helvetica, sans-serif; text-decoration: none; background-color: #ffffff; border: #000000; border-style: solid; border-top-width: 1px; border-right-width: 1px; border-left-width: 1px; border-bottom-width: 1px; margin: 2px; padding: 1px 
}//-->
</style>

<script language="Javascript">
function closewindow() {
window.close();
}
</script>
</head>
<body>
<FORM method='post' action='$PHP_SELF' NAME='duplist' ID='duplist'>
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="formlabel">EDIAnywhere Transaction ID:</div></td>
        <td><div class="formlabel"><?php Print $aRec["EDIATransID"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Authentication Qual:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Auth_Qual"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Authentication ID:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Auth_Info"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Security Qual:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Security_Qual"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Security ID:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Security_Info"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Sender Qual:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Sender_Qual"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Sender ID:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Sender"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Receiver Qual:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Receiver_Qual"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Receiver ID:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Receiver"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Date:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Date"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Time:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Time"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Control ID:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Control_ID"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Version Num:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Version_Release"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Control Num:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Control_Number"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Ack Request:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Ack_Requested"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Interchange Test Flag:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ISA_Test_Indicator"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Group Functional ID:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["GS_Functional_Id"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Group Sender ID:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["GS_Sender"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Group Receiver ID:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["GS_Receiver"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Group Create Date:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["GS_Date"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Group Create Time:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["GS_Time"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Group Control Number:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["GS_Control_Number"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Group Agency ID:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["GS_Agency"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Group Version Release:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["GS_Version_Release"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Transaction Set ID:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ST_Transaction_Set_ID"]; ?></div></td> 
    </tr>
    <tr>
        <td><div class="formlabel">Transaction Control Num:  </div></td>
        <td><div class="formlabel"><?php Print $aRec["ST_Control_Number"]; ?></div></td>
    </tr>
    <tr>
        <td><div class="formlabel">Total Records:</div></td>
        <td><div class="formlabel"><?php Print $aRec[0]["NbrRecords"]; ?></div></td>
    </tr>
    <tr>
        <td><div class="formlabel">Total Bytes:</div></td>
        <td><div class="formlabel"><?php Print $aRec[0]["NbrBytes"]; ?></div></td>
    </tr>
</table>
<br><br><input type='button' onClick='closewindow();' value='close'>
</FORM>
</body>