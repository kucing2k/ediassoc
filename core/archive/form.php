<?php

// File Location: /core/forms/form.php

require_once("tpl_secure.php");
require_once("handlers.php");
require_once("htmlformfuncs.php");
require_once("class.mailbox.php");

$oMailbox = new mailbox;


if ($tid){
   setHeader();  //Set the HTML Header
   openPage();   //Open Menu System
   $aData = $oMailbox->getTranType($tid);
   $it = $aData;
//   print($it);
   $aData = '';
   if (!strcmp("990", $it)) {
      $aData = $oMailbox->getHDRRefData($tid);
         $myArr = $_SESSION["aList"];
         $key = array_search($tid, $myArr);
         $prev = $myArr[$key - 1];
         $next = $myArr[$key + 1];
      include '../forms/Default_990.php';
      renderPrevNext($prev,$next);
      display990($aData);
      renderPrevNext($prev,$next);
   } elseif (!strcmp("210", $it)) {
      $aData = $oMailbox->getHDRRefData($tid);
         $myArr = $_SESSION["aList"];
         $key = array_search($tid, $myArr);
         $prev = $myArr[$key - 1];
         $next = $myArr[$key + 1];
      // print_r($aData);
      include '../forms/Default_210.php';
      renderPrevNext($prev,$next);
      display210($aData);
      renderPrevNext($prev,$next);
   } elseif (!strcmp("204", $it)) {
      $aData = $oMailbox->getHDRRefData($tid);
          if ($aData) {
              $aReply = $oMailbox->getOutBoundTypes($aData["GSSenderID"],$iComId);
              $aDet = $oMailbox->getRecordDetailData($tid);
              $aRef = $oMailbox->getRecordDetailRefs($tid);
              $aAdd = $oMailbox->getRecordDetailAdds($tid);
              $aTxt = $oMailbox->getRecordDetailTxt($tid);
              $aResp = $oMailbox->getResponseTo($tid);
              $trn = $aData["EDIADocTypeID"];
          }

         $myArr = $_SESSION["aList"];
         $key = array_search($tid, $myArr);
         $prev = $myArr[$key - 1];
         $next = $myArr[$key + 1];
      include '../forms/Default_204.php';
      renderReplyList($aReply,$tid,$trn,$prev,$next);
      display204($aData,$aDet,$aRef,$aAdd,$aTxt,$aResp);
      renderReplyList($aReply,$tid,$trn,$prev,$next);
   } elseif (!strcmp("860", $it)) {
      $aData = $oMailbox->getHDRRefData($tid);
          if ($aData) {
              $wow = $oMailbox->updateRead($tid);
              $aData["TransPurpose"] = RemoveShouting($oMailbox->getStandardDesc($aData["GSAgency"],$aData["GSVersion"],$aData["TransPurpose"],"0353"));
              $aData["TransType"] = RemoveShouting($oMailbox->getStandardDesc($aData["GSAgency"],$aData["GSVersion"],$aData["TransType"],"0092"));
              $aDet = $oMailbox->getRecordDetailData($tid,$aData["GSAgency"],$aData["GSVersion"]);
              $aRef = $oMailbox->getRecordRefs($tid,$aData["GSAgency"],$aData["GSVersion"]);
              $aAdd = $oMailbox->getRecordAdds($tid,$aData["GSAgency"],$aData["GSVersion"]);
          }
         $myArr = $_SESSION["aList"];
         $key = array_search($tid, $myArr);
         $prev = $myArr[$key - 1];
         $next = $myArr[$key + 1];
      include '../forms/Default_860.php';
//      print_r($aDet);
      renderPrevNext($prev,$next);
      display860($aData,$aAdd,$aRef,$aDet);
      renderPrevNext($prev,$next);
   } elseif (!strcmp("824", $it)) {
      $aData = $oMailbox->getHDRRefData($tid);
          if ($aData) {
              $wow = $oMailbox->updateRead($tid);
              $aData["TransPurpose"] = RemoveShouting($oMailbox->getStandardDesc($aData["GSAgency"],$aData["GSVersion"],$aData["TransPurpose"],"0353"));
              $aData["TransType"] = RemoveShouting($oMailbox->getStandardDesc($aData["GSAgency"],$aData["GSVersion"],$aData["TransType"],"0092"));
              $aDetTxt = $oMailbox->getRecordDetailTxt($tid);
//              $aRef = $oMailbox->getRecordRefs($tid,$aData["GSAgency"],$aData["GSVersion"]);
              $aAdd = $oMailbox->getRecordAdds($tid,$aData["GSAgency"],$aData["GSVersion"]);
          }
         $myArr = $_SESSION["aList"];
         $key = array_search($tid, $myArr);
         $prev = $myArr[$key - 1];
         $next = $myArr[$key + 1];
      include '../forms/Default_824.php';
//      print_r($aDetTxt);
      renderPrevNext($prev,$next);
      display824($aData,$aAdd,$aDetTxt);
      renderPrevNext($prev,$next);

   } elseif (!strcmp("820", $it)) {
      $aData = $oMailbox->getHDRRefData($tid);
          if ($aData) {
              $wow = $oMailbox->updateRead($tid);
              $aHDet = $oMailbox->get820HdrData($tid);
              $aDet = $oMailbox->getRecord820DetailData($tid,$aData["GSAgency"],$aData["GSVersion"]);
              $aAdd = $oMailbox->getRecordAdds($tid,$aData["GSAgency"],$aData["GSVersion"]);
          }
         $myArr = $_SESSION["aList"];
         $key = array_search($tid, $myArr);
         $prev = $myArr[$key - 1];
         $next = $myArr[$key + 1];
      include '../forms/Default_820.php';
//      print_r($aDet);
      renderPrevNext($prev,$next);
      display820($aData,$aAdd,$aHDet,$aDet);
      renderPrevNext($prev,$next);

   } elseif (!strcmp("810", $it)) {
      $aData = $oMailbox->getHDRRefData($tid);
          if ($aData) {
              $aReply = $oMailbox->getOutBoundTypes($aData["GSSenderID"],$iComId);
              $aAdd = $oMailbox->getRecordAdds($tid);
              $aDet = $oMailbox->getRecordDetailData($tid);
              // $aRef = $oMailbox->getRecordDetailRefs($tid);
              // $aAdd = $oMailbox->getRecordAdds($tid);
              // $aTxt = $oMailbox->getRecordDetailTxt($tid);
              // $aResp = $oMailbox->getResponseTo($tid);
              // $trn = $aData["EDIADocTypeID"];
          }
         $myArr = $_SESSION["aList"];
         $key = array_search($tid, $myArr);
         $prev = $myArr[$key - 1];
         $next = $myArr[$key + 1];
      // print_r($aData);
      include '../forms/Default_810.php';
      renderPrevNext($prev,$next);
      display810($aData, $aDet, $aAdd);
      renderPrevNext($prev,$next);
      $oMailbox->updateRead($tid);
   }

   
} else if ($rid){
   setHeader();  //Set the HTML Header
   openPage();   //Open Menu System
   if (!strcmp("990", $it)) {
      $aData = $oMailbox->getHDRRefData($rid);
      include '../forms/Default_990.php';
      create990($aData,$it);
   } elseif (!strcmp("210", $it)) {
      echo 'This is a 210';
      $aData = $oMailbox->getHDRRefData($rid);
      print_r($aData);
      include '../forms/Default_210.php';
      create210($aData,$it);
   } elseif (!strcmp("204", $it)) {
      echo 'This is a 204';
      $aData = $oMailbox->getHDRRefData($rid);
      print_r($aData);
      include '../forms/Default_204.php';
      create204($aData,$it);
   } elseif (!strcmp("850", $it)) {
      echo 'This is a 850';
      $aData = $oMailbox->getHDRRefData($rid);
      print_r($aData);
      include '../forms/Default_850.php';
      create850($aData,$it);
   } elseif (!strcmp("810", $it)) {
      echo 'This is a 810';
      $aData = $oMailbox->getHDRRefData($rid);
      print_r($aData);
      include '../forms/Default_810.php';
      create810($aData,$it);
   }

} else if (!strcmp("", $op) && !$tid){
   header("Location: index.php");
}

closePage(); ?>
