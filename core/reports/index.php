<?php

// File Location: /core/users/index.php

require_once("tpl_secure.php");
require_once("class.reports.php");

$oRep = new reports();

// the session class is instantiated in the tpl_secure.php file

// get Trading Partner count
$aPartns = $oRep->getPartners($iComId);

setHeader();
openPage();
?>

<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header">Report Administrator</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<?php writeReportMain($aPartns) ?>

<?php closePage(); ?>
