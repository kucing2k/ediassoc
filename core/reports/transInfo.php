<?php

// File Location: /core/users/form.php

require_once("elements.php");
require_once("class.reports.php");
// require_once("DB.php");
$oRpt = new reports();

$aData = $oRpt->getTransRpt($lDir,$lWhat,$lWho,$dStr,$dSto);
$iCnt = $oRpt->getTransRptCnt($lDir,$lWhat,$lWho,$dStr,$dSto);
// $aRec = $oEdia->getRecordInfo($id);

?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>View Transaction Information</title>
<style type="text/css"><!--
body,table,td { font-family:Arial; font-size:10px; color: #000000;
}
input { cursor: hand; font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; text-decoration: none; background-color: #ffffff; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; border: #000000; border-style: solid; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px
}
textarea { color: #000000; font-size: 10px; font-family: Arial, Helvetica, sans-serif; text-decoration: none; background-color: #ffffff; border: #000000; border-style: solid; border-top-width: 1px; border-right-width: 1px; border-left-width: 1px; border-bottom-width: 1px; margin: 2px; padding: 1px 
}//-->
</style>

<script language="Javascript">

function closewindow() {
window.close();
}
</script>
</head>
<body>
<FORM method='post' action='$PHP_SELF' NAME='duplist' ID='duplist'>
<?php 


if (is_array($aData)) { ?>
<table border="0" cellpadding="0" cellspacing="2">
    <tr>
        <td colspan="6"><a class="text">Report Date as of : <?php echo date("l F j, Y H:i:s"); ?></a></td>
    </tr>
    <tr>
        <td colspan="6"><a class="text"><?php echo 'Count : '.$iCnt; ?></a></td>
    </tr>

    <tr>
        <td colspan="6"><a class="text"><?php echo 'From Date : '.$dStr; ?></a></td>
    </tr>
    <tr>
        <td colspan="6"><a class="text"><?php echo 'To Date : '.$dSto; ?></a></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="4"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    
    $sLastP = '';
    $sLastT = '';
    $myBytes=0;
    $myRecords=0;
    $myCount=0;
    while ($i < count($aData)) {
        !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
	if ($aData[$i]["PartnerKEY"]!=$sLastP) {
		$sLastP = $aData[$i]["PartnerKEY"];
		$bg = "C6AADB";
	?>
	
    <tr>
        <td colspan="7"><hr width="100%"></td>
    </tr>
    <tr>
        <td colspan="6"><font size=5><?php echo 'Partner : '.$aData[$i]["PartnerKEY"]; ?></font></td>
    </tr>
	<?php
	
	if ($aData[$i]["TransactionSetID"]==$sLastT) {
		$sLastT = $aData[$i]["TransactionSetID"];
	?>
 
    <tr>
        <td class="dotrule" colspan="1"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
        <td colspan="5"><a class="text"><font size=4><?php echo 'Transaction : '.$aData[$i]["TransactionSetID"]; ?></font></a></td>
    </tr>
	<?php
		}
	}
	if ($aData[$i]["TransactionSetID"]!=$sLastT) {
		$sLastT = $aData[$i]["TransactionSetID"];
	?>
    <tr>
        <td class="dotrule" colspan="1"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
        <td colspan="5"><a class="text"><font size=4><?php echo 'Transaction : '.$aData[$i]["TransactionSetID"]; ?></font></a></td>
    </tr>
    <tr>
        <td><div class="listrow"><b><u></b></u></div></td>
        <td><div class="listrow"><center><b><u>Time Received</b></u></center></div></td>
        <td><div class="listrow"><center><b><u>Document Name</b></u></center></div></td>
        <td><div class="listrow"><center><b><u>Status</b></u></center></div></td>
        <td><div class="listrow"><center><b><u>Bytes</b></u></center></div></td>
        <td><div class="listrow"><center><b><u>Records</b></u></center></div></td>
    </tr>
	<?php
	}
    ?>
    <tr>
        <td width="50"><div class="listrow"><?php // print $aData[$i]["PartnerKEY"] ?></div></td>
        <td width="140" bgcolor="#<?php print $bg ?>"><div class="listrow"><center><?php print date ("j-M-Y, H:i:s", $aData[$i]["TimeCreated"]) ?></center></div></td>
        <td width="90" bgcolor="#<?php print $bg ?>"><div class="listrow"><center><?php print $aData[$i]["DocumentName"] ?></center></div></td>
        <td width="300" bgcolor="#<?php print $bg ?>"><div class="listrow"><?php $myCount++; if ($aData[$i]["ComplianceStatus"]==0) { print 'Incomplete'; } elseif ($aData[$i]["ComplianceStatus"]==1) { print 'Not Compliant'; } elseif ($aData[$i]["ComplianceStatus"]==2) { print 'Document OK'; } elseif ($aData[$i]["ComplianceStatus"]==3) { print 'Document Queued'; } elseif ($aData[$i]["ComplianceStatus"]==4) { print 'Document Send'; } elseif ($aData[$i]["ComplianceStatus"]==5) { print 'Net Received'; } elseif ($aData[$i]["ComplianceStatus"]==6) { print 'Net Delivered'; }  elseif ($aData[$i]["ComplianceStatus"]==7) { print 'F/A Received'; }  elseif ($aData[$i]["ComplianceStatus"]==8) { print 'Waiting for Acknowledgement'; }  elseif ($aData[$i]["ComplianceStatus"]==9) { print 'Acknowledgement Over Due'; }  elseif ($aData[$i]["ComplianceStatus"]==10) { print 'Net Warning'; }  elseif ($aData[$i]["ComplianceStatus"]==11) { print 'Net Error'; }  elseif ($aData[$i]["ComplianceStatus"]==12) { print 'Acknowledgement with Errors'; }  elseif ($aData[$i]["ComplianceStatus"]==13) { print 'Partial Acknowledgement'; } elseif ($aData[$i]["ComplianceStatus"]==14) { print 'Acknowledgement Rejected'; } elseif ($aData[$i]["ComplianceStatus"]==15) { print 'Net Picked Up'; } elseif ($aData[$i]["ComplianceStatus"]==15) { print 'Duplicate Document'; } elseif ($aData[$i]["ComplianceStatus"]==17) { print 'Ready to Send'; } elseif ($aData[$i]["ComplianceStatus"]==18) { print 'Send Failed'; } else { print 'Unknown Error'; } ?></div></td>
        <td width="50" bgcolor="#<?php print $bg ?>"><div class="listrow"><?php print $aData[$i]["NbrBytes"]; $myBytes = $myBytes + $aData[$i]["NbrBytes"]; ?></div></td>
        <td width="50" bgcolor="#<?php print $bg ?>"><div class="listrow"><?php print $aData[$i]["NbrRecords"]; $myRecords = $myRecords + $aData[$i]["NbrRecords"]; ?></div></td>
    </tr>
    <?php
    if (($aData[$i+1]["PartnerKEY"]!=$sLastP) || ($aData[$i]["TransactionSetID"]!=$sLastT)) {
    ?>
    
    <tr>
        <td><div class="listrow"><b><u></b></u></div></td>
        <td><div class="listrow"><center><b><u></b></u></center></div></td>
        <td><div class="listrow"><center><b><u></b></u></center></div></td>
        <td><div class="listrow"><center><b><u><?php print "Totals: ".$myCount." - Transactions"; ?></b></u></center></div></td>
        <td><div class="listrow"><center><b><u><?php print $myBytes; ?></b></u></center></div></td>
        <td><div class="listrow"><center><b><u><?php print $myRecords; ?></b></u></center></div></td>
    </tr>
    
    
    <?php
    $myBytes=0;
    $myRecords=0;
    $myCount=0;
    }

        ++$i;
    } // end loop
    ?>

</table>
<?php // renderPaging($iCursor, $iCnt) ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data available for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<br><br><input type='button' onClick='closewindow();' value='close'>
</FORM>
</body>