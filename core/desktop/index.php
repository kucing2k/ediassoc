<?php

// File Location: /core/desktop/index.php

require_once("tpl_secure.php");
require_once("class.edia.php");
require_once("class.inbox.php");

$oInbox = new inbox;


// the session class is instantiated in the tpl_secure.php file

$oTrans = new edia;

// get Trans and Trans count
$iCnt = $oTrans->getTransBoxCount($iComId);

setHeader();
openPage();
// print $iComId;
?>

<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header">Transaction Summary</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<?php renderBox($iCnt); ?> 

<?php closePage(); ?>
