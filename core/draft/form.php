<?php

// File Location: /core/draft/form.php

require_once("tpl_secure.php");
require_once("handlers.php");
require_once("class.draft.php");

$odraft = new draft;

if ($_POST) { // check for http post vars
    
    // assign post vars
    $sName = $_POST["name"];
    $sAck = $_POST["ack"];
    
   
    // if forms variables validated
    if ($FORMOK) {
        
        // assign array values
        $aData = $odraft->getHDRRefData($tid);
//        $aArgs["tp_edi_code"]=$aData["tp_edi_code"];
        $aArgs["EDIATransID"] = md5(uniqid(rand(), true));
        $aArgs["RefIBEDIATransID"] = $tid;
        $aArgs["RefTransNumber"]=$aData["TransNumber"];
        $aArgs["RefTransDate"]=$aData["TransDate"];
        $aArgs["MemberNum"] = $iComId;
        $aArgs["PartnerNumber"]=$aData["tp_id"];
        $aArgs["TransNumber"] = $sName;
        if (!strcmp("1", $sAck)) {
        $aArgs["Ack"] = 'Y';
        } else {
        $aArgs["Ack"] = 'N';
        }
        
        
        // check operation type
        if (!strcmp("add", $op)) {
            
        // try add new 990 to send
            $FORMOK = $odraft->add990($aArgs);

        }
        
        // redirect if successful
        if ($FORMOK) {
            
           header("Location: index.php");
        }
    }
}


if ($tid){
   setHeader();  //Set the HTML Header
   openPage();   //Open Menu System
   $aData = $odraft->getTranType($tid);
   $it = $aData;
   $aData = '';
   if (!strcmp("990", $it)) {
      $aData = $odraft->getHDRRefData($tid);
         $myArr = $_SESSION["aList"];
         $key = array_search($tid, $myArr);
         $prev = $myArr[$key - 1];
         $next = $myArr[$key + 1];
      include '../forms/Default_990.php';
      renderPrevNext($prev,$next);
      display990($aData);
      renderPrevNext($prev,$next);
   } elseif (!strcmp("210", $it)) {
      $aData = $odraft->getHDRRefData($tid);
         $myArr = $_SESSION["aList"];
         $key = array_search($tid, $myArr);
         $prev = $myArr[$key - 1];
         $next = $myArr[$key + 1];
      // print_r($aData);
      include '../forms/Default_210.php';
      renderPrevNext($prev,$next);
      display210($aData);
      renderPrevNext($prev,$next);
   } elseif (!strcmp("204", $it)) {
      $aData = $odraft->getHDRRefData($tid);
         $myArr = $_SESSION["aList"];
         $key = array_search($tid, $myArr);
         $prev = $myArr[$key - 1];
         $next = $myArr[$key + 1];
      // print_r($aData);
      include '../forms/Default_204.php';
      renderPrevNext($prev,$next);
      display204($aData);
      renderPrevNext($prev,$next);
   } elseif (!strcmp("850", $it)) {
      $aData = $odraft->getHDRRefData($tid);
         $myArr = $_SESSION["aList"];
         $key = array_search($tid, $myArr);
         $prev = $myArr[$key - 1];
         $next = $myArr[$key + 1];
      // print_r($aData);
      include '../forms/Default_850.php';
      renderPrevNext($prev,$next);
      display850($aData);
      renderPrevNext($prev,$next);
   } elseif (!strcmp("810", $it)) {
      $aData = $odraft->getHDRRefData($tid);
         $myArr = $_SESSION["aList"];
         $key = array_search($tid, $myArr);
         $prev = $myArr[$key - 1];
         $next = $myArr[$key + 1];
      // print_r($aData);
      include '../forms/Default_810.php';
      renderPrevNext($prev,$next);
      display810($aData);
      renderPrevNext($prev,$next);
   }

   
} else if ($rid){
   setHeader();  //Set the HTML Header
   openPage();   //Open Menu System
   if (!strcmp("990", $it)) {
      $aData = $odraft->getHDRRefData($rid);
      include '../forms/Default_990.php';
      create990($aData,$it);
   } elseif (!strcmp("210", $it)) {
      echo 'This is a 210';
      $aData = $odraft->getHDRRefData($rid);
      print_r($aData);
      include '../forms/Default_210.php';
      create210($aData,$it);
   } elseif (!strcmp("204", $it)) {
      echo 'This is a 204';
      $aData = $odraft->getHDRRefData($rid);
      print_r($aData);
      include '../forms/Default_204.php';
      create204($aData,$it);
   } elseif (!strcmp("850", $it)) {
      echo 'This is a 850';
      $aData = $odraft->getHDRRefData($rid);
      print_r($aData);
      include '../forms/Default_850.php';
      create850($aData,$it);
   } elseif (!strcmp("810", $it)) {
      echo 'This is a 810';
      $aData = $odraft->getHDRRefData($rid);
      print_r($aData);
      include '../forms/Default_810.php';
      create810($aData,$it);
   }

} else if (!strcmp("", $op) && !$tid){
   header("Location: index.php");
}

closePage(); ?>
