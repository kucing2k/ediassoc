<?php

// File Location: /core/partners/index.php

require_once("tpl_secure.php");
require_once("class.mailbox.php");

$oMailbox = new mailbox;

    // delete any topics if that was the requested action
    if ($_POST["resend"]) {
        $aResendTopic = $_POST["favorite"];
        foreach($aResendTopic as $iTopicId) {
            $tmpStr = $oMailbox->reSendTopic($iTopicId, $iComId);
        }
    }

    $sTmpString = $oMailbox->getMemberID($iComId);
    $sql = "ISA_Sender = '" . $sTmpString . "' OR ISA_Receiver = '" . $sTmpString . "'";
    
    $aTrans = $oMailbox->getTransHeaders($sql, $iCursor);

    // check for Transactions
    if (count($aTrans)) {
        
        // build page data array
        $i = 0;
        $aList = '';
        while ($i < count($aTrans)) {
    
            $aData[$i]["Usr"] = $iUserId;
            $aData[$i]["Id"] = $aTrans[$i]["Key ID"];
            $aList[$i] = $aTrans[$i]["Key ID"];
            $aData[$i]["IntStatus"] = $aTrans[$i]["IntStatus"];
            $aData[$i]["SndI"] = $aTrans[$i]["Send ID"];
            $aData[$i]["RecI"] = $aTrans[$i]["Rec ID"];
            $aData[$i]["TrnN"] = $aTrans[$i]["Trans Num"];
            $aData[$i]["LodD"] = $aTrans[$i]["Log DT"];
            ++$i;
        }
        $_SESSION["aList"] = $aList;
    }

setHeader();
openPage();
echo $tmpStr;

// form defult date (1 day before now)
$t = time();
$t -= 86400;
?>

<script type="text/javascript" language="javascript" src="/_lib/_js/datatables/jquery.js"></script>
<script type="text/javascript" language="javascript" src="/_lib/_js/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="/_lib/_js/datatables/datatables.mailbox.js"></script>
<style type="text/css">
    @import "/_lib/edia.mailbox.datatables.css";
    @import "/_lib/edia.mailbox.searchform.css";
</style>

<form name="deletethread" method="post" action="/core/mailbox/index.php">
<table border="0" cellpadding="0" cellspacing="0" width="758">
    <tbody>
    <tr>
        <td colspan="2">
            <div id="searchContainer">
                <div id="searchICNDiv">
                    Search ICN <input type="text" name="searchICN" id="searchICN"/>
                </div>
                <div id="advSearchDiv">
                    <table>
                        <tr>
                            <td class="tableLabel">
                                Start Date:
                            </td>
                            <td>
                                <input id="searchStartDate" name="searchStartDate" type="text" value="<?php echo date('Y-m-d', $t);?>"> (yyyy-mm-dd)
                            </td>
                            <td>
                                <input id="searchStartTime" name="searchStartTime" type="text" value="<?php echo date('H:i:s', $t);?>"> (hh:mm:ss)
                            </td>
                        </tr>
                        <tr>
                            <td class="tableLabel">
                                End Date:
                            </td>
                            <td>
                                <input id="searchEndDate" name="searchEndDate" type="text"> (yyyy-mm-dd)
                            </td>
                            <td>
                                <input id="searchEndTime" name="searchEndTime" type="text"> (hh:mm:ss)
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <table>
                        <tr>
                            <td class="tableLabel">
                                Direction:
                            </td>
                            <td>
                                <select name="searchDirection" id="searchDirection">
                                    <option value="Sent">Sent</option>
                                    <option value="Received">Received</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="tableLabel">
                                Partner Trading ID:
                            </td>
                            <td>
                                <input id="searchPDId" name="searchPDId" type="text">
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="advSearchButtonDiv">
                    <input type="button" value="Advanced Search" id="advSearchButton"/>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2"><div class="header">Transaction Log</div></td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="copy">
                Only transactions in the last 24 hours are shown by default.<br />
                To change this, click on "Advanced Search" and modify the "Start Date" value.
            </div>
        </td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
    <tr>
    <td align="left">
	<input type='submit' name='resend' value='resend'>
    </td>
    </tr>
</table>

<?php renderTransHeader($iCnt, $aData, $iUserId) ?>

</form>

<?php closePage(); ?>
