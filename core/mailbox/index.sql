Select doc.EDIAKeyID,
doc.EDIATransDateTime,
doc.GSSenderID,
doc.GSDocType,
doc.TransNumber,
doc.TransDate,
doc.EDIAStatus,
par.tp_name,
cfg.stat_path
from edia_edidocs doc,
edia_partners par,
edia_box_status_cfg cfg
where doc.BoxDirection='I'
and doc.GSSenderID=par.tp_edi_code
and doc.ReadFlag='N' 
and doc.MemberNum=1
and cfg.stat_id=doc.EDIAStatus;

mysql> select EDIAKeyID, EDIATransID, BoxDirection from edia_edidocs where ReadFlag='N';

update edia_partners set tp_edi_code='003005477' where tp_id=1;

Select doc.EDIAKeyID,
doc.EDIATransDateTime,
doc.GSSenderID,
doc.GSDocType,
doc.TransNumber,
doc.TransDate,
doc.EDIAStatus,
par.tp_name
FROM 
edia_edidocs doc,
edia_partners par
WHERE 
doc.BoxDirection='I'
and doc.GSSenderID=par.tp_edi_code
and doc.ReadFlag='N' 
and doc.MemberNum=3;

Select Count(doc.EDIAKeyID) as trn_Cnt
FROM 
edia_edidocs doc,
edia_partners par
WHERE
doc.BoxDirection='I'
and doc.GSSenderID=par.tp_edi_code
and doc.ReadFlag='N' 
and doc.MemberNum=3;

SELECT COUNT(EDIAKeyID) AS tran_cnt FROM edia_edidocs WHERE MemberNum=1 and EDIAStatus=1;


CREATE TABLE edia_box_config (
  stat_id int(10) NOT NULL auto_increment,
  stat_path varchar(255) NOT NULL default '',
  PRIMARY KEY  (stat_id)
);

INSERT INTO edia_box_status_cfg VALUES (1,'../../_img/icon_status1.gif');
INSERT INTO edia_box_status_cfg VALUES (2,'../../_img/icon_status2.gif');
INSERT INTO edia_box_status_cfg VALUES (3,'../../_img/icon_status3.gif');
INSERT INTO edia_box_status_cfg VALUES (4,'../../_img/icon_status3.gif');
INSERT INTO edia_box_status_cfg VALUES (5,'../../_img/icon_status3.gif');
INSERT INTO edia_box_status_cfg VALUES (6,'../../_img/icon_status3.gif');
INSERT INTO edia_box_status_cfg VALUES (7,'../../_img/icon_status3.gif');
INSERT INTO edia_box_status_cfg VALUES (8,'../../_img/icon_status3.gif');
INSERT INTO edia_box_status_cfg VALUES (9,'../../_img/icon_status3.gif');
INSERT INTO edia_box_status_cfg VALUES (10,'../../_img/icon_status3.gif');
