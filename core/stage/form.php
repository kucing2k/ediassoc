<?php

// File Location: /core/catmgr/form.php

require_once("tpl_secure.php");
require_once("handlers.php");
require_once("class.catmgr.php");

// instantiate news class
$oCats = new cat();

// check for id
if ($id) {
    
    // assign user id
    $oCats->setCatId($id);

}
    
if (!strcmp("view", $op)) {
      
    $aCat = $oCats->getCatalog($id, $iComId);
     
        $sId = $aCat["Cat Id"];
        $sNumber = $aCat["Number"];
        $sVersion = $aCat["Version"];
        $sDesc = $aCat["Desc"];
        $sStatus = $aCat["Status"];
        $sCreated = $aCat["Created Date"];
        $sModified = $aCat["Modified Date"];
         
        $aStyles = $oCats->getStyles($sId, $iComId);
}

setHeader();
openPage();
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header">Catalog Administration</div></td>
    </tr>
    <tr>
        <td><a href="index.php"> >Catalog Manager</a>&nbsp;</td>
    </tr>
    <tr>
        <td><div class="copy">To <?php print $op ?> this item in the system, please complete the form below.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<form action="<?php print SELF ?>?op=<?php print $op ?>&id=<?php print $id ?>" method="post" name="ediaform">
<table border="0" cellpadding="0" cellspacing="0">

    <tr>
        <td><div class="formlabel">Catalog Number:</div></td>
        <td><input type="text" name="cat_number" value="<?php print clean($sNumber) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Catalog Version:</div></td>
        <td><input type="text" name="cat_version" value="<?php print clean($sVersion) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Description:</div></td>
        <td><input type="text" name="cat_desc" value="<?php print clean($sDesc) ?>" class="textfield" /></td>
    </tr>
    <?php if (!strcmp("edit", $op)) { ?>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Created:</div></td>
        <td><?php print date("Y-m-d H:i:s", $sCreated) ?></td>
    </tr>
    <tr>
        <td><div class="formlabel">Modified:</div></td>
        <td><?php print date("Y-m-d H:i:s", $sModified) ?></td>
    </tr>
    <?php } ?>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
</table>
<table>
<?php  renderStyles2($iCnt, $aStyles) ?>
</table>
</form>

<?php closePage(); ?>
