<?php

// File Location: /core/catmgr/form.php

require_once("tpl_secure.php");
require_once("handlers.php");
require_once("class.catmgr.php");

// instantiate news class
$oCats = new cat();

// check for id
if ($sy) {

    $aItem["catStyleKey_id"] = $sy;

}

if ($id) {
    
    // assign item id
    $oCats->setItemId($id);
}
   
if (!strcmp("view", $op)) {
    
    $aItem = $oCats->getItem($id, $iComId);
}

setHeader();
openPage();
// print_r ($aArgs);
?>

<table border="0" cellpadding="0" cellspacing="0">
    <TR>
        <TD><div class="header">Item Information</div></TD>
    </TR>
    <TR>
        <TD><div class="error"><?php writeErrors() ?></div></TD>
    </TR>
</table>

<HR SIZE=0>
    <table cellspacing=0 cellpadding=0 border=0 width=70%>
      <TR>
        <td nowrap class=LabelCell4><strong>UPC Number </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aItem["catItemUPC"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong>Pre_Pack UPC </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aItem["catItemPreUPC"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong>ALU Number </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aItem["catItemALU"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong>NRF Color Code </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aItem["catItemNRFColor"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong>NRF Size Code </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aItem["catItemNRFSize"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong>Vendor Color Code </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aItem["catItemVenColor"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong>Vendor Color Description </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aItem["catItemVenColorDesc"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong>Vendor Size Code </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aItem["catItemVenSize"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong>Vendor Size Description </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aItem["catItemVenSizeDesc"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong>Effective Date </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aItem["catItemAffective_dt"]); ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong>Discontinue Date </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aItem["catItemDiscontinue_dt"]); ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong>Qty. on Hand </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aItem["catItemQTYOnHand"]) ?> - <?php print clean($aItem["catItemQTYOnHandQual"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong>Inner Units Per Outer Units </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aItem["catItemInnerUnitsPerPack"]) ?> - <?php print clean($aItem["catItemInnerUnitsPerPackQual"]) ?></td>
      </TR>
    </table>
<HR SIZE=0>

<table border="0" cellpadding="0" cellspacing="0">

    <?php if (!strcmp("edit", $op)) { ?>
    <TR>
        <TD><div class="formlabel">Created:</div></TD>
        <TD><?php print date("Y-m-d H:i:s", strtotime($aItem["catItemcreate_dt"])) ?></TD>
    </TR>
    <TR>
        <TD><div class="formlabel">Modified:</div></TD>
        <TD><?php print date("Y-m-d H:i:s", strtotime($aItem["catItemmodified_dt"])) ?></TD>
    </TR>
    <?php } ?>
</table>

<?php closePage(); ?>
