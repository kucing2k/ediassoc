<?php

// File Location: /core/catmgr/form.php

require_once("tpl_secure.php");
require_once("handlers.php");
require_once("class.catmgr.php");

// instantiate news class
$oCats = new cat();

// check for id
if ($id) {
    
    // assign user id
    // check operation type
    if (!strcmp($op, "del")) {
        
        // try delete style and redirect
        $oCats->setStyleId($id);
        $iCat = $oCats->setCatFormId($id);
        $oCats->deleteStyle();
        header("Location: form.php?op=edit&id=".$iCat["cId"]);
        
    } elseif (!strcmp($op, "act")) {
        
        // try activate style and redirect
        $oCats->setStyleId($id);
        $iCat = $oCats->setCatFormId($id);
        $oCats->activateStyle();
        header("Location: form.php?op=edit&id=".$iCat["cId"]);
        
    } elseif (!strcmp($op, "deact")) {
        
        // try deactivate style and redirect
        $oCats->setStyleId($id);
        $iCat = $oCats->setCatFormId($id);
        $oCats->deactivateStyle();
        header("Location: form.php?op=edit&id=".$iCat["cId"]);

    }elseif (!strcmp($op, "idel")) {
        
        // try delete style and redirect
        $oCats->setItemId($id);
        $iSty = $oCats->setStyFormId($id);
        $oCats->deleteItem();
        header("Location: styles.php?op=edit&id=".$iSty);
        
    } elseif (!strcmp($op, "iact")) {
        
        // try activate style and redirect
        $oCats->setItemId($id);
        $iSty = $oCats->setStyFormId($id);
        $oCats->activateItem();
        header("Location: styles.php?op=edit&id=".$iSty);
        
    } elseif (!strcmp($op, "ideact")) {
        
        // try deactivate style and redirect
        $oCats->setItemId($id);
        $iSty = $oCats->setStyFormId($id);
        $oCats->deactivateItem();
        header("Location: styles.php?op=edit&id=".$iSty);
    }
}


?>

