<?php

// File Location: /core/catmgr/form.php

require_once("tpl_secure.php");
require_once("handlers.php");
require_once("class.mycat.php");

// instantiate news class
$oCats = new cat();

if ($cursor) {
$iCursor = $cursor;
}
else
{
$iCursor = ROWCOUNT;
}

// check for id
if ($id) {
    
    // assign item id
    $oCats->setStyleId($id);
    $iCat = $oCats->setCatFormId($id);
}

    
    if (!strcmp("view", $op)) {
        
        $aSty = $oCats->getStyle($id, $iComId);

        $sId = $aSty["Style_Key_id"];
	$sNumber = $aSty["Style_id"];
	$sDesc = $aSty["Style_desc"];
	$sStatus = $aSty["Style_Status"];
	$sCreated = $aSty["Style_create_dt"];
	$sModified = $aSty["Style_modified_dt"];

        $iCnt = $oCats->getItemCnt($sId);

        if ($iCursor > $iCnt) {
            $iRCnt = $iCnt - ($iCursor - ROWCOUNT);
            $iRCursor = $iCnt;
        } else {
            $iRCnt = ROWCOUNT;        
            $iRCursor = $iCursor;
        }
        
        $aItems = $oCats->getItems($iRCursor, $iRCnt, $sId, $iComId);
        $sVar = "&op=view&id=".$sId;
    }



setHeader();
openPage();

?>

<table border="0" cellpadding="0" cellspacing="0">
    <TR>
        <TD><div class="header">Style Information</div></TD>
    </TR>
    <tr>
        <td><a href="index.php"> >Catalog Manager </a> > <a href="form.php?op=view&id=<?php print $iCat["cId"] ?>"><?php print $iCat["cDesc"] ?> </a> > </td>
    </tr>
    <TR>
        <TD><div class="error"><?php writeErrors() ?></div></TD>
    </TR>
</table>
<HR SIZE=0>
    <table cellspacing=0 cellpadding=0 border=0 width=70%>
      <TR>
        <td nowrap class=LabelCell4><strong>Selection Code </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print $iCat["cDesc"] ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Style ID </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["Style_id"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong>Description </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["Style_desc"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong>Manufacturer Name</strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemManufacturer"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Effective Date </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemAvailDate"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Last Changed </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print date("Y-m-d H:i:s", strtotime($aSty["Style_modified_dt"])) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Fabric </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemFabric"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Order Lead Time </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemLeadTime"]) ?> - <?php print clean($aSty["styleItemLeadTimeQual"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Lead Time Code </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemLeadTimeCode"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Min Order Qty </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemMinOrder"]) ?> - <?php print clean($aSty["styleItemMinMaxQual"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Max Order Qty</strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemMaxOrder"]) ?> - <?php print clean($aSty["styleItemMinMaxQual"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Reorder Flag </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemReOrderFlag"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Season Flag </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemSeasonFlag"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Country Of Orig </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemCountryOrig"]) ?></td>
      </TR>
    
        <td nowrap class=LabelCell4><strong> Point Of Orig </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemPointOrig"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Shipping Dimensions </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5>L x W x H : <?php print clean($aSty["styleItemShipLength"]) ?> <?php print clean($aSty["styleItemShipQualifier"]) ?>x<?php print clean($aSty["styleItemShipWidth"]) ?> <?php print clean($aSty["styleItemShipQualifier"]) ?>x<?php print clean($aSty["styleItemShipHeight"]) ?> <?php print clean($aSty["styleItemShipQualifier"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Weight </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemWeight"]) ?> <?php print clean($aSty["styleItemWeightQual"]) ?></td>
      </TR>
    
        <td nowrap class=LabelCell4><strong> Haz Mat Qualifier </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemHazMatQual"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Haz Mat Class Code </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemHazMatClass"]) ?></td>
      </TR>
      <TR>
        <td nowrap class=LabelCell4><strong> Haz Mat Description </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($aSty["styleItemHazMatDesc"]) ?></td>
      </TR>
    
        <td nowrap class=LabelCell4><strong> Total UPC Count </strong></TD>
        <td>&nbsp;</td>
        <td nowrap class=ItemDataCell5><?php print clean($iCnt) ?></td>
      </TR>
    </table>
<HR SIZE=0>

<table>
<?php renderItems2($iCnt, $aItems, $sVar) ?>
</table>

<?php closePage(); ?>
