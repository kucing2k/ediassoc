<?php

// File Location: /core/partners/form.php

require_once("tpl_secure.php");
require_once("handlers.php");
require_once("class.partners.php");

// instantiate news class
$oParts = new part();

// check for id
if ($id) {
    
    // assign user id
    $oParts->setPartId($id);
}

if ($_POST) { // check for http post vars
    
    // assign post vars
    $sName = $_POST["name"];
    $sEdic = $_POST["edicode"];
    $sAppc = $_POST["appcode"];
    $sCont = $_POST["contact"];
    $sEmai = $_POST["email"];
    $sPhon = $_POST["phone"];
    
    // validate user name
    if (!$sName) {
        
        catchErr("Enter a Company name");
        $FORMOK = false;
    }
    
    // if forms variables validated
    if ($FORMOK) {
        
        // assign array values
        $aArgs["id"] = $id;
        $aArgs["Compa"] = $iComId;
        $aArgs["CName"] = $sName;
        $aArgs["EdiCo"] = $sEdic;
        $aArgs["AppCo"] = $sAppc;
        $aArgs["Email"] = $sEmai;
        $aArgs["Conta"] = $sCont;
        $aArgs["Phone"] = $sPhon;
        
        // check operation type
        if (!strcmp("edit", $op)) {
            
            // try edit user
            $FORMOK = $oParts->editPartner($aArgs);
            
        } elseif (!strcmp("add", $op)) {
            
            // try add user
            $FORMOK = $oParts->addPartner($aArgs);
            // header("Location: index.php");
        }
        
        // redirect if successful
        if ($FORMOK) {
            
            header("Location: index.php");
        }
    }
    
} else { // post vars not sent
    
    // initialize page vars
        $sName = null;
	$sEDICode = null;
	$sAPPCode = null;
	$sContact = null;
	$sEmail = null;
	$sPhone = null;
	$sStatus = 0;
	$sImage = '';
    
    if (!strcmp("edit", $op)) {
        
        $aPart = $oParts->getPartner($id, $iComId);
        $sName = $aPart["Name"];
	$sEDICode = $aPart["EDICode"];
	$sAPPCode = $aPart["APPCode"];
	$sContact = $aPart["Contact"];
	$sEmail = $aPart["Email"];
	$sPhone = $aPart["Phone"];
	$sStatus = $aPart["Status"];
	$sImage = $aPart["Image"];

    }
   
}

$aProfiles = $oParts->getProfiles($id, $iComId);

setHeader();
openPage();

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header">Trading Partner Administration</div></td>
    </tr>
    <tr>
        <td><div class="copy">To <?php print $op ?> this item in the system, please complete the form below.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<form action="<?php print SELF ?>?op=<?php print $op ?>&id=<?php print $id ?>" method="post" name="ediaform">
<table border="0" cellpadding="0" cellspacing="0">

    <tr>
        <td><img src="<?php print $sImage; ?>" border="0" /><br></td>
    </tr>
    <tr>
        <td><div class="formlabel">Partner Name:</div></td>
        <td><input type="text" name="name" value="<?php print clean($sName) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">EDI Code:</div></td>
        <td><input type="text" name="edicode" value="<?php print clean($sEDICode) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Application Code:</div></td>
        <td><input type="text" name="appcode" value="<?php print clean($sAPPCode) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Contact:</div></td>
        <td><input type="text" name="contact" value="<?php print clean($sContact) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Email:</div></td>
        <td><input type="text" name="email" value="<?php print clean($sEmail) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Phone:</div></td>
        <td><input type="text" name="phone" value="<?php print clean($sPhone) ?>" class="textfield" /></td>
    </tr>
    <?php if (!strcmp("edit", $op)) { ?>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Created:</div></td>
        <td><?php print date("Y-m-d H:i:s", $aUser["Created Date"]) ?></td>
    </tr>
    <tr>
        <td><div class="formlabel">Modified:</div></td>
        <td><?php print date("Y-m-d H:i:s", $aUser["Modified Date"]) ?></td>
    </tr>
    <?php } ?>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
    if (count($aProfiles)) {
        $i = 0;
        while ($i < count($aProfiles)) {
    
    ?>
    <tr>
        <td><div class="formlabel"><?php if ($aProfiles[$i]["tpp_Direction"] == 0) { print "Inbound"; } else { print "Outbound";} ?></div></td>
        <td>
        
        <table border="0" cellpadding="3" cellspacing="0">
           <tr class="r">
              <td width="150">
                 <b>
                    <a href="<?php print $aProfiles[$i]["tpp_Direction"]; ?>"><?php print $aProfiles[$i]["tpp_name"]; ?></a>
                 </b>
              </td>
              <td width="14" align="center">
                 <img src="http://hp.msn.com/c/home/stock-up.gif" alt="Up" width="7" height="11" />
              </td>
              <td><?php print $aProfiles[$i]["tpp_Description"]; ?></td>
           </tr>
        </table>
        
        </td>
    </tr>
    <?php
            ++$i;
        }
    }
    ?>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td align="right" colspan="2"><input type="image" src="../../_img/buttons/btn_submit.gif" width="58" height="15" alt="" border="0" onfocus="this.blur();" /></td>
    </tr>
</table>
</form>

<?php closePage(); ?>
