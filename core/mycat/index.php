<?php

// File Location: /core/partners/index.php

require_once("tpl_secure.php");
require_once("class.mycat.php");

// instantiate news class
$oCats = new cat();

// the session class is instantiated in the tpl_secure.php file

// get catalogs
$aCat = $oCats->getCatalogs($iComId);

// check for users
if (count($aCat)) {
    
    // build page data array
    $i = 0;
    while ($i < count($aCat)) {
        $aData[$i]["Id"] = $aCat[$i]["Id"];
        $aData[$i]["Name"] = $aCat[$i]["Name"];
        $aData[$i]["Company"] = $aCat[$i]["Company"];
        $aData[$i]["Status"] = $aCat[$i]["Status"];
        $aData[$i]["Created"] =$aCat[$i]["Created Date"];
        ++$i;
    }
}

setHeader();
openPage();

?>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header">My Catalogs</div></td>
    </tr>
    <tr>
        <td colspan="2"><div class="copy">To view a catalog, select ""View" next to the catalog you wish to view.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<?php renderList2($iCnt, $aData, $iComId) ?>

<?php closePage(); ?>
