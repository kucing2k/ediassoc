<?php

// File Location: /core/catmgr/form.php

require_once("tpl_secure.php");
require_once("handlers.php");
require_once("class.mycat.php");

// instantiate news class
$oCats = new cat();

if ($cursor) {
$iCursor = $cursor;
}
else
{
$iCursor = ROWCOUNT;
}

if ($search) {
    if (!strcmp("0", $sf)) {
        $sf_temp = "AND Style_id LIKE '%".$search."%'";
    } elseif (!strcmp("1", $sf)) {
        $sf_temp = "FUCK";
    } else {
        $sf_temp = "FUCK";
    }

}
// check for id
if ($id) {
    
    // assign user id
    $oCats->setCatId($id);

    $aCat = $oCats->getCatalog($id, $iComId);
     
        $sId = $aCat["Cat Id"];
        $sNumber = $aCat["Number"];
        $sVersion = $aCat["Version"];
        $sDesc = $aCat["Desc"];
        $sStatus = $aCat["Status"];
        $sCreated = $aCat["Created Date"];
        $sModified = $aCat["Modified Date"];
        
        $iCnt = $oCats->getStyleCnt($sId, $sf_temp);
        if ($iCursor > $iCnt) {
            $iRCnt = $iCnt - ($iCursor - ROWCOUNT);
            $iRCursor = $iCnt;
        } else {
            $iRCnt = ROWCOUNT;        
            $iRCursor = $iCursor;
        }
        $aStyles = $oCats->getStyles($iRCursor, $iRCnt, $sId, $iComId, $sf_temp);
        $sVar = "&op=view&id=".$sId;

}

setHeader();
openPage();
// echo $sf_temp;
?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header">Catalog Viewer</div></td>
    </tr>
    <tr>
        <td><a href="index.php"> >Catalog Manager</a>&nbsp;</td>
    </tr>
    <tr>
        <td><div class="copy">To <?php print $op ?> this item in the system, please complete the form below.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<form action="<?php print SELF ?>?op=<?php print $op ?>&id=<?php print $id ?>" method="post" name="ediaform">
    <table border="0" cellpadding="0" cellspacing="0">
    
        <tr>
            <td><div class="formlabel">Catalog Number:</div></td>
            <td><input type="text" name="cat_number" value="<?php print clean($sNumber) ?>" class="textfield" /></td>
        </tr>
        <tr>
            <td><div class="formlabel">Catalog Version:</div></td>
            <td><input type="text" name="cat_version" value="<?php print clean($sVersion) ?>" class="textfield" /></td>
        </tr>
        <tr>
            <td><div class="formlabel">Description:</div></td>
            <td><input type="text" name="cat_desc" value="<?php print clean($sDesc) ?>" class="textfield" /></td>
        </tr>
        <tr>
            <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
        </tr>
        <tr>
            <td><div class="formlabel">Created:</div></td>
            <td><?php print date("Y-m-d H:i:s", $sCreated) ?></td>
        </tr>
        <tr>
            <td><div class="formlabel">Modified:</div></td>
            <td><?php print date("Y-m-d H:i:s", $sModified) ?></td>
        </tr>
        <tr>
            <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
        </tr>
    </table>
</form>
<form action="<?php print SELF ?>?op=<?php print $op ?>&id=<?php print $id ?>" method="post" name="ediaform">
    <table width="80%" border="1" cellspacing="0" cellpadding="10">
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                        <td width="10%"><div align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Search :</strong><img src="../../_img/clear.gif" width="10" height="10"></font></div></td>
                        <td width="35%"><input name="search" type="text" id="search" class="textfield"></td>
                        <td width="15%"><select name="sf" ID="Select1">
                            <option value="0">Style</option>
                        </td>
                        <td width="10%"><input type="submit" name="Submit" value="Search"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>

<table>
<?php  renderStyles2($iCnt, $aStyles, $sVar, $iComId) ?>
</table>

<?php closePage(); ?>
