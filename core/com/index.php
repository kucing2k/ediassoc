<?php

// File Location: /core/settings/index.php

require_once("tpl_secure.php");
require_once("handlers.php");
require_once("class.com.php");

$oCom = new com();
// the session class is instantiated in the tpl_secure.php file

if ($_POST) { // check for http post vars
   // try update settings and redirect
   $oCom->getNewFTPPassword($oSess->getMember());
   header("Location: ".SELF);
}
    
	// initialize page vars
   $aUser = $oCom->getFTPInformation($oSess->getMember());

   $sLogin = $aUser["User"];
   $sReminder = $aUser["Reminder"];


setHeader();
openPage();

?>

<form action="<?php print SELF ?>" method="post" name="ediaform">
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header" colspan="2">FTP Communication Information</div></td>
    </tr>
    <tr>
        <td><div class="copy" colspan="2">To generate a new password, click on the submit button below.</div></td>
    </tr>
    <tr>
        <td><div class="error" colspan="2"><?php writeErrors() ?></div></td>
    </tr>
    <tr>
        <td><div class="formlabel">FTP Login:</div></td>
        <td><?php print $sLogin ?></td>
    </tr>
    <tr>
        <td><div class="formlabel">Password:</div></td>
        <td><?php print $sReminder ?></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td align="right" colspan="2"><input type="image" src="../../_img/buttons/btn_submit.gif" width="58" height="15" alt="" border="0" onfocus="this.blur();" /></td>
    </tr>
</table>
</form>

<?php closePage(); ?>
