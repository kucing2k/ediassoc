<?
// Random Character Code Generator
// This program is (c) Joel Agnel.
// All the nescessary lines in this program has comments.
// Feel Free to use this code for your own purpose.
// If possible, please leave these lines intact while using this code

/*
Feel free to use this code for any purpose but I would appreciate it
if you put a link back to my site - http://www.siteskool.com on your site.
*/

/*

What does this program do?
It generates a random code that you can use if you want uniquely
identify something with the help of a unique randomly generated code.

How to use it?
Simple execute the following code in your PHP script and then
the random $id with alphabets and number will be generated,
you can set the length of the generated code by changing the value
of $numochars below. $id will contain the generated code.

If you want to use some other variable instead of $id, you can do
something like $yourvariable = $id. Insert that somewhere below this code.
To display the randomly generated code on a browser, insert : echo $id;

*/
?>


<?
/* The number of characters you want your generated code to have */
$numochars = 20;

/* The following characters will be randomly picked, you can add more characters below. */
$letter = "abcdefghijklmnopqrstuvwxyz1234567890";

echo "<PRE>";

   $ar = array
   ("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t",
    "u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P",    "Q","R","S","T","U","V","W","X","Y","Z",1,2,3,4,5,6,7,8,9,0,"~","!","@","#","$","%","&","'",
    "(",")","^","+","-","*",".","/",":",";","<","=",">","?",","," ");

   $numberofar = count($ar);
   for ($i=0 ; $i<$numberofar ; $i++) { 
   $letter = str_replace($ar[$i],$ar[$i]."-",$letter); }
   $letter = $letter . "*";
   $letter = str_replace("--*","",$letter);
   $letter = str_replace(" ","-",$letter);
   $letters = explode("--",$letter);
   $num = count($letters);
   $id = "";
mt_srand((double)microtime()*99999);
   for($i=1; $i<=$numochars; $i++) {
   $random = mt_rand(1,$num-1);
   $id = $id . $letters[$random];
   }
/******** Program ends, see how to use the code below ********/
?>

<?
/* This will output the randomly generated code */

   echo $id;
?>

