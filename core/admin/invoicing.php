<?php

// File Location: /core/users/index.php

require_once("tpl_secure.php");
require_once("class.admin.php");

// instantiate news class
$oAdmin = new admin();

// the session class is instantiated in the tpl_secure.php file

// get users and user count
// $aConfigs = $oAdmin->getConfigs();

// the session class is instantiated in the tpl_secure.php file


setHeader();
openPage();

?>

<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header">Stats</div></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td>Members Count: <?php print $oAdmin->getMemberCount() ?></td>
    </tr>
    <tr>
        <td>Total Interchanges Processed: <?php print $oAdmin->getInterchangeCount() ?></td>
    </tr>
    <tr>
        <td>Total Interchanges Processed This Year: <?php print $oAdmin->getInterchangeCount(1) ?></td>
    </tr>
    <tr>
        <td>Total Interchanges Processed This Month: <?php print $oAdmin->getInterchangeCount(2) ?></td>
    </tr>
    <tr>
        <td>Total Interchanges Processed Today: <?php print $oAdmin->getInterchangeCount(3) ?></td>
    </tr>
    <tr>
        <td>Total Groups Processed: <?php print $oAdmin->getGroupCount() ?></td>
    </tr>
    <tr>
        <td>Total Transactions Processed: <?php print $oAdmin->getTransCount() ?></td>
    </tr>
    <tr>
        <td>Invoices to Process: <?php print $oAdmin->getCustomerCnt2Invoice() ?></td>
    </tr>
    <tr>
        <td><a href="members.php" class="menu">Members Administration</a></td>
    </tr>

    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
        <td align="right" valign="top"><?php if ($iPerm > 1) { ?><a href="invoicing_run.php">Get Maang Invoices: <img src="../../_img/buttons/btn_submit.gif" width="58" height="15" alt="" border="0" /></a><?php } ?></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
        <td align="right" valign="top"><?php if ($iPerm > 1) { ?><a href="import_iif.php">Import IIF Invoices: <img src="../../_img/buttons/btn_submit.gif" width="58" height="15" alt="" border="0" /></a><?php } ?></td>
    </tr>
</table>


<?php closePage(); ?>

