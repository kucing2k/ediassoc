<?php

// File Location: /core/users/index.php

require_once("tpl_secure.php");
require_once("class.admin.php");

// instantiate news class
$oAdmin = new admin();

// the session class is instantiated in the tpl_secure.php file

// get users and user count
$aGateways = $oAdmin->getGateways();

// the session class is instantiated in the tpl_secure.php file


setHeader();
openPage();

?>

<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header">Datagate Administrator</div></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="4"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td colspan="4"><div class="header">Configure Gateways</div></td>
    </tr>
    <tr>
        <td><input type="text" name="GatewayName" value="" width="5" /></td>
        <td><input type="text" name="DirectoryPath" value="" class="textfield" /></td>
        <td>
            <select name="GatewayDirection">
                <option value=0>Inbound</option>
                <option value=1>Outbound</option>
            </select>
        </td>
    </tr>
<?php
    if (count($aGateways)) {
        $i = 0;
        while ($i < count($aGateways)) {

?>
    <tr>
        <td><?php print $aGateways[$i]["GatewayName"] ?></td>
        <td><?php print $aGateways[$i]["DirectoryPath"] ?></td>
        <td><?php !strcmp($aGateways[$i]["GatewayDirection"], "0") ? print "Inbound" : print "Outbound"; ?></td>
        <td><?php $aGateways[$i]["GatewayDesc"] ?></td>
    </tr>
<?php
            ++$i;
        }
    }
?>
    <tr>
        <td class="dotrule" colspan="4"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
</table>

<?php closePage(); ?>
