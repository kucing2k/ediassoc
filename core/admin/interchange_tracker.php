<?php

// File Location: /core/partners/index.php

require_once("tpl_secure.php");
require_once("handlers.php");
require_once("class.trans.php");

$oTrans = new trans;

// delete any topics if that was the requested action
if ($_POST["add-dates"]) {
    $sSearchSDates = $_POST["sdate"];
    $sSearchEDates = $_POST["edate"];
} else {
    $sSearchSDates = date("Y-m-d")." 00:00:01";
    $sSearchEDates = date("Y-m-d")." 23:59:59";
}

if ($_POST["icn"]) {
    $sSearchAddDates = " AND ISA_Control_Number LIKE '%" . $_POST["icn"] . "'";
} else {
    $sSearchAddDates = " AND DateStamp >= '" . $sSearchSDates . "' AND DateStamp <= '" . $sSearchEDates . "'";
}
$sSearchAddDates = "";
$aMems = $oTrans->getMembers();

// $aTrans = $oTrans->getVanDocListAll($sSearchAddDates);

// check for Transactions
// if (count($aTrans)) {
//     
//     // build page data array
//     $i = 0;
//     while ($i < count($aTrans)) {
//         $aData[$i]["van_IntControl_ID"] = $aTrans[$i]["van_IntControl_ID"];
//         $aData[$i]["van_Sender"] = $aTrans[$i]["van_Sender"];
//         $aData[$i]["van_Sender_Name"] = $aTrans[$i]["van_Sender_Name"];
//         $aData[$i]["van_Receiver"] = $aTrans[$i]["van_Receiver"];
//         $aData[$i]["van_Receiver_Name"] = $aTrans[$i]["van_Receiver_Name"];
//         $aData[$i]["van_Document_Type"] = $aTrans[$i]["van_Document_Type"];
//         ++$i;
//     }
// }


setHeader();
openPage();

// print_r($aMems);

?>

   
<SCRIPT LANGUAGE="JavaScript">

// ---------------------------------------------
// --- Name:    Easy DHTML Treeview           --
// --- Author:  D.D. de Kerf                  --
// --- Version: 0.2          Date: 13-6-2001  --
// ---------------------------------------------
function Toggle(node)
{
	// Unfold the branch if it isn't visible
	if (node.nextSibling.style.display == 'none')
	{
		// Change the image (if there is an image)
		if (node.childNodes.length > 0)
		{
			if (node.childNodes.item(0).nodeName == "IMG")
			{
				node.childNodes.item(0).src = "_img/minus.gif";
			}
		}

		node.nextSibling.style.display = 'block';
	}
	// Collapse the branch if it IS visible
	else
	{
		// Change the image (if there is an image)
		if (node.childNodes.length > 0)
		{
			if (node.childNodes.item(0).nodeName == "IMG")
			{
				node.childNodes.item(0).src = "_img/plus.gif";
			}
		}

		node.nextSibling.style.display = 'none';
	}

}
</SCRIPT>

<TABLE BORDER=0>

<?php
// loop through data and conditionally display functionality and content
$i = 0;
while ($i < count($aMems)) {
?>
   <TR><TD>
      <TABLE BORDER=0><TR><TD><?php if (count($aMems[$i]["PartnerIds"])) { echo "<A onClick=\"Toggle(this)\">"; } ?><IMG SRC="<?php if (count($aMems[$i]["PartnerIds"])) { echo "_img/plus.gif"; } else { echo "_img/leaf.gif"; } ?>"> <?php Print $aMems[$i]["Name"]." - ".count($aMems[$i]["PartnerIds"]); ?><?php if (count($aMems[$i]["PartnerIds"])) { echo "</A>"; } ?><DIV style='display:none'>
      <?php
      if (count($aMems[$i]["PartnerIds"])) { 
          $aPartners = $aMems[$i]["PartnerIds"];
          $j = 0;
          while ($j < count($aPartners)) {
      ?>  
          <TABLE BORDER=0><TR><TD WIDTH=10><?php if (count($aMems[$i]["OBTrans"])) { echo "<A onClick=\"Toggle(this)\">"; } ?></TD><TD><IMG SRC="<?php if (count($aMems[$i]["OBTrans"])) { echo "_img/plus.gif"; } else { echo "_img/leaf.gif"; } ?>"> <?php Print $aPartners[$j]["tp_edi_qual"]."/".$aPartners[$j]["tp_edi_code"]." - ".$aPartners[$j]["tp_name"]; ?><?php if (count($aMems[$i]["OBTrans"])) { echo "</A>"; } ?><DIV style='display:none'>

          </DIV></TD></TR></TABLE>
      <?php
          ++$j;
          } // end loop
      }
      ?>
      </DIV></TD></TR></TABLE>
   </TD></TR>
<?php
    ++$i;
} // end loop
?>
</TABLE>




    
                <form name="inboundthread" method="post" action="<?php if (!strcmp($op, "new")) { echo SELF."?op=new"; } else { echo SELF; } ?>">
                
                <?php // renderTransHeaderInbound($iCnt, $aIData, $iUserId) ?>
                
                </form>
        <table class=tableOnMouseOver cellSpacing=1 cellPadding=1 width="100%" border=0>
          <tbody>
            <tr> 
              <td class=th2 align=middle>
              	Line&nbsp;
              </td>
              <td class=th2 align=middle>
              	<a title="Sort by ICN" href="/css/documentSortDispatch.do?method=ICN" class="MessageLink">ICN</a>&nbsp;
              </td>
              <td class=th2 align=middle>
                <a title="Sort by Document Type" href="/css/documentSortDispatch.do?method=DocType" class="MessageLink">Doc&nbsp;<br>Type</a>&nbsp;
              </td>
              <td class=th2 align=middle>
                <a title="Sort by Sender" href="/css/documentSortDispatch.do?method=DocSender" class="MessageLink">Sender</a>&nbsp;
              </td>
              <td class=th2 align=middle>
                <a title="Sort by Receiver" href="/css/documentSortDispatch.do?method=DocReceiver" class="MessageLink">Receiver</a>&nbsp;
              </td>
            </tr>
<?php
// loop through data and conditionally display functionality and content
$i = 0;
while ($i < count($aData)) {
    !strcmp("FFFFFF", $bg) ? $bg = "efefef" : $bg = "FFFFFF";
?>
        <tr bgcolor="#<?php Print $bg; ?>" onmouseover="color=this.bgColor;this.bgColor='D6E3EF';" onmouseout="this.bgColor=color;">
          <td class=Text_Small noWrap align=center><?php Print $i; ?></td>
          <td class=Text_Small align=center><a href="/css/documentLinkDispatch.do?method=viewDocumentDetail&pageSequence=current&index=1" class="MessageLink"/><u><?php Print $aData[$i]["van_IntControl_ID"]; ?></u></td>
          <td class=Text_Small noWrap align=center><?php Print $aData[$i]["van_Document_Type"]; ?></td>
          <td class=Text_Small noWrap align=center><?php Print $aData[$i]["van_Sender_Name"]; ?>&nbsp;<br>(<?php Print $aData[$i]["van_Sender"]; ?>)</td>
          <td class=Text_Small noWrap align=center><?php Print $aData[$i]["van_Receiver_Name"]; ?>&nbsp;<br>(<?php Print $aData[$i]["van_Receiver"]; ?>)</td>
        </tr>
<?php
    ++$i;
} // end loop
?>
          </tbody>
        </table>



   
<?php closePage(); ?>
