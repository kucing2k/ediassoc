<?php

// File Location: /core/wsftp/index.php

require_once("tpl_secure.php");
require_once("class.manage_wsftp.php");
require_once("ftp_elements.php");


$oWsftp = new wsftp();
// check for id
$id = $_GET['id'];
if ($id) {
    
    // check operation type
    if (!strcmp($op, "act")) {
        
        // try activate user and redirect
        $oWsftp->FTPactivateUser($id);
        header("Location: ".SELF);
        
    } elseif (!strcmp($op, "deact")) {
        
        // try deactivate user and redirect
        $oWsftp->FTPdeactivateUser($id);
        header("Location: ".SELF);

    } elseif (!strcmp($op, "update")) {
        
        // try deactivate user and redirect
        $tmpStr = "lh_options_".$id;
        $aDataT["LockHome"] = $_POST[$tmpStr];
        $tmpStr = "dp_options_".$id;
        $aDataT["DisablePub"] = $_POST[$tmpStr];
        $tmpStr = "dc_options_".$id;
        $aDataT["DisablePWChg"] = $_POST[$tmpStr];
        $tmpStr = "FTPMAXSPACE_".$id;
        $aDataT["FTPMAXSPACE"] = $_POST[$tmpStr];
        $tmpStr = "FTPMAXFILES_".$id;
        $aDataT["FTPMAXFILES"] = $_POST[$tmpStr];

        $oWsftp->FTPUpdateUser($id, $aDataT);
        header("Location: ".SELF);
    }
}

if (!strcmp($op, "add")) {

    $aDataT["USERID"] = $_POST["Name"];
    
    // try activate user and redirect
    $oWsftp->FTPaddUser($aDataT);
    header("Location: ".SELF);
    
}

// get users and user count
$aFTPUsers = $oWsftp->FTPgetUsers();
      

// build page data array
$i = 0;
while ($i < count($aFTPUsers)) {
    $aData[$i]["Id"] = $aFTPUsers[$i]["User Id"];
    $aData[$i]["Name"] = $aFTPUsers[$i]["User Name"];
    $aData[$i]["FullName"] = $aFTPUsers[$i]["Full Name"];
    $aData[$i]["Password"] = $aFTPUsers[$i]["Password"];
    $aData[$i]["Status"] = $aFTPUsers[$i]["Status"];
    $aData[$i]["SysAdmin"] = $aFTPUsers[$i]["SysAdmin"];
    $aData[$i]["HostAdmin"] = $aFTPUsers[$i]["HostAdmin"];
    $aData[$i]["LockHome"] = $aFTPUsers[$i]["LockHome"];
    $aData[$i]["DisablePub"] = $aFTPUsers[$i]["DisablePub"];
    $aData[$i]["DisablePWChg"] = $aFTPUsers[$i]["DisablePWChg"];
    $aData[$i]["FTPMAXSPACE"] = $aFTPUsers[$i]["FTPMAXSPACE"];
    $aData[$i]["FTPMAXFILES"] = $aFTPUsers[$i]["FTPMAXFILES"];
    $aData[$i]["Created"] = $aFTPUsers[$i]["Created Date"];
    ++$i;
}



setHeader();
openPage();

// print($tmpStr);
// print_r($aDataT);

// print_r($aFTPUsers);



?>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header">FTP User Administration</div></td>
    </tr>
    <tr>
        <td colspan="2"><div class="copy">To manage users, select a user action from the list below.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<form action="<?php print SELF ?>?op=add" method="post" name="ediaform">
    <table class="T" width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td width="10%" class="header"><div class="listrow"><strong>Login</strong></div></td>
            <td class="header"><div class="listrow"><strong></strong></div></td>
        </tr>
        <tr>
            <td><input type="text" name="Name" value="" /></div></td>
            <td><input type="image" src="../../_img/buttons/btn_additem.gif" width="58" height="15" alt="" border="0" onfocus="this.blur();" /></td>
        </tr>
    </table>
</form>


<?php renderFTPUserList($aData) ?>

<?php closePage(); ?>
