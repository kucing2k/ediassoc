<?php

// File Location: /core/users/index.php

require_once("tpl_secure.php");
require_once("class.admin.php");

// instantiate news class
$oAdmin = new admin();

// the session class is instantiated in the tpl_secure.php file

// get users and user count
// $aConfigs = $oAdmin->getConfigs();

// the session class is instantiated in the tpl_secure.php file


setHeader();
openPage();

?>

<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header">Configuration Links</div></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td><a href="members.php" class="menu">Members Administration</a></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td><a href="datagate.php" class="menu">DataGate Config</a></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td><a href="wsftp.php" class="menu">FTP Users Config</a></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td><a href="invoicing.php" class="menu">Invoicing</a></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>


</table>

<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>Members Count: <?php print $oAdmin->getMemberCount() ?></td>
    </tr>
</table>


<?php closePage(); ?>
