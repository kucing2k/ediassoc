<?php

// File Location: /core/members/index.php

/** 
 *
 * @author Stephen Case <stephen@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access private
 * @copyright EDI Associates, Inc.
 *
 */

require_once("tpl_secure.php");
require_once("class.transRptFeeds.php");

// instantiate news class
$oTrans = new trans();


// YOUR E-MAIL ACCOUNT LOGIN CREDENTIALS
$buildecho="";
$user="ediir"; # bob
$alias="inovusreports";
$domain="ediassociates"; # your domain - like microsoft  or aol  or microsoft... dont put mail. or .com
$suff=".com"; # .com .net, etc...
$pw="raj6welt"; # Password...
$method="pop3"; # default for e-mail is POP3
# TO SET PORT, SEE imap_open, replace 110 with your port, below...

// REMOVE MESSAGE FROM SERVER AFTER YOU DOWNLOAD IT?
$Delete_From_Server=true; # Set to false if you want to leave the messages on the server

// YOUR E-MAIL DOWNLOAD BACKUP SETTINGS
$enablebackup=true; # recommended (true)
$backup_root="C:/phpop3/"; # your root Email Saving Directory...
$backup_sub=$backup_root . "$alias@$domain$suff/";
$backup_box=$backup_sub . "Inbox/";
$backup_arc=$backup_box . "archive/";

// Create directories if the email address does not exist...
// IF NOT EXISTS... Don't remove this. It helps if you add new accounts...
mkdir($backup_root);
mkdir($backup_sub);
mkdir($backup_box);
mkdir($backup_arc);


    //check for new messages 
    $buildecho .= Status("$domain -> <b>Connecting to server...</b><br>");
    $mailbox = imap_open ("{pop.easystreet.com:110/$method}INBOX", "$user", "$pw");
    $buildecho .= Status("$domain -> <b>Checking if connected...</b><br><br>");

    // Check messages 
    $check = imap_check($mailbox); 
    $messagecount=$check->Nmsgs;
    
    $buildecho .= Status("$domain -> <b>Counted " . $messagecount . " messages on server...</b><br>");
    
    for ($x=1; $x < ($messagecount+1); $x++) {
    
        $buildecho .= Status("<br><b>$domain $x </b>-> Downloading e-mail<br>");
        $header = imap_header($mailbox, $x); 
        $from=str_replace(">","",str_replace("<","",$header->fromaddress));
        $subject=str_replace("'","",$header->Subject);
        $datesent=str_replace("'","",$header->Date);
        $body=str_replace("'","",imap_fetchbody($mailbox,$x,'1'));
        // $body=str_replace("'","",imap_body($mailbox,$x)); 
        $msgnumber=trim($header->Msgno);
        $buildecho .= Status("$domain $msgnumber -> e-mail downloaded<br>");

        if (strlen($body)<=4) {
            # Message is too short. Possibly some sort of error?
            # Leave this message on the server
        } ELSE {
            # SAVES THE EMAIL TO MYSQL
            # IGNORE IS USED FOR ANY KIND OF ERROR. LETS JUST GET THE THING IN THERE!!
            $buildecho .= Status("$domain $msgnumber -> Saving to Database<br>");

            if ($enablebackup==true) {
                # Save a backup of this message on the server
                $fsize=1; # set higher than zero to initiate loop
                
                while ($fsize>0) {
                    # choose the appropriate file to save this as...
                    $fx=$fx+1;
                    $backupfile="C:/phpop3/$alias@$domain$suff/inbox/" . $fx . ".txt";
                    $fsize=filesize($backupfile);
                    if ($fsize<=0) { break; }
                } # while
                
                
                $fp=fopen($backupfile,"wb");
                $fw=fwrite($fp,$body);
                fclose($fp);
                $confirm=filesize($backupfile);
                
                if ($confirm<=0) {
                    $buildecho .= Status("$domain $msgnumber -> Backup failed to save ``$backupfile``<br>");
                } ELSE {
                    $delete[]=$x; # adds this index to the OK TO DELETE. Apparently a backup file + MySQL are done
                    $buildecho .= Status("$domain $msgnumber -> Backup saved to disk<br>");
                }
            } ELSE {
                # BACKUP DISABLED. Automatically enable file to be deleted...
                
                $delete[]=$msgnumber; # says its ok to delete after all is said and done.. depending on your settings
                
            } # check if enabledbackup = true...
         
        } # check eroneous body length...
 
    } # for x (checks all messages in your inbox)

if ($Delete_From_Server==true) {

// YOU REQUESTED THESE MESSAGES TO BE DELETED AFTER THEY ARE DOWNLOADED

    if (count($delete)>0) {
    
        # Delete the messages from the server!
        $buildecho .= Status("<br><b>$domain -> Deleting " . count($delete) . " messages from server...</b><br>");
        
        $delcount=0;
        for ($x=0; $x < count($delete); $x++) {
            $del=$delete[$x];
            imap_delete($mailbox,$del);
            $delcount=$delcount+1;
        } # for (delete messages)
        
        # Removes deleted items from your inbox...
        imap_expunge($mailbox);
        
        $buildecho .= Status("<b>$domain -> $delcount messages were deleted...<br></b>");
    
    } ELSE {
        $buildecho .= Status("<br>Finished. No E-Mails were deleted...<br>");
    }
} ELSE {
    $buildecho .= Status("<br>Finished. Your messages are still on the server though (none were deleted).<br>");
} # $Delete_From_Server


# Close the mailbox... (Nothing left to do right)...
imap_close($mailbox); 

$dh = opendir($backup_box) or die("couldn't open directory");

    while (!(($file = readdir($dh)) === false )) {
        if (is_file("$backup_box/$file")) {
            $filename = "$backup_box$file";
            $buildecho .= Status("$filename<br>");
            $fp = fopen($filename, "r") or die("couldn't open $filename");
            
            while (!feof($fp)) {
                $line = fgets($fp, 1024);
                $line = str_replace("\r\n", "", $line);
                $len = strlen ($line);
                if ($len >= 5) {
                    $myline_array = explode(",", $line);
                    if ($myline_array[16]) { 
                        $i++;
                        $oTrans->addTransactions($myline_array); 
                    }
                }
            }
            fclose($fp);
            $mf = newtempnam($backup_arc, "REC", ".CSV");
            if (!copy($backup_box.$file, $mf)) {
                echo "failed to copy $file...\n";
            } else {
                unlink($backup_box.$file);
            }

        }
    }
    closedir($dh);
    $buildecho .= Status("$i - Transactions Processed<br>");

function Status($Statusmsg) {
    $buildecho.='<span class="Status">' . $Statusmsg . '</span>';
    return $buildecho;
} # Echos back the $buildecho .= Status, formatted...

function newtempnam($dir, $prefix, $postfix){
   /* Creates a new non-existant file with the specified post and pre fixes */
   
   if ($dir[strlen($dir) - 1] == '/') {
       $trailing_slash = "";
   } else {
       $trailing_slash = "/";
   }
   /*The PHP function is_dir returns true on files that have no extension.
   The filetype function will tell you correctly what the file is */
   if (!is_dir(realpath($dir)) || filetype(realpath($dir)) != "dir") {
       // The specified dir is not actualy a dir
       return false;
   }
   if (!is_writable($dir)){
       // The directory will not let us create a file there
       return false;
   }
   
   do{    $seed = substr(md5(microtime()), 0, 8);
       $filename = $dir . $trailing_slash . $prefix . $seed . $postfix;
   } while (file_exists($filename));
   $fp = fopen($filename, "w");
   fclose($fp);
   return $filename;
}





// the session class is instantiated in the tpl_secure.php file

// check for id
if ($id) {
    
    // assign user id
    $oMems->setMemId($id);
    
    // check operation type
    if (!strcmp($op, "del")) {
        
        // try delete user and redirect
        $oMems->deleteMember();
        header("Location: ".SELF);
        
    } elseif (!strcmp($op, "act")) {
        
        // try activate user and redirect
        $oMems->activateMember();
        header("Location: ".SELF);
        
    } elseif (!strcmp($op, "deact")) {
        
        // try deactivate user and redirect
        $oMems->deactivateMember();
        header("Location: ".SELF);
    }
}

setHeader();
openPage();
?>

<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header">Trading Partner Administration</div></td>
    </tr>
    <tr>
        <td colspan="2"><div class="copy">To manage trading partners, select a trading partner action from the list below.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
        <td align="right" valign="top"><?php if ($iPerm > 1) { ?><a href="form.php?op=add"><img src="../../_img/buttons/btn_additem.gif" width="58" height="15" alt="" border="0" /></a><?php } ?></td>
    </tr>
</table>

<?php echo $buildecho; ?>

<?php closePage(); ?>
