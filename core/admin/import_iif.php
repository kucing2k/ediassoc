<?php

// File Location: /core/users/index.php

require_once("tpl_secure.php");
require_once("class.admin.php");

// instantiate news class
$oAdmin = new admin();

$message = null;
$allowed_extensions = array('iif');
$upload_path = '/home/casest/www/uploads';

if (!empty($_FILES['file'])) {
	if ($_FILES['file']['error'] == 0) {
		// check extension
		$file = explode(".", $_FILES['file']['name']);
		$extension = array_pop($file);
		if (in_array($extension, $allowed_extensions)) {
			if (move_uploaded_file($_FILES['file']['tmp_name'], $upload_path.'/'.$_FILES['file']['name'])) {
				if (($handle = fopen($upload_path.'/'.$_FILES['file']['name'], "r")) !== false) {
					$keys = array();
					$out = array();
					$insert = array();
					$line = 1;
					while (($row = fgetcsv($handle, 0, '	', '"')) !== FALSE) {
						
             $num = count($row);
             $line++;
             if ($row[0]=='!TRNS') {
                for ($c=0; $c < $num; $c++) {
                   if ($row[$c]=='NAME') {
                   	  $name = $c;
                   }
                   if ($row[$c]=='MEMO') {
                   	  $memo = $c;
                   }
                   if ($row[$c]=='AMOUNT') {
                   	  $amount = $c;
                   }
                   if ($row[$c]=='DATE') {
                   	  $postdate = $c;
                   }
                }
             }
             if ($row[0]=='!SPL') {
                for ($c=0; $c < $num; $c++) {
                   if ($row[$c]=='QNTY') {
                   	  $qnty = $c;
                   }
                   if ($row[$c]=='AMOUNT') {
                   	  $lineamount = $c;
                   }
                   if ($row[$c]=='MEMO') {
                   	  $desc = $c;
                   }
                }
             }
             
             if ($row[0]=='TRNS') {
                $nextInvoice = $oAdmin->getPhreeBooksNextInvoice();
                $pbCurrentPeriod = $oAdmin->getPhreeBooksAccountingPeriod();
                $pbCurrentFiscalYear = $oAdmin->getPhreeBooksAccountingFiscalYear($pbCurrentPeriod);
                $pbCurrentMaxPeriod = $oAdmin->getPhreeBooksAccountingMaxPeriod($pbCurrentFiscalYear);

                for ($c=0; $c < $num; $c++) {
                   if ($c==$name) {
                      $pbRefId = $oAdmin->getPhreeBooksReferenceNumber($row[$c]);
                      $pbAddress = $oAdmin->getPhreeBooksBillingAddress($pbRefId);
                   }
                   if ($c==$memo) {
                      $periodText = $row[$c];
                   }
                   if ($c==$postdate) {
                      extract(strptime($row[$c],'%m/%d/%Y'));
                      $tmpCurDate = strftime('%Y-%m-%d',mktime( intval($tm_hour), intval($tm_min), intval($tm_sec), intval($tm_mon)+1, intval($tm_mday), intval($tm_year)+1900 ));                      
                   }
                   if ($c==$amount) {
                      $mytotalInvoice = $row[$c];
                   }
                }
                $sql = "insert into PhreeBooksR21.pb_journal_main (period, journal_id, post_date, store_id, description, closed, freight, discount, shipper_code, terms, sales_tax, total_amount, currencies_code, currencies_value, so_po_ref_id, purchase_invoice_id, purch_order_id, admin_id, rep_id, waiting, gl_acct_id, bill_acct_id, bill_address_id, bill_primary_name, bill_contact, bill_address1, bill_address2, bill_city_town, bill_state_province, bill_postal_code, bill_country_code, bill_telephone1, bill_email, ship_acct_id, ship_address_id, ship_primary_name, ship_contact, ship_address1, ship_address2, ship_city_town, ship_state_province, ship_postal_code, ship_country_code, ship_telephone1, ship_email, terminal_date, drop_ship, recur_id) values ('".$pbCurrentPeriod."', '12', '".$tmpCurDate."', '0', 'Sales/Invoice Entry', '0', '0', '0', ':', '0::::2500.00', '0', '".$mytotalInvoice."', 'USD', '1', '0', '".$nextInvoice."', '', '1', '1', '0', '1200', '".$pbAddress["address_id"]."', '".$pbAddress["address_id"]."', '".$pbAddress["primary_name"]."', '".$pbAddress["contact"]."', '".$pbAddress["address1"]."', '".$pbAddress["address2"]."', '".$pbAddress["city_town"]."', '".$pbAddress["state_province"]."', '".$pbAddress["postal_code"]."', '".$pbAddress["country_code"]."', '".$pbAddress["telephone1"]."', '".$pbAddress["email"]."', '".$pbAddress["address_id"]."', '".$pbAddress["address_id"]."', '".$pbAddress["primary_name"]."', '".$pbAddress["contact"]."', '".$pbAddress["address1"]."', '".$pbAddress["address2"]."', '".$pbAddress["city_town"]."', '".$pbAddress["state_province"]."', '".$pbAddress["postal_code"]."', '".$pbAddress["country_code"]."', '".$pbAddress["telephone1"]."', '".$pbAddress["email"]."', '".$tmpCurDate."', '0', '0')";
                $oAdmin->_oConn->query($sql);
                $phCurrentInvoiceJournalId = $oAdmin->getPhreeBooksLastJournalId($nextInvoice);
             }
             if ($row[0]=='SPL') {
                for ($c=0; $c < $num; $c++) {
                   if ($c==$qnty) {
                      $CntRec = str_replace("-", "", $row[$c]);
                   }
                   if ($c==$lineamount) {
                      $totalCostRec = str_replace("-", "", $row[$c]);
                   }
                   if ($c==$desc) {
                      $description = $row[$c];
                   }
                }
                
                $sql = "insert into PhreeBooksR21.pb_journal_item (id, so_po_item_ref_id, gl_type, sku, qty, description, credit_amount, full_price, gl_account, taxable, serialize_number, project_id, post_date, date_1, ref_id) values ('', '', 'sos', '', '".$CntRec."', '".$description."', '".$totalCostRec."', '0', '4010', '0', '', '', '".$tmpCurDate."', '".$tmpCurDate."', '".$phCurrentInvoiceJournalId."')";
                $oAdmin->_oConn->query($sql);

             }
             if ($row[0]=='ENDTRNS') {
                $sql = "insert into PhreeBooksR21.pb_journal_item (id, so_po_item_ref_id, gl_type, sku, qty, description, credit_amount, full_price, gl_account, taxable, serialize_number, project_id, post_date, date_1, ref_id) values ('', '', 'sos', '', '1', '".$periodText."', '0', '0', '4010', '0', '', '', '".$tmpCurDate."', '".$tmpCurDate."', '".$phCurrentInvoiceJournalId."')";
                $oAdmin->_oConn->query($sql);
                $sql = "insert into PhreeBooksR21.pb_journal_item (gl_type, debit_amount, description, gl_account, post_date, ref_id) values ('ttl', '".$mytotalInvoice."', 'Sales/Invoice Total', '1200', '".$tmpCurDate."', '".$phCurrentInvoiceJournalId."')";
                $oAdmin->_oConn->query($sql);
                $sql = "update PhreeBooksR21.pb_chart_of_accounts_history set credit_amount = credit_amount + ".$mytotalInvoice.", debit_amount = debit_amount + 0, last_update = '".$tmpCurDate."' where account_id = '4010' and period = ".$pbCurrentPeriod;
                $oAdmin->_oConn->query($sql);
                $sql = "update PhreeBooksR21.pb_chart_of_accounts_history set credit_amount = credit_amount + 0, debit_amount = debit_amount + ".$mytotalInvoice.", last_update = '".$tmpCurDate."' where account_id = '1200' and period = ".$pbCurrentPeriod;
                $oAdmin->_oConn->query($sql);
                $sql = "insert into PhreeBooksR21.pb_accounts_history (ref_id, so_po_ref_id, acct_id, journal_id, purchase_invoice_id, amount, post_date) values ('".$phCurrentInvoiceJournalId."', '0', '".$pbAddress["address_id"]."', '12', '".$nextInvoice."', '".$mytotalInvoice."', '".$tmpCurDate."')";
                $oAdmin->_oConn->query($sql);

                $tmpInt = $pbCurrentPeriod;
                $tmpInt2 = $pbCurrentPeriod;
                $tmpInt++;
                for ($i = $tmpInt; $i <= $pbCurrentMaxPeriod; $i++) {
		                // Get From Account Balance
                    $fromAccount = 0;
                    $toAccount = 0;
		                
		                $sql = "select beginning_balance + debit_amount - credit_amount as beginning_balance from PhreeBooksR21.pb_chart_of_accounts_history where account_id = '4010' and period = ".$tmpInt2;
                    $oAdmin->_oConn->getOne($sql);
                    $fromAccount = $sTmp;
                    // Get To Account Balance
		                $sql = "select beginning_balance + debit_amount - credit_amount as beginning_balance from PhreeBooksR21.pb_chart_of_accounts_history where account_id = '1200' and period = ".$tmpInt2;
                    $oAdmin->_oConn->getOne($sql);
                    $toAccount = $sTmp;
                    $tmpInt2++;

		                $sql = "update PhreeBooksR21.pb_chart_of_accounts_history set beginning_balance = ".$fromAccount." where period = ".$i." and account_id = '4010'";
                    $oAdmin->_oConn->query($sql);
		                $sql = "update PhreeBooksR21.pb_chart_of_accounts_history set beginning_balance = ".$toAccount." where period = ".$i." and account_id = '1200'";
                    $oAdmin->_oConn->query($sql);
                }
                
                $id = (int)$nextInvoice;
                $id++;
                $sql = "update PhreeBooksR21.pb_current_status set next_inv_num = '".$id."' where next_inv_num = '".$nextInvoice."'";
                $oAdmin->_oConn->query($sql);
                                 
             } 
             
				  }
				 	$message = '<span class="green">File has been uploaded successfully ' . $line . '</span>';
        }
			}
			
		} else {
			$message = '<span class="red">Only .iif file format is allowed</span>';
		}
		
	} else {
		$message = '<span class="red">There was a problem with your file</span>';
	}
	
}		


setHeader();
openPage();


?>
<style type="text/css">
#wrapper {
	width:400px;
	margin:0 auto;
	text-align:left;
}
.green {
	color:green;
}
.red {
	color:red;
}
</style>

<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2">
            <section id="wrapper">	
            	<form action="" method="post" enctype="multipart/form-data">
            		<table cellpadding="0" cellspacing="0" border="0" class="table">
            			<tr>
            				<th><label for="file">Select IIF file</label> <?php echo $message; ?></th>
            			</tr>
            			<tr>
            				<td><input type="file" name="file" id="file" size="30" /></td>
            			</tr>
            			<tr>
            				<td><input type="submit" id="btn" class="fl_l" value="Submit" /></td>
            			</tr>
            		</table>
            	</form>
            </section>
        </td>
    </tr>
</table>


<?php closePage(); ?>

