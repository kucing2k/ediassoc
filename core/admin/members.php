<?php

// File Location: /core/members/index.php

/** 
 *
 * @author Stephen Case <stephen@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access private
 * @copyright EDI Associates, Inc.
 *
 */

require_once("tpl_secure.php");
require_once("class.members.php");

// instantiate news class
$oMems = new mem();

// the session class is instantiated in the tpl_secure.php file

// get users and user count
$aMems = $oMems->getMembers();

// check for users
if (count($aMems)) {
    
    // build page data array
    $i = 0;
    while ($i < count($aMems)) {
        $aData[$i]["Id"] = $aMems[$i]["Id"];
        $aData[$i]["Name"] = $aMems[$i]["Name"];
        $aData[$i]["Status"] = $aMems[$i]["Status"];
        $aData[$i]["Created"] =$aMems[$i]["Created Date"];
        ++$i;
    }
}

// check for id
if ($id) {
    
    // assign user id
    $oMems->setMemId($id);
    
    // check operation type
    if (!strcmp($op, "del")) {
        
        // try delete user and redirect
        $oMems->deleteMember();
        header("Location: ".SELF);
        
    } elseif (!strcmp($op, "act")) {
        
        // try activate user and redirect
        $oMems->activateMember();
        header("Location: ".SELF);
        
    } elseif (!strcmp($op, "deact")) {
        
        // try deactivate user and redirect
        $oMems->deactivateMember();
        header("Location: ".SELF);
    }
}
setHeader();
openPage();
?>

<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td colspan="2"><div class="header">Trading Partner Administration</div></td>
    </tr>
    <tr>
        <td colspan="2"><div class="copy">To manage trading partners, select a trading partner action from the list below.</div></td>
    </tr>
    <tr>
        <td colspan="2"><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<?php renderMembers($aData) ?>

<?php closePage(); ?>
