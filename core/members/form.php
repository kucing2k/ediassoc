<?php

// File Location: /core/partners/form.php

require_once("tpl_secure.php");
require_once("handlers.php");
require_once("class.members.php");


// instantiate news class
$oMembers = new mem();

// check for id
if ($id) {
    
    // assign user id
    $oMembers->setMemId($id);

}

if ($_POST) { // check for http post vars
    
    // assign post vars
    $sName = $_POST["name"];
    $sEdic = $_POST["loginid"];
    $smember_gateway = $_POST["gateways_options"];
    $sbilling_address_1 = $_POST["billing_address_1"];
    $sbilling_address_2 = $_POST["billing_address_2"];
    $sbilling_address_3 = $_POST["billing_address_3"];
    $sbilling_address_4 = $_POST["billing_address_4"];
    $sbilling_address_city = $_POST["billing_address_city"];
    $sbilling_address_state = $_POST["billing_address_state"];
    $sbilling_address_zip = $_POST["billing_address_zip"];
    $sbilling_address_country = $_POST["billing_address_country"];
    $sbilling_address_contactName = $_POST["billing_address_contactName"];
    $sbilling_address_telephone = $_POST["billing_address_telephone"];
    $sbilling_amount = $_POST["billing_amount"];
    !strcmp("", $sbilling_amount) ? $sbilling_amount = 0 : $sbilling_amount = $sbilling_amount;
    $slast_bill_dt = $_POST["last_bill_dt"];
    $sbilling_email = $_POST["billing_email"];
    $aPerms = $_POST["perms"];
    // validate user name
    if (!$sName) {
        
        catchErr("Enter a Company name");
        $FORMOK = false;
    }
    
    // if forms variables validated
    if ($FORMOK) {
        
        // assign array values
        $aArgs["id"] = $id;
        $aArgs["CName"] = $sName;
        $aArgs["EdiCo"] = $sEdic;
        $aArgs["member_gateway"] = $smember_gateway;
        $aArgs["billing_address_1"] = $sbilling_address_1;
        $aArgs["billing_address_2"] = $sbilling_address_2;
        $aArgs["billing_address_3"] = $sbilling_address_3;
        $aArgs["billing_address_4"] = $sbilling_address_4;
        $aArgs["billing_address_city"] = $sbilling_address_city;
        $aArgs["billing_address_state"] = $sbilling_address_state;
        $aArgs["billing_address_zip"] = $sbilling_address_zip;
        $aArgs["billing_address_country"] = $sbilling_address_country;
        $aArgs["billing_address_contactName"] = $sbilling_address_contactName;
        $aArgs["billing_address_telephone"] = $sbilling_address_telephone;
        $aArgs["billing_email"] = $sbilling_email;
        $aArgs["billing_amount"] = $sbilling_amount;
        $aArgs["last_bill_dt"] = $slast_bill_dt;

        $aArgs["Perms"] = $aPerms;
        // check operation type
        if (!strcmp("edit", $op)) {

            if ($_POST["qual"]) {
                // try to add and alias qual and id
                $FORMOK = $oMembers->addMemberAlias($id, $_POST["qual"], $_POST["isaid"]);
            } else {
                // try to edit member
                $FORMOK = $oMembers->editMember($aArgs);
            }
            
        } elseif (!strcmp("add", $op)) {
            
            // try add member
            $FORMOK = $oMembers->addMember($aArgs);
       
        }
        
        // redirect if successful
        if ($FORMOK) {
           
            header("Location: index.php");
        }
    }
} else { // post vars not sent
    
    // initialize page vars
        $sName = null;
        $sLoginID = null;
        $smember_gateway = 0;
        $sbilling_address_1 = null;
        $sbilling_address_2 = null;
        $sbilling_address_3 = null;
        $sbilling_address_4 = null;
        $sbilling_address_city = null;
        $sbilling_address_state = null;
        $sbilling_address_zip = null;
        $sbilling_address_country = null;
        $sbilling_address_contactName = null;
        $sbilling_address_telephone = null;
        $sbilling_email = null;
        $sbilling_amount = 0;
        $slast_bill_dt = null;

        //$aPerms = $oMembers->getPermissions($id);
        $aApps = $oMembers->getApplications();
    
    if (!strcmp("edit", $op)) {
        
        // $aWow = $oMembers->getNewMemberID();
        $aMem = $oMembers->getMember($id);
        $sName = $aMem["Name"];
	      $sLoginID = $aMem["LoginID"];
        $smember_gateway = $aMem["member_gateway"];
        $sbilling_address_1 = $aMem["billing_address_1"];
        $sbilling_address_2 = $aMem["billing_address_2"];
        $sbilling_address_3 = $aMem["billing_address_3"];
        $sbilling_address_4 = $aMem["billing_address_4"];
        $sbilling_address_city = $aMem["billing_address_city"];
        $sbilling_address_state = $aMem["billing_address_state"];
        $sbilling_address_zip = $aMem["billing_address_zip"];
        $sbilling_address_country = $aMem["billing_address_country"];
        $sbilling_address_contactName = $aMem["billing_address_contactName"];
        $sbilling_address_telephone = $aMem["billing_address_telephone"];
        $sbilling_amount = $aMem["billing_amount"];
        $slast_bill_dt = $aMem["last_bill_dt"];
	
        $aPerms = $oMembers->getPermissions($id);
        $aApps = $oMembers->getApplications();

    }
   
}

$aOptions = $oMembers->getBillableOptions();
$aGateways = $oMembers->getGatewayOptions();

setHeader();
openPage();

?>

<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td><div class="header">Trading Partner Administration</div></td>
    </tr>
    <tr>
        <td><div class="copy">To <?php print $op ?> this item in the system, please complete the form below.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
    </tr>
</table>

<form action="<?php print SELF ?>?op=<?php print $op ?>&id=<?php print $id ?>" method="post" name="ediaform">

	<!-- FIRST FS -->
	<FIELDSET ID=dv1 class="menu" style="display:inline">
	<LEGEND>
	<DIV class=outer><A href="javascript:show(1);" onclick="this.blur()"><SPAN>General</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(2);" onclick="this.blur()"><SPAN>Billing Info</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(3);" onclick="this.blur()"><SPAN>Permissions</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(4);" onclick="this.blur()"><SPAN>User Info</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(5);" onclick="this.blur()"><SPAN>Partner Info</SPAN></A></DIV>
	</LEGEND>
	
	<h5>Setup Address Locations for <?php print clean($sName) ?></h5>
	<p>
	<br>
	<br>
	<pre>
<table width="100%" border="0" cellpadding="0" cellspacing="0">

    <tr>
        <td><div class="formlabel">Member Name:</div></td>
        <td><input type="text" name="name" value="<?php print clean($sName) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Member Login:</div></td>
        <td><input type="text" name="loginid" value="<?php print clean($sLoginID) ?>" class="textfield" /></td>
    </tr>

    <tr>
        <td><div class="formlabel">Gateways:</div></td>
        <td>
            <select name="gateways_options">
<?php

    if (count($aGateways)) {
        $i = 0;
        while ($i < count($aGateways)) {
?>
                <option <?php !strcmp($aGateways[$i]["DataKey"], $smember_gateway) ? print "selected" : print ""; ?> value=<?php print $aGateways[$i]["DataKey"]; ?>><?php print $aGateways[$i]["GatewayName"]; ?></option>
<?php
            ++$i;
        }
    }

?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Per Transaction Amount: $</div></td>
        <td><input type="text" name="billing_amount" value="<?php Print number_format($sbilling_amount, 2, '.', ''); ?>" class="textfield" /></td>
    </tr>

    <?php if (!strcmp("edit", $op)) { ?>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Created:</div></td>
        <td><?php print date("Y-m-d H:i:s", $aMem["Created Date"]) ?></td>
    </tr>
    <tr>
        <td><div class="formlabel">Modified:</div></td>
        <td><?php print date("Y-m-d H:i:s", $aMem["Modified Date"]) ?></td>
    </tr>
    <?php } ?>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    
    <tr>
        <td><input type="text" name="qual" value="" width="5" /></td>
        <td><input type="text" name="isaid" value="" class="textfield" /></td>
    </tr>

    <?php
    $aIds = $aMem["member_ids"];
    if (count($aIds)) {
        $i = 0;
        while ($i < count($aIds)) {
    ?>
        <tr>
            <td><?php print $aIds[$i]["member_qual"] ?></td>
            <td><?php print $aIds[$i]["member_receiver_id"] ?></td>
        </tr>
    <?php
            ++$i;
        }
    }
    ?>


</table>

	</pre>
	</p>
	<br>
	<br>
	</FIELDSET>
	
	
	
	<!-- SECOND FS -->
	<FIELDSET ID=dv2 class="menu" style="display:none">
	<LEGEND>
	<DIV class=outer><A href="javascript:show(1);" onclick="this.blur()"><SPAN>General</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(2);" onclick="this.blur()"><SPAN>Billing Info</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(3);" onclick="this.blur()"><SPAN>Permissions</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(4);" onclick="this.blur()"><SPAN>User Info</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(5);" onclick="this.blur()"><SPAN>Partner Info</SPAN></A></DIV>
	</LEGEND>
	
	<h5>Configure Catalog Access for <?php print clean($sName) ?></h5>
	<p>

	To assign a catalog to this partner, select one from the list below.
	<br>
	<br>
	<pre>
<table width="100%" border="0" cellpadding="0" cellspacing="0">

    <tr>
        <td><div class="formlabel">Address:</div></td>
        <td><input type="text" name="billing_address_1" value="<?php print clean($sbilling_address_1) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel"></div></td>
        <td><input type="text" name="billing_address_2" value="<?php print clean($sbilling_address_2) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel"></div></td>
        <td><input type="text" name="billing_address_3" value="<?php print clean($sbilling_address_3) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel"></div></td>
        <td><input type="text" name="billing_address_4" value="<?php print clean($sbilling_address_4) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">City/State/Zip</div></td>
        <td><input type="text" name="billing_address_city" value="<?php print clean($sbilling_address_city) ?>" class="textfield" /> <input type="text" name="billing_address_state" value="<?php print clean($sbilling_address_state) ?>" class="textfieldState" /> <input type="text" name="billing_address_zip" value="<?php print clean($sbilling_address_zip) ?>" class="textfieldZip" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Country:</div></td>
        <td><input type="text" name="billing_address_country" value="<?php print clean($sbilling_address_country) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Contact/email:</div></td>
        <td><input type="text" name="billing_address_contactName" value="<?php print clean($sbilling_address_contactName) ?>" class="textfield" /> <input type="text" name="billing_email" value="<?php print clean($sbilling_email) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Phone/Fax:</div></td>
        <td><input type="text" name="billing_address_telephone" value="<?php print clean($sbilling_address_telephone) ?>" class="textfield" /> <input type="text" name="billing_address_fax" value="<?php print clean($sbilling_address_fax) ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>

</table>

	</pre>
	</p>
	<br>
	<br>
	</FIELDSET>
	
	
	
	<!-- THIRD FS -->
	<FIELDSET ID=dv3 class="menu" style="display:none">
	<LEGEND>
	<DIV class=outer><A href="javascript:show(1);" onclick="this.blur()"><SPAN>General</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(2);" onclick="this.blur()"><SPAN>Billing Info</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(3);" onclick="this.blur()"><SPAN>Permissions</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(4);" onclick="this.blur()"><SPAN>User Info</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(5);" onclick="this.blur()"><SPAN>Partner Info</SPAN></A></DIV>
	</LEGEND>
	
	<h5>Setup Cross Reference Tables for <?php print clean($sName) ?></h5>
	<p>
	<strong>Future Option</strong>
	<br>
	<br>
	<pre>
            <table width="450" border="0" cellpadding="0" cellspacing="0">
    <?php

    if (count($aApps)) {
        $i = 0;
        while ($i < count($aApps)) {
            $iAppId = $aApps[$i]["user_app_id"];
    ?>
    <tr>
        <td><div><?php print clean($aApps[$i]["user_app_name"]) ?>:</div></td>
        <td>
        
        <table border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td><input type="radio" name="perms[<?php print $iAppId ?>]<?php print $iAppId ?>" value="1"<?php !strcmp(1, $aPerms[$iAppId]) ? print "checked" : print ""; ?>>View&nbsp;</td>
                <td><input type="radio" name="perms[<?php print $iAppId ?>]<?php print $iAppId ?>" value="0"<?php !$aPerms[$iAppId] ? print "checked" : print ""; ?>>No Access&nbsp;</td>
            </tr>
        </table>
        
        </td>
    </tr>
    <?php
            ++$i;
        }
    }
    ?>

            </table>

	</pre>
	</p>
	<br>
	<br>
	</FIELDSET>
	
	
	
	<!-- THIRD FS -->
	<FIELDSET ID=dv4 class="menu" style="display:none">
	<LEGEND>
	<DIV class=outer><A href="javascript:show(1);" onclick="this.blur()"><SPAN>General</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(2);" onclick="this.blur()"><SPAN>Billing Info</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(3);" onclick="this.blur()"><SPAN>Permissions</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(4);" onclick="this.blur()"><SPAN>User Info</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(5);" onclick="this.blur()"><SPAN>Partner Info</SPAN></A></DIV>
	</LEGEND>
	
	<h5>Setup Address Locations for <?php print clean($sName) ?></h5>
	<p>
	<strong>Future Option</strong>
	<br>
	<br>
	
	<pre>
            <table width="450" border="0" cellpadding="0" cellspacing="0">
    <?php
//print_r($aPerms);
//print_r($aApps);
       $aUsers = $aMem["member_users"];

if (count($aUsers)) {
    
    // build page data array
    $i = 0;
    while ($i < count($aUsers)) {
        $aData[$i]["Id"] = $aUsers[$i]["User Id"];
        $aData[$i]["Name"] = $aUsers[$i]["User Name"] . "-" . $aUsers[$i]["User Password Reminder"];
        $aData[$i]["Status"] = $aUsers[$i]["Status"];
        $aData[$i]["Created"] =$aUsers[$i]["Created Date"];
        ++$i;
    }
}


    if (count($aUsers)) {
        $i = 0;
    while ($i < count($aData)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>

    <tr>
        <td width="2%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php if ($iPerm > 2) { ?><a href="index.php?op=<?php print $aState[$aData[$i]["Status"]] ?>&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();"><?php } ?><img src="../../_img/icon_status_<?php print $aData[$i]["Status"] ?>.gif" width="16" height="10" alt="" border="0" /><?php if ($iPerm > 2) { ?></a><?php } ?></div></td>
        <td width="50%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print format($aData[$i]["Name"]) ?></div></td>
        <td width="40%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print date("Y-m-d H:i:s", $aData[$i]["Created"]) ?></div></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="5"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
            ++$i;
        }
    }
    ?>

            </table>
	</pre>
	</p>
	<br>
	<br>
	</FIELDSET>

	<!-- THIRD FS -->
	<FIELDSET ID=dv5 class="menu" style="display:none">
	<LEGEND>
	<DIV class=outer><A href="javascript:show(1);" onclick="this.blur()"><SPAN>General</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(2);" onclick="this.blur()"><SPAN>Billing Info</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(3);" onclick="this.blur()"><SPAN>Permissions</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(4);" onclick="this.blur()"><SPAN>User Info</SPAN></A></DIV>
	<DIV class=outer><A href="javascript:show(5);" onclick="this.blur()"><SPAN>Partner Info</SPAN></A></DIV>
	</LEGEND>
	
	<h5>Setup Address Locations for <?php print clean($sName) ?></h5>
	<p>
	<strong>Future Option</strong>
	<br>
	<br>
	
	<pre>
            <table width="450" border="0" cellpadding="0" cellspacing="0">
    <?php

       $aPartners = $aMem["member_partners"];

if (count($aPartners)) {
    
    // build page data array
    $i = 0;
    while ($i < count($aPartners)) {
        $aDataP[$i]["Id"] = $aPartners[$i]["tp Id"];
        $aDataP[$i]["Name"] = $aPartners[$i]["tp Name"];
        $aDataP[$i]["Status"] = $aPartners[$i]["Status"];
        $aDataP[$i]["Created"] =$aPartners[$i]["Created Date"];
        ++$i;
    }
}


    if (count($aPartners)) {
        $i = 0;
    while ($i < count($aDataP)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>

    <tr>
        <td width="2%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aDataP[$i]["Status"] ?>"><?php if ($iPerm > 2) { ?><a href="index.php?op=<?php print $aState[$aDataP[$i]["Status"]] ?>&id=<?php print $aDataP[$i]["Id"] ?>" onclick="return verify();"><?php } ?><img src="../../_img/icon_status_<?php print $aDataP[$i]["Status"] ?>.gif" width="16" height="10" alt="" border="0" /><?php if ($iPerm > 2) { ?></a><?php } ?></div></td>
        <td width="50%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aDataP[$i]["Status"] ?>"><?php print format($aDataP[$i]["Name"]) ?></div></td>
        <td width="25%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aDataP[$i]["Status"] ?>"><?php print date("Y-m-d H:i:s", $aDataP[$i]["Created"]) ?></div></td>
        <td width="15%" bgcolor="#<?php print $bg ?>"><?php if ($iPerm > 1) { ?><a href="form.php?op=edit&id=<?php print $aDataP[$i]["Id"] ?>">EDIT</a>&nbsp;|&nbsp;<a href="index.php?op=del&id=<?php print $aDataP[$i]["Id"] ?>" onclick="return verify();">DELETE</a><?php } ?></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="5"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
            ++$i;
        }
    }
    ?>

            </table>
	</pre>
	</p>
	<br>
	<br>
	</FIELDSET>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="right" colspan="2"><input type="image" src="../../_img/buttons/btn_submit.gif" width="58" height="15" alt="" border="0" onfocus="this.blur();" /></td>
    </tr>
</table>

</form>

<?php closePage(); ?>
