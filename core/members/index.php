<?php

// File Location: /core/partners/index.php

require_once("tpl_secure.php");
require_once("class.members.php");

// instantiate news class
$oMems = new mem();

// the session class is instantiated in the tpl_secure.php file

// get users and user count
$aMems = $oMems->getMembers();

// check for users
if (count($aMems)) {
    
    // build page data array
    $i = 0;
    while ($i < count($aMems)) {
        $aData[$i]["Id"] = $aMems[$i]["Id"];
        $aData[$i]["Name"] = $aMems[$i]["Name"];
        $aData[$i]["Status"] = $aMems[$i]["Status"];
        $aData[$i]["Created"] =$aMems[$i]["Created Date"];
        ++$i;
    }
}

// check for id
if ($id) {
    
    // assign user id
    $oMems->setMemId($id);
    
    // check operation type
    if (!strcmp($op, "del")) {
        
        // try delete user and redirect
        $oMems->deleteMember();
        header("Location: ".SELF);
        
    } elseif (!strcmp($op, "act")) {
        
        // try activate user and redirect
        $oMems->activateMember();
        header("Location: ".SELF);
        
    } elseif (!strcmp($op, "deact")) {
        
        // try deactivate user and redirect
        $oMems->deactivateMember();
        header("Location: ".SELF);
    }
}

setHeader();
openPage();
?>

<table width="608" border="0" cellpadding="0" cellspacing="0">
    <form name="ediaform">

    <tr>
        <td><div class="formlabel">Start Invoice Number:</div></td>
        <td><input type="text" name="sinv" value="" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">Start Date:</div></td>
        <td><input type="text" name="sdate" value="<?php echo date("Y-m-d H:i:s", time()); ?>" class="textfield" /></td>
    </tr>
    <tr>
        <td><div class="formlabel">End Date:</div></td>
        <td><input type="text" name="edate" value="<?php echo date("Y-m-d H:i:s", time()); ?>" class="textfield" /></td>
    </tr>
    <tr>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="right" colspan="2"><a onclick="return showDownLoadInvoice();"><img src="../../_img/buttons/btn_submit.gif" width="58" height="15" alt="" border="0" onfocus="this.blur();" /></td>
            </tr>
        </table>
    </tr>
    </form>
    <script type="text/javascript">
    function catcalc(cal) {
    var date = cal.date;
    var time = date.getTime()
    // use the _other_ field
    if (field == cal.params.inputField) {
        var field = document.getElementById("sdate");
//        time -= Date.WEEK; // substract one week
    } else {
        var field = document.getElementById("edate");
//        time += Date.WEEK; // add one week
    }
    var date2 = new Date(time);
    field.value = date2.print("%Y-%m-%d %H:%M:%S");
    }
    Calendar.setup({
        inputField : "sdate",
        ifFormat : "%Y-%m-%d %H:%M:%S",
        showsTime : true,
        timeFormat : "24",
        onUpdate : catcalc
    });
        Calendar.setup({
        inputField : "edate",
        ifFormat : "%Y-%m-%d %H:%M:%S",
        showsTime : true,
        timeFormat : "24",
        onUpdate : catcalc
    });
    </script>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td colspan="2"><div class="header">Trading Partner Administration</div></td>
    </tr>
    <tr>
        <td colspan="2"><div class="copy">To manage trading partners, select a trading partner action from the list below.</div></td>
    </tr>
    <tr>
        <td><div class="error"><?php writeErrors() ?></div></td>
        <td align="right" valign="top"><?php if ($iPerm > 1) { ?><a href="form.php?op=add"><img src="../../_img/buttons/btn_additem.gif" width="58" height="15" alt="" border="0" /></a><?php } ?></td>
    </tr>
</table>

<?php renderList($aData) ?>

<?php closePage(); ?>
