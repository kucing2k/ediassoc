<?php

/**
 * Initiate PDO instance
 * 
 * @staticvar PDO $conn
 * @return EdiAssocPdo
 */
function get_db() {
  static $conn;
  if(!isset($conn)) {
    $conn = new EdiAssocPdo( DB_PDO_DSN, DB_PDO_USER, DB_PDO_PASS, array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ));
  }
  return $conn;
}

class EdiAssocPdo extends PDO
{
  public function getOne($sql, $params=array()) {
    $stmt = $this->prepare($sql);
    $stmt->execute($params);
    return $stmt->fetchColumn();
  }
  
  public function getCol($sql, $params=array()) {
    $stmt = $this->prepare($sql);
    $stmt->execute($params);
    $result = array();
    while($col = $stmt->fetchColumn())
      $result[] = $col;
    return $result;
  }
  
  public function getRow($sql, $params=array()) {
    $stmt = $this->prepare($sql);
    $stmt->execute($params);
    return $stmt->fetch();
  }
  
  public function getAll($sql, $params=array()) {
    $stmt = $this->prepare($sql);
    $stmt->execute($params);
    return $stmt->fetchAll();
  }
  
  public function doQuery($sql, $params=array()) {
    $stmt = $this->prepare($sql);
    return $stmt->execute($params);
  }
}