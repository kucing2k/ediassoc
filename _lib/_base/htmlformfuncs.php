<?php

function displayAddresses($aAdd='') {

$i=0;
    if (count($aAdd)) {
        $intAddWidth = 100 / count($aAdd);
        while ($i < count($aAdd)) {
        
            $strAddHdr = $strAddHdr . "<td width='" . $intAddWidth . "%' valign=top style='width:" . $intAddWidth . "%;border:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>\n";
            $strAddHdr = $strAddHdr . "<p class=MsoNormal><B>" . $aAdd[$i]["da_type_qual"] . "</B></p>\n";
            $strAddHdr = $strAddHdr . "</td>\n";
        
        
            $strAdd = $strAdd . "<td valign=top style='width:" . $intAddWidth . "%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>\n";
            $strAdd = $strAdd . "<p class=MsoNormal>";
            if (strlen(trim($aAdd[$i]["da_duns_id"])) > 0) { 
                $strAdd = $strAdd . trim($aAdd[$i]["da_duns_id"]);
            } 
            $strAdd = $strAdd . "</p>\n";
            $strAdd = $strAdd . "<p class=MsoNormal>";
            if (strlen(trim($aAdd[$i]["da_name"])) > 0) { 
                $strAdd = $strAdd . trim($aAdd[$i]["da_name"]);
            } 
            $strAdd = $strAdd . "</p>\n";
            $strAdd = $strAdd . "<p class=MsoNormal>";
            if (strlen(trim($aAdd[$i]["da_address1"])) > 0) { 
                $strAdd = $strAdd . trim($aAdd[$i]["da_address1"]);
            } 
            $strAdd = $strAdd . "</p>\n";
            $strAdd = $strAdd . "<p class=MsoNormal>";
            if (strlen(trim($aAdd[$i]["da_address2"])) > 0) { 
                $strAdd = $strAdd . trim($aAdd[$i]["da_address2"]);
            } 
            $strAdd = $strAdd . "</p>\n";
            $strAdd = $strAdd . "<p class=MsoNormal>";
            if (strlen(trim($aAdd[$i]["da_address3"])) > 0) { 
                $strAdd = $strAdd . trim($aAdd[$i]["da_address3"]);
            } 
            $strAdd = $strAdd . "</p>\n";
            $strAdd = $strAdd . "<p class=MsoNormal>";
            if (strlen(trim($aAdd[$i]["da_address4"])) > 0) { 
                $strAdd = $strAdd . trim($aAdd[$i]["da_address4"]);
            } 
            $strAdd = $strAdd . "</p>\n";
            $strAdd = $strAdd . "<p class=MsoNormal>";
            if (strlen(trim($aAdd[$i]["da_city"])) > 0) { 
                $strAdd = $strAdd . trim($aAdd[$i]["da_city"]) . ", " . trim($aAdd[$i]["da_state"]) . " " . trim($aAdd[$i]["da_postalcode"]);
            } 
            $strAdd = $strAdd . "</p>\n";
            $strAdd = $strAdd . "<p class=MsoNormal>";
            if (strlen(trim($aAdd[$i]["da_contactname"])) > 0) { 
                $strAdd = $strAdd . trim($aAdd[$i]["da_contactname"]);
            } 
            $strAdd = $strAdd . "</p>\n";
            $strAdd = $strAdd . "<p class=MsoNormal>";
            if (strlen(trim($aAdd[$i]["da_ContactPhone"])) > 0) { 
                $strAdd = $strAdd . trim($aAdd[$i]["da_ContactPhone"]);
            } 
            $strAdd = $strAdd . "</p>\n";
            $strAdd = $strAdd . "<p class=MsoNormal>";
            if (strlen(trim($aAdd[$i]["da_contactemail"])) > 0) { 
                $strAdd = $strAdd . trim($aAdd[$i]["da_contactemail"]);
            } 
            $strAdd = $strAdd . "</p>\n";
            $strAdd = $strAdd . "</td>\n";
            ++$i;
        }
        return $strAddHdr . "</tr><tr>" . $strAdd;

    }
}

function displayScheduled($aLSch='') {

$i=0;
    if (count($aLSch)) {
        while ($i < count($aLSch)) {
        
            $strAddHdr = $strAddHdr . "<tr>\n";
            $strAddHdr = $strAddHdr . "<td width='25%' valign=top style='width:25%;border:solid windowtext .5pt; border-top:none;mso-border-top-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt'>\n";
            $strAddHdr = $strAddHdr . "<p class=MsoNormal>" . $aLSch[$i]["ds_qty"] . "</p>\n";
            $strAddHdr = $strAddHdr . "</td>\n";
            $strAddHdr = $strAddHdr . "<td width='25%' valign=top style='width:25%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>\n";
            $strAddHdr = $strAddHdr . "<p class=MsoNormal>" . $aLSch[$i]["ds_qty_uom"] . "</p>\n";
            $strAddHdr = $strAddHdr . "</td>\n";
            $strAddHdr = $strAddHdr . "<td width='25%' valign=top style='width:25%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>\n";
            $strAddHdr = $strAddHdr . "<p class=MsoNormal>" . $aLSch[$i]["ds_date_time_qualifier"] . "</p>\n";
            $strAddHdr = $strAddHdr . "</td>\n";
            $strAddHdr = $strAddHdr . "<td width='25%' valign=top style='width:25%;border-top:none;border-left:none;border-bottom:solid windowtext .5pt;border-right:solid windowtext .5pt; mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt; padding:0in 5.4pt 0in 5.4pt'>\n";
            $strAddHdr = $strAddHdr . "<p class=MsoNormal>" . $aLSch[$i]["ds_date"] . "</p>\n";
            $strAddHdr = $strAddHdr . "</td>\n";
            $strAddHdr = $strAddHdr . "</tr>\n";
        
            ++$i;
        }
        return $strAddHdr;

    }
}


?>