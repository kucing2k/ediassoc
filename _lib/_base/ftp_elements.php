<?php

// File Location: /_lib/_base/ftp_elements.php

/** 
 * reuseable page elements
 *
 * @author Stephen Case <stephen@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access private
 * @copyright EDI Associates, Inc.
 *
 */

require_once("funcs.php");


// renders a paginated list for the site admin
function renderFTPUserList($aData='') {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table class="T" width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="2%" class="header"><div class="listrow"><strong></strong></div></td>
        <td width="10%" class="header"><div class="listrow"><strong>Login</strong></div></td>
        <td width="35%" class="header"><div class="listrow"><strong>Name</strong></div></td>
        <td width="10%" class="header"><div class="listrow"><strong>Password</strong></div></td>
        <td width="15%" class="header"><div class="listrow"><strong>Max Space</strong></div></td>
        <td width="15%" class="header"><div class="listrow"><strong>Max Files</strong></div></td>
        <td class="header"><div class="listrow"><strong>Option</strong></div></td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td class="dotrule" colspan="7"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    $bg = "FFFFFF";
    while ($i < count($aData)) {
        !strcmp("0", $aData[$i]["Status"]) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <form action="<?php print SELF ?>?op=update&id=<?php print $aData[$i]["Id"]; ?>" method="post" name="ediaform_<?php print $aData[$i]["Id"]; ?>">

    <tr>
        <td width="2%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php if ($iPerm > 2) { ?><a href="wsftp.php?op=<?php print $aState[$aData[$i]["Status"]] ?>&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();"><?php } ?><img src="../../_img/icon_status_<?php print $aData[$i]["Status"] ?>.gif" width="16" height="10" alt="" border="0" /><?php if ($iPerm > 2) { ?></a><?php } ?></div></td>
        <td width="10%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print format($aData[$i]["Name"]) ?></div></td>
        <td width="35%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print format($aData[$i]["FullName"]) ?></div></td>
        <td width="10%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print format($aData[$i]["Password"]) ?></div></td>
        <td width="15%" bgcolor="#<?php print $bg ?>"><input type="text" name="FTPMAXSPACE_<?php print $aData[$i]["Id"] ?>" value="<?php print clean($aData[$i]["FTPMAXSPACE"]) ?>" /></td>
        <td width="15%" bgcolor="#<?php print $bg ?>"><input type="text" name="FTPMAXFILES_<?php print $aData[$i]["Id"] ?>" value="<?php print clean($aData[$i]["FTPMAXFILES"]) ?>" /></td>
        <td bgcolor="#<?php print $bg ?>" align="right"><input type="image" src="../../_img/buttons/btn_update.gif" width="58" height="15" alt="" border="0" onfocus="this.blur();" /></td>
    </tr>
    </form>
    <tr>
        <td class="dotrule" colspan="7"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>
