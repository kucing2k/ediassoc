<?php

// File Location: /_lib/_base/elements.php

/** 
 * reuseable page elements
 *
 * @author Mike Buzzard <src@cubancouncil.com>
 * @version 1.0
 * @since 1.0
 * @access private
 * @copyright Cuban Council
 *
 */

require_once("funcs.php");



///////////////////////////////////////////////////////////////////////////
// Standards functions
///////////////////////////////////////////////////////////////////////////
// write an element of the topic list
// renders a paginated list for the site admin
function renderStdDocList($aData='') {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table class="T" width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="50%" class="header"><div class="listrow"><strong>Item Name</strong></div></td>
        <td width="25%" class="header"><div class="listrow"><strong>Created</strong></div></td>
        <td width="15%" class="header"><div class="listrow"><?php if ($iPerm > 2) { ?><strong>Actions</strong><?php } ?></div></td>
    </tr>
</table>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td class="dotrule" colspan="5"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td width="2%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php if ($iPerm > 2) { ?><a href="index.php?op=<?php print $aState[$aData[$i]["Status"]] ?>&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();"><?php } ?><img src="../../_img/icon_status_<?php print $aData[$i]["Status"] ?>.gif" width="16" height="10" alt="" border="0" /><?php if ($iPerm > 2) { ?></a><?php } ?></div></td>
        <td width="50%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print format($aData[$i]["Name"]) ?></div></td>
        <td width="25%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print date("Y-m-d H:i:s", $aData[$i]["Created"]) ?></div></td>
        <td width="15%" bgcolor="#<?php print $bg ?>"><?php if ($iPerm > 1) { ?><a href="form.php?op=edit&id=<?php print $aData[$i]["Id"] ?>">EDIT</a>&nbsp;|&nbsp;<a href="index.php?op=del&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();">DELETE</a><?php } ?></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="5"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php renderPaging($iCursor, count($aData)) ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>
<?php
function renderStandardsDocList($aData='', $sOpt='') {
    
    global $iCursor, $iPerm;
    
    if (!strcmp("ver", $sOpt)) {
        $sVal = "Standards Agency";
    } elseif (!strcmp("tra", $sOpt)) {
        $sVal = "Version";
    } elseif (!strcmp("seg", $sOpt)) {
        $sVal = "Transaction Set";
    }     
?>
<br />
<?php if (is_array($aData)) { ?>
<table width="608" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header"><?php print $sVal; ?></div></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td width="25%" bgcolor="#<?php print $bg ?>" class="copy"><a href="index1.php?op=<?php print $sOpt ?>&id=<?php print $aData[$i]["dk"] ?>"><?php print $aData[$i]["Id"] ?></a></div></td>
        <td width="75%" bgcolor="#<?php print $bg ?>" class="copy"><?php print format($aData[$i]["Name"]) ?></div></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>

<?php
function renderStandardsList($aData='', $sOpt='') {
    
    global $iCursor, $iPerm;
    
    if (!strcmp("ver", $sOpt)) {
        $sVal = "Standards Agency";
    } elseif (!strcmp("tra", $sOpt)) {
        $sVal = "Version";
    } elseif (!strcmp("seg", $sOpt)) {
        $sVal = "Transaction Set";
    }     
?>
<br />
<?php if (is_array($aData)) { ?>
<table width="608" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header"><?php print $sVal; ?></div></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td width="25%" bgcolor="#<?php print $bg ?>" class="copy"><a href="index.php?op=<?php print $sOpt ?>&aid=<?php print $aData[$i]["dk"] ?>"><?php print $aData[$i]["Id"] ?></a></div></td>
        <td width="75%" bgcolor="#<?php print $bg ?>" class="copy"><?php print format($aData[$i]["Name"]) ?></div></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>

<?php

function renderStandardsSegmentList($aData='', $sOpt='') {

?>
<br />
<?php if (is_array($aData)) { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">

    <?php
    // loop through data and conditionally display functionality and content
    $bg = "FFFFFF";
    $i = 0;
    $sLead[0] = "|-";
    $sLead[1] = "|&nbsp;|-";
    $sLead[2] = "|&nbsp;&nbsp;&nbsp;|-";
    $sLead[3] = "|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|-";
    $sLead[4] = "|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|-";
    $sLead[5] = "|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|-";
    $sLead[6] = "|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|-";
    $iLeadCnt = 0;
    while ($i < count($aData)) {

        if (strcmp($sTmpVal, $aData[$i]["Section"])) {
//            $iLeadCnt = 0;
            $sTmpVal = $aData[$i]["Section"];
            if (!strcmp("H", $sTmpVal)) {
                $sOutVal = "HEADER SEGMENTS";
            } elseif (!strcmp("D", $sTmpVal)) {
                $sOutVal = "DETAIL SEGMENTS";
            } elseif (!strcmp("S", $sTmpVal)) {
                $sOutVal = "SUMMARY SEGMENTS";
            } else {
                $sOutVal = "Other";
            }
?>    
    <tr>
        <td colspan="3" width="100%" class="header"><div class="listrow"><strong><center><?php print $sOutVal ?></center></strong></div></td>
    </tr>
    
<?php
        }
        if ($aData[$i]["BeginEnd"] == "B") {
            if ($aData[$i]["LoopID"]) {
                if (right($aData[$i]["LoopID"], 1)== "0") {
                    $iLeadCnt = 3;
                }
                if (right($aData[$i]["LoopID"], 2)== "00") {
                    $iLeadCnt = 2;
                }
                if (right($aData[$i]["LoopID"], 3)== "000") {
                    $iLeadCnt = 1;
                }
            } else {
                $iLeadCnt = 0;
            }

        ?>
    <tr>
        <td width="85%" bgcolor="#<?php print $bg ?>" class="copy"><?php print $sLead[$iLeadCnt] ?>- *** <?php print $aData[$i]["Id"] ?> Group ***</div></td>
        <td width="10%" bgcolor="#<?php print $bg ?>" class="copy" align="right">&nbsp;x<?php print format($aData[$i]["MaximumLoopRepeat"]) ?>&nbsp;</div></td>
        <td width="5%" bgcolor="#<?php print $bg ?>" class="copy">(<?php print format($aData[$i]["Req"]) ?>)</div></td>
    </tr>
            
        <?php
        ++$iLeadCnt;
            
        } 
    ?>
    <tr>
        <td width="85%" bgcolor="#<?php print $bg ?>" class="copy"><?php print $sLead[$iLeadCnt] ?><a href="index.php?op=<?php print $sOpt ?>&eid=<?php print $aData[$i]["Id"] ?>&ver=<?php print $aData[$i]["ver"] ?>&ag=<?php print $aData[$i]["ag"] ?>"><?php print $aData[$i]["Id"] ?></a> <?php print format($aData[$i]["Name"]) ?></div></td>
        <td width="10%" bgcolor="#<?php print $bg ?>" class="copy" align="right">&nbsp;x<?php print format($aData[$i]["Max"]) ?>&nbsp;</div></td>
        <td width="5%" bgcolor="#<?php print $bg ?>" class="copy">(<?php print format($aData[$i]["Req"]) ?>)</div></td>

    </tr>
    <?php
//            if ($aData[$i]["BeginEnd"] == "E") {
//            --$iLeadCnt;
//            }
        ++$i;
    } // end loop
    ?>
</table>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>

<?php
    


function form_renderStandardDocSegmentList($aData='', $sOpt='') {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table width="608" border="1" cellpadding="0" cellspacing="0">

    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {

        if (strcmp($sTmpVal, $aData[$i]["Section"])) {
           
            $sTmpVal = $aData[$i]["Section"];
            if (!strcmp("H", $sTmpVal)) {
                $sOutVal = "HEADER SEGMENTS";
            } elseif (!strcmp("D", $sTmpVal)) {
                $sOutVal = "DETAIL SEGMENTS";
            } elseif (!strcmp("S", $sTmpVal)) {
                $sOutVal = "SUMMARY SEGMENTS";
            } else {
                $sOutVal = "Other";
            }
    
            ?>
                <tr>
                    <td colspan="5" width="100%" class="header"><div class="listrow"><strong><center><?php print $sOutVal ?></center></strong></div></td>
                </tr>
                <tr>
                    <td width="5%" class="header"><div class="listrow"><strong>M/O</strong></div></td>
                    <td width="10%" class="header"><div class="listrow"><strong>Segment</strong></div></td>
                    <td width="75%" class="header"><div class="listrow"><strong>Name</strong></div></td>
                    <td width="10%" class="header"><div class="listrow"><strong>Max Usage</strong></div></td>
                </tr>
            <?php
        }        
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td width="5%" bgcolor="#<?php print $bg ?>" class="copy"><?php print $aData[$i]["Req"] ?></div></td>
        <td width="10%" bgcolor="#<?php print $bg ?>" class="copy"><?php print $aData[$i]["Id"] ?></div></td>
        <td width="75%" bgcolor="#<?php print $bg ?>" class="copy"><?php print format($aData[$i]["Name"]) ?></div></td>
        <td width="10%" bgcolor="#<?php print $bg ?>" class="copy"><?php print format($aData[$i]["Max"]) ?></div></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>

<?php


function renderStandardsDocSegmentList($aData='', $sOpt='') {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table width="608" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="5"><div class="header">Segments</div></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {

        if (strcmp($sTmpVal, $aData[$i]["Section"])) {
            $sTmpVal = $aData[$i]["Section"];
            if (!strcmp("H", $sTmpVal)) {
                $sOutVal = "Header";
            } elseif (!strcmp("D", $sTmpVal)) {
                $sOutVal = "Detail";
            } elseif (!strcmp("S", $sTmpVal)) {
                $sOutVal = "Summary";
            } else {
                $sOutVal = "Other";
            }
    
            ?>
                <tr bgcolor="C6AADB">
                    <td colspan="5" width="100%" class="header"><div class="listrow"><strong><center><?php print $sOutVal ?></center></strong></div></td>
                </tr>
                <tr>
                    <td class="header"><center><div class="listrow"><A href="javascript:CheckAll();"><strong>All</strong></A></div></td>
                    <td width="5%" class="header"><div class="listrow"><strong>M/O</strong></div></td>
                    <td width="10%" class="header"><div class="listrow"><strong>Segment</strong></div></td>
                    <td width="75%" class="header"><div class="listrow"><strong>Name</strong></div></td>
                    <td width="10%" class="header"><div class="listrow"><strong>Max Usage</strong></div></td>
                </tr>
            <?php
        }        
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td><input type="checkbox" name="favorite[]" value="<?php print $aData[$i]["dk"]; ?>" /></td>
        <td width="5%" bgcolor="#<?php print $bg ?>" class="copy"><a href="index1.php?op=<?php print $sOpt ?>&eid=<?php print $aData[$i]["Id"] ?>&ver=<?php print $aData[$i]["ver"] ?>&ag=<?php print $aData[$i]["ag"] ?>"><?php print $aData[$i]["Req"] ?></a></div></td>
        <td width="10%" bgcolor="#<?php print $bg ?>" class="copy"><a href="index1.php?op=<?php print $sOpt ?>&eid=<?php print $aData[$i]["Id"] ?>&ver=<?php print $aData[$i]["ver"] ?>&ag=<?php print $aData[$i]["ag"] ?>"><?php print $aData[$i]["Id"] ?></a></div></td>
        <td width="75%" bgcolor="#<?php print $bg ?>" class="copy"><?php print format($aData[$i]["Name"]) ?></div></td>
        <td width="10%" bgcolor="#<?php print $bg ?>" class="copy"><?php print format($aData[$i]["Max"]) ?></div></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>

<?php


function renderStandardsElementList($aData='', $sOpt='') {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table width="608" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="5"><div class="header">Elements</div></td>
    </tr>
    <tr>
        <th rowspan="2" width="5%" class="header"><div class="listrow"><strong>M/O</strong></div></th>
        <th rowspan="2" width="10%" class="header"><div class="listrow"><strong>Segment</strong></div></th>
        <th rowspan="2" width="75%" class="header"><div class="listrow"><strong>Name</strong></div></th>
        <th colspan="2" width="10%" class="header"><div class="listrow"><strong>Length</strong></div></th>
            <tr>
                <th>Min</th>
                <th>Max</th>
            </tr>
        
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {

        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td width="5%" bgcolor="#<?php print $bg ?>" class="copy"><?php print $aData[$i]["Req"] ?></td>
        <td width="20%" bgcolor="#<?php print $bg ?>" class="copy"><?php if ($aData[$i]["cnt"]) { ?><a href="index.php?op=<?php print $sOpt ?>&eid=<?php print $aData[$i]["Id"] ?>&ver=<?php print $aData[$i]["ver"] ?>&ag=<?php print $aData[$i]["ag"] ?>"><?php print "*".$aData[$i]["Id"]; ?></a></div><?php } else { print $aData[$i]["Id"]; }  ?></td>
        <td width="70%" bgcolor="#<?php print $bg ?>" class="copy"><?php print format($aData[$i]["Name"]) ?></div></td>
        <td bgcolor="#<?php print $bg ?>" class="copy"><?php print format($aData[$i]["Min"]) ?></div></td>
        <td bgcolor="#<?php print $bg ?>" class="copy"><?php print format($aData[$i]["Max"]) ?></div></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>

<?php

function renderStandardsCodeList($aData='') {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table width="608" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header">Codes</div></td>
    </tr>
    <tr>
        <td width="10%" class="header"><div class="listrow"><strong>Value</strong></div></td>
        <td width="90%" class="header"><div class="listrow"><strong>Description</strong></div></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {

        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td width="10%" bgcolor="#<?php print $bg ?>" class="copy"><?php print $aData[$i]["Id"] ?></div></td>
        <td width="90%" bgcolor="#<?php print $bg ?>" class="copy"><?php print $aData[$i]["Name"] ?></div></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>


<?php

function renderStandardsList1($aA='', $aV='', $aT='', $aS='', $aE='', $aData='') {

    global $iCursor, $iPerm;

    $sOpt = "Agency";
    if ($aA) {
        $sOpt = "Version";
    } 
    if ($aV) {
        $sOpt = "Transaction Set";
    } 
    if ($aT) {
        $sOpt = "Segments";
    } 
    if ($aS) {
        $sOpt = "Elements";
    }
    if ($aE) {
        $sOpt = "Codes";
    } 

    
?>
<br />
<?php if (is_array($aData)) { ?>
<table width="608" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="2"><div class="header"><?php print $sOpt; ?></div></td>
    </tr>
    <tr>
        <td width="10%" class="header"><div class="listrow"><strong>Value</strong></div></td>
        <td width="90%" class="header"><div class="listrow"><strong>Description</strong></div></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {

        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td width="10%" bgcolor="#<?php print $bg ?>" class="copy"><?php print $aData[$i]["Id"] ?></div></td>
        <td width="90%" bgcolor="#<?php print $bg ?>" class="copy"><?php print $aData[$i]["Name"] ?></div></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>

