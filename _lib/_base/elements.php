<?php

// File Location: /_lib/_base/elements.php

/** 
 * reuseable page elements
 *
 * @author Mike Buzzard <src@cubancouncil.com>
 * @version 1.0
 * @since 1.0
 * @access private
 * @copyright Cuban Council
 *
 */

require_once("funcs.php");

function renderPrevNext($prev='',$next='') {
    
?>
<table border="0" cellpadding="0" cellspacing="10">
    <tr>
        <td><?php if ($prev) { ?><a href="form.php?tid=<?php Print $prev; ?>"><img src="../../_img/buttons/btn_prev.gif" width="15" height="15" alt="" border="0" alt="Previous" /></a><?php } else { ?><img src="../../_img/buttons/btn_prev_null.gif" width="15" height="15" alt="" border="0" alt="Previous" /><?php } ?></td>
        <td> | </td>
        <td><?php if ($next) { ?><a href="form.php?tid=<?php Print $next; ?>"><img src="../../_img/buttons/btn_next.gif" width="15" height="15" alt="" border="0" alt="Next" /></a><?php } else { ?><img src="../../_img/buttons/btn_next_null.gif" width="15" height="15" alt="" border="0" alt="Next" /><?php } ?></td>
    </tr>
</table>

<?php } // end function ?>

<?php

function renderReplyList($aData='',$tid='',$op='',$prev='',$next='') {
    
?>
<br />
<?php if (is_array($aData)) { ?>
<table border="0" cellpadding="0" cellspacing="10">
    <tr>
        <td><?php if ($prev) { ?><a href="form.php?tid=<?php Print $prev; ?>"><img src="../../_img/buttons/btn_prev.gif" width="15" height="15" alt="" border="0" alt="Previous" /></a><?php } else { ?><img src="../../_img/buttons/btn_prev_null.gif" width="15" height="15" alt="" border="0" alt="Previous" /><?php } ?></td>
        <td><a href="javascript:showViewTrans(<?php print "'".$tid."'"; ?><?php Print ","; ?><?php Print $op; ?>);"><b><u>Printer Friendly</u></b></a></td>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $bg = "FFFFFF"; 
    ?>

        <td bgcolor="#<?php print $bg ?>"><a href="../draft/form.php?it=<?php print $aData[$i]["tpp_transaction"]; ?>&rid=<?php Print $tid; ?>"><b><u>Create : <?php print $aData[$i]["tpp_transaction"] ?></u></b></td>

    <?php
        ++$i;
    } // end loop
    ?>
        <td><?php if ($next) { ?><a href="form.php?tid=<?php Print $next; ?>"><img src="../../_img/buttons/btn_next.gif" width="15" height="15" alt="" border="0" alt="Next" /></a><?php } else { ?><img src="../../_img/buttons/btn_next_null.gif" width="15" height="15" alt="" border="0" alt="Next" /><?php } ?></td>

    </tr>
</table>
<?php renderPaging($iCursor, $iCnt) ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>
<?php

function renderTransHeader($iCnt=0, $aData='', $iUserID) {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table class="T" width="758" border="0" cellpadding="0" cellspacing="0" id="dataTable">
    <thead>
        <tr>
            <th class="header"><center><div class="listrow"><a href="javascript:CheckAll();"><strong>All</strong></a></div></th>
            <th class="header"><center><div class="listrow"><strong>Status</strong></div></center></th>
            <th class="header"><center><div class="listrow"><strong>ICN</strong></div></center></th>
            <th class="header"><center><div class="listrow"><strong>Sender ID</strong></div></center></th>
            <th class="header"><center><div class="listrow"><strong>Receiver ID</strong></div></center></th>
            <th class="header"><center><div class="listrow"><strong>Received Date</strong></div></center></th>
            <th class="header"><center><div class="listrow"><strong>Options</strong></div></center></th>
        </tr>
    </thead>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");

    ?>
    <tr>
    <td class="td2" bgcolor="#<?php print $bg ?>"><center><div class="section"><input type="checkbox" name="favorite[]" value="<?php print $aData[$i]["Id"]; ?>" /></div></td>
        <td class="td2" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><img src="../../_img/icon_status_gif_<?php print $aData[$i]["IntStatus"] ?>.jpg" width="16" height="16" alt="" border="0" /></div></center></td>
        <td class="td2" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $aData[$i]["TrnN"]; ?></div></center></td>
        <td class="td2" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $aData[$i]["SndI"]; ?></div></center></td>
        <td class="td2" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $aData[$i]["RecI"]; ?></div></center></td>
        <td class="td2" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $aData[$i]["LodD"]; ?></div></center></td>
        <td class="td2" bgcolor="#<?php print $bg ?>"><center><a href="javascript:showViewData(<?php print "'".$aData[$i]["Id"]."'"; ?>);">DATA</a></center></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php renderPaging($iCursor, $iCnt) ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data available for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>
<?php

// renders a paginated list for the site admin
function renderBox($iCnt=0) {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if ($iCnt) { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="250"><div class="listrow"><strong>Box Direction</strong></div></td>
        <td width="170"><div class="listrow"><strong>New</strong></div></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="2"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <tr>
        <td width="250"><div class="listrow"><a href="/core/mailbox/index.php?op=new"><strong>In Box</strong></a></div></td>
        <td width="170"><div class="listrow"><?php print $iCnt ?></div></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="4"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
</table>
<?php renderPaging($iCursor, $iCnt) ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data available for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>
<?php

// write user errors (usage: must be called inside html body)
function writeErrors() {
  
    global $ERRORS;
    
    if (count($ERRORS)) {
        print "<strong>Error_</strong><br />";
        while(list($key, $value) = each($ERRORS)) {
            print($value)."<br />";
        }
    }
}
// write system errors (usage: must be called in javascript tags inside html head)
function writeExceptions() {
    
    global $EXCEPTS;
    
    $sReturn .= "// exception reporting
    function trace() {
        var msg = \"\";";
        
        if (count($EXCEPTS)) {
            $sMsg = "";
            while(list($key, $value) = each($EXCEPTS)) {
                $sMsg .= "msg = msg + \"".str_replace("\n", "", addslashes($value))."\\n\";\n";
            }
            $sReturn .= $sMsg;
        }
        
    $sReturn .= "\t
        if (msg != \"\") {
            alert(msg);
        }
    }
    document.onload = trace();\n";
     return $sReturn;
}
// write a pre-formatted array contents
function dump($aArgs) {
    print "<pre>";
    print_r($aArgs);
    print "</pre>";
}

// renders a paginated list for the site admin
function renderMyMemberList($aData='') {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table class="T" width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="50%" class="header"><div class="listrow"><strong>Item Name</strong></div></td>
        <td width="25%" class="header"><div class="listrow"><strong>Created</strong></div></td>
        <td width="15%" class="header"><div class="listrow"><?php if ($iPerm > 2) { ?><strong>Actions</strong><?php } ?></div></td>
    </tr>
</table>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td class="dotrule" colspan="5"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td width="2%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php if ($iPerm > 2) { ?><a href="index.php?op=<?php print $aState[$aData[$i]["Status"]] ?>&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();"><?php } ?><img src="../../_img/icon_status_<?php print $aData[$i]["Status"] ?>.gif" width="16" height="10" alt="" border="0" /><?php if ($iPerm > 2) { ?></a><?php } ?></div></td>
        <td width="50%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print format($aData[$i]["Name"]) ?></div></td>
        <td width="25%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print date("Y-m-d H:i:s", $aData[$i]["Created"]) ?></div></td>
        <td width="15%" bgcolor="#<?php print $bg ?>"><?php if ($iPerm > 1) { ?><a href="form.php?op=edit&id=<?php print $aData[$i]["Id"] ?>">EDIT</a><?php } ?></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="5"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php renderPaging($iCursor, count($aData)) ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>
<?php


// renders a paginated list for the site admin
function renderList($aData='') {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table class="T" width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="50%" class="header"><div class="listrow"><strong>Item Name</strong></div></td>
        <td width="25%" class="header"><div class="listrow"><strong>Created</strong></div></td>
        <td width="15%" class="header"><div class="listrow"><?php if ($iPerm > 2) { ?><strong>Actions</strong><?php } ?></div></td>
    </tr>
</table>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td class="dotrule" colspan="5"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td width="2%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php if ($iPerm > 2) { ?><a href="index.php?op=<?php print $aState[$aData[$i]["Status"]] ?>&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();"><?php } ?><img src="../../_img/icon_status_<?php print $aData[$i]["Status"] ?>.gif" width="16" height="10" alt="" border="0" /><?php if ($iPerm > 2) { ?></a><?php } ?></div></td>
        <td width="50%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print format($aData[$i]["Name"]) ?></div></td>
        <td width="25%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print date("Y-m-d H:i:s", $aData[$i]["Created"]) ?></div></td>
        <td width="15%" bgcolor="#<?php print $bg ?>"><?php if ($iPerm > 1) { ?><a href="form.php?op=edit&id=<?php print $aData[$i]["Id"] ?>">EDIT</a>&nbsp;|<?php if ($aData[$i]["Name"] != "Admin") { ?>&nbsp;<a href="index.php?op=del&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();">DELETE</a><?php } else { ?>&nbsp;DELETE<?php } ?><?php } ?></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="5"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php renderPaging($iCursor, count($aData)) ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>

<?php


// renders a paginated list for the site admin
function renderMembers($aData='') {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table class="T" width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="50%" class="header"><div class="listrow"><strong>Item Name</strong></div></td>
        <td width="25%" class="header"><div class="listrow"><strong>Created</strong></div></td>
        <td width="15%" class="header"><div class="listrow"><?php if ($iPerm > 2) { ?><strong>Actions</strong><?php } ?></div></td>
    </tr>
</table>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td class="dotrule" colspan="5"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td width="2%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php if ($iPerm > 2) { ?><a href="members.php?op=<?php print $aState[$aData[$i]["Status"]] ?>&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();"><?php } ?><img src="../../_img/icon_status_<?php print $aData[$i]["Status"] ?>.gif" width="16" height="10" alt="" border="0" /><?php if ($iPerm > 2) { ?></a><?php } ?></div></td>
        <td width="50%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print format($aData[$i]["Name"]) ?></div></td>
        <td width="25%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print date("Y-m-d H:i:s", $aData[$i]["Created"]) ?></div></td>
        <td width="15%" bgcolor="#<?php print $bg ?>"><?php if ($iPerm > 1) { ?><a href="form.php?op=edit&id=<?php print $aData[$i]["Id"] ?>">EDIT</a>&nbsp;|<?php if ($aData[$i]["Name"] != "Admin") { ?>&nbsp;<a href="members.php?op=del&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();">DELETE</a><?php } else { ?>&nbsp;DELETE<?php } ?><?php } ?></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="5"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php renderPaging($iCursor, count($aData)) ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>


<?php
function renderList1($iCnt=0, $aData='', $iComId=0) {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table class="T" width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="2%" class="header"><div class="listrow"><strong></strong></div></td>
        <td width="5%"  class="header"><div class="listrow"><strong></strong></div></td>
        <td width="50%" class="header"><div class="listrow"><strong>Item Name</strong></div></td>
        <td width="25%" class="header"><div class="listrow"><strong>Created</strong></div></td>
        <td width="15%" class="header"><div class="listrow"><?php if ($iPerm > 2) { ?><strong>Actions</strong><?php } ?></div></td>
    </tr>
</table>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td class="dotrule" colspan="5"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td width="2%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php if ($iPerm > 2) { ?><a href="index.php?op=<?php print $aState[$aData[$i]["Status"]] ?>&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();"><?php } ?><img src="../../_img/icon_status_<?php print $aData[$i]["Status"] ?>.gif" width="16" height="10" alt="" border="0" /><?php if ($iPerm > 2) { ?></a><?php } ?></div></td>
        <td width="5%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><a onclick="return showDownLoadCat(<?php print $aData[$i]["Id"] ?>,<?php print $iComId ?>);"><img src="../../_img/buttons/i.p.dload.gif" alt="Download" border="0" /></div></td>
        <td width="50%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print format($aData[$i]["Name"]) ?></div></td>
        <td width="25%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print date("Y-m-d H:i:s", $aData[$i]["Created"]) ?></div></td>
        <td width="15%" bgcolor="#<?php print $bg ?>"><?php if ($iPerm > 1) { ?><a href="form.php?op=edit&id=<?php print $aData[$i]["Id"] ?>">EDIT</a>&nbsp;|&nbsp;<a href="index.php?op=del&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();">DELETE</a><?php } ?></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="5"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php renderPaging($iCursor, $iCnt) ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>
<?php

// renders a paginated list for the site admin
function renderList2($iCnt=0, $aData='', $iComId=0) {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table class="T" width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="45%" colspan="2" class="header"><div class="listrow"><strong>Item Name</strong></div></td>
        <td width="35%" colspan="2" class="header"><div class="listrow"><strong>Company</strong></div></td>
        <td width="20%" class="header"><div class="listrow"><strong>Created</strong></div></td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        
        !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
    ?>
    <tr>
        <td width="5%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><a onclick="return showDownLoadCat(<?php print $aData[$i]["Id"] ?>,<?php print $iComId ?>,0);"><img src="../../_img/buttons/i.p.dload.gif" alt="Download" border="0" /></div></td>
        <td width="40%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><a href="form.php?op=view&id=<?php print $aData[$i]["Id"] ?>"><?php print format($aData[$i]["Name"]) ?></a></div></td>
        <td width="30%" bgcolor="#<?php print $bg ?>"><?php print format($aData[$i]["Company"]) ?></td>
        <td width="25%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print date("Y-m-d H:i:s", $aData[$i]["Created"]) ?></div></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php // renderPaging($iCursor, $iCnt) ?>
<?php } else { ?>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>
<?php
// renders a paginated list for the site admin
function renderStyles2($iCnt=0, $aData='', $sVar='', $sComId='') {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<?php renderPaging($iCursor, $iCnt, $sVar, "Styles") ?>
<table class="T" width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="348" colspan="2" class="header"><div class="listrow"><strong>Item Name</strong></div></td>
        <td width="170" class="header"><div class="listrow"><strong>Created</strong></div></td>
    </tr>
</table>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td class="dotrule" colspan="4"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td width="5%" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><a onclick="return showDownLoadCat(<?php print $aData[$i]["Id"] ?>,<?php print $sComId ?>,1);"><img src="../../_img/buttons/i.p.dload.gif" alt="Download" border="0" /></div></td>
        <td width="332" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><a href="styles.php?op=view&id=<?php print $aData[$i]["Id"] ?>"><?php print format($aData[$i]["Name"]) ?></a></div></td>
        <td width="170" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print date("Y-m-d H:i:s", $aData[$i]["Created"]) ?></div></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="4"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php renderPaging($iCursor, $iCnt, $sVar, "Styles") ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>
<?php
// renders a paginated list for the site admin
function renderStyles($iCnt=0, $aData='', $sVar='') {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<?php renderPaging($iCursor, $iCnt, $sVar, "Styles") ?>
<table class="T" width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="348" colspan="2" class="header"><div class="listrow"><strong>Item Name</strong></div></td>
        <td width="170" class="header"><div class="listrow"><strong>Created</strong></div></td>
        <td width="90" class="header"><div class="listrow"><?php if ($iPerm > 2) { ?><strong>Actions</strong><?php } ?></div></td>
    </tr>
</table>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td class="dotrule" colspan="4"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td width="16" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php if ($iPerm > 2) { ?><a href="formops.php?op=<?php print $aState[$aData[$i]["Status"]] ?>&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();"><?php } ?><img src="../../_img/icon_status_<?php print $aData[$i]["Status"] ?>.gif" width="16" height="10" alt="" border="0" /><?php if ($iPerm > 2) { ?></a><?php } ?></div></td>
        <td width="332" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print format($aData[$i]["Name"]) ?></div></td>
        <td width="170" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print date("Y-m-d H:i:s", $aData[$i]["Created"]) ?></div></td>
        <td width="90" bgcolor="#<?php print $bg ?>"><?php if ($iPerm > 1) { ?><a href="styles.php?op=edit&id=<?php print $aData[$i]["Id"] ?>">EDIT</a>&nbsp;|&nbsp;<a href="formops.php?op=del&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();">DELETE</a><?php } ?></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="4"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php renderPaging($iCursor, $iCnt, $sVar, "Styles") ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>
<?php
// renders a paginated list for the site admin
function renderItems($iCnt=0, $aData='', $sVar='') {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<?php renderPaging($iCursor, $iCnt, $sVar, "Items") ?>
<table class="T" width="95%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="348" colspan="2" class="header"><div class="listrow"><strong>Item Name</strong></div></td>
        <td width="170" class="header"><div class="listrow"><strong>Created</strong></div></td>
        <td width="90" class="header"><div class="listrow"><?php if ($iPerm > 2) { ?><strong>Actions</strong><?php } ?></div></td>
    </tr>
</table>
<table width="95%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td class="dotrule" colspan="4"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("iact", "ideact");
    ?>
    <tr>
        <td width="16" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php if ($iPerm > 2) { ?><a href="formops.php?op=<?php print $aState[$aData[$i]["Status"]] ?>&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();"><?php } ?><img src="../../_img/icon_status_<?php print $aData[$i]["Status"] ?>.gif" width="16" height="10" alt="" border="0" /><?php if ($iPerm > 2) { ?></a><?php } ?></div></td>
        <td width="332" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print format($aData[$i]["Name"]) ?></div></td>
        <td width="170" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print date("Y-m-d H:i:s", $aData[$i]["Created"]) ?></div></td>
        <td width="90" bgcolor="#<?php print $bg ?>"><?php if ($iPerm > 1) { ?><a href="edititem.php?op=edit&id=<?php print $aData[$i]["Id"] ?>">EDIT</a>&nbsp;|&nbsp;<a href="formops.php?op=idel&id=<?php print $aData[$i]["Id"] ?>" onclick="return verify();">DELETE</a><?php } ?></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="4"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php renderPaging($iCursor, $iCnt, $sVar, "Items") ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>
<?php
// renders a paginated list for the site admin
function renderItems2($iCnt=0, $aData='', $sVar='') {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table class="T" width="95%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="348" colspan="2" class="header"><div class="listrow"><strong>Item Name</strong></div></td>
        <td width="170" class="header"><div class="listrow"><strong>Created</strong></div></td>
        <td width="90" class="header"><div class="listrow"><?php if ($iPerm > 2) { ?><strong>Actions</strong><?php } ?></div></td>
    </tr>
</table>
<?php renderPaging($iCursor, $iCnt, $sVar, "Items") ?>
<table width="95%" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td class="dotrule" colspan="4"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("iact", "ideact");
    ?>
    <tr>
        <td width="332" bgcolor="#<?php print $bg ?>"><a href="edititem.php?op=view&id=<?php print $aData[$i]["Id"] ?>"><?php print format($aData[$i]["Name"]) ?></a></td>
        <td width="170" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print date("Y-m-d H:i:s", $aData[$i]["Created"]) ?></div></td>
     </tr>
    <tr>
        <td class="dotrule" colspan="4"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php renderPaging($iCursor, $iCnt, $sVar, "Items") ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>
<?php

// sets a generic pagination element
function renderPaging($iCursor=0, $iCnt=0, $sVar='', $sWhat='') {
?>
    <?php if ($iCnt > ROWCOUNT) { ?>
<table width="100%" border="0" cellpadding="0" cellspacing="0">    
    <tr>
        <td align="right"><div class="paging">
        <!--| paging |-->
        
        
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="15"><?php if ($iCursor > ROWCOUNT) { ?><a href="<?php print SELF ?>?cursor=<?php print $iCursor - ROWCOUNT ?><?php print $sVar ?>"><img src="../../_img/buttons/btn_prev.gif" width="15" height="15" alt="" border="0" /><?php } else { ?><img src="../../_img/buttons/btn_prev_null.gif" width="15" height="15" alt="" border="0" /><?php } ?></a></td>
                <td width="5"><img src="../../_img/spc.gif" width="5" height="1" alt="" border="0" /></td>
                <td><?php print $iCnt." ".$sWhat ?> - Page  of <?php if (round($iCnt / ROWCOUNT) < $iCnt / ROWCOUNT){ echo round($iCnt / ROWCOUNT) + 1; } else {echo round($iCnt / ROWCOUNT);} ?></td>
                <td width="5"><img src="../../_img/spc.gif" width="5" height="1" alt="" border="0" /></td>
                <td width="15"><?php if ($iCursor < $iCnt) { ?><a href="<?php print SELF ?>?cursor=<?php print $iCursor + ROWCOUNT ?><?php print $sVar ?>"><img src="../../_img/buttons/btn_next.gif" width="15" height="15" alt="" border="0" /><?php } else { ?><img src="../../_img/buttons/btn_next_null.gif" width="15" height="15" alt="" border="0" /><?php } ?></a></td>
            </tr>
        </table>
        
        <!--| paging |-->
        </div></td>
    </tr>
</table>
<br />
    <?php } // end condition ?>
<?php
} // end function

// renders a list of list box options
function renderOptions($aData='', $sOpt='') {
    
    global $iCursor, $iPerm;
    ?>
    <OPTION VALUE="" >
    <?php
 if (is_array($aData)) { 
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        if (!strcmp($sOpt, $aData[$i]["val"])) {
    ?>
                            <OPTION selected VALUE="<?php print $aData[$i]["val"] ?>" > <?php print $aData[$i]["des"]."\n"; ?>
    <?php
        } else {
    ?>
                            <OPTION VALUE="<?php print $aData[$i]["val"] ?>" > <?php print $aData[$i]["des"]."\n"; ?>
    <?php
        
        } //end if
        ++$i;
    } // end loop
 } 
 ?>
<?php } // end function ?>
<?php

// renders a paginated list for the site admin
function renderSupportList($aData='') {

    global $iCursor, $iPerm;

?>
<br />
<?php if (is_array($aData)) { ?>
      <style type="text/css">
          td.hrow {
              background-color: #C6D7CF;
              padding-right: 4pt;
              padding-left: 4pt;
              padding-top: 2pt;
              padding-bottom: 1pt;
              border-width:1px;
              border-style:solid;
              border-top-width:0;
              border-bottom-width:0;
              border-right-width:0;
              font-size : 12px;
              color: #115B99;
              font-weight: bold;
              text-align:center;
              text-decoration:none;
              border-color: #FFFFFF;
          }
          td.orow {
              background-color: #E8E8CF;
              padding-right: 4pt;
              padding-left: 4pt;
              padding-top: 2pt;
              padding-bottom: 1pt;
              border-width:1px;
              border-style:solid;
              border-top-width:0;
              border-bottom-width:0;
              border-right-width:0;
              font-size : 11px;
              border-color: #FFFFFF;
          }
          td.erow {
              background-color: #E8DCCF;
              padding-right: 4pt;
              padding-left: 4pt;
              padding-top: 2pt;
              padding-bottom: 1pt;
              border-width:1px;
              border-style:solid;
              border-top-width:0;
              border-bottom-width:0;
              border-right-width:0;
              font-size : 11px;
              border-color: #FFFFFF;
          }
      </style>
<table class="T" width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td class="hrow" width="5%"><span>Ticket</span></td>
        <td class="hrow"><span>Subject</span></td>
        <td class="hrow" width="10%"><span>From</span></td>
        <td class="hrow" width="18%"><span>Date</span></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        $bg = "e";
        // !strcmp("e", $bg) ? $bg = "o" : $bg = "e";
        $aState = array("act", "deact");

    ?>
                  <tr class="content" onmouseout="this.className='content'" onmouseover="this.className='mouseover'" onclick="location.href='form.php?id=<?php print $aData[$i]["Id"] ?>'">
   
                     <td class="<?php print $bg ?>row" style="cursor: pointer;" onclick="openentry(70);" align="center"><?php print $aData[$i]["Id"] ?></td>
                     <td class="<?php print $bg ?>row" style="cursor: pointer;" onclick="openentry(70);" align="left">&nbsp;<?php print $aData[$i]["support_title"] ?></td>
                     <td class="<?php print $bg ?>row" style="cursor: pointer;" onclick="openentry(70);" align="left">&nbsp;<?php print $aData[$i]["user_login"] ?>&nbsp;</td>
                     <td class="<?php print $bg ?>row" style="cursor: pointer;" onclick="openentry(70);" align="center" nowrap="nowrap"><?php print date("Y-m-d H:i:s", $aData[$i]["log_date"]) ?></td>
                  </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php renderPaging($iCursor, count($aData)) ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>
<?php

///////////////////////////////////////////////////////////////////////////
// forums functions
///////////////////////////////////////////////////////////////////////////
// write an element of the topic list
function writeTopicLink($topicId,$topicTitle,$replyCount) {
    global $aSess;
?>
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="left">
                    <div class="section">
<?php if( count($aSess) ) { ?>
                    <input type="checkbox" name="favorite[]" value="<?php print $topicId; ?>" />
                    <img src="../../_img/spc.gif" width="10" height="1" alt="" border="0" />
<?php } ?>
                    <a href="showtopic.php?topicId=<?php print $topicId; ?>"><?php print format($topicTitle); ?></a>
                    </td>
                    <td align="right">
             [ <?php print $replyCount; ?> replies ]</div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="dotrule"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
<?php
}
// write a topic element
function writeTopic($sPoster,$sDate,$iReplies,$sTitle,$sText) {
?>
    <tr>
        <td><div class="forumhead">
        [ posted by <?php print $sPoster; ?> on <?php print $sDate; ?> ] [ <?php print $iReplies; ?> replies ]<br />
        <br />
        <b><?php print format($sTitle); ?></b><br />
        <br />
        <?php print format($sText); ?>     
        </div></td>
    </tr>
    <tr>
        <td class="dotrule"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
<?php
}
// write a reply element
function writeReply($sPoster,$sDate,$sReply,$bDelete = FALSE, $iReplyId = -1) {
?>
    <tr>
        <td><div class="forumreplyhead">
        [ posted by <?php print $sPoster; ?> on <?php print $sDate; ?> ]
        </div>
        <div class="copy">
        <?php print format($sReply); ?>     
        </div>
<?php if( $bDelete ) { ?>
        <br>
        <div class="error">Delete:: <input type="checkbox" name="deletereply[]" value="<?php print $iReplyId; ?>"></div>
<?php } ?>
        </td>
    </tr>
    <tr>
        <td class="dotrule"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
<?php
}
function activate($sID='') {
?>
<P><?php print $sID; ?></p>
<?php
}

///////////////////////////////////////////////////////////////////////////
// end of forums functions
///////////////////////////////////////////////////////////////////////////
// renders a paginated list for the site admin
function renderProfiles($aData='') {
    
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table class="T" width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="50" class="header"><div class="listrow"><strong>Type</strong></div></td>
        <td width="70" class="header"><div class="listrow"><strong>Status</strong></div></td>
        <td width="300" class="header"><div class="listrow"><strong>Profile Name</strong></div></td>
        <td width="170" class="header"><div class="listrow"><strong>Created</strong></div></td>
        <td width="90" class="header"><div class="listrow"><?php if ($iPerm > 2) { ?><strong>Actions</strong><?php } ?></div></td>
    </tr>
</table>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td class="dotrule" colspan="5"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    while ($i < count($aData)) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
        $aState = array("act", "deact");
    ?>
    <tr>
        <td width="40" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><center><?php if ($aData[$i]["Type"] == 0) { Print "OB"; } elseif ($aData[$i]["Type"] == 1) { Print "IB"; } elseif ($aData[$i]["Type"] == 2) { Print "CAT"; } else { Print "Pend"; }?></center></div></td>
        <td width="60" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><center><img src="../../_img/icon_status_<?php print $aData[$i]["Status"] ?>.gif" width="16" height="10" alt="" border="0" /></center></div></td>
        <td width="300" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print format($aData[$i]["Name"]) ?></div></td>
        <td width="170" bgcolor="#<?php print $bg ?>"><div class="listrow<?php print $aData[$i]["Status"] ?>"><?php print date("Y-m-d H:i:s", $aData[$i]["Created"]) ?></div></td>
        <td width="90" bgcolor="#<?php print $bg ?>"><?php if ($iPerm > 1) { ?><a href="form.php?op=view&id=<?php print $aData[$i]["Id"] ?>">VIEW</a>&nbsp;<?php } ?></td>
    </tr>
    <tr>
        <td class="dotrule" colspan="5"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data avaiable for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>
<?php
///////////////////////////////////////////////////////////////////////////
// Reports functions
///////////////////////////////////////////////////////////////////////////
// write an element of the topic list
function writeReportMain($aData) {
?>
<form action="" method="post" name="searchList" id="searchList">
<table width="758" border="0" cellpadding="0" cellspacing="0">
    <tr>
    <input type=hidden name=alDir value=TRUE ID=Hidden1>
    <select name="lDir"  style="font-size: 8pt" ID="Select1">
    <option value=0>Inbound</option>
    <option value=1>Outbound</option>
     </tr>
    <tr>
    <input type=hidden name=alWhat value=TRUE ID=Hidden1>
    <select name="lWhat"  style="font-size: 8pt" ID="Select1">
    <option value="A">EDI Activity Report</option>
    <option value="F">EDI F/A Activity Report</option>
     </tr>
     <tr>
    <input type=hidden name=alWho value=TRUE ID=Hidden1>
    <select name="lWho"  style="font-size: 8pt" ID="Select1">
<?php
// loop through data and conditionally display functionality and content
$i = 0;
$current = getdate();
if ($current["mday"] < 10) { 
   $current["mday"] = "0" . $current["mday"]; 
}
if ($current["mon"] < 10) { 
   $current["mon"] = "0" . $current["mon"]; 
}
while ($i < count($aData)) {
?>
    <option value="<?php Print $aData[$i]["PartnerKEY"]; ?>"><?php Print $aData[$i]["PartnerName"]; ?></option>
<?php
++$i;
} // end loop
?>
     </tr>
     <table>
     <tr>
    <td>Year</td>
    <td>Month</td>
    <td>Day</td>
    <td>Hour</td>
    <td>Min</td>
    <td>Sec</td>
    <td></td>
     </tr>
     <tr>
    <td><INPUT type="text" name="DateStartY" size="4" value="<?php Print $current[year]; ?>" onKeyUp="KeyUp(this,event,4,'searchList.DateStartM.focus()')"></td>
    <td><INPUT type="text" name="DateStartM" size="2" value="<?php Print $current[mon];  ?>" onKeyUp="KeyUp(this,event,2,'searchList.DateStartD.focus()')"></td>
    <td><INPUT type="text" name="DateStartD" size="2" value="<?php Print $current[mday]; ?>" onKeyUp="KeyUp(this,event,2,'searchList.DateStartHH.focus()')"></td>
    <td><INPUT type="text" name="DateStartHH" size="2" value="00" onKeyUp="KeyUp(this,event,2,'searchList.DateStartMM.focus()')"></td>
    <td><INPUT type="text" name="DateStartMM" size="2" value="00" onKeyUp="KeyUp(this,event,2,'searchList.DateStartSS.focus()')"></td>
    <td><INPUT type="text" name="DateStartSS" size="2" value="01" onKeyUp="KeyUp(this,event,2,'searchList.DateStopY.focus()')"></td>
    <td><A>Start Date</A></td>
     </tr>
     <tr>
    <td><INPUT type="text" name="DateStopY" size="4" value="<?php Print $current[year]; ?>" onKeyUp="KeyUp(this,event,4,'searchList.DateStopM.focus()')"></td>
    <td><INPUT type="text" name="DateStopM" size="2" value="<?php Print $current[mon];  ?>" onKeyUp="KeyUp(this,event,2,'searchList.DateStopD.focus()')"></td>
    <td><INPUT type="text" name="DateStopD" size="2" value="<?php Print $current[mday]; ?>" onKeyUp="KeyUp(this,event,2,'searchList.DateStopHH.focus()')"></td>
    <td><INPUT type="text" name="DateStopHH" size="2" value="23" onKeyUp="KeyUp(this,event,2,'searchList.DateStopMM.focus()')"></td>
    <td><INPUT type="text" name="DateStopMM" size="2" value="59" onKeyUp="KeyUp(this,event,2,'searchList.DateStopSS.focus()')"></td>
    <td><INPUT type="text" name="DateStopSS" size="2" value="59" onKeyUp="KeyUp(this,event,2,'searchList.go.focus()')"></td>

    <td><A>Stop Date</A></td>
     </tr>
     <table>
     <tr align="Center" style="width:50%;">
    <td>
    <table border="0">
    <tr>
        <td class="dotrule" colspan="4"><img src="../../_img/spc.gif" width="1" height="15" alt="" border="0" /></td>
    </tr>
        <tr>
            <td><input type="image" src="../../_img/buttons/btn_submit.gif" width="58" height="15" alt="" border="0" onClick="showViewReportDetails()"></td>
        </tr>
    </table>
    </td>
     </tr>
</table>
</form>
<?php
}


function renderSiltronicTrans($iCnt=0, $aData='', $iUserID) {
    global $iCursor, $iPerm;
?>
<br />
<?php if (is_array($aData)) { ?>
<table class="T" width="758" border="0" cellpadding="0" cellspacing="0" id="dataTable">
    <thead>
        <tr>
            <th class="header"><center><div class="listrow"><strong>Trans ID</strong></div></center></th>
            <th class="header"><center><div class="listrow"><strong>Shipment Number</strong></div></center></th>
            <th class="header"><center><div class="listrow"><strong>Delivery Number</strong></div></center></th>
            <th class="header"><center><div class="listrow"><strong>Customer Name</strong></div></center></th>
            <th class="header"><center><div class="listrow"><strong>Country</strong></div></center></th>
            <th class="header"><center><div class="listrow"><strong>Message ID</strong></div></center></th>
            <th class="header"><center><div class="listrow"><strong>Load Timestamp</strong></div></center></th>
        </tr>
    </thead>
    <?php
    // loop through data and conditionally display functionality and content
    $i = 0;
    
    foreach($aData as $row) {
        $bg = "FFFFFF";
        // !strcmp("FFFFFF", $bg) ? $bg = "C6AADB" : $bg = "FFFFFF";
    ?>
    <tr>
        <td class="td2" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $row["EDIATransID"]; ?></div></center></td>
        <td class="td2" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $row["TransNumber"]; ?></div></center></td>
        <td class="td2" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $row["DetailReference"]; ?></div></center></td>
        <td class="td2" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $row["Address_Name"]; ?></div></center></td>
        <td class="td2" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $row["Country"]; ?></div></center></td>
        <td class="td2" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $row["DetailRefDescription"]; ?></div></center></td>
        <td class="td2" bgcolor="#<?php print $bg ?>"><center><div class="listrow"><?php Print $row["TransDate"]; ?></div></center></td>
    </tr>
    <?php
        ++$i;
    } // end loop
    ?>
</table>
<?php renderPaging($iCursor, $iCnt) ?>
<?php } else { ?>
<table width="608" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>There is currently no data available for this section.</td>
    </tr>
</table>
<?php } // end condition ?>
<?php } // end function ?>