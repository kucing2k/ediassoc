<?php

// File Location: /_lib/_base/config.php

/** 
 * Assign configuration variables
 *
 * @author Mike Buzzard <src@cubancouncil.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright Cuban Council
 *
 */

/* set version number */
define("VERSION", "1.0");                              // apps version

/* assign php configuration variables */
ini_set("track_errors", "1");                          // error tracking

/* assign base system constants */
define("SITE_URL", "http://192.168.56.101");    // base site url
define("SITE_DIR", "/");                               // base site directory
define("BASE_DIR", $_SERVER["DOCUMENT_ROOT"]);         // base file directory
define("SELF", $_SERVER["PHP_SELF"]);                  // self file directive
define("FILEMAX", 1500000);                            // file size max
//define("GEN_DOCS", "d:/gensrvnt/");                    // Gentran data root directory
define("CDL_DOCS", "C:/wwwroot/home/core/mycat/files/");    // Catalog Download root directory

/* assign base database constants */
define("PREFIX", "edia");                              // database table prefix
define("TIMEOUT", 3600);                               // timeout (seconds)
define("ROWCOUNT", 25);                                // rows to show per page
define("DSN", "mysql://ediassoc:x9NNnjc9XFVFB8tt@192.168.56.1/ediassoc");         // DSN for PEAR usage
define('DB_PDO_DSN', "sqlsrv:server=(local);Database=ediassoc");
define('DB_PDO_USER', 'sa');
define('DB_PDO_PASS', 'dodolipet');
// define("DSN", "mssql://sa:ca555e@192.168.1.7/EDIA");         // DSN for PEAR usage

/* assign base mail constants */
define("SMTP_HOST", "localhost");                      // SMTP hostname
define("SMTP_PORT","25");                              // SMTP port default=25
define("FROM_NAME","EDIAnywhere");                        // newsletter from name
define("FROM_EMAIL","edianywhere@ediassociates.com");         // newsletter from email

/* assign base entity constants */
define("TITLE", "EDIAssociates.com");                  // base page title
define("ENTITY", "EDI Associates");                    // entity name
define("EMAIL", "support@ediassociates.com");          // admin email

define('HITS_FILE', "C:/ediassoc/hits.txt");

/* assign instance variables */
$EXCEPTS = array();                                    // exceptions array
$ERRORS = array();                                     // errors array
$FILES = array("jpg", "gif", "png");                   // file types array
$FORMOK = true;                                       // form status

set_magic_quotes_runtime(1);                           // magic quotes on
