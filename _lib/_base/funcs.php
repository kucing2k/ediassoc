<?php

// File Location: /_lib/_base/funcs.php

/** 
 * Common utility functions
 *
 * @author Mike Buzzard <src@cubancouncil.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright Cuban Council
 *
 */

require_once("config.php");

// catch system exception
function catchExc($sMsg) {
    
    global $EXCEPTS;
    array_push($EXCEPTS, $sMsg);
}

// catch page error
function catchErr($sMsg) {
    
    global $ERRORS;
    array_push($ERRORS, $sMsg);
}

// cleans a string
function clean($sStr) {
    
    $return = stripslashes($sStr);
    $return = htmlentities($return);
    return $return;
}

// add links to a string
function addLinks($sStr) {
    
    $return = preg_replace("/((http(s?):\/\/)|(www\.))([\w\.]+)([\w|\/]+)/i", "<a href=\"http$3://$4$5$6\" target=\"_blank\">$4$5$6</a>", $sStr);
    $return = preg_replace("/([\w|\.|\-|_]+)(@)([\w\.]+)([\w]+)/i", "<a href=\"mailto:$0\">$0</a>", $return);
    return $return;
}

// formats a string
function format($sStr) {
    
    $return = clean($sStr);
    $return = nl2br($return);
    $return = addLinks($return);
    return $return;
}

// get image properties
function getImage($sFile) {
    
    $aParts = getimagesize($sFile);
    $return["x"] = $aParts[0];
    $return["y"] = $aParts[1];
    $return["type"] = $aParts[2];
    $return["properties"] = $aParts[3];
    return $return;
}

    function currency($c_f_x)	//THIS IS THE START OF THE CURRENCY FUNCTION
    				//THIS FUNCTION WILL TAKE A PARAMETER WHICH IS
    				//A FLOAT AND WILL RETURN THAT ANSWER IN THE 
    				//FORMAT OF CURRENCY 
    				//EXAMPLE 235.23164 BECOMES $235.23
    {
    	$c_f_x = round($c_f_x,2);	//THIS WILL ROUND THE ACCEPTED PARAMETER TO THE 
    					//PRECISION OF 2		
    	$temp_c_f_variable = strstr(round($c_f_x,2),".");	//THIS WILL ASSIGN THE "." AND WHAT EVER 
    								//ELSE COMES AFTER IT. REMEMBER DUE TO 
    								//ROUNDING THERE ARE ONLY THREE THINGS
    								//THIS VARIABLE CAN CONTAIN, A PERIOD 
    								//WITH ONE NUMBER, A PERIOD WITH TWO NUMBERS,
    								//OR NOTHING AT ALL
    								//EXAMPLE : ".1",".12",""
    	if (strlen($temp_c_f_variable) == 2)	//THIS IF STATEMENT WILL CHECK TO SEE IF 
    						//LENGTH OF THE VARIABLE IS EQUAL TO 2. IF
    						//IT IS, THEN WE KNOW THAT IT LOOKS LIKE 
    						//THIS ".1" SO YOU WOULD ADD A ZERO TO GIVE IT 
    						// A NICE LOOKING FORMAT 
    	{
    		$c_f_x = $c_f_x."0";
    	}
    	if (strlen($temp_c_f_variable) == 0)	//THIS IF STATEMENT WILL CHECK TO SEE IF 
    						//LENGTH OF THE VARIABLE IS EQUAL TO 2. IF
    						//IT IS, THEN WE KNOW THAT IT LOOKS LIKE 
    						//THIS "" SO YOU WOULD ADD TWO ZERO'S TO GIVE IT 
    						// A NICE LOOKING FORMAT
    	{
    		$c_f_x = $c_f_x.".00";
    	}
    	$c_f_x = "$".$c_f_x;	//THIS WILL ADD THE "$" TO THE FRONT 
    	return $c_f_x;	//THIS WILL RETURN THE VARIABLE IN A NICE FORMAT
    			//BUT REMEMBER THIS NEW VARIABLE WILL BE A STRING 
    			//THEREFORE CAN BE USED IN ANY FURTHER CALCULATIONS
    		
    }	//THIS IS THE END OF THE CURRENCY FUNCTION

    Function formatPH($ph) {
        $ph = ereg_replace ('[^0-9]+', '', $ph); // ##### Strip all Non-Numeric Characters
        $phlen = strlen($ph);
        switch (TRUE)
          {
          case ($phlen == 0):
            $ext = $ph;
            return $ph;
          case ($phlen < 7):
            $ext = $ph;
            break;
          case ($phlen == 7):
            sscanf($ph, "%3s%4s", $pfx, $exc);
            break;
          case ($phlen > 7 AND $phlen < 10):
            sscanf($ph, "%3s%4s%s", $pfx, $exc, $ext);
            break;
          case ($phlen == 10):
            sscanf($ph, "%3s%3s%4s", $area, $pfx, $exc);
            break;
          case ($phlen == 11):
            sscanf($ph, "%1s%3s%3s%4s", $cty, $area, $pfx, $exc);
            break;
          case ($phlen > 11):
            sscanf($ph, "%1s%3s%3s%4s%s", $cty, $area, $pfx, $exc, $ext);
            break;
          }
        $out = '';
        $out .= isset($cty) ? $cty.' ' : '';
        $out .= isset($area) ? '('.$area.') ' : '';
        $out .= isset($pfx) ? $pfx.' - ' : '';
        $out .= isset($exc) ? $exc.' ' : '';
        $out .= isset($ext) ? 'x'.$ext : '';
        return $out;
    }

    function RemoveShouting($string) {
        $lower_exceptions = array( "to" => "1", "a" => "1", "the" => "1", "of" => "1" );
                                            
        $higher_exceptions = array(
              "I" => "1", "II" => "1", "III" => "1", "IV" => "1",
              "V" => "1", "VI" => "1", "VII" => "1", "VIII" => "1",
              "XI" => "1", "X" => "1"
        );
       
        $words = split(" ", $string);
        $newwords = array(); 
        
        foreach ($words as $word)
        {
              if (!$higher_exceptions[$word])
                      $word = strtolower($word);
              if (!$lower_exceptions[$word])
                      $word = ucfirst($word);
                array_push($newwords, $word);
        
        }
              
        return join(" ", $newwords);  
    } 

//    function fputcsv ($fi, $array, $deliminator=",") { 
    // function fputcsv ($array, $deliminator=",") { 
    
//        $line = ""; 
//        $fi=str_replace("/", "\\", $fi);
//        $fp = fopen($fi,"a");
//        foreach($array as $val) { 
//            $val = str_replace("\r\n", "\n", $val); 
//            
//                if(ereg("[$deliminator\"\n\r]", $val)) { 
//                    $val = '"'.str_replace('"', '""', $val).'"'; 
//                }
//            
//            $line .= $val.$deliminator; 
//        
//        }
//        
//        $line = substr($line, 0, (strlen($deliminator) * -1)); 
//        $line .= "\r\n"; 
//        
//        return fputs($fp, $line); 
//    
//    }

    /*
    $date - date (mm/dd/yyyy)
    $n - number of days
    $t - resulting timestamp
    */

    function getNthWorkingDay($date,$n,&$t)
    {
       $arr = array();
       list($month,$day,$year) = explode("/",$date);
    
       $ts = mktime(0,0,0,$month,$day,$year);
    
       for($i = 1; $i <= $n; $i++)
       {
           $_ts = mktime(0,0,0,$month,$day + $i,$year);
           $arr[$i - 1]["d"] = date("w",$_ts);
           $arr[$i - 1]["ts"] = $_ts;
       }
    
       $working_days = array();
       $j = 0;
    
       for($i = 1; $i <= $n; $i++)
       {
           if($arr[$i - 1]["d"] != 6 AND $arr[$i - 1]["d"] != 0)
           {
               $working_days[$j]["d"] = $arr[$i - 1]["d"];
               $working_days[$j]["ts"] = $arr[$i - 1]["ts"];
               $j++;
           }
       }
    
       if(sizeOf($working_days) >= $n)
       {
           $t = $working_days[sizeOf($working_days) - 1]["ts"];
       }
       else
       {
           $_n = $n - sizeOf($working_days);
           
           // return getNthWorkingDay(date("m/d/Y",$arr[$n-1]["ts"]),$_n,$t);
           $return = date("m/d/Y",$arr[$n-1]["ts"]);
           return $return;
       }
    }

// right() returns the substring beginning at $numchars characters from the right end of a string.
function right($str, $numchars) {
   return substr($str, strlen($str)-$numchars);
}

// left() returns the first $numchars characters of a string.
function left($str, $numchars) {
   return substr($str, 0, $numchars);
}

function num_pad($variable, $size) {
    while (strlen($variable) < $size) {
        $variable = "0" . $variable;
    }
    return $variable;
}

function getFileName() {

    $today = getdate(); 
    $myfile = $today[year].$today[yday].$today[hours].$today[minutes].$today[seconds];
    $numochars = 4;
    
    /* The following characters will be randomly picked, you can add more characters below. */
    $letter = "abcdefghijklmnopqrstuvwxyz1234567890";
    
       $ar = array
       ("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t",
        "u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P",    "Q","R","S","T","U","V","W","X","Y","Z",1,2,3,4,5,6,7,8,9,0,"~","!","@","#","$","%","&","'",
        "(",")","^","+","-","*",".","/",":",";","<","=",">","?",","," ");
    
       $numberofar = count($ar);
       for ($i=0 ; $i<$numberofar ; $i++) { 
       $letter = str_replace($ar[$i],$ar[$i]."-",$letter); }
       $letter = $letter . "*";
       $letter = str_replace("--*","",$letter);
       $letter = str_replace(" ","-",$letter);
       $letters = explode("--",$letter);
       $num = count($letters);
       $id = "";
       mt_srand((double)microtime()*99999);
       for($i=1; $i<=$numochars; $i++) {
       $random = mt_rand(1,$num-1);
       $id = $id . $letters[$random];
       }
    
       // echo $id;
       $myfile = $myfile."-".$id.".csv";
       return $myfile;
              
}

function forceFile($fi) {

$filename=str_replace("/", "\\", $fi);

$fileext = substr( $filename,-3 );
if( $filename == "" ) 
{
  echo "<html><title>eLouai's Download Script</title><body>ERROR: download file NOT SPECIFIED. USE force-download.php?file=filepath</body></html>";
  exit;
} elseif ( ! file_exists( $filename ) ) 
{
  echo "<html><title>eLouai's Download Script</title><body>ERROR: File not found. USE force-download.php?file=filepath</body></html>";
  exit;
};
switch( $fileext )
{
  case "pdf": $ctype="application/pdf"; break;
  case "exe": $ctype="application/octet-stream"; break;
  case "zip": $ctype="application/zip"; break;
  case "doc": $ctype="application/msword"; break;
  case "xls": $ctype="application/vnd.ms-excel"; break;
  case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
  case "gif": $ctype="image/gif"; break;
  case "png": $ctype="image/png"; break;
  case "jpg": $ctype="image/jpg"; break;
  default: $ctype="application/force-download";
}
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public"); 
header("Content-Type: $ctype");
$user_agent = strtolower ($_SERVER["HTTP_USER_AGENT"]);
if ((is_integer (strpos($user_agent, "msie"))) && (is_integer (strpos($user_agent, "win")))) 
{
  header( "Content-Disposition: filename=".basename($filename).";" );
} else {
  header( "Content-Disposition: attachment; filename=".basename($filename).";" );
}
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".filesize($filename));
readfile("$filename");
exit();

}

/**
	* Return variable from an array
	* 
	* If field $name does not exists in array this function will return $default
	*
	* @param array $from Hash
	* @param string $name
	* @param mixed $default
	* @param boolean $and_unset
	* @return mixed
	*/
 function array_var(&$from, $name, $default = null, $and_unset = false) {
	 if(is_array($from) || (is_object($from) && instance_of($from, 'ArrayAccess'))) {
		 if($and_unset) {
			 if(array_key_exists($name, $from)) {
				 $result = $from[$name];
				 unset($from[$name]);
				 return $result;
			 } // if
		 } else {
			 return array_key_exists($name, $from) ? $from[$name] : $default;
		 } // if
	 } // if
	 return $default;
 } // array_var