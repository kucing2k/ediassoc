    function showViewData(id)
    {
    	window.open("../popups/transData.php?id=" + id,"window","width=600,height=400,resizable=yes");
    }
    
    function showViewDetails(id)
    {
    	window.open("../popups/transInfo.php?id=" + id,"window","width=400,height=500,resizable=yes");
    }

    function showDownLoadInvoice()
    {
    	window.open("showDownLoadInvoices.php?sd=" + ediaform.sdate.value + "&ed=" + ediaform.edate.value + "&si=" + ediaform.sinv.value,"window","width=400,height=200,resizable=yes");
    }

    function showDownLoadCat(id,ty,ex)
    {
    	window.open("showDownLoadCat.php?ty=" + ty + "&id=" + id + "&ex=" + ex,"window","width=400,height=200,resizable=yes");
    }


    function showViewReportDetails()
    {
	StartDate=searchList.DateStartY.value;
	StartDate=StartDate + searchList.DateStartM.value;
	StartDate=StartDate + searchList.DateStartD.value + " ";
	StartDate=StartDate + searchList.DateStartHH.value + ":";
	StartDate=StartDate + searchList.DateStartMM.value + ":";
	StartDate=StartDate + searchList.DateStartSS.value;

	StopDate=searchList.DateStopY.value;
	StopDate=StopDate + searchList.DateStopM.value;
	StopDate=StopDate + searchList.DateStopD.value + " ";
	StopDate=StopDate + searchList.DateStopHH.value + ":";
	StopDate=StopDate + searchList.DateStopMM.value + ":";
	StopDate=StopDate + searchList.DateStopSS.value;

    	window.open("transInfo.php?lDir=" + searchList.lDir.value + "&lWhat=" + searchList.lWhat.value + "&lWho=" + searchList.lWho.value + "&dStr=" + StartDate + "&dSto=" + StopDate,"window","width=700,height=500,resizable=yes,scrollbars=yes");
    }

    function showViewTrans(id,ty)
    {
    	window.open("../forms/form.php?ty=" + ty + "&tid=" + id,"window","width=700,height=500,resizable=yes,scrollbars=yes");
    }

    function getObject(obj)
    {
    	alert(obj.value);
    }

    function Toggle(e)
    {
	if (e.checked) {
	    Highlight(e);
	    document.deletethread.toggleAll.checked = AllChecked();
	}
	else {
	    Unhighlight(e);
	    document.deletethread.toggleAll.checked = false;
	}
    }

    function ToggleAll(e)
    {
	if (e.checked) {
	    CheckAll();
	}
	else {
	    ClearAll();
	}
    }

    function Check(e)
    {
	e.checked = true;
	Highlight(e);
    }

    function Clear(e)
    {
	e.checked = false;
	Unhighlight(e);
    }

    function CheckAll()
    {
	var ml = document.deletethread;
	var len = ml.elements.length;
	for (var i = 0; i < len; i++) {
	    var e = ml.elements[i];
	    if (e.name == "favorite[]") {
		Check(e);
	    }
	}
	// ml.favorite[0].checked = true;
    }

    function ClearAll()
    {
	var ml = document.deletethread;
	var len = ml.elements.length;
	for (var i = 0; i < len; i++) {
	    var e = ml.elements[i];
	    if (e.name == "favorite[]") {
		Clear(e);
	    }
	}
	// ml.toggleAll.checked = false;
    }

    function Highlight(e)
    {
	var r = null;
	if (e.parentNode && e.parentNode.parentNode) {
	    r = e.parentNode.parentNode;
	}
	else if (e.parentElement && e.parentElement.parentElement) {
	    r = e.parentElement.parentElement;
	}
	if (r) {
	    if (r.className == "msgnew") {
		r.className = "msgnews";
	    }
	    else if (r.className == "msgold") {
		r.className = "msgolds";
	    }
	}
    }

    function show(x){
        try {
            for (var i=1;i<6;i++){
                document.all("dv"+i).style.display = 'none';
            }
            document.all("dv"+x).style.display = 'inline';
        }
        catch (e) {
            alert("Error --> "+ e.description);
        }
    }
