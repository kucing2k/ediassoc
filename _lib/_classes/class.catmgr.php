<?php

// File Location: /_lib/_classes/class.partners.php

// require PEAR objects
require_once("DB.php");

// require USER objects
require_once("config.php");
require_once("funcs.php");

/** 
 * handles user functions
 *
 * @author Stephen Case <Stephen@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright EDI Associates, Inc.
 *
 */
class cat { // open the class definition
    
    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setCatId()
     */
    var $_iCatId;

    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setStyleId()
     */
    var $_iStyleId;

    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setItemId()
     */
    var $_iItemId;
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer user id [optional]
     * @access public
     */
    function cat() {
        
        // Instanciate the database connection
        $this->_oConn = get_db();
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
     
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    function setCatId($iCatId) {
        
        if (is_int($iCatId)) {
            
            $this->_iCatId = $iCatId;
        }
    }

    function setCatFormId2($iCatId) {
        
        $sql = "SELECT 
                    cat_Key_id,
                    cat_num_id,
                    cat_vernum_id,
                    cat_desc
                FROM 
                    ".PREFIX."_catalogs
                WHERE
                    cat_Key_id =?";
        
        $aRow = $this->_oConn->getRow($sql,array($iCatId));
        
        if ($aRow["cat_num_id"]) { $tmpStr = $aRow["cat_num_id"]; }
        if ($aRow["cat_vernum_id"]) { $tmpStr = $tmpStr ."-". $aRow["cat_vernum_id"]; }
        if ($aRow["cat_desc"]) { $tmpStr = $tmpStr ."-". $aRow["cat_desc"]; }

	$return["cId"] = $aRow["cat_Key_id"];
	$return["cDesc"] = $tmpStr;
        
        
        return $return;
    }


    function setCatFormId($iStyId) {
        
        $sql = "SELECT 
                    c.cat_Key_id,
                    c.cat_num_id,
                    c.cat_vernum_id,
                    c.cat_desc
                FROM 
                    ".PREFIX."_catalog_styles as s,
                    ".PREFIX."_catalogs as c
                WHERE
                    c.cat_Key_id = s.cat_Key_id
                    and s.Style_Key_id=?";
        
        $aRow = $this->_oConn->getRow($sql,array($iStyId));
        
        if ($aRow["cat_num_id"]) { $tmpStr = $aRow["cat_num_id"]; }
        if ($aRow["cat_vernum_id"]) { $tmpStr = $tmpStr ."-". $aRow["cat_vernum_id"]; }
        if ($aRow["cat_desc"]) { $tmpStr = $tmpStr ."-". $aRow["cat_desc"]; }

	$return["cId"] = $aRow["cat_Key_id"];
	$return["cDesc"] = $tmpStr;
        
        
        return $return;
    }


    function setStyleId($iStyleId) {
        
        if (is_int($iStyleId)) {
            
            $this->_iStyleId = $iStyleId;
        }
    }

    function setStyFormId($iItemId) {
        
        $sql = "SELECT catStyleKey_id 
                FROM 
                    ".PREFIX."_catalog_items 
                WHERE 
                    catItemKey_id=?";
        
        return $this->_oConn->getOne($sql,array($iItemId));
    }

    function setItemId($iItemId) {
        
        if (is_int($iItemId)) {
            
            $this->_iItemId = $iItemId;
        }
    }
    
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    function exportCatalog($wMem, $wCat) {
        
        // get a list of all users

        $sql = "Select 
                    c.cat_num_id,    
                    c.cat_vernum_id,
                    c.cat_desc,
                    c.cat_date,
                    s.Style_id,
                    s.Style_desc,
                    s.stylePublishDate,
                    s.styleItemManufacturer,
                    s.styleItemFabric,
                    s.styleItemLeadTimeQual,
                    s.styleItemLeadTimeCode,
                    s.styleItemLeadTime,
                    s.styleItemAvailDate,
                    s.styleItemMinOrder,
                    s.styleItemMaxOrder,
                    s.styleItemMinMaxQual,
                    s.styleItemReOrderFlag,
                    s.styleItemSeasonFlag,
                    s.styleItemCountryOrig,
                    s.styleItemPointOrig,
                    s.styleItemShipLength,
                    s.styleItemShipHeight,
                    s.styleItemShipWidth,
                    s.styleItemShipQualifier,
                    s.styleItemWeight,
                    s.styleItemWeightQual,
                    s.styleItemHazMatQual,
                    s.styleItemHazMatClass,
                    s.styleItemHazMatDesc,
                    s.styleItemImageURL,
                    i.catItemUPC,
                    i.catItemPreUPC,
                    i.catItemALU,
                    i.catItemNRFColor,
                    i.catItemNRFSize,
                    i.catItemNRFSize1,
                    i.catItemNRFSize2,
                    i.catItemVenColor,
                    i.catItemVenColorDesc,
                    i.catItemVenSize,
                    i.catItemVenSizeDesc,
                    i.catItemAffective_dt,
                    i.catItemDiscontinue_dt,
                    i.catItemQTYOnHandQual,
                    i.catItemQTYOnHand,
                    i.catItemInnerUnitsPerPackQual,
                    i.catItemInnerUnitsPerPack 
                FROM 
                    edia_catalogs as c,
                    edia_catalog_styles as s,
                    edia_catalog_items as i
                WHERE
                    i.catStyleKey_id = s.Style_Key_id
                    and s.cat_Key_id = c.cat_Key_id
                    and c.cat_Key_id IN (SELECT 
                            c.cat_Key_id
                        FROM 
                            edia_catalogs as c,
                            edia_catalog_perms as cp,
                            edia_members as m
                        WHERE
                            cp.cat_perm = 1
                            and cp.cat_Key_id = c.cat_Key_id
                            and cp.cat_member_id = m.member_id
                            and c.cat_delete=0
                            and c.cat_status = 1
                            and cp.cat_partner_id IN (SELECT 
                                tp_id 
                            from 
                                edia_partners 
                            where 
                                tp_edi_code = (select 
                                    member_login 
                                from 
                                    edia_members 
                                where 
                                    member_id = 2)))
                    and c.cat_Key_id = 46";
        
        $stmt = $this->_oConn->prepare($sql);
        
        // loop through result and build return array
        while ($aRow = $stmt->fetch()) {
            
		$return = $this->fputcsv($aRow);
        }
        return $return;
    }    

    // function fputcsv ($fp, $array, $deliminator=",") { 
    function fputcsv ($array, $deliminator=",") { 
    
        $line = ""; 
        // $fp = "testfile.csv";
        $fp = fopen("something.txt","a");
        foreach($array as $val) { 
            $val = str_replace("\r\n", "\n", $val); 
            
                if(ereg("[$deliminator\"\n\r]", $val)) { 
                    $val = '"'.str_replace('"', '""', $val).'"'; 
                }
            
            $line .= $val.$deliminator; 
        
        }
        
        $line = substr($line, 0, (strlen($deliminator) * -1)); 
        $line .= "\n"; 
        
        return fputs($fp, $line); 
    
    }





    function _catalogExists($aArgs) {
 
         $sql = "SELECT 
                    count(1) 
                FROM 
                    ".PREFIX."_catalogs 
                WHERE 
                    cat_num_id=?
                    AND cat_vernum_id=?
                    AND cat_delete=0";
        
        return $this->_oConn->getOne($sql, array($aArgs["Number"], $aArgs["Version"]));
    }

    function _styleExists($aArgs) {
 
         $sql = "SELECT 
                    count(1)
                FROM 
                    ".PREFIX."_catalog_styles as s,
                    ".PREFIX."_catalogs as c
                WHERE 
                    c.cat_Key_id = s.cat_Key_id
                    and c.cat_member_id = ?
                    and s.Style_id= ?
                    and s.Style_delete = 0";

        return $this->_oConn->getOne($sql, array($iComId, $aArgs["Style_id"]));
    }

    function _itemExists($aArgs) {
 
         $sql = "SELECT 
                    count(1)
                FROM 
                    ".PREFIX."_catalog_items as i,
                    ".PREFIX."_catalog_styles as s,
                    ".PREFIX."_catalogs as c
                WHERE 
                    i.catItemUPC = ?
                    and i.catStyleKey_id = s.Style_Key_id
                    and c.cat_Key_id = s.cat_Key_id
                    and c.cat_member_id = ?
                    and i.catItemdelete = 0";

        return $this->_oConn->getOne($sql, array($aArgs["Number"], $iComId));
    }

    function getCatLkupVals($sVal) {
    
    
        // get a list of all users

        $sql = "SELECT 
                    val, 
                    des 
                FROM 
                    ".PREFIX."_catalog_lookup_values
                WHERE
                    val_type = ?";

        return $this->_oConn->getAll($sql, array($sVal));
    }


    function getItemCnt($iStyId) {
        
        $sql = "SELECT count(*) 
                FROM 
                    ".PREFIX."_catalog_items 
                WHERE 
                    catStyleKey_id=?
                    and catItemdelete = 0";
        
        return $this->_oConn->getOne($sql,array($iStyId));
    }


    function getItem($wItem, $wComId) {
    
    
        // get a list of all users

        $sql = "SELECT 
                    * 
                FROM 
                    ".PREFIX."_catalog_items
                where
                    catItemKey_id = ?";

        return $this->_oConn->getRow($sql, array($wItem));
    }


    function getItems($iCursor=10, $iRCnt, $wSty, $wComId) {
    
        // get a list of all items

        $sql = "select * from ( select top ".$iRCnt." * from (select TOP ".$iCursor."
                    * 
                from 
                    ".PREFIX."_catalog_items 
                where 
                    catStyleKey_id = ?
                    and catItemdelete = 0 
                ORDER BY catItemUPC) as t1 ORDER BY catItemUPC DESC)as t2 order by catItemUPC";

        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($wSty));
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
		$return[$i]["Id"] = $aRow["catItemKey_id"];
		$return[$i]["Name"] = $aRow["catItemUPC"] ." - ". $aRow["catItemVenSizeDesc"];
		$return[$i]["NFRCode"] = $aRow["catItemNFRCode"];
		$return[$i]["NFRColor"] = $aRow["catItemNRFColor"];
		$return[$i]["NFRSize"] = $aRow["catItemNRFSize"];
		$return[$i]["NFRSize1"] = $aRow["catItemNRFSize1"];
		$return[$i]["Status"] = $aRow["catItemStatus"];
		$return[$i]["Created"] = strtotime($aRow["catItemcreate_dt"]);
		++$i;
        }
        return $return;
    }


    function getStyle($wSty, $wComId) {
    
    
        // get a list of all users

        $sql = "select 
                    * 
                from 
                    ".PREFIX."_catalog_styles 
                where 
                    Style_Key_id = ?
                    and Style_Status = 1";

            
        return $this->_oConn->getRow($sql, array($wSty));
    }

    function getStyleCnt($wCat, $sSF='') {
        
        $sql = "SELECT count(*) 
                from 
                    ".PREFIX."_catalog_styles 
                where 
                    cat_Key_id = ?
                    ".$sSF."
                    and Style_delete = 0";
                    
        return $this->_oConn->getOne($sql, array($wCat));
    }


     function getStyles($iCursor=10, $iRCnt, $wCat, $wComId, $sSF='') {

        // get a list of all users

        $sql = "select * from ( select top ".$iRCnt." * from (select TOP ".$iCursor."
                    Style_Key_id, 
                    Style_id, 
                    Style_desc,
                    Style_Status,
                    Style_create_dt 
                from 
                    ".PREFIX."_catalog_styles 
                where 
                    cat_Key_id = ? 
                    and Style_delete = 0
                    ".$sSF."
                ORDER BY
                    Style_id) as t1 ORDER BY Style_id DESC)as t2 order by Style_id";        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($wCat));
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
		$return[$i]["Id"] = $aRow["Style_Key_id"];
		$return[$i]["Name"] = $aRow["Style_id"] ." - ". $aRow["Style_desc"];
		$return[$i]["Status"] = $aRow["Style_Status"];
		$return[$i]["Created"] = strtotime($aRow["Style_create_dt"]);
		++$i;
        }
        return $return;
    }    

    function getCatalog($wCat, $wComId) {
        
        // get a list of all users

        $sql = "SELECT 
			cat_Key_id,
			cat_num_id,
			cat_vernum_id,
			cat_desc,
			cat_status,
			cat_create_dt,
			cat_modified_dt
                FROM 
			".PREFIX."_catalogs
                WHERE 
			cat_member_id=?
			and cat_Key_id=?";
       
        $aRow = $this->_oConn->getRow($sql, array($wComId, $wCat));
        
  $return = array();
	$return["Cat Id"] = $aRow["cat_Key_id"];
	$return["Number"] = $aRow["cat_num_id"];
	$return["Version"] = $aRow["cat_vernum_id"];
	$return["Desc"] = $aRow["cat_desc"];
	$return["Status"] = $aRow["cat_status"];
	$return["Created Date"] = strtotime($aRow["cat_create_dt"]);
	$return["Modified Date"] = strtotime($aRow["cat_modified_dt"]);

        return $return;
    }    

    function getCatalogs($wMem) {
        
        // get a list of all users

        $sql = "SELECT 
			cat_Key_id,
			cat_num_id,
			cat_vernum_id,
			cat_desc,
			cat_status,
			cat_create_dt
                FROM 
			".PREFIX."_catalogs
                WHERE 
			cat_member_id=?
			and cat_delete=0
		ORDER BY
			cat_num_id, cat_vernum_id";
         //       LIMIT ".$iPage.", ".ROWCOUNT;
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($wMem));
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
		$return[$i]["Id"] = $aRow["cat_Key_id"];
		$return[$i]["Name"] = $aRow["cat_num_id"] ."-". $aRow["cat_vernum_id"] ."-". $aRow["cat_desc"];
		$return[$i]["Status"] = $aRow["cat_status"];
		$return[$i]["Created Date"] = strtotime($aRow["cat_create_dt"]);
		++$i;
        }
        return $return;
    }    
     

 
    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * add a new style record
     *
     * @param array $aArgs user data
     * @return boolean
     * @access public
     */

    function addStyle($iComId, $aArgs) {
        
        // check for existing user name
        if ($this->_styleExists($aArgs)) {
            
            catchErr("This Style already exists, please ");
            return false;
            
        } else {
            // add new style record

            $sql = "INSERT INTO ".PREFIX."_catalog_styles (
                    cat_Key_id,
                    Style_id,
                    Style_desc,
                    styleItemManufacturer,
                    styleItemCountryOrig,
                    styleItemPointOrig,
                    styleItemFabric,
                    styleItemSeasonFlag,
                    styleItemReOrderFlag,
                    styleItemAvailDate,
                    styleItemLeadTime,
                    styleItemLeadTimeQual,
                    styleItemLeadTimeCode,
                    styleItemMinOrder,
                    styleItemMaxOrder,
                    styleItemMinMaxQual,
                    styleItemShipLength,
                    styleItemShipHeight,
                    styleItemShipWidth,
                    styleItemShipQualifier,
                    styleItemWeight,
                    styleItemWeightQual,
                    styleItemHazMatClass,
                    styleItemHazMatDesc,
                    styleItemHazMatQual,
                    Style_create_dt,
                    Style_modified_dt
                    ) VALUES (
                    ".$aArgs["cat_Key_id"].",
                    '".$aArgs["Style_id"]."',
                    '".$aArgs["Style_desc"]."',
                    '".$aArgs["styleItemManufacturer"]."',
                    '".$aArgs["styleItemCountryOrig"]."',
                    '".$aArgs["styleItemPointOrig"]."',
                    '".$aArgs["styleItemFabric"]."',
                    '".$aArgs["styleItemSeasonFlag"]."',
                    '".$aArgs["styleItemReOrderFlag"]."',
                    '".$aArgs["styleItemAvailDate"]."',
                    ".$aArgs["styleItemLeadTime"].",
                    '".$aArgs["styleItemLeadTimeQual"]."',
                    '".$aArgs["styleItemLeadTimeCode"]."',
                    ".$aArgs["styleItemMinOrder"].",
                    ".$aArgs["styleItemMaxOrder"].",
                    '".$aArgs["styleItemMinMaxQual"]."',
                    ".$aArgs["styleItemShipLength"].",
                    ".$aArgs["styleItemShipHeight"].",
                    ".$aArgs["styleItemShipWidth"].",
                    '".$aArgs["styleItemShipQualifier"]."',
                    ".$aArgs["styleItemWeight"].",
                    '".$aArgs["styleItemWeightQual"]."',
                    '".$aArgs["styleItemHazMatClass"]."',
                    '".$aArgs["styleItemHazMatDesc"]."',
                    '".$aArgs["styleItemHazMatQual"]."',
                    GETDATE(),
                    GETDATE()
                    )";

            $this->_oConn->doQuery($sql);
            
            return true;
            
        }
    }


    /** 
     * add a new catalog record
     *
     * @param array $aArgs user data
     * @return boolean
     * @access public
     */

    function addCatalog($iComId, $aArgs) {
        
        // check for existing user name
        if ($this->_catalogExists($aArgs)) {
            
            catchErr("This Catalog already exists, please ");
            return false;
            
        } else {
            // add new user record

            $sql = "INSERT INTO ".PREFIX."_catalogs (
			cat_member_id,
			cat_num_id,
			cat_vernum_id,
			cat_desc,
			cat_create_dt,
			cat_modified_dt
                    ) values (
			'".$iComId."',
			'".$aArgs["Number"]."',
			'".$aArgs["Version"]."',
			'".$aArgs["Desc"]."',
			GETDATE(),
			GETDATE()
                    )";
            
            $this->_oConn->doQuery($sql);
            
            return true;
        }
    }

    /** 
     * add a new catalog record
     *
     * @param array $aArgs user data
     * @return boolean
     * @access public
     */

    function addItem($aArgs) {
        
        // check for existing user name
        if ($this->_catalogExists($aArgs)) {
            
            catchErr("This Catalog already exists, please ");
            return false;
            
        } else {
            // add new user record

            $sql = "INSERT INTO ".PREFIX."_catalog_items(
                        catStyleKey_id,
                        catItemUPC,
                        catItemPreUPC,
                        catItemALU,
                        catItemNRFColor,
                        catItemNRFSize,
                        catItemNRFSize1,
                        catItemNRFSize2,
                        catItemVenColor,
                        catItemVenColorDesc,
                        catItemVenSize,
                        catItemVenSizeDesc,
                        catItemAffective_dt,
                        catItemDiscontinue_dt,
                        catItemQTYOnHandQual,
                        catItemQTYOnHand,
                        catItemInnerUnitsPerPackQual,
                        catItemInnerUnitsPerPack
                    ) VALUES (
                        ".$aArgs["catStyleKey_id"].",
                        '".$aArgs["catItemUPC"]."',
                        '".$aArgs["catItemPreUPC"]."',
                        '".$aArgs["catItemALU"]."',
                        '".$aArgs["catItemNRFColor"]."',
                        '".$aArgs["catItemNRFSize"]."',
                        '".$aArgs["catItemNRFSize1"]."',
                        '".$aArgs["catItemNRFSize2"]."',
                        '".$aArgs["catItemVenColor"]."',
                        '".$aArgs["catItemVenColorDesc"]."',
                        '".$aArgs["catItemVenSize"]."',
                        '".$aArgs["catItemVenSizeDesc"]."',
                        '".$aArgs["catItemAffective_dt"]."',
                        '".$aArgs["catItemDiscontinue_dt"]."',
                        '".$aArgs["catItemQTYOnHandQual"]."',
                        ".$aArgs["catItemQTYOnHand"].",
                        '".$aArgs["catItemInnerUnitsPerPackQual"]."',
                        ".$aArgs["catItemInnerUnitsPerPack"]."
                        )";
            
            
            $this->_oConn->doQuery($sql);
            
            return true;
        }
    }

    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * edit an existing item record
     *
     * @param array item data
     * @return boolean
     * @access public
     */
    function editItem($aArgs) {
        
        // update item record

        $sql = "UPDATE ".PREFIX."_catalog_items SET 
                    catItemPreUPC='".$aArgs["catItemPreUPC"]."',
                    catItemALU='".$aArgs["catItemALU"]."',
                    catItemNRFColor='".$aArgs["catItemNRFColor"]."',
                    catItemNRFSize='".$aArgs["catItemNRFSize"]."',
                    catItemNRFSize1='".$aArgs["catItemNRFSize1"]."',
                    catItemNRFSize2='".$aArgs["catItemNRFSize2"]."',
                    catItemVenColor='".$aArgs["catItemVenColor"]."',
                    catItemVenColorDesc='".$aArgs["catItemVenColorDesc"]."',
                    catItemVenSize='".$aArgs["catItemVenSize"]."',
                    catItemVenSizeDesc='".$aArgs["catItemVenSizeDesc"]."',
                    catItemAffective_dt='".$aArgs["catItemAffective_dt"]."',
                    catItemDiscontinue_dt='".$aArgs["catItemDiscontinue_dt"]."',
                    catItemQTYOnHandQual='".$aArgs["catItemQTYOnHandQual"]."',
                    catItemQTYOnHand=".$aArgs["catItemQTYOnHand"].",
                    catItemInnerUnitsPerPackQual='".$aArgs["catItemInnerUnitsPerPackQual"]."',
                    catItemInnerUnitsPerPack=".$aArgs["catItemInnerUnitsPerPack"].",
                    catItemmodified_dt=GETDATE(),
                    catItemLastChange_dt=GETDATE()
                WHERE 
                    catItemKey_id=".$aArgs["catItemKey_id"];        

        $this->_oConn->doQuery($sql);
        return true;
    }

    /** 
     * edit an existing style record
     *
     * @param array style data
     * @return boolean
     * @access public
     */
    function editStyle($aArgs) {
        
        // update style record
        $sql = "UPDATE ".PREFIX."_catalog_styles SET 
                        styleItemManufacturer='".$aArgs["styleItemManufacturer"]."', 
                        styleItemCountryOrig='".$aArgs["styleItemCountryOrig"]."',  
                        styleItemPointOrig='".$aArgs["styleItemPointOrig"]."',    
                        styleItemFabric='".$aArgs["styleItemFabric"]."',       
                        styleItemSeasonFlag='".$aArgs["styleItemSeasonFlag"]."',   
                        styleItemReOrderFlag='".$aArgs["styleItemReOrderFlag"]."',  
                        styleItemAvailDate='".$aArgs["styleItemAvailDate"]."',    
                        styleItemLeadTime=".$aArgs["styleItemLeadTime"].",       
                        styleItemLeadTimeQual='".$aArgs["styleItemLeadTimeQual"]."', 
                        styleItemLeadTimeCode='".$aArgs["styleItemLeadTimeCode"]."', 
                        styleItemMinOrder=".$aArgs["styleItemMinOrder"].",       
                        styleItemMaxOrder=".$aArgs["styleItemMaxOrder"].",       
                        styleItemMinMaxQual='".$aArgs["styleItemMinMaxQual"]."',   
                        styleItemShipLength=".$aArgs["styleItemShipLength"].",     
                        styleItemShipHeight=".$aArgs["styleItemShipHeight"].",     
                        styleItemShipWidth=".$aArgs["styleItemShipWidth"].",      
                        styleItemShipQualifier='".$aArgs["styleItemShipQualifier"]."',
                        styleItemWeight=".$aArgs["styleItemWeight"].",         
                        styleItemWeightQual='".$aArgs["styleItemWeightQual"]."',   
                        styleItemHazMatClass='".$aArgs["styleItemHazMatClass"]."',  
                        styleItemHazMatDesc='".$aArgs["styleItemHazMatDesc"]."',   
                        styleItemHazMatQual='".$aArgs["styleItemHazMatQual"]."',   
                        Style_modified_dt=GETDATE()                              
                WHERE 
			Style_Key_id=".$aArgs["id"];        
        $this->_oConn->doQuery($sql);
        return true;
    }


    /** 
     * edit an existing partner record
     *
     * @param array partner data
     * @return boolean
     * @access public
     */
    function editCatalog($aArgs) {
        
        // update catalog record
        $sql = "UPDATE ".PREFIX."_catalogs SET 
			cat_num_id='".$aArgs["Number"]."',
			cat_vernum_id='".$aArgs["Version"]."',
			cat_desc='".$aArgs["Desc"]."',
			cat_modified_dt=GETDATE()
                WHERE 
			cat_Key_id=".$aArgs["id"];
        
        $this->_oConn->doQuery($sql);
        return true;
    }
    
    /** 
     * activate a Catalog record
     *
     * @return boolean
     * @access public
     */
    function activateCatalog() {
        
        $sql = "UPDATE ".PREFIX."_catalogs SET 
                    cat_status=1,
                    cat_modified_dt=GETDATE() 
                WHERE 
                    cat_Key_id=".$this->_iCatId;
         
        $this->_oConn->doQuery($sql);
    }
    
    /** 
     * deactivate a Catalog record
     *
     * @return boolean
     * @access public
     */
    function deactivateCatalog() {
        
        $sql = "UPDATE ".PREFIX."_catalogs SET 
                    cat_status=0,
                    cat_modified_dt=GETDATE() 
                WHERE 
                    cat_Key_id=".$this->_iCatId;
        
        $this->_oConn->doQuery($sql);
    }

    /** 
     * activate a Style record
     *
     * @return boolean
     * @access public
     */
    function activateStyle() {
        
        $sql = "UPDATE ".PREFIX."_catalog_styles SET 
                    Style_Status=1,
                    Style_modified_dt=GETDATE() 
                WHERE 
                    Style_Key_id=".$this->_iStyleId;
         
        $this->_oConn->doQuery($sql);
    }
    
    /** 
     * deactivate a Style record
     *
     * @return boolean
     * @access public
     */
    function deactivateStyle() {
        
        $sql = "UPDATE ".PREFIX."_catalog_styles SET 
                    Style_status=0,
                    Style_modified_dt=GETDATE() 
                WHERE 
                    Style_Key_id=".$this->_iStyleId;
        
        $this->_oConn->doQuery($sql);
    }

    /** 
     * activate a item record
     *
     * @return boolean
     * @access public
     */
    function activateItem() {
        
        $sql = "UPDATE ".PREFIX."_catalog_items SET 
                    catItemStatus=1,
                    catItemStatus_dt=GETDATE() 
                WHERE 
                    catItemKey_id=".$this->_iItemId;
         
        $this->_oConn->doQuery($sql);
    }
    
    /** 
     * deactivate a item record
     *
     * @return boolean
     * @access public
     */
    function deactivateItem() {
        
        $sql = "UPDATE ".PREFIX."_catalog_items SET 
                    catItemStatus=0,
                    catItemStatus_dt=GETDATE() 
                WHERE 
                    catItemKey_id=".$this->_iItemId;
        
        $this->_oConn->doQuery($sql);
    }
    
    // DELETE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /** 
     * delete a Catalog record
     *
     * @return boolean
     * @access public
     */
    function deleteCatalog() {
        
        $sql = "DELETE FROM ".PREFIX."_catalogs 
                WHERE 
                    cat_Key_id=".$this->_iCatId;

        $this->_oConn->doQuery($sql);
        return true;
    }

    /** 
     * delete a Style record
     *
     * @return boolean
     * @access public
     */
    function deleteStyle() {
        
        
        $sql = "DELETE FROM ".PREFIX."_catalog_styles 
                WHERE 
                    Style_Key_id=".$this->_iStyleId;

        $this->_oConn->doQuery($sql);
        return true;
    }

    /** 
     * delete a item record
     *
     * @return boolean
     * @access public
     */
    function deleteItem() {
        
        $sql = "DELETE FROM ".PREFIX."_catalog_items 
                WHERE 
                    catItemKey_id=".$this->_iItemId;

        $this->_oConn->doQuery($sql);
        return true;
    }

    // TO BE REMOVED ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /** 
     * delete a item record
     *
     * @return boolean
     * @access public
     */
    function XXXdeleteItemXXX() {
        
        
        $sql = "UPDATE ".PREFIX."_catalog_items SET 
                    catItemdelete=1,
                    catItemdelete_dt=GETDATE()
                WHERE 
                    catItemKey_id=".$this->_iItemId;

        $this->_oConn->doQuery($sql);
        
        $this->deactivateItem();
        return true;
    }
    /** 
     * delete a Style record
     *
     * @return boolean
     * @access public
     */
    function XXXdeleteStyleXXX() {
        
        
        $sql = "UPDATE ".PREFIX."_catalog_styles SET 
                    Style_delete=1,
                    Style_delete_dt=GETDATE()
                WHERE 
                    Style_Key_id=".$this->_iStyleId;

        $this->_oConn->doQuery($sql);
        
        $this->deactivateStyle();
        return true;
    }
    /** 
     * delete a Catalog record
     *
     * @return boolean
     * @access public
     */
    function XXdeleteCatalogXX() {
        
        
        $sql = "UPDATE ".PREFIX."_catalogs SET 
                    cat_delete=1,
                    cat_delete_dt=GETDATE()
                WHERE 
                    cat_Key_id=".$this->_iCatId;

        $this->_oConn->doQuery($sql);
        
        $this->deactivateCatalog();
        return true;
    }


}

?>
