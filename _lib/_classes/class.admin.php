<?php

// File Location: /_lib/_classes/class.admin.php

require_once("DB.php");

/**
 * handles user functions
 *
 * @author Mike Buzzard <src@cubancouncil.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright Apress
 *
 */
class admin { // open the class definition

    /**
     * @var EdiAssocPdo
     */

    var $_oConn;

    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * class constructor
     *
     * @param integer user id [optional]
     * @access public
     */
    function admin($iUserId = '') {


        // implement db object
        $this->_oConn = get_db();
    }

    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    function dateDiff($startDate, $endDate) {
        // Parse dates for conversion 
        $startArry = date_parse($startDate);
        $endArry = date_parse($endDate);

        // Convert dates to Julian Days 
        $start_date = gregoriantojd($startArry["month"], $startArry["day"], $startArry["year"]);
        $end_date = gregoriantojd($endArry["month"], $endArry["day"], $endArry["year"]);

        // Return difference 
        return round(($end_date - $start_date), 0);
    }

    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * get member count for paging
     *
     * @return integer record count
     * @access public
     */
    function getMemberCount() {

        $sql = "select COUNT(member_id) from " . PREFIX . "_members where member_deleted = 0 and member_status = 1 and member_login != 'EDIA000000'";

        return $this->_oConn->getOne($sql);
    }

    /**
     * get Interchange count for Stats
     *
     * @return integer record count
     * @access public
     */
    function getInterchangeCount($opt = 0) {
        try {
            $sql = "SELECT COUNT(DataKey) AS int_cnt FROM datagate.dg_interchanges";
            if ($opt == 1) {
                $cdate = $this->getCurrentDate();
                $info = date_parse($cdate);
                $sql.=" WHERE DateStamp >= '" . $info["year"] . "-01-01'";
            }
            if ($opt == 2) {
                $cdate = $this->getCurrentDate();
                $info = date_parse($cdate);
                $sql.=" WHERE DateStamp >= '" . $info["year"] . "-" . $info["month"] . "-01'";
            }
            if ($opt == 3) {
                $cdate = $this->getCurrentDate();
                $info = date_parse($cdate);
                $sql.=" WHERE DateStamp >= '" . $info["year"] . "-" . $info["month"] . "-" . $info["day"] . "'";
            }

            return $this->_oConn->getOne($sql);
        } catch (PDOException $e) {
            return false;
        }
            
    }

    /**
     * get Group count for Stats
     *
     * @return integer record count
     * @access public
     */
    function getGroupCount() {
        try {
            $sql = "SELECT SUM(GroupCount) AS grp_cnt FROM datagate.dg_interchanges";

            return $this->_oConn->getOne($sql);
        } catch (PDOException $e) {
            return false;
        }
            
    }

    /**
     * get Transaction count for Stats
     *
     * @return integer record count
     * @access public
     */
    function getTransCount() {
        try {
            $sql = "SELECT SUM(TransactionCount) AS trn_cnt FROM datagate.dg_interchanges";

            return $this->_oConn->getOne($sql);
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * get Transaction count for Invoice
     *
     * @return integer record count
     * @access public
     */
    function getInvoiceTransCount($mi, $sd, $ed, $dir) {
        try {
            $sql = "SELECT SUM(TransactionCount) AS trn_cnt FROM datagate.dg_interchanges 
            WHERE " . $dir . " = '" . $mi . "'
            AND 
                DateStamp >= '" . $sd . "' 
            AND 
                DateStamp <= '" . $ed . "'";

            return (int) $this->_oConn->getOne($sql);
        } catch (PDOException $e) {
            return false;
        }
            
    }

    /**
     * get current date from MySql, this makes up for a version specific bug with PHP. 
     * The date() function on this version on Linux returns 01-01-1970 as the current date.
     *
     * @return date as string
     * @access public
     */
    function getCurrentDate($opt = 0) {

        $sql = "SELECT GETDATE()";
        if ($opt == 1) {
            $sql = "SELECT DATE_SUB(LAST_DAY(DATE_SUB(GETDATE(), INTERVAL 1 MONTH)), INTERVAL DAYOFMONTH(LAST_DAY(DATE_SUB(GETDATE(), INTERVAL 1 MONTH)))-1 DAY)";
        }
        if ($opt == 2) {
            $sql = "SELECT LAST_DAY(DATE_SUB(GETDATE(), INTERVAL 1 MONTH))";
        }

        return $this->_oConn->getOne($sql);
    }

    // PhreeBooks Specific METHODS Start :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * Get next PhreeBooks Invoice Number.
     *
     * @return date as string
     * @access public
     */
    function getPhreeBooksNextInvoice() {
        try {
            $sql = "select next_inv_num from PhreeBooksR21.pb_current_status limit 1";

            return $this->_oConn->getOne($sql);
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Get Member PhreeBooks Reference Number.
     *
     * @return date as string
     * @access public
     */
    function getPhreeBooksReferenceNumber($mi) {
        try {
            $sql = "select a.ref_id from PhreeBooksR21.pb_contacts c left join PhreeBooksR21.pb_address_book a on c.id = a.ref_id where a.type = 'cm' and (c.short_name like '" . $mi . "')";

            return $this->_oConn->getOne($sql);
        } catch (PDOException $e) {
            return false;
        }
            
    }

    /**
     * get PhreeBooks Member Address
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function getPhreeBooksBillingAddress($mi) {
        try {
            $sql = "select * from PhreeBooksR21.pb_address_book where ref_id = " . $mi . " and type in ('cm', 'cb')";

            $aRow = $this->_oConn->getRow($sql);

            $return["address_id"] = $aRow["address_id"];
            $return["ref_id"] = $aRow["ref_id"];
            $return["type"] = $aRow["type"];
            $return["primary_name"] = $aRow["primary_name"];
            $return["contact"] = $aRow["contact"];
            $return["address1"] = $aRow["address1"];
            $return["address2"] = $aRow["address2"];
            $return["city_town"] = $aRow["city_town"];
            $return["state_province"] = $aRow["state_province"];
            $return["postal_code"] = $aRow["postal_code"];
            $return["country_code"] = $aRow["country_code"];
            $return["telephone1"] = $aRow["telephone1"];
            $return["telephone2"] = $aRow["telephone2"];
            $return["telephone3"] = $aRow["telephone3"];
            $return["email"] = $aRow["email"];
            $return["website"] = $aRow["website"];
            $return["notes"] = $aRow["notes"];

            return $return;
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Get PhreeBooks Current Accounting Period.
     *
     * @return number
     * @access public
     */
    function getPhreeBooksAccountingPeriod() {
        try {
            $sql = "select configuration_value as cfgValue from PhreeBooksR21.pb_configuration where configuration_key LIKE 'CURRENT_ACCOUNTING_PERIOD'";

            return $this->_oConn->getOne($sql);
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Get PhreeBooks Current Accounting Period.
     *
     * @return number
     * @access public
     */
    function getPhreeBooksAccountingFiscalYear($mi) {
        try {
            $sql = "select fiscal_year from PhreeBooksR21.pb_accounting_periods where period = " . $mi;

            return $this->_oConn->getOne($sql);
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Get PhreeBooks Max Accounting Period.
     *
     * @return number
     * @access public
     */
    function getPhreeBooksAccountingMaxPeriod($mi) {
        try {
            $sql = "select max(period) as period from PhreeBooksR21.pb_accounting_periods where fiscal_year = " . $mi;

            return $this->_oConn->getOne($sql);
        } catch (PDOException $e) {
            return false;
        }
       
    }

    /**
     * Get PhreeBooks pb_journal_main.id after insert of invoice.
     *
     * @return number
     * @access public
     */
    function getPhreeBooksLastJournalId($mi) {
        try {
            $sql = "select id from PhreeBooksR21.pb_journal_main where purchase_invoice_id = '" . $mi . "'";

            return $this->_oConn->getOne($sql);
        } catch (PDOException $e) {
            return false;
        }
    }

    // PhreeBooks Specific METHODS End :::::::::::::::::::::::::::::::::::::::::::::::::::::::::


    function getCustomerCnt2Invoice() {
        $edate = $this->getCurrentDate();
        $sql = "select * from " . PREFIX . "_members where member_deleted = 0 and member_status = 1 and member_login != 'EDIA000000'";

        $return = "";
        $i = 0;
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        // Loop through billable members.
        while ($aRow = $stmt->fetch()) {
            // Get the member creation date, can not invoice if less than 30 days old.
            $sdate = $aRow["member_created_dt"];
            // Get the number of day old the account is.
            $diff = $this->dateDiff($sdate, $edate);
            $tmpCurDate = date("Y-m-d", strtotime($edate));
            // Check if the account is more than 30 day and weather or not there has been a previous invoice.
            // if greater than 30 days and no invoice then the invoice date is set to today.
            // if greater than 30 days and there is a previous invoice date then set invoice date to that date plus one month.
            if (($diff >= 30 ) and (strcmp("", $aRow["last_bill_dt"]))) {
                $invoice_edt = $edate;
                $invoice_sdt = $sdate;
                $valid = "yes";
            } elseif (($diff >= 30 ) and (!strcmp("", $aRow["last_bill_dt"]))) {
                $invoice_edt = date("Y-m-d", strtotime(date("Y-m-d", strtotime($aRow["last_bill_dt"])) . " +1 month"));
                $invoice_sdt = $aRow["last_bill_dt"];
                $valid = "no";
            }
            // if todays date is greater than or equal to the new invoice date and the signup date diff is greater than 30 days.
            if (($diff >= 30 ) and (strtotime($edate) >= $invoice_edt)) {
                ++$i;
            }
        }
        return $i;
    }

    /**
     * This function handles the bulk of the invoicing based on the general business rules.
     *
     * @return integer record count
     * @access public
     */
    function getInvoiceCustomers() {

        // Pickup current date.
        $sdate = $this->getCurrentDate(1);
        $edate = $this->getCurrentDate(2);
        $TmpCntSent = "";
        // Pickup all active members that are billable.
        $sql = "select * from " . PREFIX . "_members where member_deleted = 0 and member_status = 1 and member_login != 'EDIA000000'";

        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        $return = "";
        $i = 0;
        // Loop through billable members.
        while ($aRow = $stmt->fetch()) {

            // Get the member creation date, can not invoice if less than 30 days old.
            // $sdate = $aRow["member_created_dt"];
            // Get the number of day old the account is.
            // $diff = $this->dateDiff($sdate, $edate);
            // $tmpCurDate = date("Y-m-d",strtotime($edate));
            $tmpCurDate = $this->getCurrentDate();
            // Check if the account is more than 30 day and weather or not there has been a previous invoice.
            // if greater than 30 days and no invoice then the invoice date is set to today.
            // if greater than 30 days and there is a previous invoice date then set invoice date to that date plus one month.
            // if (($diff >= 30 ) and (strcmp("",$aRow["last_bill_dt"]))) { 
            $invoice_edt = $edate;
            $invoice_sdt = $sdate;
            //     $valid = "yes"; 
            // } elseif (($diff >= 30 ) and (!strcmp("",$aRow["last_bill_dt"]))) { 
            //     $invoice_edt = date("Y-m-d",strtotime(date("Y-m-d", strtotime($aRow["last_bill_dt"])) . " +1 month"));
            //     $invoice_sdt = $aRow["last_bill_dt"];
            //     $valid = "no";
            // }
            // if todays date is greater than or equal to the new invoice date and the signup date diff is greater than 30 days.
            // if (($diff >= 30 ) and (strtotime($edate) >= $invoice_edt)) {
            // Get Transaction Counts Sent.
            $CntSent = $this->getInvoiceTransCount($aRow["member_login"], $invoice_sdt, $invoice_edt, 'ISA_Sender');
            // Get Transaction Counts Received.
            $CntRec = $this->getInvoiceTransCount($aRow["member_login"], $invoice_sdt, $invoice_edt, 'ISA_Receiver');
            // Total number of transactions.
            $CntSum = $CntSent + $CntRec;
            $totalCostSent = $CntSent * $aRow["billing_amount"];
            $totalCostRec = $CntRec * $aRow["billing_amount"];
            // if there are transactions go ahead and start putting together the invoices.
            if ($CntSum > 0) {

                $nextInvoice = $this->getPhreeBooksNextInvoice();
                $pbRefId = $this->getPhreeBooksReferenceNumber($aRow["member_login"]);
                $pbAddress = $this->getPhreeBooksBillingAddress($pbRefId);
                $pbCurrentPeriod = $this->getPhreeBooksAccountingPeriod();
                $pbCurrentFiscalYear = $this->getPhreeBooksAccountingFiscalYear($pbCurrentPeriod);
                $pbCurrentMaxPeriod = $this->getPhreeBooksAccountingMaxPeriod($pbCurrentFiscalYear);
                $totalInvoice = $CntSum * $aRow["billing_amount"];
                $mytotalInvoice = $totalInvoice;
                if ($totalInvoice <= 25.00) {
                    $mytotalInvoice = $mytotalInvoice + 5.00;
                }

                $sql = "insert into PhreeBooksR21.pb_journal_main (period, journal_id, post_date, store_id, description, closed, freight, discount, shipper_code, terms, sales_tax, total_amount, currencies_code, currencies_value, so_po_ref_id, purchase_invoice_id, purch_order_id, admin_id, rep_id, waiting, gl_acct_id, bill_acct_id, bill_address_id, bill_primary_name, bill_contact, bill_address1, bill_address2, bill_city_town, bill_state_province, bill_postal_code, bill_country_code, bill_telephone1, bill_email, ship_acct_id, ship_address_id, ship_primary_name, ship_contact, ship_address1, ship_address2, ship_city_town, ship_state_province, ship_postal_code, ship_country_code, ship_telephone1, ship_email, terminal_date, drop_ship, recur_id) values ('" . $pbCurrentPeriod . "', '12', '" . $tmpCurDate . "', '0', 'Sales/Invoice Entry', '0', '0', '0', ':', '0::::2500.00', '0', '" . $mytotalInvoice . "', 'USD', '1', '0', '" . $nextInvoice . "', '', '1', '1', '0', '1200', '" . $pbAddress["address_id"] . "', '" . $pbAddress["address_id"] . "', '" . $pbAddress["primary_name"] . "', '" . $pbAddress["contact"] . "', '" . $pbAddress["address1"] . "', '" . $pbAddress["address2"] . "', '" . $pbAddress["city_town"] . "', '" . $pbAddress["state_province"] . "', '" . $pbAddress["postal_code"] . "', '" . $pbAddress["country_code"] . "', '" . $pbAddress["telephone1"] . "', '" . $pbAddress["email"] . "', '" . $pbAddress["address_id"] . "', '" . $pbAddress["address_id"] . "', '" . $pbAddress["primary_name"] . "', '" . $pbAddress["contact"] . "', '" . $pbAddress["address1"] . "', '" . $pbAddress["address2"] . "', '" . $pbAddress["city_town"] . "', '" . $pbAddress["state_province"] . "', '" . $pbAddress["postal_code"] . "', '" . $pbAddress["country_code"] . "', '" . $pbAddress["telephone1"] . "', '" . $pbAddress["email"] . "', '" . $tmpCurDate . "', '0', '0')";
                $this->_oConn->doQuery($sql);
                $phCurrentInvoiceJournalId = $this->getPhreeBooksLastJournalId($nextInvoice);

                $sql = "insert into PhreeBooksR21.pb_journal_item (id, so_po_item_ref_id, gl_type, sku, qty, description, credit_amount, full_price, gl_account, taxable, serialize_number, project_id, post_date, date_1, ref_id) values ('', '', 'sos', '', '" . $CntRec . "', 'Total Inbound Transactions', '" . $totalCostRec . "', '0', '4010', '0', '', '', '" . $tmpCurDate . "', '" . $tmpCurDate . "', '" . $phCurrentInvoiceJournalId . "')";
                $this->_oConn->doQuery($sql);
                $sql = "insert into PhreeBooksR21.pb_journal_item (id, so_po_item_ref_id, gl_type, sku, qty, description, credit_amount, full_price, gl_account, taxable, serialize_number, project_id, post_date, date_1, ref_id) values ('', '', 'sos', '', '" . $CntSent . "', 'Total Outbound Transactions', '" . $totalCostSent . "', '0', '4010', '0', '', '', '" . $tmpCurDate . "', '" . $tmpCurDate . "', '" . $phCurrentInvoiceJournalId . "')";
                $this->_oConn->doQuery($sql);
                if ($totalInvoice <= 25.00) {
                    $sql = "insert into PhreeBooksR21.pb_journal_item (id, so_po_item_ref_id, gl_type, sku, qty, description, credit_amount, full_price, gl_account, taxable, serialize_number, project_id, post_date, date_1, ref_id) values ('', '', 'sos', '', '1', 'Postage and Handling', '5.00', '0', '4010', '0', '', '', '" . $tmpCurDate . "', '" . $tmpCurDate . "', '" . $phCurrentInvoiceJournalId . "')";
                    $this->_oConn->doQuery($sql);
                }
                $sql = "insert into PhreeBooksR21.pb_journal_item (id, so_po_item_ref_id, gl_type, sku, qty, description, credit_amount, full_price, gl_account, taxable, serialize_number, project_id, post_date, date_1, ref_id) values ('', '', 'sos', '', '1', 'Period " . $sdate . " - " . $edate . "', '0', '0', '4010', '0', '', '', '" . $tmpCurDate . "', '" . $tmpCurDate . "', '" . $phCurrentInvoiceJournalId . "')";
                $this->_oConn->doQuery($sql);
                $sql = "insert into PhreeBooksR21.pb_journal_item (gl_type, debit_amount, description, gl_account, post_date, ref_id) values ('ttl', '" . $mytotalInvoice . "', 'Sales/Invoice Total', '1200', '" . $tmpCurDate . "', '" . $phCurrentInvoiceJournalId . "')";
                $this->_oConn->doQuery($sql);
                $sql = "update PhreeBooksR21.pb_chart_of_accounts_history set credit_amount = credit_amount + " . $mytotalInvoice . ", debit_amount = debit_amount + 0, last_update = '" . $tmpCurDate . "' where account_id = '4010' and period = " . $pbCurrentPeriod;
                $this->_oConn->doQuery($sql);
                $sql = "update PhreeBooksR21.pb_chart_of_accounts_history set credit_amount = credit_amount + 0, debit_amount = debit_amount + " . $mytotalInvoice . ", last_update = '" . $tmpCurDate . "' where account_id = '1200' and period = " . $pbCurrentPeriod;
                $this->_oConn->doQuery($sql);
                $sql = "insert into PhreeBooksR21.pb_accounts_history (ref_id, so_po_ref_id, acct_id, journal_id, purchase_invoice_id, amount, post_date) values ('" . $phCurrentInvoiceJournalId . "', '0', '" . $pbAddress["address_id"] . "', '12', '" . $nextInvoice . "', '" . $mytotalInvoice . "', '" . $tmpCurDate . "')";
                $this->_oConn->doQuery($sql);
                $tmpInt = $pbCurrentPeriod;
                $tmpInt2 = $pbCurrentPeriod;
                $tmpInt++;
                for ($i = $tmpInt; $i <= $pbCurrentMaxPeriod; $i++) {
                    // Get From Account Balance
                    $fromAccount = 0;
                    $toAccount = 0;

                    $sql = "select beginning_balance + debit_amount - credit_amount as beginning_balance from PhreeBooksR21.pb_chart_of_accounts_history where account_id = '4010' and period = " . $tmpInt2;
                    $fromAccount = $this->_oConn->getOne($sql);
                    // Get To Account Balance
                    $sql = "select beginning_balance + debit_amount - credit_amount as beginning_balance from PhreeBooksR21.pb_chart_of_accounts_history where account_id = '1200' and period = " . $tmpInt2;
                    $toAccount = $this->_oConn->getOne($sql);
                    $tmpInt2++;

                    $sql = "update PhreeBooksR21.pb_chart_of_accounts_history set beginning_balance = " . $fromAccount . " where period = " . $i . " and account_id = '4010'";
                    $this->_oConn->doQuery($sql);
                    $sql = "update PhreeBooksR21.pb_chart_of_accounts_history set beginning_balance = " . $toAccount . " where period = " . $i . " and account_id = '1200'";
                    $this->_oConn->doQuery($sql);
                }

                $id = $aRow["member_id"];
                $sql = "update " . PREFIX . "_members set last_bill_dt = GETDATE() where member_id = " . $id;
                $this->_oConn->doQuery($sql);

                $id = (int) $nextInvoice;
                $id++;
                $sql = "update PhreeBooksR21.pb_current_status set next_inv_num = '" . $id . "' where next_inv_num = '" . $nextInvoice . "'";
                $this->_oConn->doQuery($sql);
            }
            // }
            $invoice_edt = "";
            $invoice_sdt = "";
        }
        if ($i > 0) {
            return $return;
        } else {
            return "Nothing to do";
        }
    }

    /**
     * get Config list
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function getGateways() {

        // get a list of all admin

        $sql = "SELECT 
			      *
        FROM 
			      datagate.dg_gateways";

        try {
            $stmt = $this->_oConn->prepare($sql);
            $stmt->execute();

            // loop through result and build return array
            $i = 0;
            while ($aRow = $stmt->fetch()) {

                $return[$i]["DataKey"] = $aRow["DataKey"];
                $return[$i]["GatewayName"] = $aRow["GatewayName"];
                $return[$i]["DirectoryPath"] = $aRow["DirectoryPath"];
                $return[$i]["GatewayDirection"] = $aRow["GatewayDirection"];
                $return[$i]["GatewayDesc"] = $aRow["GatewayDesc"];
                ++$i;
            }
            return $return;
        } catch (PDOException $e) {
            return false;
        }
    }

    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
}

// close the class definition
