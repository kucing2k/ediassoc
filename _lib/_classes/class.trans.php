<?php

// File Location: /_lib/_classes/class.trans.php

require_once("DB.php");

/** 
 * handles transactions functions
 *
 * @author Stephen Case <src@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright EDI Associates, Inc.
 *
 */
class trans { // open the class definition

    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer $id [optional] advertisement identifier
     * @access public
     */
    function trans() {
        
        // implement pear db object
        $this->_oConn = get_db();
        
         
    }
    
    // PRIVATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    function fputqueue ($fi, $aDat) { 
    
        $line = ""; 
        $fi=str_replace("/", "\\", $fi);
        $fp = fopen($fi,"a");
        foreach($aDat as $val) { 
            
            $line .= $val; 
        
        }
        
        return fputs($fp, $line); 
    
    }

    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    function getMembers() {
        
        // get a list of all users

        $sql = "SELECT 
            member_id,
            member_company_name,
            member_status,
            member_created_dt,
            member_login
        FROM 
            ".PREFIX."_members
        WHERE 
            member_deleted=0
        ORDER BY
            member_company_name";
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            $return[$i]["Id"] = $aRow["member_id"];
            $return[$i]["Login"] = $aRow["member_login"];
            $return[$i]["Name"] = $aRow["member_login"]." - ".$aRow["member_company_name"];
            $return[$i]["Status"] = $aRow["member_status"];
            $return[$i]["Created Date"] = strtotime($aRow["member_created_dt"]);
            $return[$i]["PartnerIds"] = $this->getPartners($aRow["member_id"]);
            $return[$i]["OBTrans"] = $this->getVanDocListOBAll($aRow["member_id"]);
            ++$i;
        }
        return $return;
    }    

    function getAliasIds($wMem, $iPage=0) {
        
        // get a list of all users

        $sql = "SELECT 
			member_qual,
			member_receiver_id
                FROM 
			".PREFIX."_member_ids
                WHERE 
			member_id=".$wMem;
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
		$return[$i]["member_qual"] = $aRow["member_qual"];
		$return[$i]["member_receiver_id"] = $aRow["member_receiver_id"];
		++$i;
        }
        return $return;
    }    

    function getPartners($wMem, $iPage=0) {
        
        // get a list of all users

        $sql = "SELECT 
			tp_edi_qual,
			tp_edi_code,
			tp_name
                FROM 
			".PREFIX."_partners
                WHERE 
			tp_member_id=".$wMem."
			and tp_deleted=0
		ORDER BY
			tp_name";
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
		$return[$i]["tp_edi_qual"] = $aRow["tp_edi_qual"];
		$return[$i]["tp_edi_code"] = $aRow["tp_edi_code"];
		$return[$i]["tp_name"] = $aRow["tp_name"];
		++$i;
        }
        return $return;
    }    


    /** 
     * get users count for paging
     *
     * @return integer record count
     * @access public
     */
     function getVanDocListOBAll($iId, $sSql) {
        
        // get a list of all Partner Keys and Partners Name
        $sql = "SELECT DISTINCT
                    van_IntControl_ID,
                    van_Document_Type
                FROM 
                    ".PREFIX."_VanReportData
                WHERE 
                    van_Sender_ID IN (select member_receiver_id from edia_member_ids where member_id = " . $iId . ")";

        if ($sSql) {
            $sql = $sql.$sSql;
        }

        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
            $return[$i]["van_IntControl_ID"] = $aRow["van_IntControl_ID"];
            $return[$i]["van_Document_Type"] = $aRow["van_Document_Type"];
            ++$i;
        }
        return $return;
     }

    /** 
     * get users count for paging
     *
     * @return integer record count
     * @access public
     */
     function getDocumentList($sSql, $iId) {
        
        // get a list of all Partner Keys and Partners Name
        $sql = "SELECT 
                    van_IntControl_ID,
                    van_Sender_Qual,
                    van_Sender_ID,
                    van_Sender_Name,
                    van_Receiver_Qual,
                    van_Receiver_ID,
                    van_Receiver_Name,
                    van_Document_Type
                FROM 
                    ".DG_PREFIX."..dg_interchanges
        WHERE 
            (ISA_Sender IN (select member_receiver_id from edia_member_ids where member_id = " . $iId . ") 
        OR
            ISA_Receiver IN (select member_receiver_id from edia_member_ids where member_id = " . $iId . "))";

        if ($sSql) {
            $sql = $sql.$sSql;
        }

        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
            $return[$i]["DataKey"] = $aRow["DataKey"];
            $return[$i]["IntStatus"] = $aRow["IntStatus"];
            $return[$i]["ISA_Sender"] = $aRow["ISA_Sender_Qual"] ." - ". $aRow["ISA_Sender"];
            $return[$i]["ISA_Receiver"] = $aRow["ISA_Receiver_Qual"] ." - ". $aRow["ISA_Receiver"];
            $return[$i]["ISA_Control_Number"] = $aRow["ISA_Control_Number"];
            $return[$i]["DateStamp"] = $aRow["DateStamp"];
            ++$i;
        }
        return $return;
     }

    /** 
     * get interchange from datablob
     *
     * @return integer record count
     * @access public
     */
     function queueDocument($wDocId, $fi) {
        // get interchange from datablob

        $sql = "Select 
                    DataBlob    
                FROM 
                    ".DG_PREFIX."..dg_interchanges
                WHERE
                    DataKey = " . $wDocId;
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
		$return = $this->fputqueue($fi, $rsTmp);
        }
        return $return;
          
    }


    function getRecordInfo($wRec) {
        
        $sql = "SELECT FileName
        from 
            ".DG_PREFIX."..dg_store 
        where 
            InterchangeKey=".$wRec;
        	
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC);        
            
    return $aRow;		
    }


    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::




} // close the class definition

?>