<?php

// File Location: /_lib/_classes/class.members.php

// require PEAR objects
require_once("DB.php");
require_once("class.users.php");

// require USER objects
require_once("config.php");
require_once("funcs.php");

/** 
 * handles user functions
 *
 * @author Stephen Case <Stephen@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright EDI Associates, Inc.
 *
 */
class mem { // open the class definition
    
    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setPartId()
     */
    var $_iMemId;
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer user id [optional]
     * @access public
     */
    function mem() {
        
        // Instanciate the database connection
        $this->_oConn = get_db();
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::

     /** 
     * set user permissions
     *
     * @param array permissions
     * @return boolean
     * @access private
     */ 
    
    function _setMPerms($aPerms, $wComId) {
        
        // check perms array
        if (count($aPerms)) {
            
            $sql = "DELETE FROM 
                        ".PREFIX."_member_perms
                    WHERE 
                        member_id = ?";
            
            $this->_oConn->doQuery($sql, array($wComId));
            
            // loop through permissions array
            while (list($key, $val) = each($aPerms)) {
                
                // add new permission record
                $sql = "INSERT INTO ".PREFIX."_member_perms (
                            user_app_id, 
                            member_id,
                            member_perm
                        ) values (
                            ?,
                            ?,
                            ?
                        )";
                
                $this->_oConn->doQuery($sql, array($key, $wComId, $val));
            }
            
            return true;
        }
    }
    
     
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    function setMemId($iMemId) {
        
        if (is_int($iMemId)) {
            
            $this->_iMemId = $iMemId;
        }
    }
    
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    

    /** 
     * get member count for paging
     *
     * @return integer record count
     * @access public
     */
     function getNewMemberID() {
     
        $sql = "EXEC usp_GenerateID";
        
        if (DB::isError($iCnt = $this->_oConn->getOne($sql))) {
            
            catchExc($iCnt->getMessage());
            return false;
        }
        
        return $iCnt;
     
     }

    /** 
     * get member count for paging
     *
     * @return integer record count
     * @access public
     */
     function getMemberCount() {
        
        $sql = "SELECT COUNT(member_id) AS mem_cnt 
                FROM 
            ".PREFIX."_members
                WHERE 
            member_deleted=0";
        
        return $this->_oConn->getOne($sql);
     }

    function getMember($wMem) {
        
        // get a list of all users

        $sql = "SELECT 
            member_id,
            member_company_name,
            member_modified_dt,
            member_created_dt,
            member_login,
            member_gateway,
            billing_address_1, 
            billing_address_2, 
            billing_address_3, 
            billing_address_4, 
            billing_address_city, 
            billing_address_state, 
            billing_address_zip, 
            billing_address_country, 
            billing_address_contactName, 
            billing_address_telephone, 
            billing_email,
            billing_amount,
            last_bill_dt
        FROM 
            ".PREFIX."_members
        WHERE 
            member_id=?
            and member_deleted=0";
        
        // loop through result and build return array
       
        $aRow = $this->_oConn->getRow($sql, array($wMem));
            
        $return["MemId"] = $aRow["member_id"];
        $return["Name"] = $aRow["member_company_name"];
        $return["LoginID"] = $aRow["member_login"];
        $return["Created Date"] = strtotime($aRow["member_created_dt"]);
        $return["Modified Date"] = strtotime($aRow["member_modified_dt"]);
        $return["member_gateway"] = $aRow["member_gateway"];
        $return["billing_address_1"] = $aRow["billing_address_1"];
        $return["billing_address_2"] = $aRow["billing_address_2"];
        $return["billing_address_3"] = $aRow["billing_address_3"];
        $return["billing_address_4"] = $aRow["billing_address_4"];
        $return["billing_address_city"] = $aRow["billing_address_city"];
        $return["billing_address_state"] = $aRow["billing_address_state"];
        $return["billing_address_zip"] = $aRow["billing_address_zip"];
        $return["billing_address_country"] = $aRow["billing_address_country"];
        $return["billing_address_contactName"] = $aRow["billing_address_contactName"];
        $return["billing_address_telephone"] = $aRow["billing_address_telephone"];
        $return["billing_email"] = $aRow["billing_email"];
        $return["billing_amount"] = $aRow["billing_amount"];
        $return["last_bill_dt"] = $aRow["last_bill_dt"];
        $return["member_users"] = $this->getUsers($wMem);
        $return["member_partners"] = $this->getPartners($wMem);
        $return["member_ids"] = $this->getAliasIds($wMem);
        
        return $return;
    }    

    /** 
     * get users list
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function getUsers($wMem, $iPage=0) {
        
        // get a list of all users

        $sql = "SELECT 
			usr.user_id,
			usr.user_login,
			usr.user_pass,
			usr.user_remind,
			usr.user_email,
			usr.user_status,
			usr.user_created_dt,
			usr.user_modified_dt
                FROM 
			".PREFIX."_members mem,
			".PREFIX."_member_users usr
                WHERE 
			mem.member_id=?
			and mem.member_id=usr.user_member_id
			and usr.user_deleted=0
		ORDER BY
			usr.user_login";
       //         LIMIT ".$iPage.", ".ROWCOUNT;
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($wMem));
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
            $return[$i]["User Id"] = $aRow["user_id"];
            $return[$i]["User Name"] = $aRow["user_login"];
            $return[$i]["User Password"] = $aRow["user_pass"];
            $return[$i]["User Password Reminder"] = $aRow["user_remind"];
            $return[$i]["User Email"] = $aRow["user_email"];
            $return[$i]["Status"] = $aRow["user_status"];
            $return[$i]["Created Date"] = strtotime($aRow["user_created_dt"]);
            $return[$i]["Modified Date"] = strtotime($aRow["user_modified_dt"]);
            ++$i;
        }
        return $return;
    }    

    function getAliasIds($wMem, $iPage=0) {
        
        // get a list of all users

        $sql = "SELECT 
            member_qual,
            member_receiver_id
         FROM 
            ".PREFIX."_member_ids
         WHERE 
            member_id=?";
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($wMem));
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
            $return[$i]["member_qual"] = $aRow["member_qual"];
            $return[$i]["member_receiver_id"] = $aRow["member_receiver_id"];
            ++$i;
        }
        return $return;
    }    

    function getPartners($wMem, $iPage=0) {
        
        // get a list of all users

        $sql = "SELECT 
            tp_id,
            tp_name,
            tp_status,
            tp_created_dt
        FROM 
            ".PREFIX."_partners
        WHERE 
            tp_member_id=?
            and tp_deleted=0
        ORDER BY
            tp_name";
         //       LIMIT ".$iPage.", ".ROWCOUNT;
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($wMem));
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
            $return[$i]["tp Id"] = $aRow["tp_id"];
            $return[$i]["tp Name"] = $aRow["tp_name"];
            $return[$i]["Status"] = $aRow["tp_status"];
            $return[$i]["Created Date"] = strtotime($aRow["tp_created_dt"]);
            ++$i;
        }
        return $return;
    }    

    function getMembers() {
        
        // get a list of all users

        $sql = "SELECT 
            member_id,
            member_company_name,
            member_status,
            member_created_dt,
            member_login
        FROM 
            ".PREFIX."_members
        WHERE 
            member_deleted=0
        ORDER BY
            member_company_name";
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
            $return[$i]["Id"] = $aRow["member_id"];
            $return[$i]["Name"] = $aRow["member_login"]." - ".$aRow["member_company_name"];
            $return[$i]["Status"] = $aRow["member_status"];
            $return[$i]["Created Date"] = strtotime($aRow["member_created_dt"]);
            ++$i;
        }
        return $return;
    }    

    function getMyMembers($wMem) {
        
        // get a list of all users

        $sql = "SELECT 
            member_id,
            member_company_name,
            member_status,
            member_created_dt,
            member_login
        FROM 
            ".PREFIX."_members
        WHERE 
            member_deleted=0
        AND
            member_parent_id=?
        ORDER BY
            member_company_name";
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($wMem));
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
        $return[$i]["Id"] = $aRow["member_id"];
        $return[$i]["Name"] = $aRow["member_login"]." - ".$aRow["member_company_name"];
        $return[$i]["Status"] = $aRow["member_status"];
        $return[$i]["Created Date"] = strtotime($aRow["member_created_dt"]);
        ++$i;
        }
        return $return;
    }    
     
     function getPermissions($iId) {

        // get a list of all users

        $sql = "SELECT 
            user_app_id,
            member_perm
        FROM 
            ".PREFIX."_member_perms 
        where
            member_id = ?";        

        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($iId));
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
            $return[$aRow["user_app_id"]] = $aRow["member_perm"];

        }
        return $return;
    }    

     function getApplications() {

        // get a list of all users

        $sql = "SELECT 
            user_app_id,
            user_app_name,
            user_app_path 
        FROM 
            ".PREFIX."_user_apps";        

        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
            $return[$i]["user_app_id"] = $aRow["user_app_id"];
            $return[$i]["user_app_name"] = $aRow["user_app_name"];
            $return[$i]["user_app_path"] = $aRow["user_app_path"];
            ++$i;
        }
        return $return;
    }

     function getMyMemberApplications() {

        // get a list of all users

        $sql = "SELECT 
            user_app_id,
            user_app_name,
            user_app_path 
        FROM 
            ".PREFIX."_user_apps
        WHERE
            is_childable = 1";        

        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {

            $return[$i]["user_app_id"] = $aRow["user_app_id"];
            $return[$i]["user_app_name"] = $aRow["user_app_name"];
            $return[$i]["user_app_path"] = $aRow["user_app_path"];
            ++$i;
        }
        return $return;
    }

     function getBillableOptions() {

        // get a list of all users

        $sql = "SELECT 
            billing_option_id,
            billing_option,
            billing_option_desc 
        FROM 
            ".PREFIX."_members_billing_options";        

        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            $return[$i]["billing_option_id"] = $aRow["billing_option_id"];
            $return[$i]["billing_option"] = $aRow["billing_option"];
            $return[$i]["billing_option_desc"] = $aRow["billing_option_desc"];
            ++$i;
        }
        return $return;
    }

     function getMyMemberBillableOptions() {

        // get a list of all users

        $sql = "SELECT 
            billing_option_id,
            billing_option,
            billing_option_desc 
        FROM 
            ".PREFIX."_members_billing_options
        WHERE
            is_childable = 1";        

        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            $return[$i]["billing_option_id"] = $aRow["billing_option_id"];
            $return[$i]["billing_option"] = $aRow["billing_option"];
            $return[$i]["billing_option_desc"] = $aRow["billing_option_desc"];
            ++$i;
        }
        return $return;
    }

     function getGatewayOptions() {

        // get a list of all users

        $sql = "SELECT 
            DataKey,
            GatewayName
        FROM 
            ".PREFIX."_dg_gateways";        

        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            $return[$i]["DataKey"] = $aRow["DataKey"];
            $return[$i]["GatewayName"] = $aRow["GatewayName"];
            ++$i;
        }
        return $return;
    }


    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


    /** 
     * insert a new member record
     *
     * @param array partner data
     * @return boolean
     * @access public
     */
    function addMyMember($iId, $aArgs) {

    $oUsers = new users;

        
        if ($aArgs["EdiCo"] == "") {
            $aArgs["EdiCo"] = $this->getNewMemberID();
        }
        // update user record
        $sql = "INSERT INTO ".PREFIX."_members(
             member_company_name, 
             member_login,
             member_parent_id,
             billing_option, 
             billing_address_name, 
             billing_address_1, 
             billing_address_2, 
             billing_address_3, 
             billing_address_4, 
             billing_address_city, 
             billing_address_state, 
             billing_address_zip, 
             billing_address_country, 
             billing_address_contactName, 
             billing_address_telephone, 
             billing_address_fax, 
             billing_email
        ) VALUES (
             '".$aArgs["CName"]."',
             '".$aArgs["EdiCo"]."',
             ".$iId.",
             '".$aArgs["billing_option"]."',
             '".$aArgs["billing_address_name"]."',
             '".$aArgs["billing_address_1"]."',
             '".$aArgs["billing_address_2"]."',
             '".$aArgs["billing_address_3"]."',
             '".$aArgs["billing_address_4"]."',
             '".$aArgs["billing_address_city"]."',
             '".$aArgs["billing_address_state"]."',
             '".$aArgs["billing_address_zip"]."',
             '".$aArgs["billing_address_country"]."',
             '".$aArgs["billing_address_contactName"]."',
             '".$aArgs["billing_address_telephone"]."',
             '".$aArgs["billing_address_fax"]."',
             '".$aArgs["billing_email"]."')";
        
        $this->_oConn->doQuery($sql);
            // get unique identifier for new record
            
        $sql = "SELECT IDENT_CURRENT('".PREFIX."_members')";


            $iMemId = $this->_oConn->getOne($sql);
            
            // set member variable for unique identifier
            settype($iMemId, "integer");

            $this->_setMPerms($aArgs["Perms"], $iMemId);

            $aUArgs["Compa"] = $iMemId;
            $aUArgs["Login"] = "Admin";
            $aUArgs["LName"] = $aArgs["billing_address_contactName"];
            $aUArgs["Email"] = $aArgs["billing_email"];
            $aUArgs["EdiCo"] = $aArgs["EdiCo"];

            // Setting Default Admin Perms...
            $aUArgs["Perms"]["7"] = "3";
            $aUArgs["Perms"]["8"] = "3";
            $aUArgs["Perms"]["13"] = "3";
            
            $aPerms = $aArgs["Perms"];

            if (count($aPerms)) {
                
                // loop through permissions array
                $i = 0;
            
                while (list($key, $val) = each($aPerms)) {
                    
                    if ($val == "1") {
                        $aUArgs["Perms"][$key] = "3";
                        ++$i;
                        
                    }
                }
                // $aUArgs["Perms"] = $aUPerms;
                $this->addMyPartner($iId, $aArgs);
                $oUsers->addUser($aUArgs);
                return true;
            }
    }

    /** 
     * add a new partner record
     *
     * @param array $aArgs user data
     * @return boolean
     * @access public
     */
    function addMyPartner($iComId, $aArgs) {
        
        // check for existing user name
        $sql = "INSERT INTO ".PREFIX."_partners (
            tp_member_id,
            tp_name,
            tp_app_code,
            tp_edi_code,
            tp_contact,
            tp_phone,
            tp_email,
            tp_created_dt,
            tp_modified_dt
        ) values (
            '".$iComId."',
            '".$aArgs["CName"]."',
            '".$aArgs["AppCo"]."',
            '".$aArgs["EdiCo"]."',
            '".$aArgs["billing_address_contactName"]."',
            '".$aArgs["billing_address_telephone"]."', 
            '".$aArgs["billing_email"]."',
            GETDATE(),
            GETDATE()
        )";
            
        $this->_oConn->doQuery($sql);
        
        return true;
    }




    /** 
     * insert a new member record
     *
     * @param array partner data
     * @return boolean
     * @access public
     */
    function addMember($aArgs) {

    $oUsers = new users;

        
        if ($aArgs["EdiCo"] == "") {
            $aArgs["EdiCo"] = $this->getNewMemberID();
        }
        // update user record
        $sql = "INSERT INTO ".PREFIX."_members(
             member_company_name, 
             member_login,
             billing_option,
             member_gateway, 
             billing_address_name, 
             billing_address_1, 
             billing_address_2, 
             billing_address_3, 
             billing_address_4, 
             billing_address_city, 
             billing_address_state, 
             billing_address_zip, 
             billing_address_country, 
             billing_address_contactName, 
             billing_address_telephone, 
             billing_address_fax, 
             billing_ccard, 
             billing_ccard_name, 
             billing_ccard_dt, 
             billing_email,
             billing_contract_amount,
             billing_contract_qty,
             billing_overage_amount,
             billing_monthly_service_fee,
             billing_kilo_character_mem,
             billing_kilo_character_nonmem,
             billing_cat_subscription_fee,
             billing_cat_per_cust_fee
        ) VALUES (
             '".$aArgs["CName"]."',
             '".$aArgs["EdiCo"]."',
             '".$aArgs["billing_option"]."',
             ".$aArgs["member_gateway"].",
             '".$aArgs["billing_address_name"]."',
             '".$aArgs["billing_address_1"]."',
             '".$aArgs["billing_address_2"]."',
             '".$aArgs["billing_address_3"]."',
             '".$aArgs["billing_address_4"]."',
             '".$aArgs["billing_address_city"]."',
             '".$aArgs["billing_address_state"]."',
             '".$aArgs["billing_address_zip"]."',
             '".$aArgs["billing_address_country"]."',
             '".$aArgs["billing_address_contactName"]."',
             '".$aArgs["billing_address_telephone"]."',
             '".$aArgs["billing_address_fax"]."',
             '".$aArgs["billing_ccard"]."',
             '".$aArgs["billing_ccard_name"]."',
             '".$aArgs["billing_ccard_dt"]."',
             '".$aArgs["billing_email"]."',
             ".$aArgs["billing_contract_amount"].",
             ".$aArgs["billing_contract_qty"].",
             ".$aArgs["billing_overage_amount"].",
             ".$aArgs["billing_monthly_service_fee"].",
             ".$aArgs["billing_kilo_character_mem"].",
             ".$aArgs["billing_kilo_character_nonmem"].",
             ".$aArgs["billing_cat_subscription_fee"].",
             ".$aArgs["billing_cat_per_cust_fee"].")";
        
        $this->_oConn->doQuery($sql);
            // get unique identifier for new record
            
        $sql = "SELECT IDENT_CURRENT('".PREFIX."_members')";


            $iMemId = $this->_oConn->getOne($sql);
            
            // set member variable for unique identifier
            settype($iMemId, "integer");

            
            $this->_setMPerms($aArgs["Perms"], $iMemId);

            $aUArgs["Compa"] = $iMemId;
            $aUArgs["Login"] = "Admin";
            $aUArgs["LName"] = $aArgs["billing_address_contactName"];
            $aUArgs["Email"] = $aArgs["billing_email"];
            
            $aPerms = $aArgs["Perms"];


            if (count($aPerms)) {
                
                // loop through permissions array
                $i = 0;
            
                while (list($key, $val) = each($aPerms)) {
                    
                    if ($val == "1") {
                        $aUArgs["Perms"][$key] = "3";
                        ++$i;
                        
                    }
                }
                // $aUArgs["Perms"] = $aUPerms;
                $oUsers->addUser($aUArgs);
                return true;
            }
    }

    /** 
     * edit an existing member record
     *
     * @param array partner data
     * @return boolean
     * @access public
     */
    function addMemberAlias($mid, $mqual, $misaid) {

        $sql = "INSERT INTO ".PREFIX."_member_ids(
            member_id,
            member_qual,
            member_receiver_id
        ) VALUES (
            ".$mid.",
            '".$mqual."',
            '".$misaid."')";
        
        $this->_oConn->doQuery($sql);
        
        return true;
   
    }
    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * edit an existing member record
     *
     * @param array partner data
     * @return boolean
     * @access public
     */
    function editMyMember($aArgs) {
        
        // update user record
        $sql = "UPDATE ".PREFIX."_members SET 
             member_company_name='".$aArgs["CName"]."',
             member_modified_dt=(GETDATE()),
             billing_address_name='".$aArgs["billing_address_name"]."',       
             billing_address_1='".$aArgs["billing_address_1"]."',          
             billing_address_2='".$aArgs["billing_address_2"]."',          
             billing_address_3='".$aArgs["billing_address_3"]."',          
             billing_address_4='".$aArgs["billing_address_4"]."',          
             billing_address_city='".$aArgs["billing_address_city"]."',       
             billing_address_state='".$aArgs["billing_address_state"]."',      
             billing_address_zip='".$aArgs["billing_address_zip"]."',        
             billing_address_country='".$aArgs["billing_address_country"]."',    
             billing_address_contactName='".$aArgs["billing_address_contactName"]."',
             billing_address_telephone='".$aArgs["billing_address_telephone"]."',  
             billing_address_fax='".$aArgs["billing_address_fax"]."',        
             billing_email='".$aArgs["billing_email"]."'

        WHERE 
             member_id=".$aArgs["id"];
        
        $this->_oConn->doQuery($sql);
        $this->_setMPerms($aArgs["Perms"], $aArgs["id"]);
        return true;
    }


    /** 
     * edit an existing member record
     *
     * @param array partner data
     * @return boolean
     * @access public
     */
    function editMember($aArgs) {
        
        // update user record
        $sql = "UPDATE ".PREFIX."_members SET 
             member_company_name = '".$aArgs["CName"]."',
             member_login = '".$aArgs["EdiCo"]."',
             member_modified_dt = GETDATE(),
             member_gateway = ".$aArgs["member_gateway"].",
             billing_address_1 = '".$aArgs["billing_address_1"]."',          
             billing_address_2 = '".$aArgs["billing_address_2"]."',          
             billing_address_3 = '".$aArgs["billing_address_3"]."',          
             billing_address_4 = '".$aArgs["billing_address_4"]."',          
             billing_address_city = '".$aArgs["billing_address_city"]."',       
             billing_address_state = '".$aArgs["billing_address_state"]."',      
             billing_address_zip = '".$aArgs["billing_address_zip"]."',        
             billing_address_country = '".$aArgs["billing_address_country"]."',    
             billing_address_contactName = '".$aArgs["billing_address_contactName"]."',
             billing_address_telephone = '".$aArgs["billing_address_telephone"]."',  
             billing_amount = ".$aArgs["billing_amount"].",         
             last_bill_dt = '".$aArgs["last_bill_dt"]."',           
             billing_email = '".$aArgs["billing_email"]."'
        WHERE 
             member_id=".$aArgs["id"];
        
        $this->_oConn->doQuery($sql);
        $this->_setMPerms($aArgs["Perms"], $aArgs["id"]);
        return true;
    }
    
    /** 
     * delete a member record
     *
     * @return boolean
     * @access public
     */
    function deleteMember() {
        
        $sql = "UPDATE ".PREFIX."_members SET 
                    member_deleted=1, 
                    member_deleted_dt=GETDATE() 
                WHERE 
                    member_id=".$this->_iMemId;
        
        $this->_oConn->doQuery($sql);
        
        $this->deactivateMember();
        return true;
    }
    
    /** 
     * activate a member record
     *
     * @return boolean
     * @access public
     */
    function activateMember() {
        
        $sql = "UPDATE ".PREFIX."_members SET 
                    member_status=1 
                WHERE 
                    member_id=".$this->_iMemId;
         
        $this->_oConn->doQuery($sql);
    }
    
    /** 
     * deactivate a member record
     *
     * @return boolean
     * @access public
     */
    function deactivateMember() {
        
        $sql = "UPDATE ".PREFIX."_members SET 
                    member_status=0 
                WHERE 
                    member_id=".$this->_iMemId;
        
        $this->_oConn->doQuery($sql);
    }
}

?>
