<?php

// File Location: /_lib/_classes/class.draft.php

// require PEAR objects
require_once("DB.php");

// require USER objects
require_once("config.php");
require_once("funcs.php");

/** 
 * handles user functions
 *
 * @author Mike Buzzard <src@cubancouncil.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright Apress
 *
 */
class inbox { // open the class definition
    
    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setUserId()
     */
    var $_iInboxId;
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer user id [optional]
     * @access public
     */
    function inbox() {
        
        // Instanciate the database connection
        $this->_oConn = get_db();
        
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
     
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    function getTranType($sTid) {

	$sql = "Select EDIADocTypeID
        FROM 
		".PREFIX."_edidocs
	WHERE 
		EDIATransID='".$sTid."'";

    return $this->_oConn->getOne($sql);
    }

    function getResponseTo($wRec) {

    $sql = "select d.EDIATransID, d.TransNumber, d.EDIATransDateTime, d.EDIADocTypeID, s.stat_desc
    FROM 
        ".PREFIX."_edidocs as d,
        ".PREFIX."_box_status_cfg as s
    WHERE 
        s.stat_id=d.EDIAStatus
        and d.EDIAStatus!=11
        and d.EDIAStatus!=5
        and d.RefIBEDIATransID='".$wRec."'";
        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["EDIATransID"] = $aRow["EDIATransID"];
        $return[$i]["TransNumber"] = $aRow["TransNumber"];
        $return[$i]["EDIATransDateTime"] = $aRow["EDIATransDateTime"];
        $return[$i]["EDIADocTypeID"] = $aRow["EDIADocTypeID"];
        $return[$i]["EDIAStatus"] = $aRow["stat_desc"];
        ++$i;
        }
    return $return;
    }

    function getOutBoundTypes($wRec,$wMem) {

    $sql = "select 
        tpp.tpp_transaction
    from
        ".PREFIX."_partner_profiles as tpp,
        ".PREFIX."_partners as ep
    where
        tpp.tpp_partner_id=ep.tp_id
        and ep.tp_edi_code='".$wRec."'
        and ep.tp_member_id=".$wMem."
        and tpp.tpp_Direction=1";

        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["tpp_transaction"] = $aRow["tpp_transaction"];
        ++$i;
        }
    return $return;
    }

    function getRecordDetailTxt($wRec) {

    $sql = "select dt_det_key, dt_det_text
    FROM 
        ".PREFIX."_det_text 
    WHERE 
        dt_edia_key='".$wRec."'";
        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["dt_det_key"] = $aRow["dt_det_key"];
        $return[$i]["dt_det_text"] = $aRow["dt_det_text"];
        ++$i;
        }
    return $return;
    }

    function getRecordDetailAdds($wRec) {

    $sql = "select *
    FROM 
        ".PREFIX."_det_address 
    WHERE 
        da_edia_key='".$wRec."'";
        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["da_det_key"] = $aRow["da_det_key"];
        $return[$i]["da_type_qual"] = $aRow["da_type_qual"];
        $return[$i]["da_name"] = $aRow["da_name"];
        $return[$i]["da_duns_id"] = $aRow["da_duns_id"];
        $return[$i]["da_address1"] = $aRow["da_address1"];
        $return[$i]["da_address2"] = $aRow["da_address2"];
        $return[$i]["da_address3"] = $aRow["da_address3"];
        $return[$i]["da_address4"] = $aRow["da_address4"];
        $return[$i]["da_city"] = $aRow["da_city"];
        $return[$i]["da_state"] = $aRow["da_state"];
        $return[$i]["da_postalcode"] = $aRow["da_postalcode"];
        $return[$i]["da_country"] = $aRow["da_country"];
        $return[$i]["da_contactname"] = $aRow["da_contactname"];
        $return[$i]["da_ContactPhone"] = $aRow["da_ContactPhone"];
        ++$i;
        }
    return $return;
    }


    function getRecordDetailRefs($wRec) {

    $sql = "select *
    FROM 
        ".PREFIX."_det_ref 
    WHERE 
        dr_edia_key='".$wRec."'";
        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["dr_key"] = $aRow["dr_key"];
        $return[$i]["dr_det_key"] = $aRow["dr_det_key"];
        $return[$i]["dr_qual"] = $aRow["dr_qual"];
        $return[$i]["dr_id"] = $aRow["dr_id"];
        $return[$i]["dr_desc"] = $aRow["dr_desc"];
        ++$i;
        }
    return $return;
    }

    function getHDRRefData($wRec='') {

    $sql = "select trn.*,par.*,mem.*
    FROM
       ".PREFIX."_partners as par,
       ".PREFIX."_edidocs as trn,
       ".PREFIX."_members as mem
    WHERE 
        trn.MemberNum=mem.member_id
        and trn.PartnerNumber=par.tp_id
        and trn.EDIATransID='".$wRec."'";

        
    return $this->_oConn->getRow($sql);        
    }

    function getRecordDetailData($wRec) {

    $sql = "select *
    FROM 
        ".PREFIX."_edidoc_detail 
    WHERE 
        det_edia_key='".$wRec."'";
        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["det_key"] = $aRow["det_key"];
        $return[$i]["det_ediakey"] = $aRow["det_edia_key"];
        $return[$i]["det_itemlinenum"] = $aRow["det_itemlinenum"];
        $return[$i]["det_stop_id"] = $aRow["det_stop_id"];
        $return[$i]["det_stop_dt"] = $aRow["det_stop_dt"];
        $return[$i]["det_stop_tm"] = $aRow["det_stop_tm"];
        $return[$i]["det_item_qty"] = $aRow["det_item_qty"];
        $return[$i]["det_itemunitcost"] = $aRow["det_itemunitcost"];
        $return[$i]["det_itemcatprice"] = $aRow["det_itemcatprice"];
        $return[$i]["det_itemtotalcost"] = $aRow["det_itemtotalcost"];
        $return[$i]["det_itemdescription1"] = $aRow["det_itemdescription1"];
        $return[$i]["det_itemdescription2"] = $aRow["det_itemdescription2"];
        $return[$i]["det_reasoncode"] = $aRow["det_reasoncode"];
        $return[$i]["det_lineweight"] = $aRow["det_lineweight"];
        $return[$i]["det_lineweightuom"] = $aRow["det_lineweightuom"];
        ++$i;
        }
    return $return;        
    }

    function getRecordHeaderData($wRec) {

    $sql = "select trn.*, par.tp_name
    FROM 
        ".PREFIX."_edidocs AS trn,
        ".PREFIX."_partners AS par 
    WHERE 
        trn.PartnerNumber=par.tp_id
        AND trn.EDIATransID='".$wRec."'";
        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["tp_name"] = $aRow["tp_name"];
        $return[$i]["EDIATransID"] = $aRow["EDIATransID"];
        $return[$i]["EDIATransDateTime"] = $aRow["EDIATransDateTime"];
        $return[$i]["BoxDirection"] = $aRow["BoxDirection"];
        $return[$i]["EDIADocTypeID"] = $aRow["EDIADocTypeID"];
        $return[$i]["RefIBEDIATransID"] = $aRow["RefIBEDIATransID"];
        $return[$i]["MemberNum"] = $aRow["MemberNum"];
        $return[$i]["PartnerNumber"] = $aRow["PartnerNumber"];
        $return[$i]["ISASenderID"] = $aRow["ISASenderID"];
        $return[$i]["ISAControlNum"] = $aRow["ISAControlNum"];
        $return[$i]["ISADate"] = $aRow["ISADate"];
        $return[$i]["ISATestFlag"] = $aRow["ISATestFlag"];
        $return[$i]["GSSenderID"] = $aRow["GSSenderID"];
        $return[$i]["GSReceiverID"] = $aRow["GSReceiverID"];
        $return[$i]["GSControlNum"] = $aRow["GSControlNum"];
        $return[$i]["GSDate"] = $aRow["GSDate"];
        $return[$i]["GSDocType"] = $aRow["GSDocType"];
        $return[$i]["GSAgency"] = $aRow["GSAgency"];
        $return[$i]["GSVersion"] = $aRow["GSVersion"];
        $return[$i]["STControlNum"] = $aRow["STControlNum"];
        $return[$i]["STTransType"] = $aRow["STTransType"];
        $return[$i]["TransPurpose"] = $aRow["TransPurpose"];
        $return[$i]["TransType"] = $aRow["TransType"];
        $return[$i]["TransNumber"] = $aRow["TransNumber"];
        $return[$i]["TransReleaseNum"] = $aRow["TransReleaseNum"];
        $return[$i]["TransDate"] = $aRow["TransDate"];
/*
        $return[$i]["RefTransNumber"] = $aRow["RefTransNumber"];
        $return[$i]["RefTransDate"] = $aRow["RefTransDate"];
        $return[$i]["RefTransNumber1"] = $aRow["RefTransNumber1"];
        $return[$i]["RefTransDate1"] = $aRow["RefTransDate1"];
        $return[$i]["DepartmentNum"] = $aRow["DepartmentNum"];
        $return[$i]["InternalVenNum"] = $aRow["InternalVenNum"];
        $return[$i]["TermType"] = $aRow["TermType"];
        $return[$i]["TermBasisDate"] = $aRow["TermBasisDate"];
        $return[$i]["TermPercent"] = $aRow["TermPercent"];
        $return[$i]["TermDiscountDaysDue"] = $aRow["TermDiscountDaysDue"];
        $return[$i]["TermNetDays"] = $aRow["TermNetDays"];
        $return[$i]["NetAmountDue"] = $aRow["NetAmountDue"];
        $return[$i]["DateCancelAfter"] = $aRow["DateCancelAfter"];
        $return[$i]["TimeCancelAfter"] = $aRow["TimeCancelAfter"];
        $return[$i]["DateRequestedShip"] = $aRow["DateRequestedShip"];
        $return[$i]["TimeRequestedShip"] = $aRow["TimeRequestedShip"];
        $return[$i]["DateShipNoLater"] = $aRow["DateShipNoLater"];
        $return[$i]["TimeShipNoLater"] = $aRow["TimeShipNoLater"];
        $return[$i]["DateDoNotDeliverBefore"] = $aRow["DateDoNotDeliverBefore"];
        $return[$i]["TimeDoNotDeliverBefore"] = $aRow["TimeDoNotDeliverBefore"];
        $return[$i]["OrderNotes"] = $aRow["OrderNotes"];
        $return[$i]["BillToName"] = $aRow["BillToName"];
        $return[$i]["BillToID"] = $aRow["BillToID"];
        $return[$i]["BillToAddress1"] = $aRow["BillToAddress1"];
        $return[$i]["BillToAddress2"] = $aRow["BillToAddress2"];
        $return[$i]["BillToAddress3"] = $aRow["BillToAddress3"];
        $return[$i]["BillToAddress4"] = $aRow["BillToAddress4"];
        $return[$i]["BillToCity"] = $aRow["BillToCity"];
        $return[$i]["BillToState"] = $aRow["BillToState"];
        $return[$i]["BillToPostalCode"] = $aRow["BillToPostalCode"];
        $return[$i]["BillToCountry"] = $aRow["BillToCountry"];
        $return[$i]["ShipToName"] = $aRow["ShipToName"];
        $return[$i]["ShipToID"] = $aRow["ShipToID"];
        $return[$i]["ShipToAddress1"] = $aRow["ShipToAddress1"];
        $return[$i]["ShipToAddress2"] = $aRow["ShipToAddress2"];
        $return[$i]["ShipToAddress3"] = $aRow["ShipToAddress3"];
        $return[$i]["ShipToAddress4"] = $aRow["ShipToAddress4"];
        $return[$i]["ShipToCity"] = $aRow["ShipToCity"];
        $return[$i]["ShipToState"] = $aRow["ShipToState"];
        $return[$i]["ShipToPostalCode"] = $aRow["ShipToPostalCode"];
        $return[$i]["ShipToCountry"] = $aRow["ShipToCountry"];
        $return[$i]["ContactName"] = $aRow["ContactName"];
        $return[$i]["ContactPhone"] = $aRow["ContactPhone"];
        $return[$i]["ContactRefInfo"] = $aRow["ContactRefInfo"];
        $return[$i]["TransIDOld"] = $aRow["TransIDOld"];
        $return[$i]["DocComment"] = $aRow["DocComment"];
        $return[$i]["InvToName"] = $aRow["InvToName"];
        $return[$i]["InvToID"] = $aRow["InvToID"];
        $return[$i]["InvToAddress1"] = $aRow["InvToAddress1"];
        $return[$i]["InvToAddress2"] = $aRow["InvToAddress2"];
        $return[$i]["InvToAddress3"] = $aRow["InvToAddress3"];
        $return[$i]["InvToAddress4"] = $aRow["InvToAddress4"];
        $return[$i]["InvToCity"] = $aRow["InvToCity"];
        $return[$i]["InvToState"] = $aRow["InvToState"];
        $return[$i]["InvToPostalCode"] = $aRow["InvToPostalCode"];
        $return[$i]["InvToCountry"] = $aRow["InvToCountry"];
        $return[$i]["PayToName"] = $aRow["PayToName"];
        $return[$i]["PayToID"] = $aRow["PayToID"];
        $return[$i]["PayToAddress1"] = $aRow["PayToAddress1"];
        $return[$i]["PayToAddress2"] = $aRow["PayToAddress2"];
        $return[$i]["PayToAddress3"] = $aRow["PayToAddress3"];
        $return[$i]["PayToAddress4"] = $aRow["PayToAddress4"];
        $return[$i]["PayToCity"] = $aRow["PayToCity"];
        $return[$i]["PayToState"] = $aRow["PayToState"];
        $return[$i]["PayToPostalCode"] = $aRow["PayToPostalCode"];
        $return[$i]["PayToCountry"] = $aRow["PayToCountry"];
*/
        $return[$i]["EDIAStatus"] = $aRow["EDIAStatus"];
        $return[$i]["EDIAStatusDate"] = $aRow["EDIAStatusDate"];
        $return[$i]["EDIACreateDate"] = $aRow["EDIACreateDate"];
        $return[$i]["EDIALastModifyDate"] = $aRow["EDIALastModifyDate"];
        $return[$i]["EDIACreateLogin"] = $aRow["EDIACreateLogin"];
        $return[$i]["EDIALastModifyLogin"] = $aRow["EDIALastModifyLogin"];
        $return[$i]["ShipperName"] = $aRow["ShipperName"];
        $return[$i]["ShipperTrackingNum"] = $aRow["ShipperTrackingNum"];
        $return[$i]["ReadFlag"] = $aRow["ReadFlag"];
        $return[$i]["LastReadBy"] = $aRow["LastReadBy"];
        $return[$i]["SCACCode"] = $aRow["SCACCode"];
        $return[$i]["TransportationMethodID"] = $aRow["TransportationMethodID"];
        $return[$i]["EqptDescriptionID"] = $aRow["EqptDescriptionID"];
        $return[$i]["Ack"] = $aRow["Ack"];
        $return[$i]["OKToSend"] = $aRow["OKToSend"];
        $return[$i]["AckEDIATransID"] = $aRow["AckEDIATransID"];
        $return[$i]["DocumentBlobKEY"] = $aRow["DocumentBlobKEY"];
        $return[$i]["TranslationReportFile"] = $aRow["TranslationReportFile"];
        $return[$i]["NbrRecords"] = $aRow["NbrRecords"];
        $return[$i]["NbrBytes"] = $aRow["NbrBytes"];
        $return[$i]["Notified"] = $aRow["Notified"];
        $return[$i]["Weekly"] = $aRow["Weekly"];
        $return[$i]["Monthly"] = $aRow["Monthly"];
        $return[$i]["Invoiced"] = $aRow["Invoiced"];
        $return[$i]["FolderID"] = $aRow["FolderID"];
        ++$i;
        }
    return $return;        
    }


     function getStatus($sTid) {
        
        $sql = "SELECT EDIAStatus 
                FROM 
                    ".PREFIX."_edidocs 
                WHERE 
                    EDIATransID='".$sTid."'";
        
        return $this->_oConn->getOne($sql);
    }
     
    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    function updateAck($sTid) {
        $sql = "UPDATE ".PREFIX."_edidocs SET 
                    EDIAStatus=3,
                    EDIAStatusDate=GETDATE() 
                WHERE 
                    EDIATransID='".$sTid."'";
        $this->_oConn->doQuery($sql);
    }
    
    function updateRead($sTid) {
        $sql = "UPDATE ".PREFIX."_edidocs SET 
                    EDIAStatus=2,
                    EDIAStatusDate=GETDATE() 
                WHERE 
                    EDIATransID='".$sTid."'";
        $this->_oConn->doQuery($sql);
    }
    
    function deleteTopic($sTid) {
        $sql = "UPDATE ".PREFIX."_edidocs SET 
                    EDIAStatus=5,
                    EDIAStatusDate=GETDATE() 
                WHERE 
                    EDIATransID='".$sTid."'";
        $this->_oConn->doQuery($sql);
    }

    function archiveTopic($sTid) {
        $sql = "UPDATE ".PREFIX."_edidocs SET 
                    EDIAStatus=4,
                    EDIAStatusDate=GETDATE() 
                WHERE 
                    EDIATransID='".$sTid."'";
        $this->_oConn->doQuery($sql);
    }
}

?>
