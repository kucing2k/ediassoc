<?php

// File Location: /_lib/_classes/class.draft.php

// require PEAR objects
require_once("DB.php");

// require USER objects
require_once("config.php");
require_once("funcs.php");

/** 
 * handles user functions
 *
 * @author Mike Buzzard <src@cubancouncil.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright Apress
 *
 */
class mailbox { // open the class definition
    
    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setUserId()
     */
    var $_iMailboxId;
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer user id [optional]
     * @access public
     */
    function mailbox() {
        
        // Instanciate the database connection
        $this->_oConn = get_db();
        
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

     function getMemberID($a) {
     
        $sql = "select member_login from ".PREFIX."_members where member_id = ".(int)$a;
        
        return $this->_oConn->getOne($sql);
     
     }

    function getRecordInfo($wRec) {

        $sql = "SELECT convert(varchar(36), EDIATransID) as EDIATransID, 
            ISA_Auth_Qual, 
            ISA_Auth_Info, 
            ISA_Security_Qual, 
            ISA_Security_Info, 
            ISA_Sender_Qual, 
            ISA_Sender, 
            ISA_Receiver_Qual, 
            ISA_Receiver, 
            ISA_Date, 
            ISA_Time, 
            ISA_Control_ID, 
            ISA_Version_Release, 
            ISA_Control_Number, 
            ISA_Ack_Requested, 
            ISA_Test_Indicator, 
            GS_Functional_Id, 
            GS_Sender, 
            GS_Receiver, 
            GS_Date, 
            GS_Time, 
            GS_Control_Number, 
            GS_Agency, 
            GS_Version_Release, 
            ST_Transaction_Set_ID, 
            ST_Control_Number
        from 
            ".PREFIX."_interchanges 
        where 
            EDIATransID='".$wRec."'";
        
    return $this->_oConn->getRow($sql);        
    }

    function getFileInfo($wRec) {

        $sql = "SELECT 
            IntFileName
        from 
            datagate.dg_interchanges 
        where 
            DataKey=".$wRec;
        
    return $this->_oConn->getOne($sql);
    }
    function getFileTerm($wRec) {

        $sql = "SELECT 
            Terminator
        from 
            datagate.dg_interchanges 
        where 
            DataKey=".$wRec;
        
    return $this->_oConn->getOne($sql);
    }
    
    function reSendTopic($sTid, $iCId) {
        $sql = "SELECT 
            IntFileName
        from 
            datagate.dg_interchanges 
        where 
            DataKey=".$sTid;
        
        $rsTmpFile = $this->_oConn->getOne($sql);
        
        $rsTmpMember = $this->getMemberID($iCId);
        
        $sql = "select DirectoryPath from datagate.dg_gateways where member_id in(select DataKey from datagate.dg_partners where tp_edi_code = '".$rsTmpMember."') and GatewayDirection = 0";

        $rsTmpToDir = $this->_oConn->getOne($sql);

        $currentFile = $rsTmpFile; 
        $parts = Explode('/', $currentFile); 
        $currentFile = $parts[count($parts) - 1]; 
        
        if (!copy($rsTmpFile, $rsTmpToDir."/".$currentFile)) {
            echo "failed to copy $file...\n";
        }

        
        // echo $rsTmpToDir;
        
    }

    function getTransHeaders($wWhere, $iPage=0) {
        
        // get a list of all users

        $sql = "Select *
        FROM 
            datagate.dg_interchanges
        WHERE 
            ".$wWhere."
        ORDER BY
            DateStamp DESC";
        
        try {
            $stmt = $this->_oConn->prepare($sql);
            $stmt->execute();
            
            // loop through result and build return array
            $i = 0;
            while ($aRow = $stmt->fetch()) {
                $return[$i]["Key ID"] = $aRow["DataKey"];
                $return[$i]["IntStatus"] = $aRow["IntStatus"];
                $return[$i]["Send ID"] = $aRow["ISA_Sender_Qual"] . "/" . $aRow["ISA_Sender"];
                $return[$i]["Rec ID"] = $aRow["ISA_Receiver_Qual"] . "/" . $aRow["ISA_Receiver"];
                $return[$i]["Trans Num"] = $aRow["ISA_Control_Number"];
                $return[$i]["Log DT"] = $aRow["DateStamp"];
                ++$i;
            }
            return $return;
        } catch (PDOException $e) {
            return false;
        }
    }    



    function getTranType($sTid) {

    $sql = "Select EDIADocTypeID
        FROM 
        ".PREFIX."_edidocs
    WHERE 
        EDIATransID='".$sTid."'";

    return $this->_oConn->getOne($sql);
    }

    function getResponseTo($wRec) {

    $sql = "select d.EDIATransID, d.TransNumber, d.EDIATransDateTime, d.EDIADocTypeID, s.stat_desc
    FROM 
        ".PREFIX."_edidocs as d,
        ".PREFIX."_box_status_cfg as s
    WHERE 
        s.stat_id=d.EDIAStatus
        and d.EDIAStatus!=11
        and d.EDIAStatus!=5
        and d.RefIBEDIATransID='".$wRec."'";
        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["EDIATransID"] = $aRow["EDIATransID"];
        $return[$i]["TransNumber"] = $aRow["TransNumber"];
        $return[$i]["EDIATransDateTime"] = $aRow["EDIATransDateTime"];
        $return[$i]["EDIADocTypeID"] = $aRow["EDIADocTypeID"];
        $return[$i]["EDIAStatus"] = $aRow["stat_desc"];
        ++$i;
        }
    return $return;
    }

    function getOutBoundTypes($wRec,$wMem) {

    $sql = "select 
        tpp.tpp_transaction
    from
        ".PREFIX."_partner_profiles as tpp,
        ".PREFIX."_partners as ep
    where
        tpp.tpp_partner_id=ep.tp_id
        and ep.tp_edi_code='".$wRec."'
        and ep.tp_member_id=".$wMem."
        and tpp.tpp_Direction=1";

        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["tpp_transaction"] = $aRow["tpp_transaction"];
        ++$i;
        }
    return $return;
    }

    function getRecordSchedule($wRec,$wAge,$wVer) {

    $sql = "select *
    FROM 
        ".PREFIX."_edidoc_detail_schedule 
    WHERE 
        ds_edia_key='".$wRec."'";
        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["ds_qty"] = $aRow["ds_qty"];
        $return[$i]["ds_qty_uom"] = $aRow["ds_qty_uom"];
        $return[$i]["ds_date_time_qualifier"] = RemoveShouting($this->getStandardDesc($wAge,$wVer,$aRow["ds_date_time_qualifier"],"0374"));
        $return[$i]["ds_date"] = $aRow["ds_date"];
        ++$i;
        }
    return $return;
    }

    function getRecordDetailTxt($wRec) {

    $sql = "select *
    FROM 
        ".PREFIX."_edidoc_text 
    WHERE 
        dt_edia_key='".$wRec."'";
        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["dt_det_text"] = $aRow["dt_det_text"];
        ++$i;
        }
    return $return;
    }

    function getRecordAdds($wRec,$wAge,$wVer) {

    $sql = "select *
    FROM 
        ".PREFIX."_edidoc_address 
    WHERE 
        da_edia_key='".$wRec."'";
        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["da_det_key"] = $aRow["da_det_key"]."";
        $return[$i]["da_type_qual"] = RemoveShouting($this->getStandardDesc($wAge,$wVer,$aRow["da_type_qual"],"0098"));
        $return[$i]["da_name"] = $aRow["da_name"]."";
        $return[$i]["da_duns_id"] = $aRow["da_duns_id"]."";
        $return[$i]["da_address1"] = $aRow["da_address1"]."";
        $return[$i]["da_address2"] = $aRow["da_address2"]."";
        $return[$i]["da_address3"] = $aRow["da_address3"]."";
        $return[$i]["da_address4"] = $aRow["da_address4"]."";
        $return[$i]["da_city"] = $aRow["da_city"]."";
        $return[$i]["da_state"] = $aRow["da_state"]."";
        $return[$i]["da_postalcode"] = $aRow["da_postalcode"]."";
        $return[$i]["da_country"] = $aRow["da_country"]."";
        $return[$i]["da_contactname"] = $aRow["da_contactname"]."";
        $return[$i]["da_ContactPhone"] = formatPH(trim($aRow["da_ContactPhone"]))."";
        $return[$i]["da_contactemail"] = $aRow["da_contactemail"]."";  
        ++$i;
        }
    return $return;
    }
    

    function getStandardDesc($wAge,$wVer,$wVal,$wEle) {
    
       $sql = "select Description from standards_db..Code where Agency = '".$wAge."' and Version = '".$wVer."' and ElementID = '".$wEle."' and Value = '".$wVal."'";

        return $this->_oConn->getOne($sql);
    
    }


    function getRecordRefs($wRec,$wAge,$wVer) {

    $sql = "select *
    FROM 
        ".PREFIX."_edidoc_ref 
    WHERE 
        dr_edia_key='".$wRec."'";
        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["dr_key"] = $aRow["dr_key"];
        $return[$i]["dr_det_key"] = $aRow["dr_det_key"];
        $return[$i]["dr_qual"] = RemoveShouting($this->getStandardDesc($wAge,$wVer,$aRow["dr_qual"],"0128"));
        $return[$i]["dr_id"] = $aRow["dr_id"];
        $return[$i]["dr_desc"] = $aRow["dr_desc"];
        ++$i;
        }
    return $return;
    }

    function getHDRRefData($wRec='') {

    $sql = "select trn.*,par.*,mem.*
    FROM
       ".PREFIX."_partners as par,
       ".PREFIX."_edidocs as trn,
       ".PREFIX."_members as mem
    WHERE 
        trn.MemberNum=mem.member_id
        and trn.PartnerNumber=par.tp_id
        and trn.EDIATransID='".$wRec."'";

        
    return $this->_oConn->getRow($sql);        
    }

    function getRecordDetailData($wRec='',$wAge='',$wVer='') {

    $sql = "SELECT
        DataKey,
        convert(varchar(36),det_key) as det_key,
        convert(varchar(36),det_edia_key) as det_edia_key,
        det_itemlinenum,
        det_item_qty_Order,
        det_item_qty_ToReceive,
        det_Item_uom,
        det_Item_upc,
        det_Item_ean,
        det_Item_cust_part,
        det_Item_Cat_Num,
        det_itemunitcost,
        det_itemcatprice,
        det_itemtotalcost,
        det_itemdescription1,
        det_itemdescription2,
        det_reasoncode,
        det_lineweight,
        det_lineweightuom,
        det_Item_sku,
        det_stop_dt,
        det_stop_id,
        det_stop_tm 
    FROM 
        ".PREFIX."_edidoc_detail
    WHERE 
        det_edia_key='".$wRec."'";
        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["DataKey"] = $aRow["DataKey"];
        $return[$i]["det_key"] = $aRow["det_key"];
        $return[$i]["det_edia_key"] = $aRow["det_edia_key"];
        $return[$i]["det_itemlinenum"] = $aRow["det_itemlinenum"];
        $return[$i]["det_item_qty_Order"] = $aRow["det_item_qty_Order"];
        $return[$i]["det_item_qty_ToReceive"] = $aRow["det_item_qty_ToReceive"];
        $return[$i]["det_Item_uom"] = $aRow["det_Item_uom"];
        $return[$i]["det_Item_upc"] = $aRow["det_Item_upc"];
        $return[$i]["det_Item_ean"] = $aRow["det_Item_ean"];
        $return[$i]["det_Item_cust_part"] = $aRow["det_Item_cust_part"];
        $return[$i]["det_Item_Cat_Num"] = $aRow["det_Item_Cat_Num"];
        $return[$i]["det_itemunitcost"] = currency($aRow["det_itemunitcost"]);
        $return[$i]["det_itemcatprice"] = $aRow["det_itemcatprice"];
        $return[$i]["det_itemtotalcost"] = $aRow["det_itemtotalcost"];
        $return[$i]["det_itemdescription1"] = $aRow["det_itemdescription1"];
        $return[$i]["det_itemdescription2"] = $aRow["det_itemdescription2"];
        $return[$i]["det_reasoncode"] = $aRow["det_reasoncode"];
        $return[$i]["det_lineweight"] = $aRow["det_lineweight"];
        $return[$i]["det_lineweightuom"] = $aRow["det_lineweightuom"];
        $return[$i]["det_Item_sku"] = $aRow["det_Item_sku"];
        $return[$i]["det_stop_dt"] = $aRow["det_stop_dt"];
        $return[$i]["det_stop_id"] = $aRow["det_stop_id"];
        $return[$i]["det_stop_tm"] = $aRow["det_stop_tm"];
        $return[$i]["det_msg_txt"] = $this->getRecordDetailTxt($aRow["det_key"]);
        $return[$i]["det_add_rec"] = $this->getRecordAdds($aRow["det_key"],$wAge,$wVer);
        $return[$i]["det_lin_sch"] = $this->getRecordSchedule($aRow["det_key"],$wAge,$wVer);
        ++$i;
        }
    return $return;
    }


    function getRecord820DetailData($wRec='',$wAge='',$wVer='') {

    $sql = "SELECT DataKey,
        convert(varchar(36),det_key) as det_key,
        convert(varchar(36),det_edia_key) as det_edia_key,
        det_ref_doc_type,
        det_ref_doc_number,
        det_ref_doc_date,
        det_pay_action,
        det_paid_amount,
        det_invoice_amount,
        det_cash_discount,
        det_cash_discount_reason
    FROM 
        ".PREFIX."_edidoc_remit_advice
    WHERE 
        det_edia_key='".$wRec."'";
        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["DataKey"] = $aRow["DataKey"];
        $return[$i]["det_key"] = $aRow["det_key"];
        $return[$i]["det_edia_key"] = $aRow["det_edia_key"];
        $return[$i]["det_ref_doc_type"] = RemoveShouting($this->getStandardDesc($wAge,$wVer,$aRow["det_ref_doc_type"],"0128"));
        $return[$i]["det_ref_doc_number"] = $aRow["det_ref_doc_number"];
        $return[$i]["det_ref_doc_date"] = $aRow["det_ref_doc_date"];
        $return[$i]["det_pay_action"] = RemoveShouting($this->getStandardDesc($wAge,$wVer,$aRow["det_pay_action"],"0482"));
        $return[$i]["det_paid_amount"] = currency($aRow["det_paid_amount"]);
        $return[$i]["det_invoice_amount"] = currency($aRow["det_invoice_amount"]);
        $return[$i]["det_cash_discount"] = currency($aRow["det_cash_discount"]);
        $return[$i]["det_cash_discount_reason"] = RemoveShouting($this->getStandardDesc($wAge,$wVer,$aRow["det_cash_discount_reason"],"0426"));
        $return[$i]["det_ref_rec"] = $this->getRecordRefs($aRow["det_key"],$wAge,$wVer);
        ++$i;
        }
    return $return;
    }

    function getRecord812DetailData($wRec='',$wAge='',$wVer='') {

    $sql = "SELECT DataKey,
        convert(varchar(36),det_key) as det_key,
        convert(varchar(36),det_edia_key) as det_edia_key,
        det_ref_doc_type,
        det_ref_doc_number,
        det_ref_doc_date,
        det_pay_action,
        det_paid_amount,
        det_invoice_amount,
        det_cash_discount,
        det_cash_discount_reason
    FROM 
        ".PREFIX."_edidoc_remit_advice
    WHERE 
        det_edia_key='".$wRec."'";
        
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute();
    
    // loop through result and build return array
    $i = 0;
    while ($aRow = $stmt->fetch()) {

        $return[$i]["DataKey"] = $aRow["DataKey"];
        $return[$i]["det_key"] = $aRow["det_key"];
        $return[$i]["det_edia_key"] = $aRow["det_edia_key"];
        $return[$i]["det_ref_doc_type"] = RemoveShouting($this->getStandardDesc($wAge,$wVer,$aRow["det_ref_doc_type"],"0128"));
        $return[$i]["det_ref_doc_number"] = $aRow["det_ref_doc_number"];
        $return[$i]["det_ref_doc_date"] = $aRow["det_ref_doc_date"];
        $return[$i]["det_pay_action"] = RemoveShouting($this->getStandardDesc($wAge,$wVer,$aRow["det_pay_action"],"0482"));
        $return[$i]["det_paid_amount"] = currency($aRow["det_paid_amount"]);
        $return[$i]["det_invoice_amount"] = currency($aRow["det_invoice_amount"]);
        $return[$i]["det_cash_discount"] = currency($aRow["det_cash_discount"]);
        $return[$i]["det_cash_discount_reason"] = RemoveShouting($this->getStandardDesc($wAge,$wVer,$aRow["det_cash_discount_reason"],"0426"));
        $return[$i]["det_ref_rec"] = $this->getRecordRefs($aRow["det_key"],$wAge,$wVer);
        ++$i;
        }
    return $return;
    }

    function getStatus($sTid) {
       
       $sql = "SELECT EDIAStatus 
           FROM 
               ".PREFIX."_edidocs 
           WHERE 
               EDIATransID='".$sTid."'";
        
        return $this->_oConn->getOne($sql);
    }
     
    function get820HdrData($wRec='') {

    $sql = "select *
        FROM
           ".PREFIX."_edidoc_ra
        WHERE 
            ra_edia_key='".$wRec."'";

    return $this->_oConn->getRow($sql);        
    }

    function get812HdrData($wRec='',$wAge='',$wVer='') {

    $sql = "select *
        FROM
           ".PREFIX."_edidoc_ra
        WHERE 
            ra_edia_key='".$wRec."'";

    $aRow = $this->_oConn->getRow($sql);

    $aRow["ra_trans_handling_code"] = RemoveShouting($this->getStandardDesc($wAge,$wVer,$aRow["ra_trans_handling_code"],"0305"));
    $aRow["creditdebit_flag_code"] = RemoveShouting($this->getStandardDesc($wAge,$wVer,$aRow["creditdebit_flag_code"],"0478"));

    return $aRow;        
    }


    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    function updateAck($sTid) {
        $sql = "UPDATE ".PREFIX."_edidocs SET 
                EDIAStatus=3,
                EDIAStatusDate=GETDATE() 
            WHERE 
                EDIATransID='".$sTid."'";
        
        $this->_oConn->doQuery($sql);
    }
    
    function updateRead($sTid) {
        $sql = "UPDATE ".PREFIX."_edidocs SET 
                EDIAStatus=2,
                EDIAStatusDate=GETDATE() 
            WHERE 
                EDIATransID='".$sTid."'";
        
        $this->_oConn->doQuery($sql);
    }
    
    function deleteTopic($sTid) {
        $sql = "UPDATE ".PREFIX."_edidocs SET 
                EDIAStatus=5,
                EDIAStatusDate=GETDATE() 
            WHERE 
                EDIATransID='".$sTid."'";
        
        $this->_oConn->doQuery($sql);
    }

    function archiveTopic($sTid) {
        $sql = "UPDATE ".PREFIX."_edidocs SET 
                EDIAStatus=4,
                EDIAStatusDate=GETDATE() 
            WHERE 
                EDIATransID='".$sTid."'";
        
        $this->_oConn->doQuery($sql);
    }
}

?>
