<?php

// File Location: /_lib/_classes/class.edia.php

require_once("DB.php");

/** 
 * handles transactions functions
 *
 * @author Stephen Case <src@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright EDI Associates, Inc.
 *
 */
class edia { // open the class definition
    
    /** 
     * unique identifier for a company
     *
     * @var integer
     * @access private
     * @see setId()
     */
    var $_iPartId;
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer $id [optional] advertisement identifier
     * @access public
     */
    function edia() {
        
        // implement pear db object
        $this->_oConn = get_db();
        
         
    }
    
    // PRIVATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /** 
     * verify unique partner name
     *
     * @param string $sPart company name
     * @return boolean
     * @access private
     */
    function _partnExists($sPart, $sComp) {
        
        $sql = "SELECT 
                    count(1) 
                FROM 
                    ".PREFIX."_partners 
                WHERE 
                    tp_name='".$sPart."'
                    and tp_member_id=".$sComp."
                    AND tp_deleted=0";
        
        return $this->_oConn->getOne($sql);
    }

    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   
    /**
     * set the _iUserId variable for the class
     *
     * @param integer $iUserId unique identifier
     * @access public
     */
    function setPartId($iPartId) {
        
        if (is_int($iPartId)) {
            
            $this->_iPartId = $iPartId;
        }
    }

    function setTransId($iTransId) {
        
        if ($iTransId) {
            
            $this->_iTransId = $iTransId;
        }
    }
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::



    /** 
     * get users count for paging
     *
     * @return integer record count
     * @access public
     */
     function getTransBoxCount($wComId) {
        
        $sql = "SELECT COUNT(*) AS tran_cnt 
                FROM 
                    ".PREFIX."_edidocs 
                WHERE 
                    MemberNum=".$wComId." and EDIAStatus=1";
        
        return $this->_oConn->getOne($sql);
     }

    /** 
     * get new transaction count
     *
     * @return boolean
     * @access private
     */
    function getNewTransBox() {
    
        
        $sql = "SELECT DISTINCT 
            BoxDirection
        FROM 
            ".PREFIX."_edidocs 
        WHERE 
            MemberNum=".$iComp." and EDIAStatus=1";
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
            $return[$i]["bdir"] = $aRow["BoxDirection"];
            $return[$i]["cnt"] = $this->getTransBoxCount($aRow["BoxDirection"], $iComId);
            ++$i;
	}

        return $return;
    }
    


    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    /** 
     * add a new user record
     *
     * @param array $aArgs user data
     * @return boolean
     * @access public
     */
    function addPartner($aArgs) {
        
        // check for existing user name
        if ($this->_partnExists($aArgs["CName"], $aArgs["Compa"])) {
            
            catchErr("This user name already exists");
            return false;
            
        } else {
            
            // add new user record
            $sql = "INSERT INTO ".PREFIX."_partners (
                        tp_member_id,
                        tp_name,
                        tp_app_code,
                        tp_edi_code, 
                        tp_contact, 
                        tp_phone, 
                        tp_email, 
                        tp_created_dt, 
                        tp_modified_dt
                    ) values (
			".$aArgs["Compa"].",
			'".$aArgs["CName"]."',
			'".$aArgs["AppCo"]."',
			'".$aArgs["EdiCo"]."',
                        '".$aArgs["Conta"]."', 
                        '".$aArgs["Phone"]."', 
			'".$aArgs["Email"]."',
                        GETDATE(), 
                        GETDATE())
                    )";
            
            $this->_oConn->doQuery($sql);
            
            // set member variable for unique identifier
            // settype($iPartId, "integer");
            // $this->setPartId($iPartId);
        }
        return true;
    }
    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /** 
     * edit an existing partner record
     *
     * @param array partner data
     * @return boolean
     * @access public
     */
    function editPartner($aArgs) {
        
        // update user record
        $sql = "UPDATE ".PREFIX."_partners SET 
			tp_name='".$aArgs["CName"]."',
			tp_app_code='".$aArgs["AppCo"]."',
			tp_edi_code='".$aArgs["EdiCo"]."',
			tp_contact='".$aArgs["Conta"]."',
			tp_phone='".$aArgs["Phone"]."',
			tp_email='".$aArgs["Email"]."',
			tp_modified_dt=GETDATE()
                WHERE 
			tp_id=".$aArgs["id"]." 
			and tp_member_id=".$aArgs["Compa"];
        
        $this->_oConn->doQuery($sql);
        return true;
    }
    
    /** 
     * delete a trading partner record
     *
     * @return boolean
     * @access public
     */
    function deletePartner() {
        
        $sql = "UPDATE ".PREFIX."_partners SET 
                    tp_deleted=1, 
                    tp_deleted_dt=GETDATE() 
                WHERE 
                    tp_id=".$this->_iPartId;
        
        $this->_oConn->doQuery($sql);
        
        $this->deactivatePartner();
        return true;
    }
    
    /** 
     * activate a trading partner record
     *
     * @return boolean
     * @access public
     */
    function activatePartner() {
        
        $sql = "UPDATE ".PREFIX."_partners SET 
                    tp_status=1 
                WHERE 
                    tp_id=".$this->_iPartId;
         
        $this->_oConn->doQuery($sql);
    }
    
    /** 
     * deactivate a user record
     *
     * @return boolean
     * @access public
     */
    function deactivatePartner() {
        
        $sql = "UPDATE ".PREFIX."_partners SET 
                    tp_status=0 
                WHERE 
                    tp_id=".$this->_iPartId;
        
        $this->_oConn->doQuery($sql);
    }
} // close the class definition

?>