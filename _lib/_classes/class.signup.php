<?php
// File Location: /_lib/_classes/class.signup.php
require_once("Mail.php");
require_once("pdo.php");

/** 
 * handles user functions
 *
 * @author Stephen Case <stephen@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright EDI Associates, Inc.
 *
 */
class signup { // open the class definition
    
    /** 
     * PEAR mail object
     *
     * @var object
     * @access private
     */
    var $_oMail;
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer user id [optional]
     * @access public
     */
    function signup($iSignId = '') {
        
        // implement pear mail object
        $params["host"] = "mail.ediassociates.com";
        $params["port"] = "25";
        $params["auth"] = true;
        $params["username"] = "car@ediassociates.com";
        $params["password"] = "mko09ijn";

        $this->_oMail =& Mail::factory("smtp", $params);
        
        // implement db object
        $this->_oConn = get_db();
        
        
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    
    /** 
     * verify unique user name
     *
     * @param string $sUser user name
     * @return boolean
     * @access private
     */
    function _userExists($sLogin, $sComp) {

        $sql = "SELECT 
                    count(1) 
                FROM 
                    ".PREFIX."_member_users 
                WHERE 
                    user_login='".$sLogin."'
                    AND user_member_id=".$sComp."
                    AND user_deleted=0";
       
        return $this->_oConn->getOne($sql);
    }
    
    /** 
     * send email notification
     *
     * @param string $sBody email body
     * @return boolean
     * @access private
     */
    function _createAdministratorsEmail($newId, $sPasswordRemind, $aArgs) {
        
        // assign mail properties
        $aHeaders["To"] = $sRecipients = $aArgs["a_email"];
        $aHeaders["From"] = "car@ediassociates.com";
        $aHeaders["Subject"] = "EDI Associates - New Account Notification";
        $aHeaders["Priority"] = "3";
        
        // build user email
        $sBody = "Dear Member,\r\n\r\n";
        $sBody .= "Welcome, Your EDI Associates Van-On-Demand account has been created successfully.\r\n\r\n";
        $sBody .= "   Company ID: ".$newId."\r\n\r\n";
        $sBody .= "Here is your administrators login and password, please keep it in a safe place:\r\n\r\n";
        $sBody .= "   User Name: Admin\r\n";
        $sBody .= "   Password: ".$sPasswordRemind."\r\n\r\n";
        $sBody .= "*** Please not that this user account was created but is currently not active. You may activate the account by selecting the following link or by copy and pasting the link into your favorite web browser\r\n\r\n";
        $sBody .= "http://www.ediassociates.com/site/login/activation.php?sid=".$sPasswordRemind."\r\n\r\n";
        $sBody .= "Use the login information listed above to access your EDI Associates Van-On-Demand account, by selecting Van-On-Demand from the http://www.ediassociates.com website.\r\n\r\n";
        $sBody .= "Van-On-Demand data submissions:\r\n\r\n";
        $sBody .= "Currently Van-On-Demand supports FTP as it primary method of communications and a FTP account as been automatically created for you.\r\n\r\n";
        $sBody .= "   host: ediassociates.com\r\n";
        $sBody .= "   user: ".$newId."\r\n";
        $sBody .= "   pass: ".$sPasswordRemind."\r\n\r\n";
        $sBody .= "*** Note about directory structure ***\r\n";
        $sBody .= "   Inbound - Data coming to you.\r\n";
        $sBody .= "   Outbound - Data being sent by you.\r\n\r\n";
        $sBody .= "If you have any questions, please contact Support@ediassociates.com.\r\n\r\n";
        $sBody .= "Regards,\r\n\r\n";
        $sBody .= "EDI Associates Services Team\r\n";
        
        
        // try to send mail
        $mailTmp = $this->_oMail->send($sRecipients, $aHeaders, $sBody);
        
//        return true;
        return "Account Created Successfully. You should receive any email with your login and activation instructions shortly";
    }
    
     
     /** 
     * set user permissions
     *
     * @param array permissions
     * @return boolean
     * @access private
     */ 
    
    function _setMPerms($aPerms, $wComId) {
        
        // check perms array
        if (count($aPerms)) {
            
            $sql = "DELETE FROM 
                        ".PREFIX."_member_perms
                    WHERE 
                        member_id = ".$wComId;
            $this->_oConn->doQuery($sql);
            // loop through permissions array
            while (list($key, $val) = each($aPerms)) {
                
                // add new permission record
                $sql = "INSERT INTO ".PREFIX."_member_perms (
                            user_app_id, 
                            member_id,
                            member_perm
                        ) values (
                            ".$key.",
                            ".$wComId.",
                            '1'
                        )";
                
                $this->_oConn->doQuery($sql);
            }
            return true;
        }
    }

    /** 
     * set user permissions
     *
     * @param array permissions
     * @return boolean
     * @access private
     */
    function _setPerms($aPerms, $iUserId) {
        
        // check perms array
        if (count($aPerms)) {
            
            $sql = "DELETE FROM 
                        ".PREFIX."_user_perms 
                    WHERE 
                        user_user_id=".$iUserId;
            
            $this->_oConn->doQuery($sql);
            
            // loop through permissions array
            while (list($key, $val) = each($aPerms)) {
                
                // add new permission record
                $sql = "INSERT INTO ".PREFIX."_user_perms (
                            user_user_id, 
                            user_app_id, 
                            user_perm
                        ) values (
                            ".$iUserId.", 
                            ".$key.", 
                            ".$val."
                        )";
                
                $this->_oConn->doQuery($sql);
            }
            return true;
        }
    }

    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * get member count for paging
     *
     * @return integer record count
     * @access public
     */
     function getNewGWID($sPasswordRemind) {
     
        $sql = "select DataKey from datagate.dg_gateways where GatewayName = '".$sPasswordRemind."'";
        
        return $this->_oConn->getOne($sql);
     
     }

    /** 
     * get member count for paging
     *
     * @return integer record count
     * @access public
     */
     function getNewMemberID($sPasswordRemind) {
     
        $sql = "select member_id from EDIA.edia_members where member_login = '".$sPasswordRemind."'";
        
        return $this->_oConn->getOne($sql);
     
     }

    /** 
     * get member count for paging
     *
     * @return integer record count
     * @access public
     */
     function getNewUserAdminID($sPasswordRemind) {
     
        $sql = "select user_id from EDIA.edia_member_users where user_login = 'Admin' and user_member_id = '".$sPasswordRemind."'";
        
        return $this->_oConn->getOne($sql);
     
     }

     
    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    function createNewMemberAccount($aArgs) {
    
        // generate random unique password
        $sPasswordRemind = substr(md5(uniqid(rand(1, 100000))), 3, 8);
        
        // create member company
        $sTmpReturn = $this->createNewMember($sPasswordRemind, $aArgs);
        
        // get the member company id
        $iNewId = $this->getNewMemberID($sPasswordRemind);
        
        // use id to create login id
        $newId = "EDIA" . $iNewId;
        $sTmpString = "Account $newId has been created...<br />";
        $sTmpString = $sTmpString . "Password $sPasswordRemind...<br />";

        // update member company with new login id
        $this->updateMemberLogin($iNewId, $newId);
        
        // create member company perms
        $aArgs["Perms"]["7"] = "3"; // admin settings
        $aArgs["Perms"]["8"] = "3"; // admin users
        $aArgs["Perms"]["2"] = "3"; // admin mailbox
        $aArgs["Perms"]["19"] = "3"; // admin mailbox
        $aPerms = $aArgs["Perms"];
        
        // update default perms for the user admin screen
        $sTmpVal = $this->_setMPerms($aPerms, $iNewId);
        
        // create routing alias
        $sTmpVal = $this->createMemberAlias($iNewId, $newId);
        
        // create administrator
        $sTmpVal = $this->createNewAdminUser($aArgs, $iNewId, $sPasswordRemind);
        $sTmpString = $sTmpString . "Account Admin account has been created...<br />";
        
        // Create ftp account, login to it and create directories
        $sTmpReturn = $this->createNewFTPAccount($newId, $sPasswordRemind, $iNewId);
        $sTmpString = $sTmpString . "Create FTP...<br />";
        
//        /home/ftpusers
        
        $sTmpReturn = $this->createNewPhreeBooksCustomer($newId, $aArgs);
        $sTmpString = $sTmpString . "Accounting Record Created...<br />";

//        print "<p>Router Record Created...";
//        print "<p>Administrator email Created...";

        $sTmpReturn = $this->_createAdministratorsEmail($newId, $sPasswordRemind, $aArgs);

        
        return $sTmpReturn;
        
    
    }
    
    /** 
     * insert a new pureftpd user record then login as user to create in and out directories.
     * 
     * @param New Member ID and New Password data
     * @return summary string
     * @access public
     */
    function createNewFTPAccount($NewMemberID, $sPasswordRemind, $iNewId) {
    
        // generate FTP home directory
        $newFTPDirectory = "/home/ftpusers/" . $NewMemberID;

        $sTmpReturn = "Creating FTP Account...<br />";

        // Create FTP Account entry in SQL
        $sql = "INSERT INTO pureftpd.ftpd (
            `User`,
            `status`, 
            `Password`, 
            `Uid`, 
            `Gid`, 
            `Dir`, 
            `ULBandwidth`, 
            `DLBandwidth`, 
            `comment`, 
            `ipaccess`, 
            `QuotaSize`, 
            `QuotaFiles`,
            `Reminder`
        ) VALUES (
            '".$NewMemberID."', 
            '1', 
            MD5('".$sPasswordRemind."'), 
            '2001', 
            '2001', 
            '".$newFTPDirectory."', 
            '100', 
            '100', 
            '', 
            '*', 
            '100', 
            '0',
            '".$sPasswordRemind."'
        )";
        
        $this->_oConn->doQuery($sql);
        
        $sTmpReturn = $sTmpReturn . "FTP Account Created...<br />";
        
        $sql = "INSERT INTO datagate.dg_gateways (
            `member_id`,
            `DirectoryPath`,
            `GatewayName`,
            `GatewayDesc`,
            `GatewayDirection`,
            `Status`
        ) VALUES (
            ".$iNewId.",
            '".$newFTPDirectory."/Inbound',
            '".$sPasswordRemind."',
            '".$NewMemberID." Default Inbound Directory',
            1,
            0
        )";
        
        $this->_oConn->doQuery($sql);
        
        $iGWId = $this->getNewGWID($sPasswordRemind);

        $this->updateDatagateDesc($sPasswordRemind);
        
        $sql = "INSERT INTO datagate.dg_gateways (
            `member_id`,
            `DirectoryPath`,
            `GatewayName`,
            `GatewayDesc`,
            `GatewayDirection`,
            `Status`
        ) VALUES (
            ".$iNewId.",
            '".$newFTPDirectory."/Outbound',
            'Outbound',
            '".$NewMemberID." Default Outbound Directory',
            0,
            0
        )";
        
        $this->_oConn->doQuery($sql);
        

        $sql = "INSERT INTO datagate.dg_partners (
            `member_id`,
            `tp_edi_code`,
            `tp_gateway_id`,
            `tp_status`
        ) VALUES (
            ".$iNewId.",
            '".$NewMemberID."',
            ".$iGWId.",
            0
        )";
        
        $this->_oConn->doQuery($sql);


        $newFTPDirectoryInbound = "Inbound";
        $newFTPDirectoryOutbound = "Outbound";

        $sTmpReturn = $sTmpReturn . "Logging into FTP Account...<br />";

        // Global Connection Settings
        $ftp_server = "127.0.0.1";              // FTP Server Address (exclude ftp://)
        $ftp_user_name = $NewMemberID;          // FTP Server Username
        $ftp_user_pass = $sPasswordRemind;      // Password
        
        // Connect to FTP Server
        $conn_id = ftp_connect($ftp_server);
        // Login to FTP Server
        $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
        
        // Verify Log In Status
        if ((!$conn_id) || (!$login_result)) {
            echo "FTP connection has failed! <br />";
            echo "Attempted to connect to $ftp_server for user $ftp_user_name";
            exit;
        } else {
            $sTmpReturn = $sTmpReturn . "Connected to $ftp_server, for user $ftp_user_name <br />";
            if (ftp_mkdir($conn_id, $newFTPDirectoryInbound)) {
                ftp_chmod($conn_id, 0775, $newFTPDirectoryInbound);
                $sTmpReturn = $sTmpReturn . "successfully created $newFTPDirectoryInbound <br />";
            } else {
                $sTmpReturn = $sTmpReturn . "There was a problem while creating $newFTPDirectoryInbound <br />";
            }
            if (ftp_mkdir($conn_id, $newFTPDirectoryOutbound)) {
                $sTmpReturn = $sTmpReturn . "successfully created $newFTPDirectoryOutbound <br />";
                ftp_chmod($conn_id, 0775, $newFTPDirectoryOutbound);
            } else {
                $sTmpReturn = $sTmpReturn . "There was a problem while creating $newFTPDirectoryOutbound <br />";
            }
        }
        ftp_close($conn_id); // Close the FTP Connection
        return $sTmpReturn;
    }

    /** 
     * insert a new member record
     *
     * @param array partner data
     * @return boolean
     * @access public
     */
    function createNewMember($newId, $aArgs) {

        // Create Member Record
        $sql = "INSERT INTO EDIA.edia_members(
             member_company_name, 
             member_login,
             member_status,
             member_deleted,
             member_created_dt,
             member_modified_dt,
             billing_address_1, 
             billing_address_2, 
             billing_address_city, 
             billing_address_state, 
             billing_address_zip, 
             billing_address_country, 
             billing_address_contactName, 
             billing_address_telephone, 
             billing_email
        ) VALUES (
             '".$aArgs["primary_name"]."',
             '".$newId."',
             1,
             0,
             GETDATE(),
             GETDATE(),
             '".$aArgs["address1"]."',
             '".$aArgs["address2"]."',
             '".$aArgs["city_town"]."',
             '".$aArgs["state_province"]."',
             '".$aArgs["postal_code"]."',
             'US',
             '".$aArgs["contact_first"]." ".$aArgs["contact_last"]."',
             '".$aArgs["a_telephone"]."',
             '".$aArgs["a_email"]."')";
        
        $this->_oConn->doQuery($sql);
    return "Done";
    }

    /** 
     * insert a new member record
     *
     * @param array partner data
     * @return boolean
     * @access public
     */
    function createNewPhreeBooksCustomer($newId, $aArgs) {

        $sql = "INSERT INTO PhreeBooksR21.pb_contacts (
            type,
            short_name,
            inactive,
            contact_first,
            contact_middle,
            contact_last,
            gl_type_account,
            dept_rep_id,
            special_terms,
            first_date,
            last_update
        ) VALUES (
            'c',
            '".$newId."',
            0,
            '".$aArgs["contact_first"]."',
            '".$aArgs["contact_middle"]."',
            '".$aArgs["contact_last"]."',
            '1200',
            '0',
            '0::::2500.00',
            GETDATE(),
            GETDATE()
        )";

        $this->_oConn->doQuery($sql);
        
        $sql = "select id from PhreeBooksR21.pb_contacts where short_name = '".$newId."'";
        
        $this->_oConn->doQuery($sql);

        $sql = "Insert into PhreeBooksR21.pb_address_book (
            ref_id,
            type,
            primary_name,
            contact,
            address1,
            address2,
            city_town,
            state_province,
            postal_code,
            country_code,
            telephone1,
            email
        ) VALUES (
            ".$iCnt.",
            'cm',
            '".$aArgs["primary_name"]."',
            '".$aArgs["contact"]."',
            '".$aArgs["address1"]."',
            '".$aArgs["address2"]."',
            '".$aArgs["city_town"]."',
            '".$aArgs["state_province"]."',
            '".$aArgs["postal_code"]."',
            'USA',
            '".$aArgs["a_telephone"]."',
            '".$aArgs["a_email"]."'
        )";
        
        $this->_oConn->doQuery($sql);

        $sql = "Insert into PhreeBooksR21.pb_audit_log (
            user_id,
            reference_id,
            action
        ) VALUES (
            4,
            '".$newId."',
            'Added account - Customers'
        )";
        
        $this->_oConn->doQuery($sql);
    return $sql;
    }


    /** 
     * add a new user record
     *
     * @param array $aArgs user data
     * @return boolean
     * @access public
     */
    function createNewAdminUser($aArgs, $iNewId, $sPasswordRemind) {
        
         // encrypt new password
        $sPassword = substr(md5($sPasswordRemind), 3 , 8);

        // add new user record
        $sql = "INSERT INTO ".PREFIX."_member_users (
            user_member_id,
            user_login,
            user_fname,
            user_lname, 
            user_pass, 
            user_remind, 
            user_email,
            activation_key,
            user_status,
            user_deleted,
            user_created_dt, 
            user_modified_dt
        ) values (
            '".$iNewId."',
            'Admin',
            '".$aArgs["contact_first"]."',
            '".$aArgs["contact_last"]."',
            '".$sPassword."', 
            '".$sPasswordRemind."', 
            '".$aArgs["a_email"]."',
            '".$sPasswordRemind."', 
            0,
            0,
            GETDATE(), 
            GETDATE()
        )";
        
        $this->_oConn->doQuery($sql);
        
        $iUserId = $this->getNewUserAdminID($iNewId);

        // get unique identifier for new record
        
        // set member variable for unique identifier
        settype($iUserId, "integer");

        // set permissions values
        $this->_setPerms($aArgs["Perms"], $iUserId);
            
    }

    function createMemberAlias($mid, $misaid) {

        $sql = "INSERT INTO ".PREFIX."_member_ids(
            member_id,
            member_qual,
            member_receiver_id
        ) VALUES (
            ".$mid.",
            'ZZ',
            '".$misaid."')";
        
        $this->_oConn->doQuery($sql);
        
        return true;
   
    }
    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * update member login id
     *
     * @return boolean
     * @access public
     */
    function updateDatagateDesc($sPasswordRemind) {
        
        $sql = "UPDATE datagate.dg_gateways SET 
                    GatewayName = 'Inbound' 
                WHERE 
                    GatewayName = '".$sPasswordRemind."'";
         
        $this->_oConn->doQuery($sql);
    }
    /** 
     * update member login id
     *
     * @return boolean
     * @access public
     */
    function updateMemberLogin($iNewId, $newId) {
        
        $sql = "UPDATE ".PREFIX."_members SET 
                    member_login='".$newId."' 
                WHERE 
                    member_id=".$iNewId;
         
        $this->_oConn->doQuery($sql);
    }

    /** 
     * send email notification
     *
     * @param string $sBody email body
     * @return boolean
     * @access private
     */
    function _notifyUser($sBody) {
        
        // assign mail properties
        $aUser = $this->getUser();
        $aHeaders["To"] = $sRecipients = $aUser["Email"];
        $aHeaders["From"] = ENTITY." Admin <stephen@ediassociates.com>";
        $aHeaders["Subject"] = ENTITY." System Account Notification";
        $aHeaders["Priority"] = "3";
        
        // try to send mail
        $mailTmp = $this->_oMail->send($sRecipients, $aHeaders, $sBody);

//        if (Mail::isError($mailTmp = $this->_oMail->send($sRecipients, $aHeaders, $sBody))) {
//            catchExc($mailTmp->getMessage());
//            return false;
//        }
        
        return true;
    }

} // close the class definition
