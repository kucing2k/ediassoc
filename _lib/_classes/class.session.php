<?php
// File Location: /_lib/_classes/class.session.php
require_once("class.users.php");
require_once("pdo.php");

/** 
 * user access security
 *
 * @author Mike Buzzard <src@cubancouncil.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright Apress
 *
 */
class session extends users { // open the class definition
    
    /** 
     * string file path
     *
     * @var string
     * @access private
     */
    var $_sFilePath;
    
    /** 
     * @var PDO
     */
    var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @access public
     */
    function __construct() {
// echo DSN;      
        // implement db object$this->_oConn
        $this->_oConn = get_db();
        
        // assign file path variable
        $this->_sFilePath =  str_replace($_SERVER["DOCUMENT_ROOT"], "", 
                                         $_SERVER["SCRIPT_FILENAME"]);
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
     /*
     * set session
     *
     * @param array $aArgs session data
     * @access private
     */
    function _setSession($aArgs) {
        
        // start session
        session_start();
        
        // check session state
        if (!session_is_registered("sUSER")) {
            
            // register session
            session_register("sUSER");
            $_SESSION["sUSER"] = array();
        }
        
        // assign session value
        $sVal = implode("|", $aArgs);
        $_SESSION["sUSER"] = $sVal;
    }
    
     /* 
     * authenticate user
     *
     * @param array $aArgs user login
     * @return boolean
     * @access private
     */
    function _authenticate($aArgs) {
        //print_r($aArgs);
	$sql = "SELECT
		member_id,
		member_company_name,
		member_status
	FROM
		".PREFIX."_members
	WHERE
		member_login=?
		AND member_deleted=0";

        
        $stmt = $this->_oConn->prepare($sql);
        
        // assign result to array
        $stmt->execute(array($aArgs["Company"]));
        $aRowa = $stmt->fetch();

        // check row count
        if ($aRowa === false) {
            
            catchErr("Company does not exist");
            return false;   
            
        // check record status
        } elseif ($aRowa["member_status"] < 1) {
            
            catchErr("Company Account inactive");
            return false;
            
        // check password string
        } elseif (!empty($aRowa["member_id"])) {

	
		$sql = "SELECT
			user_id,
			user_login,
			user_pass,
			user_email,
			user_status
		FROM
			".PREFIX."_member_users
		WHERE
			user_member_id=?
			AND user_login=?
			AND user_deleted=0";
    
    $stmt = $this->_oConn->prepare($sql);
    $stmt->execute(array($aRowa["member_id"], $aArgs["User Name"]));
	
            // assign result to array
            $aRow = $stmt->fetch();
            
            // check row count
            if ($aRow === false) {
                
                catchErr("User does not exist");
                return false;   
                
            // check record status
            } elseif ($aRow["user_status"] < 1) {
                
                catchErr("Account inactive");
                return false;
                
            // check password string
            } elseif (!empty($aRow["user_pass"])) {
                
                // match passwords
                if (strcmp(substr(md5($aArgs["Password"]), 3, 8), $aRow["user_pass"])) {
                    
                    catchErr("Invalid password");
                    return false;
                }
                
                // assign user session array
                $aUser = array($aRow["user_id"], $aRow["user_login"], 
                               $aRow["user_pass"], $aRow["user_email"], 
                               $aRowa["member_id"]);
                
                // set user session
                settype($aRowa["member_id"], "integer");
                settype($aRow["user_id"], "integer");
                $this->setUserId($aRow["user_id"]);
                $this->setComId($aRowa["member_id"]);
                $this->_setSession($aUser);
                return true;
            }
      }
    }
    
    /** 
     * update user login values
     *
     * @return boolean
     * @access private
     */
    function _updateLogin() {
        
        // get user ip address
        $sAddress = $_SERVER["REMOTE_ADDR"];
        
        // get user host name
        $sHost = gethostbyaddr($sAddress);
        
        $sql = "UPDATE ".PREFIX."_member_users SET 
                    user_last_login_dt=GETDATE(), 
                    user_last_login_ip=?, 
                    user_last_login_host=? 
                WHERE 
                    user_id=?";
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($sAddress, $sHost, $this->_iUserId));
    }
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /** 
     * login user
     *
     * @param array $aArgs user data
     * @return string
     * @access public
     */
    function login($aArgs) {
        // authenticate user
        if (!$this->_authenticate($aArgs)) {
            
            catchErr("Could not authenticate user");
            return false;
        }
        
        $sql = "SELECT 
                    app.user_app_path 
                FROM 
                    ".PREFIX."_user_apps app, 
                    ".PREFIX."_user_perms perm 
                WHERE 
                    app.user_app_id=perm.user_app_id 
                    and perm.user_user_id=? 
                    and perm.user_perm > 0 
                ORDER BY 
                    app.user_app_id";
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($this->_iUserId));
        $sPath = $stmt->fetchColumn();
        
        // verify path value
        if (empty($sPath)) {
            
            catchErr("No acceptable application permissions");
            return false;
        }
        
        // update login and return path
        $this->_updateLogin();
        return $sPath;
    }
    
    /** 
     * get user permissions
     *
     * @param array user$aArgs session data
     * @return array
     * @access public
     */
    function getPerms($aArgs) {
        
        // apply path parts to array
        $sFile = array_pop(explode("/", $this->_sFilePath));
        
        // reformat path value
        $sApp = str_replace($sFile, "", $this->_sFilePath);
	if($sApp[0]!='/')
	$sApp = '/'.$sApp;
        
        $sql = "SELECT 
                    perm.user_perm 
                FROM 
                    ".PREFIX."_user_perms perm, 
                    ".PREFIX."_user_apps app, 
                    ".PREFIX."_member_users users, 
                    ".PREFIX."_members mem 
                WHERE
                    mem.member_id=:member_id
                    AND users.user_login=:user_login
                    AND users.user_pass=:user_pass
                    AND users.user_id=:user_id
                    AND perm.user_app_id=app.user_app_id 
                    AND perm.user_user_id=users.user_id 
                    AND app.user_app_path=:user_app_path";
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->bindValue(':member_id', $aArgs[4]);
        $stmt->bindValue(':user_login', $aArgs[1]);
        $stmt->bindValue(':user_pass', $aArgs[2]);
        $stmt->bindValue(':user_id', $aArgs[0]);
        $stmt->bindValue(':user_app_path', $sApp);
        $stmt->execute();
        $iPerm = $stmt->fetchColumn();
//        var_dump($iPerm);exit;
        // if permitted, set session and return permission
        if ($iPerm > 0) {
            
            $this->_setSession($aArgs);
            return $iPerm;
        }
        
        return false;
    }
    
    /** 
     * get application menu
     *
     * @return array
     * @access public
     */
    function getMenu() {
        
        $sql = "SELECT 
                    app.user_app_name, 
                    app.user_app_path, 
                    app.user_app_icon, 
                    perm.user_perm 
                FROM 
                    ".PREFIX."_user_apps app, 
                    ".PREFIX."_user_perms perm 
                WHERE 
                    app.user_app_id=perm.user_app_id 
                    AND perm.user_user_id=?
                    AND perm.user_perm > 0
                    AND is_menu=0 
                ORDER BY 
                    app.user_app_id";
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($this->_iUserId));
        
        // loop result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            $return[$i]["App Name"] = $aRow["user_app_name"];
            $return[$i]["App Path"] = $aRow["user_app_path"];
            $return[$i]["App Perm"] = $aRow["user_perm"];
            $return[$i]["App Icon"] = $aRow["user_app_icon"];
            ++$i;
        }
        return $return;
    }

    /** 
     * get member login
     *
     * @return array
     * @access public
     */
    function getMember() {
        
        $sql = "SELECT 
                    member_login 
                FROM 
                    ".PREFIX."_members 
                WHERE 
                    member_id=?";
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($this->_iComId));
        return $stmt->fetchColumn();
    }
} // close the class definition

?>
