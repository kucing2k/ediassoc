<?php

// File Location: /_lib/_classes/class.reports.php

require_once("DB.php");

/** 
 * Assign configuration variables
 *
 * @author Stephen Case <stephen@ediassociates.com>
 * @copyright EDI Associates, Inc.
 * @version 1.0
 * @since 1.0
 * @access public
 *
 */

class reports { // open the class definition
    
  
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @access public
     */
    function reports() {
        
       
        // implement db object
        $this->_oConn = get_db();
        
        
        
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
     
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * get Transactions
     *
     * @return array user data
     * @access public
     */
function getTransRpt($iDir,$sWhat,$sWho,$sStr,$sSto) {
    

if (!strcmp("*", $sWho)) {
    $sSql = "Direction=".$iDir;
} else {
    $sSql = "Direction=".$iDir." AND PartnerKEY='".$sWho."'";
}

if (!strcmp("F", $sWhat)) {
    $sSql = "TransactionSetID='997' AND ".$sSql;
} else {
    $sSql = "TransactionSetID!='997' AND ".$sSql;
}

if ($sStr && $sSto) {
   $sSql = "TimeCreated >= ".strtotime ($sStr)." AND TimeCreated <= ".strtotime ($sSto)." AND ".$sSql;

}
if ($sStr && $sSto) {
   $sOrd = ", TimeCreated DESC";
   // $sOrd = ", DocumentName DESC";

}
       
        // get a list of all Partner Keys and Partners Name
        $sql = "SELECT 
                    PartnerKEY, 
                    TransactionSetID, 
                    TimeCreated, 
                    DocumentName, 
                    ReferenceData, 
                    ComplianceStatus,
                    DocumentKEY,
                    NbrBytes,
                    NbrRecords
                FROM 
                    GENTRANDatabase..Document_tb
                WHERE
                    ".$sSql."
                ORDER BY PartnerKEY ASC, TransactionSetID DESC".$sOrd;

        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
            $return[$i]["PartnerKEY"] = $aRow["PartnerKEY"];
            $return[$i]["TransactionSetID"] = $aRow["TransactionSetID"];
            $return[$i]["TimeCreated"] = $aRow["TimeCreated"];
            $return[$i]["DocumentName"] = $aRow["DocumentName"];
            $return[$i]["ReferenceData"] = $aRow["ReferenceData"];
            $return[$i]["ComplianceStatus"] = $aRow["ComplianceStatus"];
            $return[$i]["DocumentKEY"] = $aRow["DocumentKEY"];
            $return[$i]["NbrBytes"] = $aRow["NbrBytes"];
            $return[$i]["NbrRecords"] = $aRow["NbrRecords"];
            ++$i;
        }
        return $return;
    }    

    /** 
     * get Transactions
     *
     * @return array user data
     * @access public
     */
function getTransRptCnt($iDir,$sWhat,$sWho,$sStr,$sSto) {
    

	if (!strcmp("*", $sWho)) {
	    $sSql = "Direction=".$iDir;
	} else {
	    $sSql = "Direction=".$iDir." AND PartnerKEY='".$sWho."'";
	}

	if (!strcmp("F", $sWhat)) {
	    $sSql = "TransactionSetID='997' AND ".$sSql;
	} else {
	    $sSql = "TransactionSetID!='997' AND ".$sSql;
	}

	if ($sStr && $sSto) {
	   $sSql = "TimeCreated >= ".strtotime ($sStr)." AND TimeCreated <= ".strtotime ($sSto)." AND ".$sSql;
	
	}
       
        // get a list of all Partner Keys and Partners Name
        $sql = "SELECT count(PartnerKEY) as cnt
                FROM 
                    Document_tb 
                WHERE
                    ".$sSql;

		if (DB::isError($iCnt = $this->_oConn->getOne($sql))) {
		    
		    catchExc($iCnt->getMessage());
		    return false;
		}
	
	return $iCnt;
	}



    /** 
     * get Partner list
     *
     * @return array user data
     * @access public
     */
    function getPartners($wComId) {
        
        // get a list of all Partner Keys and Partners Name
        $sql = "select 
                    tpp_RelationshipTemplateKEY, 
                    tpp_Description 
                from 
                    ".PREFIX."_partner_profiles 
                where 
                    tpp_member_id = ".$wComId." 
                    and tpp_status = 1
                ORDER BY
                    tpp_Description";        


        $stmt = $this->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
            $return[$i]["PartnerKEY"] = $aRow["tpp_RelationshipTemplateKEY"];
            $return[$i]["PartnerName"] = $aRow["tpp_Description"];
            ++$i;
        }
        return $return;
    }    
    
    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
} // close the class definition
