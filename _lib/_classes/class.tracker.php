<?php

// require PEAR objects
require_once("DB.php");

// require USER objects
require_once("config.php");
require_once("funcs.php");

define("ISSUES_TABLE", PREFIX . "_tracker_issues");
define("ISSUES_COMMENTS_TABLE", PREFIX . "_tracker_issues_comments");
define("ISSUES_COMMENTS_ATTACHMENTS_DIR", "/home/casest/www/uploads/.tracker_attachments/");
define("MASTER_MEMBER_ID", 1);
define("DUMP_DB_ERRORS", false);

class Tracker {

    private $statuses = array('OPEN', 'PENDING', 'ON HOLD', 'CLOSED');
    /**
     * PEAR db object
     *
     * @var object
     * @access private
     */
    private $_oConn;
    private $userId;
    private $memberId;

    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /**
     * class constructor
     *
     * @param
     * @access public
     */
    function __construct() {

        $this->getUserInfo();
        if (!$this->userId) {
            die("Error: no user ID.");
        } else {
            $this->_oConn = get_db();
        }
    }

    function getListOfIssues($status) {
        $sth = "SELECT `edia_members`.`member_login`,\n" .
                "`" . ISSUES_TABLE . "`.`issue_id`, `" . ISSUES_TABLE . "`.`subject`, `" . ISSUES_TABLE . "`.`created`, `" . ISSUES_TABLE . "`.`status`\n" .
                "FROM `edia_members` JOIN `edia_member_users` JOIN `" . ISSUES_TABLE . "`\n" .
                "WHERE `edia_members`.`member_id` = `edia_member_users`.`user_member_id` " .
                "AND `" . ISSUES_TABLE . "`.`user_id` = `edia_member_users`.`user_id`";
        if (in_array($status, $this->statuses)) {
            $sth .= " AND `" . ISSUES_TABLE . "`.`status` = '" . $status . "'"; // already checked
        }

        if ($this->memberId != MASTER_MEMBER_ID) {
            $sth .= " AND `edia_member_users`.`user_member_id` = ?";
            // var_dump($sth, $this->memberId);
            $res = $this->_oConn->getAll($sth, array($this->memberId), DB_FETCHMODE_OBJECT);
        } else {
            $res = $this->_oConn->getAll($sth, array(), DB_FETCHMODE_OBJECT);
        }

        $this->checkDBError($res);

        return $res;
    }

    function addIssue($data, $files) {
        $sth = $this->_oConn->prepare("INSERT INTO `" . ISSUES_TABLE . "` (`user_id`, `subject`, `status`, `created`)
                                   VALUES (?, ?, 'OPEN', GETDATE())");
        $res = $this->_oConn->execute($sth, array($this->userId, $data['subject']));

        $this->checkDBError($res);

        $lastInsertId = &$this->_oConn->getOne('select last_insert_id()');
        if ($lastInsertId != null) {
            return ($this->addCommentToDB($data, $files, $lastInsertId));
        } else {
            die("Error adding data to DB. Please try again or contact support by mail or phone.");
        }
    }

    // Assumes the issue ownership has already been checked
    private function changeStatus($status, $issueId) {
        $sth = $this->_oConn->prepare("UPDATE `" . ISSUES_TABLE . "` SET `status` = ? WHERE `issue_id` = ?");
        $res = $this->_oConn->execute($sth, array($status, $issueId));

        $this->checkDBError($res);

        return true;
    }

    function addComment($data, $files, $issueId) {
        if ($this->memberId != MASTER_MEMBER_ID) {
            // check
            $sth = "SELECT `edia_members`.`member_id`\n" .
                    "FROM `edia_members` JOIN `edia_member_users` JOIN `" . ISSUES_TABLE . "`\n" .
                    "WHERE `edia_members`.`member_id` = `edia_member_users`.`user_member_id` \n" .
                    "AND `" . ISSUES_TABLE . "`.`user_id` = `edia_member_users`.`user_id` \n" .
                    "AND `" . ISSUES_TABLE . "`.`issue_id` = ? \n" .
                    "AND `edia_members`.`member_id` = ?";
            $res = $this->_oConn->getOne($sth, array($issueId, $this->memberId), DB_FETCHMODE_OBJECT);

            // Check for DB class exceptions
            if (PEAR::isError($res)) {
                die($res->getMessage() . ". Please try again or contact support by mail or phone.");
            }

            if (empty($res)) {
                die("Not allowed. Please try again or contact support by mail/phone.");
            }
        }

        if ($this->addCommentToDB($data, $files, $issueId)) {
            if (isset($data['status'])) {
                if (in_array($data['status'], $this->statuses)) {
                    return $this->changeStatus($data['status'], $issueId);
                }
            }
        }
        return;
    }

    private function addCommentToDB($data, $files, $issueId) {
        if (!isset($data['internal'])) {
            $data['internal'] = 0;
        }
        if (empty($data['internal'])) {
            $data['internal'] = 0;
        }

        if (!isset($data['status'])) {
            $data['status'] = NULL;
        }

        if (!in_array($data['status'], $this->statuses)) {
            $data['status'] = NULL;
        }

        $filename = null;
        if (!empty($files["attachment"]["name"])) {
            $filename = $files["attachment"]["name"];
        }

        $sth = $this->_oConn->prepare("INSERT INTO `" . ISSUES_COMMENTS_TABLE .
                        "` (`issue_id`, `user_id`, `comment`, `attachment`, `status_change`, `internal`, `date`)
                                   VALUES (?, ?, ?, ?,  ?, ?, GETDATE())");
        $res = $this->_oConn->execute($sth, array($issueId, $this->userId, $data['comment'], $filename, $data['status'], $data['internal']));

        $this->checkDBError($res);

        if ($filename != null) {
            $lastInsertId = &$this->_oConn->getOne('select last_insert_id()');

            $this->checkDBError($res);

            if (($files["attachment"]["size"] > 4096000)) {
                die("Max file size is 4MB. Please try again.");
            }
            if ($files["attachment"]["error"] > 0) {
                die("Error uploading file code: " . $files["file"]["error"] . "<br />");
            }

            if (file_exists(ISSUES_COMMENTS_ATTACHMENTS_DIR . $lastInsertId)) {
                die($filename . " already exists. ");
            } else {
                move_uploaded_file($files["attachment"]["tmp_name"],
                        ISSUES_COMMENTS_ATTACHMENTS_DIR . $lastInsertId);
            }
        }
        return true;
    }

    function getIssue($issueId) {
        $sth = "SELECT `edia_members`.`member_login`, `edia_member_users`.`user_login`,\n"
                . "`" . ISSUES_TABLE . "` . *,\n"
                . "`" . ISSUES_COMMENTS_TABLE . "` . *\n"
                . "FROM `edia_members` JOIN `edia_member_users` JOIN `" . ISSUES_TABLE . "` JOIN `" . ISSUES_COMMENTS_TABLE . "`\n"
                . "WHERE `" . ISSUES_TABLE . "`.`issue_id` = `" . ISSUES_COMMENTS_TABLE . "`.`issue_id`\n"
                . "AND `" . ISSUES_COMMENTS_TABLE . "`.`user_id` = `edia_member_users`.`user_id`\n"
                . "AND `edia_members`.`member_id` = `edia_member_users`.`user_member_id`\n"
                . "AND `" . ISSUES_TABLE . "`.`issue_id` = ?\n";
        ;
        if ($this->memberId != MASTER_MEMBER_ID) {
            //$sth .= ' WHERE `user_id` = ?';
            $sth .= " AND `edia_member_users`.`user_member_id` in (?, " . MASTER_MEMBER_ID . ")\n";
            $sth .= "AND `" . ISSUES_COMMENTS_TABLE . "`.`internal` = 0";
            $res = $this->_oConn->getAll($sth, array($issueId, $this->memberId), DB_FETCHMODE_OBJECT);
        } else {
            $res = $this->_oConn->getAll($sth, array($issueId), DB_FETCHMODE_OBJECT);
        }

        $this->checkDBError($res);

        if (empty($res)) {
            die("Not allowed. Please try again or contact support by mail/phone.");
        }

        return $res;
    }

    function getIssueAttachment($issueId, $commentId) {

        $sth = "SELECT `" . ISSUES_COMMENTS_TABLE . "`.`attachment`\n"
                . "FROM `edia_members` JOIN `edia_member_users` JOIN `" . ISSUES_TABLE . "` JOIN `" . ISSUES_COMMENTS_TABLE . "` "
                . "WHERE `" . ISSUES_TABLE . "`.`issue_id` = `" . ISSUES_COMMENTS_TABLE . "`.`issue_id` "
                . "AND `" . ISSUES_TABLE . "`.`user_id` = `edia_member_users`.`user_id` "
                . "AND `edia_members`.`member_id` = `edia_member_users`.`user_member_id` "
                . "AND `" . ISSUES_TABLE . "`.`issue_id` = ?\n"
                . "AND `" . ISSUES_COMMENTS_TABLE . "`.`comment_id` = ?\n";
        if ($this->memberId != MASTER_MEMBER_ID) {
            $sth .= "AND `edia_member_users`.`user_member_id` = ?\n";
            $sth .= "AND `" . ISSUES_COMMENTS_TABLE . "`.`internal` = 0\n";

            $res = $this->_oConn->getOne($sth, array($issueId, $commentId, $this->memberId));
        }

        $res = $this->_oConn->getOne($sth, array($issueId, $commentId));

        $this->checkDBError($res);

        if (empty($res)) {
            // var_dump($sth);
            die("Not allowed. Please try again or contact support by mail/phone.");
        }

        return $res;
    }

    private function getUserInfo() {
        // 1|admin|48ad6dc8|stephen@ediassociates.com|1
        $arr = explode('|', $_SESSION["sUSER"]);
        if (count($arr) == 5) {
            if (is_numeric($arr[0]) && is_numeric($arr[4])) {
                $this->userId = $arr[0];
                $this->memberId = $arr[4];
                return true;
            }
        }
        die("Invalid user. Please try again or contact support by mail/phone.");
    }

    // Check for DB class exceptions
    private function checkDBError($dbResource, $message = "Please try again or contact support by mail or phone.") {

        if (PEAR::isError($dbResource)) {
            if (DUMP_DB_ERRORS) {
                echo "<pre>";
                var_dump($dbResource);
                echo "</ pre>";
            }
            die($res->getMessage() . ". " . $message);
        }
    }

    function close() {
        $this->_oConn->disconnect();
    }

    function isMasterMember(){
        return $this->memberId == MASTER_MEMBER_ID;
    }

}

?>