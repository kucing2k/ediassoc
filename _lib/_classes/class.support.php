<?php

// File Location: /_lib/_classes/class.support.php

// require PEAR objects
require_once("DB.php");

// require USER objects
require_once("config.php");
require_once("funcs.php");

/** 
 * handles Support functions
 *
 * @author Stephen Case <Stephen@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright EDI Associates, Inc.
 *
 */
class support { // open the class definition
    
    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setSupportId()
     */
    var $_iSupportId;
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer Support id [optional]
     * @access public
     */
    function Support() {
        
        // Instanciate the database connection
        $this->_oConn = get_db();
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    function setSupportId($iSupportId) {
        
        if (is_int($iSupportId)) {
            
            $this->_iSupportId = $iSupportId;
        }
    }
    
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * get Support count for paging
     *
     * @return integer record count
     * @access public
     */
     function getSupportCount($wMem) {
        
         $sql = "SELECT COUNT(id) AS support_cnt 
             FROM 
			           ".PREFIX."_support
             WHERE 
			           from_member_id=".$wMem."
			       and 
			           support_status=0";
        
        return $this->_oConn->getOne($sql);
     }

    function getSupportList($wMem, $iPage=0) {
        
        // get a list of all users
        
        if ($wMem = 1) {
            $needed = " and a.to_member_id = " . $wMem;
        } else {
            $needed = " and a.from_member_id = " . $wMem;
        }

        $sql = "select 
            a.id,
            a.support_title, 
            c.lookup_note, 
            d.user_login,
            b.log_date 
        from 
            edia_support a, 
            edia_support_log b, 
            edia_misc_lookups c, 
            edia_member_users d 
        where 
            b.support_id = a.id 
        and 
            c.lookup_id = a.type_id 
        and 
            b.user_id = d.user_id 
        and 
            b.log_note_id = 100 
        ".$needed."
			  AND 
			      support_status = 0";
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
            $return[$i]["sid"] = $aRow["id"];
            $return[$i]["support_title"] = $aRow["support_title"];
            $return[$i]["lookup_note"] = $aRow["lookup_note"];
            $return[$i]["user_login"] = $aRow["user_login"];
            $return[$i]["log_date"] = strtotime($aRow["log_date"]);
            ++$i;
        }
        return $return;
    }    

     function getProfiles($wSupport, $wComId) {

        // get a list of all users

        $sql = "select 
                    tpp_id, 
                    tpp_Description, 
                    tpp_status,
                    tpp_Direction, 
                    tpp_created_dt 
                from 
                    ".PREFIX."_partner_profiles 
                where 
                    tpp_member_id = ".$wComId." 
                    and tpp_partner_id = ".$wPart." 
--                    and tpp_status = 1
                ORDER BY
                    tpp_Direction";        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
		$return[$i]["Id"] = $aRow["tpp_id"];
		$return[$i]["Name"] = $aRow["tpp_Description"];
		$return[$i]["Status"] = $aRow["tpp_status"];
		$return[$i]["Type"] = $aRow["tpp_Direction"];
		$return[$i]["Created"] = strtotime($aRow["tpp_created_dt"]);
		++$i;
        }
        return $return;
    }    

    function getCatalogs($wMem) {
        
        // get a list of all users

        $sql = "SELECT 
			cat_Key_id,
			cat_num_id,
			cat_vernum_id,
			cat_desc
                FROM 
			".PREFIX."_catalogs
                WHERE 
			cat_member_id=".$wMem."
			and cat_delete=0
			and cat_status=1
		ORDER BY
			cat_num_id, cat_vernum_id";
         //       LIMIT ".$iPage.", ".ROWCOUNT;
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
		$return[$i]["val"] = $aRow["cat_Key_id"];
		$return[$i]["des"] = $aRow["cat_num_id"] ."-". $aRow["cat_vernum_id"] ."-". $aRow["cat_desc"];
		++$i;
        }
        return $return;
    }    


 
    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * add a new partner record
     *
     * @param array $aArgs user data
     * @return boolean
     * @access public
     */
    function addPartner($iComId, $aArgs) {
        
        // check for existing user name
        /*
        if ($this->_partnerExists($aArgs["Login"], $aArgs["Compa"])) {
            
            catchErr("This partner name already exists");
            return false;
            
        } else {
        */
            // add new user record
            $sql = "INSERT INTO ".PREFIX."_partners (
			tp_member_id,
			tp_name,
			tp_app_code,
			tp_edi_code,
			tp_contact,
			tp_phone,
			tp_email,
			tp_created_dt,
			tp_modified_dt
                    ) values (
			'".$iComId."',
			'".$aArgs["CName"]."',
			'".$aArgs["AppCo"]."',
			'".$aArgs["EdiCo"]."',
			'".$aArgs["Conta"]."',
                        '".$aArgs["Phone"]."', 
                        '".$aArgs["Email"]."',
			GETDATE(),
			GETDATE()
                    )";
            $this->_oConn->doQuery($sql);
            
            // get unique identifier for new record
            
        $sql = "SELECT 
                    tp_id 
                FROM 
                    ".PREFIX."_partners 
                WHERE 
                    tp_member_id='".$iComId."'
                    AND tp_name='".$aArgs["CName"]."'
                    AND tp_edi_code='".$aArgs["EdiCo"]."'
                    AND tp_deleted=0";

            $iPartId = $this->_oConn->getOne($sql);
            // set member variable for unique identifier
            settype($iPartId, "integer");

            
            $this->_setPerms($aArgs["Perms"], $iPartId, $iComId);
            return true;
            
    }

    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * edit an existing partner record
     *
     * @param array partner data
     * @return boolean
     * @access public
     */
    function editPartner($aArgs, $id, $iComId) {
        
        // update user record
        $sql = "UPDATE ".PREFIX."_partners SET 
			tp_name='".$aArgs["CName"]."',
			tp_app_code='".$aArgs["AppCo"]."',
			tp_edi_code='".$aArgs["EdiCo"]."',
			tp_contact='".$aArgs["Conta"]."',
			tp_phone='".$aArgs["Phone"]."',
			tp_email='".$aArgs["Email"]."',
			tp_modified_dt=GETDATE()
                WHERE 
			tp_id=".$aArgs["id"];
        $this->_oConn->doQuery($sql);
        // update catalog permission records
        $this->_setPerms($aArgs["Perms"], $id, $iComId);
        return true;
    }
    
    /** 
     * delete a trading partner record
     *
     * @return boolean
     * @access public
     */
    function deletePartner() {
        
        $sql = "UPDATE ".PREFIX."_partners SET 
                    tp_deleted=1, 
                    tp_deleted_dt=GETDATE() 
                WHERE 
                    tp_id=".$this->_iPartId;
        
        $this->_oConn->doQuery($sql);
        $this->deactivatePartner();
        return true;
    }
    
    /** 
     * activate a trading partner record
     *
     * @return boolean
     * @access public
     */
    function activatePartner() {
        
        $sql = "UPDATE ".PREFIX."_partners SET 
                    tp_status=1,
                    tp_modified_dt=GETDATE() 
                WHERE 
                    tp_id=".$this->_iPartId;
        $this->_oConn->doQuery($sql);
    }
    
    /** 
     * deactivate a user record
     *
     * @return boolean
     * @access public
     */
    function deactivatePartner() {
        
        $sql = "UPDATE ".PREFIX."_partners SET 
                    tp_status=0,
                    tp_modified_dt=GETDATE() 
                WHERE 
                    tp_id=".$this->_iPartId;
        $this->_oConn->doQuery($sql);
    }
}

?>
