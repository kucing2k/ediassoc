<?php

// File Location: /_lib/_classes/class.transRptFeeds.php

// require USER objects
require_once("config.php");
require_once("funcs.php");
require_once("DB.php");

/** 
 * handles transactions functions
 *
 * @author Stephen Case <src@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright EDI Associates, Inc.
 *
 */
class trans { // open the class definition

    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer $id [optional] advertisement identifier
     * @access public
     */
    function trans() {
        
        // implement pear db object
        $this->_oConn = get_db();
        
         
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // INSERT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * add new advertisement record
     *
     * @param array $aArgs advertisement data
     * @return boolean
     * @access public
     */
    function addTransactions($aArgs) {
    
        $van_Sender = explode("/", $aArgs[2]);
        $van_Receiver = explode("/", $aArgs[4]);
       
        $van_Sender_Qual = $van_Sender[1];
        $van_Sender_ID = $van_Sender[0];
        $van_Receiver_Qual = $van_Receiver[1]; 
        $van_Receiver_ID = $van_Receiver[0]; 
        
        // add new user record
        $sql = "INSERT INTO edia_VanReportData ( 
                    van_Source_ID, 
                    van_Document_ID, 
                    van_IntControl_ID, 
                    van_Sender_Qual, 
                    van_Sender_ID, 
                    van_Sender_Name, 
                    van_Receiver_Qual, 
                    van_Receiver_ID, 
                    van_Receiver_Name, 
                    van_Document_Type, 
                    van_Direction, 
                    van_Status, 
                    van_DateCreated, 
                    van_DateDelivered, 
                    van_DateReceived, 
                    van_FileSize, 
                    van_Mailbag_ID, 
                    van_Network_ID
                ) VALUES ( 
                    2, 
                    ".$aArgs[0].",
                    '".$aArgs[1]."',
                    '".$van_Sender_Qual."',
                    '".$van_Sender_ID."',
                    '".$aArgs[3]."',
                    '".$van_Receiver_Qual."',
                    '".$van_Receiver_ID."',
                    '".$aArgs[5]."',
                    '".$aArgs[6]."',
                    '".$aArgs[7]."',
                    '".$aArgs[8]."',
                    '".left($aArgs[9],19)."',
                    '".left($aArgs[10],19)."',
                    '".left($aArgs[11],19)."',
                    ".$aArgs[12].",
                    ".$aArgs[15].",
                    '".$aArgs[16]."'
                )";
        $this->_oConn->doQuery($sql);

    }
    // UPDATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
} // close the class definition

?>