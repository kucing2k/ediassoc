<?php

// File Location: /_lib/_classes/class.partners.php

// require PEAR objects
require_once("DB.php");

// require USER objects
require_once("config.php");
require_once("funcs.php");

/** 
 * handles user functions
 *
 * @author Stephen Case <Stephen@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright EDI Associates, Inc.
 *
 */
class part { // open the class definition
    
    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setPartId()
     */
    var $_iPartId;
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer user id [optional]
     * @access public
     */
    function part() {
        
        // Instanciate the database connection
        $this->_oConn = get_db();
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  
     /** 
     * set user permissions
     *
     * @param array permissions
     * @return boolean
     * @access private
     */ 
    
    function _setPerms($aPerms, $wPart, $wComId) {
        
        // check perms array
        if (count($aPerms)) {
            
            $sql = "DELETE FROM 
                        ".PREFIX."_catalog_perms
                    WHERE 
                        cat_member_id = ".$wComId."
                        and cat_partner_id = ".$wPart;
            
            $this->_oConn->doQuery($sql);
            
            // loop through permissions array
            while (list($key, $val) = each($aPerms)) {
                
                // add new permission record
                $sql = "INSERT INTO ".PREFIX."_catalog_perms (
                            cat_Key_id, 
                            cat_member_id,
                            cat_partner_id,
                            cat_perm
                        ) values (
                            ?,
                            ?,
                            ?,
                            ?
                        )";
                
                $this->_oConn->doQuery($sql, array($key, $wComId, $wPart, $val));
            }
            
            return true;
        }
    }
  
     
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    function setPartId($iPartId) {
        
        if (is_int($iPartId)) {
            
            $this->_iPartId = $iPartId;
        }
    }
    
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * get users count for paging
     *
     * @return integer record count
     * @access public
     */
     function getPartnerCount($wMem) {
        
        $sql = "SELECT COUNT(tp_id) AS tp_cnt 
                FROM 
			".PREFIX."_partners
                WHERE 
			tp_member_id=?
			and tp_deleted=0";
        
        return $this->_oConn->getOne($sql, array($wMem));
     }

    function getPartner($wPart, $wComId) {
        
        // get a list of all users

        $sql = "SELECT 
			tp_id,
			tp_name,
			tp_app_code,
			tp_edi_code,
			tp_contact,
			tp_email,
			tp_phone,
			tp_catalog,
			tp_status,
			tp_modified_dt,
			tp_created_dt,
			tp_image
                FROM 
			".PREFIX."_partners
                WHERE 
			tp_member_id=?
			and tp_id=?
			and tp_deleted=0";
        
        // loop through result and build return array
        $aRow = $this->_oConn->getRow($sql, array($wComId, $wPart));
            
	$return["tp Id"] = $aRow["tp_id"];
	$return["Name"] = $aRow["tp_name"];
	$return["APPCode"] = $aRow["tp_app_code"];
	$return["EDICode"] = $aRow["tp_edi_code"];
	$return["Contact"] = $aRow["tp_contact"];
	$return["Phone"] = $aRow["tp_phone"];
	$return["Email"] = $aRow["tp_email"];
	$return["Status"] = $aRow["tp_status"];
	$return["catalog"] = $aRow["tp_catalog"];
	$return["Image"] = $aRow["tp_image"];
	$return["Created Date"] = strtotime($aRow["tp_created_dt"]);
	$return["Modified Date"] = strtotime($aRow["tp_modified_dt"]);

        // get permissions for user
        $sql = "SELECT 
                    cat_Key_id, 
                    cat_perm 
                FROM 
                    ".PREFIX."_catalog_perms
                WHERE 
		    cat_member_id=?
		    and cat_partner_id=?
                ORDER BY 
                    cat_Key_id";
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($wComId, $wPart));
        
        // build permissions array to return
        while ($aRow = $stmt->fetch()) {
            $return["Perms"][$aRow["cat_Key_id"]] = $aRow["cat_perm"];
        }
        
        return $return;
    }    

    function getPartners($wMem, $iPage=0) {
        
        // get a list of all users

        $sql = "SELECT 
			tp_id,
			tp_name,
			tp_status,
			tp_created_dt
                FROM 
			".PREFIX."_partners
                WHERE 
			tp_member_id=".$wMem."
			and tp_deleted=0
		ORDER BY
			tp_name";
         //       LIMIT ".$iPage.", ".ROWCOUNT;
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
            $return[$i]["tp Id"] = $aRow["tp_id"];
            $return[$i]["tp Name"] = $aRow["tp_name"];
            $return[$i]["Status"] = $aRow["tp_status"];
            $return[$i]["Created Date"] = strtotime($aRow["tp_created_dt"]);
            ++$i;
        }
        return $return;
    }    
     
     function getProfiles($wPart, $wComId) {

        // get a list of all users

        $sql = "select 
                    tpp_id, 
                    tpp_Description, 
                    tpp_status,
                    tpp_Direction, 
                    tpp_created_dt 
                from 
                    ".PREFIX."_partner_profiles 
                where 
                    tpp_member_id = ?
                    and tpp_partner_id = ?
--                    and tpp_status = 1
                ORDER BY
                    tpp_Direction";        
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($wComId, $wPart));
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
            $return[$i]["Id"] = $aRow["tpp_id"];
            $return[$i]["Name"] = $aRow["tpp_Description"];
            $return[$i]["Status"] = $aRow["tpp_status"];
            $return[$i]["Type"] = $aRow["tpp_Direction"];
            $return[$i]["Created"] = strtotime($aRow["tpp_created_dt"]);
            ++$i;
        }
        return $return;
    }    

    function getCatalogs($wMem) {
        
        // get a list of all users

        $sql = "SELECT 
			cat_Key_id,
			cat_num_id,
			cat_vernum_id,
			cat_desc
                FROM 
			".PREFIX."_catalogs
                WHERE 
			cat_member_id=?
			and cat_delete=0
			and cat_status=1
		ORDER BY
			cat_num_id, cat_vernum_id";
         //       LIMIT ".$iPage.", ".ROWCOUNT;
        
        try {
            $stmt = $this->_oConn->prepare($sql);
            $stmt->execute(array($wMem));

            // loop through result and build return array
            $i = 0;
            while ($aRow = $stmt->fetch()) {
                $return[$i]["val"] = $aRow["cat_Key_id"];
                $return[$i]["des"] = $aRow["cat_num_id"] ."-". $aRow["cat_vernum_id"] ."-". $aRow["cat_desc"];
                ++$i;
            }
        } catch (PDOException $e) {
            return false;
        }
            
        return $return;
    }    


 
    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * add a new partner record
     *
     * @param array $aArgs user data
     * @return boolean
     * @access public
     */
    function addPartner($iComId, $aArgs) {
        
        // check for existing user name
        /*
        if ($this->_partnerExists($aArgs["Login"], $aArgs["Compa"])) {
            
            catchErr("This partner name already exists");
            return false;
            
        } else {
        */
            // add new user record
            $sql = "INSERT INTO ".PREFIX."_partners (
			tp_member_id,
			tp_name,
			tp_app_code,
			tp_edi_code,
			tp_contact,
			tp_phone,
			tp_email,
			tp_created_dt,
			tp_modified_dt
                    ) values (
			:iComId,
			:CName,
			:AppCo,
			:EdiCo,
			:Conta,
            :Phone,
            :Email,
			GETDATE(),
			GETDATE()
                    )";
            
            $stmt = $this->_oConn->prepare($sql);
            $stmt->bindValue(':iComId', $iComId);
            $stmt->bindValue(':CName', $aArgs["CName"]);
            $stmt->bindValue(':AppCo', $aArgs["AppCo"]);
            $stmt->bindValue(':EdiCo', $aArgs["EdiCo"]);
            $stmt->bindValue(':Conta', $aArgs["Conta"]);
            $stmt->bindValue(':Phone', $aArgs["Phone"]);
            $stmt->bindValue(':Email', $aArgs["Email"]);
            $stmt->execute();
            
            // get unique identifier for new record
            
        $sql = "SELECT 
                    tp_id 
                FROM 
                    ".PREFIX."_partners 
                WHERE 
                    tp_member_id=?
                    AND tp_name=?
                    AND tp_edi_code=?
                    AND tp_deleted=0";

            $iPartId = $this->_oConn->getOne($sql, array($iComId, $aArgs["CName"], $aArgs["EdiCo"]));
            
            // set member variable for unique identifier
            settype($iPartId, "integer");

            
            $this->_setPerms($aArgs["Perms"], $iPartId, $iComId);
            return true;
            
    }

    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * edit an existing partner record
     *
     * @param array partner data
     * @return boolean
     * @access public
     */
    function editPartner($aArgs, $id, $iComId) {
        
        // update user record
        $sql = "UPDATE ".PREFIX."_partners SET 
			tp_name=?,
			tp_app_code=?,
			tp_edi_code=?,
			tp_contact=?,
			tp_phone=?,
			tp_email=?,
			tp_modified_dt=GETDATE()
                WHERE 
			tp_id=?";
        
        $this->_oConn->doQuery($sql, array($aArgs["CName"], $aArgs["AppCo"], $aArgs["EdiCo"], $aArgs["Conta"], $aArgs["Phone"], $aArgs["Email"], $aArgs["id"]));
        // update catalog permission records
        $this->_setPerms($aArgs["Perms"], $id, $iComId);
        return true;
    }
    
    /** 
     * delete a trading partner record
     *
     * @return boolean
     * @access public
     */
    function deletePartner() {
        
        $sql = "UPDATE ".PREFIX."_partners SET 
                    tp_deleted=1, 
                    tp_deleted_dt=GETDATE() 
                WHERE 
                    tp_id=?";
        
        $this->_oConn->doQuery($sql, array($this->_iPartId));
        
        $this->deactivatePartner();
        return true;
    }
    
    /** 
     * activate a trading partner record
     *
     * @return boolean
     * @access public
     */
    function activatePartner() {
        
        $sql = "UPDATE ".PREFIX."_partners SET 
                    tp_status=1,
                    tp_modified_dt=GETDATE() 
                WHERE 
                    tp_id=?";
         
        $this->_oConn->doQuery($sql, array($this->_iPartId));
    }
    
    /** 
     * deactivate a user record
     *
     * @return boolean
     * @access public
     */
    function deactivatePartner() {
        
        $sql = "UPDATE ".PREFIX."_partners SET 
                    tp_status=0,
                    tp_modified_dt=GETDATE() 
                WHERE 
                    tp_id=?";
        
        $this->_oConn->doQuery($sql, array($this->_iPartId));
    }
}

?>
