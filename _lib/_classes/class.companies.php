<?php

// File Location: /_lib/_classes/class.companies.php

// require PEAR objects
require_once("DB.php");

// require USER objects
require_once("config.php");
require_once("funcs.php");

/** 
 * handles user functions
 *
 * @author Stephen Case <Stephen@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright EDI Associates, Inc.
 *
 */
class company { // open the class definition
    
    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setPartId()
     */
    var $_iCompanyId;
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer user id [optional]
     * @access public
     */
    function company() {
        
        // Instanciate the database connection
        $this->_oConn = get_db();
        
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
     
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    function setCompId($iCompId) {
        
        if (is_int($iCompId)) {
            
            $this->_iCompId = $iCompId;
        }
    }
    
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * get users count for paging
     *
     * @return integer record count
     * @access public
     */
     function getPartnerCount($wMem) {
        
        $sql = "SELECT COUNT(tp_id) AS tp_cnt 
                FROM 
            ".PREFIX."_partners
                WHERE 
            tp_member_id=".$wMem."
            and tp_deleted=0";
        
        return $this->_oConn->getOne($sql);
     }

     function getSetupUser($wUser) {
        
        $sql = "SELECT user_login 
                FROM 
            ".PREFIX."_member_users
                WHERE 
            user_id=".$wUser;
        
        return $this->_oConn->getOne($sql);
     }


    function getCompany($wPart, $wComId) {
        
        // get a list of all users

        $sql = "SELECT 
            *
                FROM 
            ".PREFIX."_crm_company
                WHERE 
            member_id=".$wComId."
            and DataKey=".$wPart;
        
        // loop through result and build return array
       
        $aRow = $this->_oConn->getRow($sql);

        $return["DataKey"] = $aRow["DataKey"];
        $return["member_id"] = $aRow["member_id"];
        $return["com_source_id"] = $aRow["com_source_id"];
        $return["com_industry_id"] = $aRow["com_industry_id"];
        $return["com_name"] = $aRow["com_name"];
        $return["com_legal_name"] = $aRow["com_legal_name"];
        $return["com_code"] = $aRow["com_code"];
        $return["com_tax_id"] = $aRow["com_tax_id"];
        $return["com_phone"] = $aRow["com_phone"];
        $return["com_phone2"] = $aRow["com_phone2"];
        $return["com_fax"] = $aRow["com_fax"];
        $return["com_url"] = $aRow["com_url"];
        $return["com_employees"] = $aRow["com_employees"];
        $return["com_revenue"] = $aRow["com_revenue"];
        $return["com_created_by"] = $this->getSetupUser($aRow["com_created_by"]);
        $return["com_created_dt"] = $aRow["com_created_dt"];
        $return["com_modified_by"] = $this->getSetupUser($aRow["com_modified_by"]);
        $return["com_modified_dt"] = $aRow["com_modified_dt"];
        $return["com_primary_address"] = $aRow["com_primary_address"];
        $return["com_billing_address"] = $aRow["com_billing_address"];
        $return["com_shipping_address"] = $aRow["com_shipping_address"];
        $return["com_payment_address"] = $aRow["com_payment_address"];
        $return["com_custom1"] = $aRow["com_custom1"];
        $return["com_custom2"] = $aRow["com_custom2"];
        $return["com_custom3"] = $aRow["com_custom3"];
        $return["com_custom4"] = $aRow["com_custom4"];
        if ($aRow["com_status"] < 1) { $return["com_status"] = "Deactivated"; } else { $return["com_status"] = "Active"; }
        
        return $return;
    }    

    function getCompanyContactList($wMem) {
        
        // get a list of all users

        $sql = "SELECT 
                    DataKey, 
                    con_last_name, 
                    con_first_name,
                    con_description, 
                    con_summary, 
                    con_title, 
                    con_work_phone, 
                    con_email
                FROM 
                    ".PREFIX."_crm_company_contacts
                WHERE 
                    company_id=".$wMem;
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
            $return[$i]["DataKey"] = $aRow["DataKey"];
            $return[$i]["con_last_name"] = $aRow["con_last_name"];
            $return[$i]["con_first_name"] = $aRow["con_first_name"];
            $return[$i]["con_summary"] = $aRow["con_summary"];
            $return[$i]["con_description"] = $aRow["con_description"];
            $return[$i]["con_title"] = $aRow["con_title"];
            $return[$i]["con_work_phone"] = $aRow["con_work_phone"];
            $return[$i]["con_email"] = $aRow["con_email"];
            
            ++$i;
  
        }
        return $return;
    }    


    function getCompanies($wMem, $iPage=0) {
        
        // get a list of all users

        $sql = "SELECT 
                    DataKey,
                    com_industry_id,
                    com_name,
                    com_code,
                    com_status,
                    com_created_by,
                    com_created_dt
                FROM 
                    ".PREFIX."_crm_company
                WHERE 
                    member_id=".$wMem."
        ORDER BY
                    com_name";
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
        $return[$i]["Id"] = $aRow["DataKey"];
        $return[$i]["Industry"] = $aRow["com_industry_id"];
        $return[$i]["Name"] = $aRow["com_name"];
        $return[$i]["Code"] = $aRow["com_code"];
        $return[$i]["Status"] = $aRow["com_status"];
        $return[$i]["CreatedBy"] = $aRow["com_created_by"];
        $return[$i]["Created"] = strtotime($aRow["com_created_dt"]);
        ++$i;
        }
        return $return;
    }    


 
    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * add a new partner record
     *
     * @param array $aArgs user data
     * @return boolean
     * @access public
     */
    function addPartner($iComId, $aArgs) {
        
        // check for existing user name
        /*
        if ($this->_partnerExists($aArgs["Login"], $aArgs["Compa"])) {
            
            catchErr("This partner name already exists");
            return false;
            
        } else {
        */
            // add new user record
            $sql = "INSERT INTO ".PREFIX."_partners (
            tp_member_id,
            tp_name,
            tp_app_code,
            tp_edi_code,
            tp_contact,
            tp_phone,
            tp_email,
            tp_created_dt,
            tp_modified_dt
                    ) values (
            '".$iComId."',
            '".$aArgs["CName"]."',
            '".$aArgs["AppCo"]."',
            '".$aArgs["EdiCo"]."',
            '".$aArgs["Conta"]."',
                        '".$aArgs["Phone"]."', 
                        '".$aArgs["Email"]."',
            GETDATE(),
            GETDATE()
                    )";
            
            $this->_oConn->doQuery($sql);

            // get unique identifier for new record
            
        $sql = "SELECT 
                    tp_id 
                FROM 
                    ".PREFIX."_partners 
                WHERE 
                    tp_member_id='".$iComId."'
                    AND tp_name='".$aArgs["CName"]."'
                    AND tp_edi_code='".$aArgs["EdiCo"]."'
                    AND tp_deleted=0";

            $iCompId = $this->_oConn->getOne($sql);
            
            // set member variable for unique identifier
            settype($iCompId, "integer");

            
            $this->_setPerms($aArgs["Perms"], $iCompId, $iComId);
            return true;
            
    }

    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * edit an existing partner record
     *
     * @param array partner data
     * @return boolean
     * @access public
     */
    function editPartner($aArgs, $id, $iComId) {
        
        // update user record
        $sql = "UPDATE ".PREFIX."_partners SET 
            tp_name='".$aArgs["CName"]."',
            tp_app_code='".$aArgs["AppCo"]."',
            tp_edi_code='".$aArgs["EdiCo"]."',
            tp_contact='".$aArgs["Conta"]."',
            tp_phone='".$aArgs["Phone"]."',
            tp_email='".$aArgs["Email"]."',
            tp_modified_dt=GETDATE()
                WHERE 
            tp_id=".$aArgs["id"];
        
        $this->_oConn->doQuery($sql);
        // update catalog permission records
        $this->_setPerms($aArgs["Perms"], $id, $iComId);
        return true;
    }
    
    
    /** 
     * activate a trading partner record
     *
     * @return boolean
     * @access public
     */
    function activateCompany() {
        
        $sql = "UPDATE ".PREFIX."_crm_company SET 
                    com_status=1,
                    com_modified_dt=GETDATE() 
                WHERE 
                    DataKey=".$this->_iCompId;
         
        $this->_oConn->doQuery($sql);
    }
    
    /** 
     * deactivate a user record
     *
     * @return boolean
     * @access public
     */
    function deactivateCompany() {
        
        $sql = "UPDATE ".PREFIX."_crm_company SET 
                    com_status=0,
                    com_modified_dt=GETDATE() 
                WHERE 
                    DataKey=".$this->_iCompId;
        
        $this->_oConn->doQuery($sql);
    }

    // DELETE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /** 
     * delete a trading partner record
     *
     * @return boolean
     * @access public
     */
    function deleteCompany() {
        
        $sql = "DELETE FROM ".PREFIX."_crm_company  
                WHERE 
                    DataKey=".$this->_iCompId;
        
        $this->_oConn->doQuery($sql);
        
        return true;
    }


}

?>
