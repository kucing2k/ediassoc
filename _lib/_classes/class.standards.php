<?php

// File Location: /_lib/_classes/class.users.php

require_once("DB.php");

/** 
 * handles user functions
 *
 * @author Mike Buzzard <src@cubancouncil.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright Apress
 *
 */
class standards { // open the class definition
    
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer user id [optional]
     * @access public
     */
    function standards() {
        
        // Instanciate the database connection
        $this->_oConn = get_db();
        
        // Check for DB class exceptions
        if (DB::isError($this->_oConn)) {
            
            // Report class exceptions if present
            catchExc($this->_oConn->getMessage());
        }
        
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * get Agency list
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function getStandardDocTitle($iComId, $iDocId) {
        
        // get a list of all users

        $sql = "select * from ".PREFIX."_std where member_id = ".$iComId." and DataKey = ".$iDocId;
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        $aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC);
            
            $return["Id"] = $aRow["DataKey"];
            $return["Name"] = $aRow["Name"];
            $return["Description"] = $aRow["Description"];
            $return["Created"] = strtotime($aRow["create_date"]);
            $return["Modified"] = strtotime($aRow["modified_date"]);
            
        return $return;
    }    

    /** 
     * get Agency list
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function getStandardDocsList($iComId) {
        
        // get a list of all users

        $sql = "SELECT * FROM ".PREFIX."_std";
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
            $return[$i]["Id"] = $aRow["DataKey"];
            $return[$i]["Name"] = $aRow["Name"];
            $return[$i]["Status"] = $aRow["status"];
            $return[$i]["Created"] = strtotime($aRow["create_date"]);
            
            ++$i;
        }
        return $return;
    }    

    /** 
     * get Segments list
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function getStandardDocsSegments($pk) {
        
        // get a list of all users

        $sql = "Select * from ".PREFIX."_std_segments WHERE ParentKey = ".$pk." ORDER BY Position";
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
            $return[$i]["dk"] = $aRow["DataKey"];
            $return[$i]["Section"] = $aRow["Section"];
            $return[$i]["Req"] = $aRow["RequirementDesignator"];
            $return[$i]["Max"] = $aRow["MaximumUsage"];
            $return[$i]["Id"] = $aRow["SegmentID"];
            $return[$i]["Name"] = $aRow["SegmentDesc"];
            ++$i;
        }
        return $return;
    }    


    /** 
     * get Agency list
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function getAgency() {
        
        // get a list of all users

        $sql = "select Agency, Description from Agency";
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
            $return[$i]["dk"] = $aRow["Agency"];
            $return[$i]["Id"] = $aRow["Agency"];
            $return[$i]["Name"] = $aRow["Description"];
            ++$i;
        }
        return $return;
    }    
    
    /** 
     * get Versions list
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function getVersions($pk) {
        
        // get a list of all users

        $sql = "Select Agency, Version, Description from Version where Agency = '".$pk."'";
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        // print $aid;
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
            $return[$i]["dk"] = $aRow["Version"];
            $return[$i]["Id"] = $aRow["Version"];
            $return[$i]["Name"] = $aRow["Description"];
            ++$i;
        }
        return $return;
    }    
   

    /** 
     * get Transactions list
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function getTransactions($pk='') {
        
        // get a list of all users

        $sql = "Select DataKey, TransactionSet, FunctionalGroupID, Description from Standards_db.dbo.TransactionSet where ParentKey =".$pk;
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
            $return[$i]["dk"] = $aRow["DataKey"];
            $return[$i]["Id"] = $aRow["TransactionSet"]." - ".$aRow["FunctionalGroupID"];
            $return[$i]["Name"] = $aRow["Description"];
            ++$i;
        }
        return $return;
    }    
    /** 
     * get Segments list
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function getSegments($pk='') {
        
        // get a list of all users

        $sql = "Select u.*, d.Description from Standards_db.dbo.SegmentUsage as u, Standards_db.dbo.SegmentDescription as d where u.Agency = d.Agency and u.Version = d.Version and u.SegmentID = d.SegmentID and u.ParentKey = ".$pk." ORDER BY u.Position";
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
            $return[$i]["dk"] = $aRow["DataKey"];
            $return[$i]["ag"] = $aRow["Agency"];
            $return[$i]["ver"] = $aRow["Version"];
            $return[$i]["Section"] = $aRow["Section"];
            $return[$i]["Req"] = $aRow["RequirementDesignator"];
            $return[$i]["Max"] = $aRow["MaximumUsage"];
            $return[$i]["Id"] = $aRow["SegmentID"];
            $return[$i]["Name"] = $aRow["Description"];
            $return[$i]["LoopID"] = $aRow["LoopID"];
            $return[$i]["BeginEnd"] = $aRow["BeginEnd"];
            $return[$i]["MaximumLoopRepeat"] = $aRow["MaximumLoopRepeat"];
            ++$i;
        }
        return $return;
    }    

    /** 
     * get Elements list
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function getElements($ag='', $ver='', $id='') {
        
        // get a list of all users

        $sql = "select Agency, Version, ElementID, RequirementDesignator, Type, Description, MinimumLength, MaximumLength from Standards_db.dbo.ElementUsageDefs where Agency = '" . $ag . "' and Version = '" . $ver . "' and SegmentID = '" . $id . "' ORDER BY Position";
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
            $return[$i]["ag"] = $aRow["Agency"];
            $return[$i]["ver"] = $aRow["Version"];
            $return[$i]["Section"] = $aRow["Type"];
            $return[$i]["Req"] = $aRow["RequirementDesignator"];
            $return[$i]["Id"] = $aRow["ElementID"];
            $return[$i]["Name"] = $aRow["Description"];
            $return[$i]["Max"] = $aRow["MaximumLength"];
            $return[$i]["Min"] = $aRow["MinimumLength"];
            $return[$i]["cnt"] = $this->getCodeCnt($ag, $ver, $aRow["ElementID"]);
            ++$i;
        }
        return $return;
    }    
    
    /** 
     * get Code list
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function getCodes($ag='', $ver='', $id='') {
        
        // get a list of all users

        $sql = "select Value, Description from Standards_db.dbo.Code where Agency = '" . $ag . "' and Version = '" . $ver . "' and ElementID = '" . $id . "' ORDER BY Value";
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
            $return[$i]["Id"] = $aRow["Value"];
            $return[$i]["Name"] = $aRow["Description"];
            ++$i;
        }
        return $return;
    }    

    /** 
     * get Code list
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function getCodeCnt($ag='', $ver='', $id='') {
        
        // get a list of all users

        $sql = "select 
            count(Value) 
        from 
            Standards_db.dbo.Code 
        where 
            Agency = '" . $ag . "' 
            and Version = '" . $ver . "' 
            and ElementID = '" . $id . "'";
        
        if (DB::isError($iCnt = $this->_oConn->getOne($sql))) {
            
            catchExc($iCnt->getMessage());
            return false;
        }
        
        return $iCnt;
    }    


    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /** 
     * Add Standards Document Key
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
     function addStandardsDoc($iComId, $iUserId, $sName, $sDesc, $sSegK) {
     

        $sql = "select distinct 
                    Agency, 
                    Version, 
                    TransactionSetID 
                from 
                    Standards_db.dbo.SegmentUsage 
                where 
                    ParentKey=".$sSegK;

        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // assign result to array
        $aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC);
        
        // build user array to return
        $ag = $aRow["Agency"];
        $ve = $aRow["Version"];
        $ts = $aRow["TransactionSetID"];



         // add new user record
         $sql = "INSERT INTO ".PREFIX."_std (
             member_id, 
             create_user_id, 
             modified_user_id, 
             Name, 
             Description, 
             Agency, 
             Version, 
             TransactionSetID
         ) values (
             ".$iComId.",
             ".$iUserId.",
             ".$iUserId.",
             '".$sName."', 
             '".$sDesc."', 
             '".$ag."',
             '".$ve."',
             '".$ts."'
         )";
         
     if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
         
         catchExc($rsTmp->getMessage());
         return false;
     }
     
     // get unique identifier for new record
         
        $sql = "SELECT IDENT_CURRENT('".PREFIX."_std')";

        if (DB::isError($iDocId = $this->_oConn->getOne($sql))) {
            
            catchExc($iUserId->getMessage());
            return false;
        }
            
        return $iDocId;
     
     }

    /** 
     * Add Standards Segment Information and get Key
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
     function addStandardsSegments($iStdDocKey, $iTopicId) {
     

        $sql = "select 
                    u.*, d.Description 
                from 
                    Standards_db.dbo.SegmentUsage as u, 
                    Standards_db.dbo.SegmentDescription as d 
                where u.Agency = d.Agency 
                and u.Version = d.Version 
                and u.SegmentID = d.SegmentID 
                and u.DataKey=".$iTopicId;

        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // assign result to array
        $aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC);
        
        // build user array to return
        $DataKey = $aRow["DataKey"];
        $Agency = $aRow["Agency"];
        $Version = $aRow["Version"];
        $Position = $aRow["Position"];
        $SegmentID = $aRow["SegmentID"];
        $Section = $aRow["Section"];
        $RequirementDesignator = $aRow["RequirementDesignator"];
        $LoopID = $aRow["LoopID"];
        $BeginEnd = $aRow["BeginEnd"];
        $RequiredFirstSegment = $aRow["RequiredFirstSegment"];
        $MaximumLoopRepeat = $aRow["MaximumLoopRepeat"];
        $MaximumUsage = $aRow["MaximumUsage"];
        $Description = $aRow["Description"];

         // add new user record
         $sql = "INSERT INTO ".PREFIX."_std_segments (
             ParentKey, 
             Position, 
             SegmentID, 
             SegmentDesc, 
             Section, 
             RequirementDesignator, 
             LoopID, 
             BeginEnd, 
             RequiredFirstSegment, 
             MaximumLoopRepeat, 
             MaximumUsage
         ) values (
             ".$iStdDocKey.",
             ".$Position.",
             '".$SegmentID."',
             '".$Description."',
             '".$Section."',
             '".$RequirementDesignator."',
             '".$LoopID."',
             '".$BeginEnd."',
             '".$RequiredFirstSegment."',
             ".$MaximumLoopRepeat.",
             ".$MaximumUsage."
         )";
         
     if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
         
         catchExc($rsTmp->getMessage());
         return false;
     }
     
     // get unique identifier for new record
         
        $sql = "SELECT IDENT_CURRENT('".PREFIX."_std_segments')";

        if (DB::isError($iDocId = $this->_oConn->getOne($sql))) {
            
            catchExc($iDocId->getMessage());
            return false;
        }
        $myStr = $iDocId;
        return $myStr;
//        $this->addStandardsElements($iDocId, $Agency, $Version, $SegmentID);
     
     }

    /** 
     * Add Standards Elements Information 
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
     function addStandardsElements($iDocID, $Agency, $Version, $SegmentID) {
     

        $sql = "select * from Standards_db.dbo.ElementUsageDefs where Agency = '".$Agency."' and Version = '".$Version."' and SegmentID = '".$SegmentID."'";

        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
            $this->StoreElements($iDocID, $aRow);
            
            // get unique identifier for new record
                
            $sql = "SELECT IDENT_CURRENT('".PREFIX."_std_elements')";
    
            if (DB::isError($iDocEId = $this->_oConn->getOne($sql))) {
                
                catchExc($iUserId->getMessage());
                return false;
            }
            
            $ElementID = $aRow["ElementID"];
            $this->GetAndStoreCodes($iDocEId, $Agency, $Version, $ElementID);
            
            ++$i;
        }
    }

    function GetAndStoreCodes($iDocEId, $Agency, $Version, $ElementID='') {

        $sql = "select Value, Description from Standards_db.dbo.code where Agency = '".$Agency."' and Version = '".$Version."' and ElementID = '".$ElementID."'";
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
            $sql = "INSERT INTO ".PREFIX."_std_code (
                ParentKey,
                Value,
                Description
            ) VALUES (
                ".$iDocEId.",
                '".$aRow["Value"]."',
                '".clean(str_replace("'", "''", $aRow["Description"]))."'
            )";

            if (DB::isError($rsTmp1 = $this->_oConn->doQuery($sql))) {
                
                catchExc($rsTmp1->getMessage());
                return false;
            }
            
            ++$i;
        }
    
    }


    Function StoreElements($iDocID, $aRow=''){
        $sql = "INSERT INTO ".PREFIX."_std_elements (
            ParentKey,
            ElementID,
            Position,
            RequirementDesignator,
            GroupRequirementDesignatorID,
            SubElementReqDesignator,
            GroupReqDesignator,
            GroupBeginEnd,
            Type,
            Description,
            MinimumLength,
            MaximumLength
        ) VALUES (
            ".$iDocID.",
            '".$aRow["ElementID"]."',
            ".$aRow["Position"].",
            '".$aRow["RequirementDesignator"]."',
            '".$aRow["GroupRequirementDesignatorID"]."',
            '".$aRow["SubElementReqDesignator"]."',
            '".$aRow["GroupReqDesignator"]."',
            '".$aRow["GroupBeginEnd"]."',
            '".$aRow["Type"]."',
            '".$aRow["Description"]."',
            ".$aRow["MinimumLength"].",
            ".$aRow["MaximumLength"]."
        )";

        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
    }

    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /** 
     * delete a user record
     *
     * @return boolean
     * @access public
     */
    function deleteStdDoc($id, $iComId) {
        
        $sql = "delete from ".PREFIX."_std 
                   WHERE DataKey=".$id." and member_id = ".$iComId;
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
    }
    
    /** 
     * activate a user record
     *
     * @return boolean
     * @access public
     */
    function activateStdDoc($id, $iComId) {
        
        $sql = "UPDATE ".PREFIX."_std SET 
                    status=1,
                    modified_date=GETDATE() 
                WHERE DataKey=".$id." and member_id = ".$iComId;
         
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
    }
    
    /** 
     * deactivate a user record
     *
     * @return boolean
     * @access public
     */
    function deactivateStdDoc($id, $iComId) {
        
        $sql = "UPDATE ".PREFIX."_std SET 
                    status=0,
                    modified_date=GETDATE() 
                WHERE DataKey=".$id." and member_id = ".$iComId;
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
    }
} // close the class definition

?>
