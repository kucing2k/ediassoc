<?php
// File Location: /_lib/_classes/class.users.php
require_once("Mail.php");
require_once("DB.php");

/** 
 * handles user functions
 *
 * @author Mike Buzzard <src@cubancouncil.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright Apress
 *
 */
class users { // open the class definition
    
    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setUserId()
     */
    var $_iUserId;
    
    /** 
     * PEAR mail object
     *
     * @var object
     * @access private
     */
    var $_oMail;
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer user id [optional]
     * @access public
     */
    function users($iUserId = '') {
        
        // implement pear mail object
        $params["host"] = "mail.ediassociates.com";
        $params["port"] = "25";
        $params["auth"] = true;
        $params["username"] = "stephen@ediassociates.com";
        $params["password"] = "mko09ijn";

        $this->_oMail =& Mail::factory("smtp", $params);
        
        // implement db object
        $this->_oConn = get_db();
        
        // set unique identifier
        if (is_int($iUserId)) {
            
            $this->setUserId($iUserId);
        }
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * reset user password
     *
     * @return boolean
     * @access private
     */
    function _resetPassword($tmpUserId) {
        
        // generate random unique password
        $sPasswordRemind = substr(md5(uniqid(rand(1, 100000))), 3, 8);
        
        // encrypt password
        $sPassword = substr(md5($sPasswordRemind), 3, 8);
        
        // update user account record
        $sql = "UPDATE ".PREFIX."_member_users SET 
                    user_pass=?, 
                    user_remind=?, 
                    user_modified_dt=GETDATE()
                WHERE 
                    user_id=?";
        

        $this->_oConn->doQuery($sql, array($sPassword, $sPasswordRemind, $tmpUserId));
        
        // send user email
        $sBody = "Your ".ENTITY." password has been reset.\r\n\r\n";
        $sBody .= "Your new password is ".$sPasswordRemind;
        
        return $this->_notifyUser($sBody, $tmpUserId);
    }
    
    /** 
     * verify unique user name
     *
     * @param string $sUser user name
     * @return boolean
     * @access private
     */
    function _userExists($sLogin, $sComp) {

        $sql = "SELECT 
                    count(1) 
                FROM 
                    ".PREFIX."_member_users 
                WHERE 
                    user_login=?
                    AND user_member_id=?
                    AND user_deleted=0";
       
        return $this->_oConn->getOne($sql, array($sLogin, $sComp));
    }

    function _getUserId($sLogin, $sComp) {

        $sql = "SELECT 
                    user_id 
                FROM 
                    ".PREFIX."_member_users 
                WHERE 
                    user_login=?
                    AND user_member_id=?
                    AND user_deleted=0";
       
        return $this->_oConn->getOne($sql, array($sLogin, $sComp));
    }
    
    /** 
     * verify unique email address
     *
     * @param string $sEmail email address
     * @return boolean
     * @access private
     */
    function _emailExists($sEmail) {
        
        $sql = "SELECT 
                    count(1) 
                FROM 
                    ".PREFIX."_admin_users 
                WHERE 
                    admin_user_email=?
                    AND deleted=0";
        
        return $this->_oConn->getOne($sql, array($sEmail));
    }
    
    /** 
     * send email notification
     *
     * @param string $sBody email body
     * @return boolean
     * @access private
     */
    function _notifyUser($sBody, $tmpUserId) {
        
        // assign mail properties
        $aUser = $this->getUser($tmpUserId);
        $aHeaders["To"] = $sRecipients = $aUser["Email"];
        $aHeaders["From"] = " Van-On-Demand Admin <car@ediassociates.com>";
        $aHeaders["Subject"] = ENTITY." System Account Notification";
        $aHeaders["Priority"] = "3";
        
        // try to send mail
        $mailTmp = $this->_oMail->send($sRecipients, $aHeaders, $sBody);

//        if (Mail::isError($mailTmp = $this->_oMail->send($sRecipients, $aHeaders, $sBody))) {
//            catchExc($mailTmp->getMessage());
//            return false;
//        }
        
        return true;
    }
    
    /** 
	 * set user permissions
	 *
     * @param array permissions
     * @return boolean
	 * @access private
	 */
    function _setPerms($aPerms, $iUserId) {
        
        // check perms array
        if (count($aPerms)) {
            
            $this->_oConn->beginTransaction();
            $sql = "DELETE FROM 
                        ".PREFIX."_user_perms 
                    WHERE 
                        user_user_id=?";
            
            $this->_oConn->doQuery($sql, array($iUserId));
            
            $sql = "INSERT INTO ".PREFIX."_user_perms (
                            user_user_id, 
                            user_app_id, 
                            user_perm
                        ) values (
                            :user_id, 
                            :app_id, 
                            :perm
                        )";
            
            $key = $val = null;
            $stmt = $this->_oConn->prepare($sql);
            $stmt->bindValue(':user_id', $iUserId);
            $stmt->bindParam(':app_id', $key);
            $stmt->bindParam(':perm', $val);
            // loop through permissions array
            while (list($key, $val) = each($aPerms)) {
                
                // add new permission record
                $stmt->execute();
            }
            $this->_oConn->commit();
            
            return true;
        }
    }
     
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /**
     * set the _iUserId variable for the class
     *
     * @param integer $iUserId unique identifier
     * @access public
     */
    function setUserId($iUserId) {
        
        if (is_int($iUserId)) {
            
            $this->_iUserId = $iUserId;
        }
    }

    /**
     * set the _iComId variable for the class
     *
     * @param integer $iComId unique identifier
     * @access public
     */
    function setComId($iComId) {
        
        if (is_int($iComId)) {
            
            $this->_iComId = $iComId;
        }
    }
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /**
     * send password reminder
     *
     * @param array $aArgs user values
     * @return boolean
     * @access public
     */
//     function sendReminder($aArgs) {
//         
//         $sql = "SELECT 
//                     usr.user_id, 
//                     usr.user_email 
//                 FROM 
//                     ".PREFIX."_member_users usr,
//                     ".PREFIX."_members mem
//                 WHERE 
//                     mem.member_login='".$aArgs["Company"]."'
//                     and usr.user_member_id=mem.member_id
//                     and usr.user_login='".$aArgs["Login"]."'
//                     AND usr.user_status=1
//                     AND usr.user_deleted=0";
//         
//         if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
//             
//             catchExc($rsTmp->getMessage());
//             return false;
//         }
//             
//         if ($rsTmp->numRows() > 0) {
//             
//             $aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC);
//             
// //            if (strcmp($aRow["user_email"], $aArgs["Email"])) {
// //                
// //                catchErr("User email does not match account information");
// //                return false;
// //            }
//             
//             // set class member unique identifier and get user data
//             settype($aRow["user_id"], "integer");
//             $this->setUserId($aRow["user_id"]);
//             $aUser = $this->getUser($tmpUserId);
//             
//             // build email body
//             $sBody = "Your ".ENTITY." account login information is:\r\n\r\n";
//             $sBody .= "Login Name: ".$aUser["User Login"]."\r\n";
//             $sBody .= "Password: ".$aUser["Password Reminder"];
//             
//             return $this->_notifyUser($sBody);
// 
//         } else {
//             
//             catchErr("Cannot find user account information");
//             return false;
//         }
//     }
    
    /** 
     * get a user single user by id
     *
     * @return array user data
     * @access public
     */
    function getUser($tmpUserId) {
        
        $sql = "SELECT 
			user_id,
			user_login,
			user_fname,
			user_lname,
			user_pass,
			user_remind,
			user_email,
			user_last_login_ip,
			user_last_login_host,
			user_last_login_dt,
			user_status,
			user_created_dt,
			user_modified_dt
                FROM 
			".PREFIX."_member_users 
                WHERE 
			user_id=?";
        
        // assign result to array
        $aRow = $this->_oConn->getRow($sql, array($tmpUserId));
        
        // build user array to return
        $return["User Id"] = $aRow["user_id"];
        $return["User Login"] = $aRow["user_login"];
        $return["User FName"] = $aRow["user_fname"];
        $return["User LName"] = $aRow["user_lname"];
        $return["Password"] = $aRow["user_pass"];
        $return["Password Reminder"] = $aRow["user_remind"];
        $return["Email"] = $aRow["user_email"];
        $return["Login Date"] = strtotime($aRow["user_last_login_dt"]);
        $return["Login Address"] = $aRow["user_last_login_ip"];
        $return["Login Host"] = $aRow["user_last_login_host"];
        $return["Status"] = $aRow["user_status"];
        $return["Created Date"] = strtotime($aRow["user_created_dt"]);
        $return["Modified Date"] = strtotime($aRow["user_modified_dt"]);
        
        // get permissions for user
        $sql = "SELECT 
                    app.user_app_id, 
                    perm.user_perm 
                FROM 
                    ".PREFIX."_user_apps app, 
                    ".PREFIX."_user_perms perm 
                WHERE 
                    perm.user_app_id=app.user_app_id 
                    AND perm.user_user_id=:user_id 
                ORDER BY 
                    app.user_app_id";
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->bindValue(':user_id', $this->_iUserId);
        $stmt->execute();
        
        // build permissions array to return
        while ($aRow = $stmt->fetch()) {
            
            $return["Perms"][$aRow["user_app_id"]] = $aRow["user_perm"];
        }
        
        return $return;
    }
    
    /** 
     * get users count for paging
     *
     * @return integer record count
     * @access public
     */
     function getUsersCount($wMem) {
        
        $sql = "SELECT COUNT(usr.user_id) AS user_cnt 
                FROM 
			".PREFIX."_members mem,
			".PREFIX."_member_users usr
                WHERE 
			mem.member_id=?
			and mem.member_id=usr.user_member_id
			and usr.user_deleted=0";
        
        return $this->_oConn->getOne($sql, array($wMem));
     }
     
    /** 
     * get users list
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function getUsers($wMem, $iPage=0) {
        
        // get a list of all users

        $sql = "SELECT 
			usr.user_id,
			usr.user_login,
			usr.user_pass,
			usr.user_remind,
			usr.user_email,
			usr.user_status,
			usr.user_created_dt,
			usr.user_modified_dt
                FROM 
			".PREFIX."_members mem,
			".PREFIX."_member_users usr
                WHERE 
			mem.member_id=?
			and mem.member_id=usr.user_member_id
			and usr.user_deleted=0
		ORDER BY
			usr.user_login";
       //         LIMIT ".$iPage.", ".ROWCOUNT;
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($wMem));
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
            $return[$i]["User Id"] = $aRow["user_id"];
            $return[$i]["User Name"] = $aRow["user_login"];
            $return[$i]["User Password"] = $aRow["user_pass"];
            $return[$i]["User Password Reminder"] = $aRow["user_remind"];
            $return[$i]["User Email"] = $aRow["user_email"];
            $return[$i]["Status"] = $aRow["user_status"];
            $return[$i]["Created Date"] = strtotime($aRow["user_created_dt"]);
            $return[$i]["Modified Date"] = strtotime($aRow["user_modified_dt"]);
            ++$i;
        }
        return $return;
    }    
    
    /** 
     * get applications list
     *
     * @return array applications data
     * @access public
     */
    function getApps($iComId) {

        $sql = "SELECT 
                    user_app_id, 
                    user_app_name 
                FROM 
                    ".PREFIX."_user_apps
		WHERE user_app_id IN
			(Select 
				user_app_id 
			from 
				".PREFIX."_member_perms
			where
				member_id=?
				and member_perm=1)
                ORDER BY 
                    user_app_id";

        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute(array($iComId));
        
        $i = 0;
        while ($aRow = $stmt->fetch()) {
        
            $return[$i]["App Id"] = $aRow["user_app_id"];
            $return[$i]["App Name"] = $aRow["user_app_name"];
            $return[$i]["Perm"] = 0;
            ++$i;
        }
        return $return;
    }
     
    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * add a new user record
     *
     * @param array $aArgs user data
     * @return boolean
     * @access public
     */
    function addUser($aArgs) {
        
        // generate random unique password
        $sPasswordRemind = substr(md5(uniqid(rand(1, 100000))), 3, 8);
        
        // encrypt new password
        $sPassword = substr(md5($sPasswordRemind), 3 , 8);
        
        // check for existing user name
        if ($this->_userExists($aArgs["Login"], $aArgs["Compa"])) {
            
            catchErr("This user name already exists");
            return false;
            
        } else {
            
          
            // add new user record
            $sql = "INSERT INTO ".PREFIX."_member_users (
                        user_member_id,
                        user_login,
                        user_fname,
                        user_lname, 
                        user_pass, 
                        user_remind,
                        user_status,
                        user_deleted, 
                        user_email, 
                        user_created_dt, 
                        user_modified_dt
                    ) values (
			?,
			?,
			?,
			?,
                       ?, 
                        ?,
                        1,
                        0,
			'".$aArgs["Email"]."',
                        GETDATE(), 
                        GETDATE()
                    )";
            
            $this->_oConn->doQuery($sql, array($aArgs["Compa"], $aArgs["Login"], $aArgs["FName"], $aArgs["LName"], $sPassword, $sPasswordRemind));
            
            // get unique identifier for new record
            
            $iUserId = $this->_getUserId($aArgs["Login"], $aArgs["Compa"]);
            
            
            // set member variable for unique identifier
            settype($iUserId, "integer");
//            $this->setUserId($iUserId);
            
            // set permissions values
            $this->_setPerms($aArgs["Perms"], $iUserId);
            
            // build user email
            $sBody = "Your Van-On-Demand account has been created.\r\n";
            $sBody .= "Your login information is:\r\n\r\n"; 
            $sBody .= "User Name: ".$aArgs["Login"]."\r\n";
            $sBody .= "Password: ".$sPasswordRemind;
//            if (!empty($aArgs["EdiCo"])) {
//                $sBody = "Dear Member,\r\n\r\n";
//                $sBody .= "Welcome, Your EDI Associates account has been created.\r\n\r\n";
//                $sBody .= "   Company ID: ".$aArgs["EdiCo"]."\r\n\r\n";
//                $sBody .= "Here is a list of the login name(s) and password(s) associated with that company id:\r\n\r\n";
//                $sBody .= "   User Name: ".$aArgs["Login"]."\r\n";
//                $sBody .= "   Password: ".$sPasswordRemind."\r\n\r\n";
//                $sBody .= "Use one of the login names listed above to access your EDI Associates Business Services account, by selecting Login from the http://www.ediassociates.com website.\r\n\r\n";
//                $sBody .= "If you have any questions, please contact Support@ediassociates.com.\r\n\r\n";
//                $sBody .= "Regards,\r\n\r\n";
//                $sBody .= "EDI Associates Business Services\r\n";
//            }
            return $this->_notifyUser($sBody, $iUserId);
        }
    }
    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * edit an existing user record
     *
     * @param array user data
     * @return boolean
     * @access public
     */
    function editUser($aArgs, $tmpUserId) {
        
        // update user record
        $sql = "UPDATE ".PREFIX."_member_users SET 
                    user_email=?, 
                    user_fname=?, 
                    user_lname=?, 
                    user_modified_dt=GETDATE() 
                WHERE 
                    user_id=?";
        
        $this->_oConn->doQuery($sql, array($aArgs["Email"], $aArgs["FName"], $aArgs["LName"], $tmpUserId));
        
        // check to reset user password

        if ($aArgs["Reset"]) {
            
            // reset user password
            $this->_resetPassword($tmpUserId);
        }
        
        // update user permission records
        $this->_setPerms($aArgs["Perms"], $tmpUserId);
        return true;
    }
    
    /** 
     * update user settings
     *
     * @param array user data
     * @return boolean
     * @access public
     */
    function updateSettings($aArgs) {
        
        // initialize sql filter
        $sFilter = "";
        
        if (!empty($aArgs["Password"])) {
            
            // generate new password and add sql filter
            $sPasswordRemind = $aArgs["Password"];
            $sPassword = substr(md5($sPasswordRemind), 3 , 8);
            $sFilter = " user_pass='".$sPassword."', 
            user_remind='".$sPasswordRemind."',";
        }
        
        // update user record
        $sql = "UPDATE ".PREFIX."_member_users SET 
                    user_email='".$aArgs["Email"]."',".$sFilter." 
                    user_modified_dt=GETDATE() 
                WHERE 
                    user_id=".$this->_iUserId;
        
        $this->_oConn->doQuery($sql);
        
        return true;
    }
    
    /** 
     * delete a user record
     *
     * @return boolean
     * @access public
     */
    function deleteUser($tmpUserId) {
        
        $sql = "DELETE FROM ".PREFIX."_user_perms WHERE user_user_id=".$tmpUserId;

        $this->_oConn->doQuery($sql);

        $sql = "DELETE FROM ".PREFIX."_member_users WHERE user_id=".$tmpUserId;
        
        $this->_oConn->doQuery($sql);
        
        return true;
    }
    
    /** 
     * activate a user record
     *
     * @return boolean
     * @access public
     */
    function activateUser($tmpUserId) {
        
        $sql = "UPDATE ".PREFIX."_member_users SET 
                    user_status=1,
                    user_modified_dt=GETDATE() 
                WHERE 
                    user_id=".$tmpUserId;
         
        $this->_oConn->doQuery($sql);
    }
    
    /** 
     * deactivate a user record
     *
     * @return boolean
     * @access public
     */
    function deactivateUser($tmpUserId) {
        
        $sql = "UPDATE ".PREFIX."_member_users SET 
                    user_status=0,
                    user_modified_dt=GETDATE()
                WHERE 
                    user_id=".$tmpUserId;
        
        $this->_oConn->doQuery($sql);
    }


    /** 
     * activate a user record
     *
     * @return boolean
     * @access public
     */
    function newActivateUser($sId='') {
        
        $sql = "SELECT user_login FROM ".PREFIX."_member_users
                WHERE 
                    activation_key = '".$sId."'";
        

        $sTempId = $this->_oConn->getOne($sql);
        
            if (!empty($sTempId)) {
    
            $sql = "UPDATE ".PREFIX."_member_users SET 
                        user_status=1,
                        user_modified_dt=GETDATE(),
                        activation_key='' 
                    WHERE 
                        activation_key = '".$sId."'";
             
            $this->_oConn->doQuery($sql);
            $sBody = "Congratulations your Van-On-Demand account has been activated<br><br><img src='/_img/arrow.jpg' alt='Login Link' /> You may select the Van-On-Demand link to begin login.";
        } else {
            $sBody = "*** Sorry this account either does not exist or is already active. ***";
        }
        
        return $sBody;
    }
} // close the class definition

?>
