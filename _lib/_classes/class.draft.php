<?php

// File Location: /_lib/_classes/class.draft.php

// require PEAR objects
require_once("DB.php");

// require USER objects
require_once("config.php");
require_once("funcs.php");

/** 
 * handles user functions
 *
 * @author Mike Buzzard <src@cubancouncil.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright Apress
 *
 */
class draft { // open the class definition
    
    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setUserId()
     */
    var $_iDraftId;
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer user id [optional]
     * @access public
     */
    function draft() {
        
        // Instanciate the database connection
        $this->_oConn = get_db();
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
     
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    function getTranType($sTid) {

	$sql = "Select EDIADocTypeID
        FROM 
		".PREFIX."_edidocs
	WHERE 
		EDIATransID='".$sTid."'";

        return $this->_oConn->getOne($sql);
    }

    function getStatus($sTid) {

	$sql = "Select EDIAStatus
        FROM 
		".PREFIX."_edidocs
	WHERE 
		EDIATransID='".$sTid."'";

    return $this->_oConn->getOne($sql);
    }
   
    function getHDRRefData($wRec='') {

    $sql = "select trn.*,par.*,mem.*
    FROM
       ".PREFIX."_partners as par,
       ".PREFIX."_edidocs as trn,
       ".PREFIX."_members as mem
    WHERE 
        trn.MemberNum=mem.member_id
        and trn.PartnerNumber=par.tp_id
        and trn.EDIATransID='".$wRec."'";

    return $this->_oConn->getRow($sql);        
    }

    function get_990($wRec='') {

    $sql = "select trn.TransNumber,trn.TransDate,par.tp_name,par.tp_edi_code
    FROM 
        ".PREFIX."_partners as par,
        ".PREFIX."_edidocs as trn
    WHERE 
        trn.GSSenderID=par.tp_edi_code
        and trn.EDIATransID='".$wRec."'";
        
    return $this->_oConn->getRow($sql);        
    }

    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /** 
    * add a new 990 record
    *
    * @param array $aArgs transaction data
    * @return boolean
    * @access public
    */

    function add990($aArgs) {
    
    // add new user record
    $sql = "INSERT INTO ".PREFIX."_edidocs (
        EDIATransID,
        EDIATransDateTime,
        MemberNum,
        PartnerNumber,
        TransNumber,
        TransDate, 
        RefTransNumber,
        RefTransDate,
        Ack, 
        RefIBEDIATransID, 
        EDIADocTypeID, 
        EDIACreateDate, 
        EDIAStatus,
        EDIAStatusDate
        ) values (
        '".$aArgs["EDIATransID"]."',
        (GETDATE()),
        ".$aArgs["MemberNum"].",
        ".$aArgs["PartnerNumber"].",
        '".$aArgs["TransNumber"]."',
        (GETDATE()),
        '".$aArgs["RefTransNumber"]."',
        '".$aArgs["RefTransDate"]."',
        '".$aArgs["Ack"]."',
        '".$aArgs["RefIBEDIATransID"]."',
        '990', 
        (GETDATE()),
        7, 
        (GETDATE())
        )";
    $this->_oConn->doQuery($sql);
        $sql = "UPDATE ".PREFIX."_edidocs SET 
                    EDIAStatus=3,
                    EDIAStatusDate=GETDATE() 
                WHERE 
                    EDIAStatus=2
                    and EDIATransID='".$aArgs["RefNum"]."'";
        $this->_oConn->doQuery($sql);
   return true;
   }
    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /** 
    * Set Status to O/B Deleted
    *
    * @param array $aArgs transaction data
    * @return boolean
    * @access public
    */
    function deleteTopic($sTid) {
	$iSta = $this->getStatus($sTid);
        $sql = "UPDATE ".PREFIX."_edidocs SET 
                    EDIALastStatus=".$iSta.",
                    EDIAStatus=11,
                    EDIAStatusDate=GETDATE() 
                WHERE 
                    EDIATransID='".$sTid."'";
        $this->_oConn->doQuery($sql);
   return true;
    }

    /** 
    * Set Status to O/B Archived
    *
    * @param array $aArgs transaction data
    * @return boolean
    * @access public
    */

    function archiveTopic($sTid) {
	$iSta = $this->getStatus($sTid);
        $sql = "UPDATE ".PREFIX."_edidocs SET 
                    EDIALastStatus=".$iSta.",
                    EDIAStatus=10,
                    EDIAStatusDate=GETDATE() 
                WHERE 
                    EDIATransID='".$sTid."'";
        
        $this->_oConn->doQuery($sql);
   return true;
    }

    /** 
    * Set Restore Archived and Delete transaction to their last Status
    *
    * @param array $aArgs transaction data
    * @return boolean
    * @access public
    */

    function restoreTopic($sTid) {
	$iSta = $this->getStatus($sTid);
        $sql = "UPDATE ".PREFIX."_edidocs SET 
                    EDIAStatus=EDIALastStatus,
                    EDIALastStatus=".$iSta.",
                    EDIAStatusDate=GETDATE() 
                WHERE 
                    EDIATransID='".$sTid."'";
        $this->_oConn->doQuery($sql);
   return true;
    }
}
?>
