<?php

// File Location: /_lib/_classes/class.manage_wsftp.php

require_once("DB.php");

/** 
 * handles user functions
 *
 * @author Mike Buzzard <src@cubancouncil.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright Apress
 *
 */
class wsftp { // open the class definition
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer user id [optional]
     * @access public
     */
    function wsftp() {
        
        // Instanciate the database connection
        $this->_oConn = get_db();
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
     
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * get users list
     *
     * @param string $sSort sort key
     * @param integer $iPage [optional] cursor
     * @return array user data
     * @access public
     */
    function FTPgetUsers() {
        
        // get a list of all users

        $sql = "SELECT * FROM pureftpd.ftpd WHERE status = '1' ORDER BY User";
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            
            $return[$i]["User Id"] = $aRow["User"];
            $return[$i]["User Name"] = $aRow["User"];
            $return[$i]["Full Name"] = $aRow["Dir"];
            $return[$i]["Password"] = $aRow["Reminder"];
            $return[$i]["FTPMAXSPACE"] = $aRow["QuotaSize"];
            $return[$i]["FTPMAXFILES"] = $aRow["QuotaFiles"];
            $return[$i]["Status"] = $aRow["status"];
            ++$i;
        }
        return $return;
    }    
    
     
    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * Update an FTP user record
     *
     * @return boolean
     * @access public
     */
    function FTPaddUser($aData='') {
    
        $NewMemberID = $aData["USERID"];
        $newFTPDirectory = "/home/ftpusers/" . $NewMemberID;
        $sPasswordRemind = substr(md5(uniqid(rand(1, 100000))), 3, 8);

        // Create FTP Account entry in SQL
        $sql = "INSERT INTO pureftpd.ftpd (
            `User`,
            `status`, 
            `Password`, 
            `Uid`, 
            `Gid`, 
            `Dir`, 
            `ULBandwidth`, 
            `DLBandwidth`, 
            `comment`, 
            `ipaccess`, 
            `QuotaSize`, 
            `QuotaFiles`,
            `Reminder`
        ) VALUES (
            '".$NewMemberID."', 
            '1', 
            MD5('".$sPasswordRemind."'), 
            '2001', 
            '2001', 
            '".$newFTPDirectory."', 
            '100', 
            '100', 
            '', 
            '*', 
            '100', 
            '0',
            '".$sPasswordRemind."'
        )";
        
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
    }
   
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
   
    /** 
     * Update an FTP user record
     *
     * @return boolean
     * @access public
     */
    function FTPUpdateUser($FTPComp=0, $aData='') {
    
        $tmpVal = 32;
        $tmpVal = $tmpVal + $aData["LockHome"];
        $tmpVal = $tmpVal + $aData["DisablePub"];
        $tmpVal = $tmpVal + $aData["DisablePWChg"];
        
        $sql = "UPDATE ".PREFIX."_ftp_access SET 
                    FTPFLAGS = ".$tmpVal.",
                    FTPMAXSPACE = ".$aData["FTPMAXSPACE"].",
                    FTPMAXFILES = ".$aData["FTPMAXFILES"]."
                where 
                    DataKey =". $FTPComp;
         
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
    }

    /** 
     * activate an FTP user record
     *
     * @return boolean
     * @access public
     */
    function FTPactivateUser($FTPComp='') {
    
        $sql = "UPDATE pureftpd.ftpd SET status = '1' where User = '". $FTPComp ."'";
       
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
    }
    
    /** 
     * deactivate a FTP user record
     *
     * @return boolean
     * @access public
     */
    function FTPdeactivateUser($FTPComp='') {
        
        $sql = "UPDATE pureftpd.ftpd SET status = '0' where User = '". $FTPComp ."'";
         
        if (DB::isError($rsTmp = $this->_oConn->doQuery($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
    }
} // close the class definition

?>
