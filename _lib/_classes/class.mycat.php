<?php

// File Location: /_lib/_classes/class.partners.php

// require PEAR objects
require_once("DB.php");

// require USER objects
require_once("config.php");
require_once("funcs.php");

/** 
 * handles user functions
 *
 * @author Stephen Case <Stephen@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright EDI Associates, Inc.
 *
 */
class cat { // open the class definition
    
    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setCatId()
     */
    var $_iCatId;

    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setStyleId()
     */
    var $_iStyleId;

    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setItemId()
     */
    var $_iItemId;
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer user id [optional]
     * @access public
     */
    function cat() {
        
        // Instanciate the database connection
        $this->_oConn = get_db();
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
     
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    function setCatId($iCatId) {
        
        if (is_int($iCatId)) {
            
            $this->_iCatId = $iCatId;
        }
    }

    function setCatFormId2($iCatId) {
        
        $sql = "SELECT 
                    cat_Key_id,
                    cat_num_id,
                    cat_vernum_id,
                    cat_desc
                FROM 
                    ".PREFIX."_catalogs
                WHERE
                    cat_Key_id =".$iCatId;
        
        $aRow = $this->_oConn->getRow($sql);
        
        if ($aRow["cat_num_id"]) { $tmpStr = $aRow["cat_num_id"]; }
        if ($aRow["cat_vernum_id"]) { $tmpStr = $tmpStr ."-". $aRow["cat_vernum_id"]; }
        if ($aRow["cat_desc"]) { $tmpStr = $tmpStr ."-". $aRow["cat_desc"]; }

	$return["cId"] = $aRow["cat_Key_id"];
	$return["cDesc"] = $tmpStr;
        
        
        return $return;
    }


    function setCatFormId($iStyId) {
        
        $sql = "SELECT 
                    c.cat_Key_id,
                    c.cat_num_id,
                    c.cat_vernum_id,
                    c.cat_desc
                FROM 
                    ".PREFIX."_catalog_styles as s,
                    ".PREFIX."_catalogs as c
                WHERE
                    c.cat_Key_id = s.cat_Key_id
                    and s.Style_Key_id=".$iStyId;
        
        $aRow = $this->_oConn->getRow($sql);        
        
        if ($aRow["cat_num_id"]) { $tmpStr = $aRow["cat_num_id"]; }
        if ($aRow["cat_vernum_id"]) { $tmpStr = $tmpStr ."-". $aRow["cat_vernum_id"]; }
        if ($aRow["cat_desc"]) { $tmpStr = $tmpStr ."-". $aRow["cat_desc"]; }

	$return["cId"] = $aRow["cat_Key_id"];
	$return["cDesc"] = $tmpStr;
        
        
        return $return;
    }


    function setStyleId($iStyleId) {
        
        if (is_int($iStyleId)) {
            
            $this->_iStyleId = $iStyleId;
        }
    }

    function setStyFormId($iItemId) {
        
        $sql = "SELECT catStyleKey_id 
                FROM 
                    ".PREFIX."_catalog_items 
                WHERE 
                    catItemKey_id=".$iItemId;
        
        return $this->_oConn->getOne($sql);
    }

    function setItemId($iItemId) {
        
        if (is_int($iItemId)) {
            
            $this->_iItemId = $iItemId;
        }
    }
    
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    function _catalogExists($aArgs) {
 
         $sql = "SELECT 
                    count(1) 
                FROM 
                    ".PREFIX."_catalogs 
                WHERE 
                    cat_num_id='".$aArgs["Number"]."'
                    AND cat_vernum_id='".$aArgs["Version"]."'
                    AND cat_delete=0";
        
        return $this->_oConn->getOne($sql);
    }

    function _styleExists($aArgs) {
 
         $sql = "SELECT 
                    count(1)
                FROM 
                    ".PREFIX."_catalog_styles as s,
                    ".PREFIX."_catalogs as c
                WHERE 
                    c.cat_Key_id = s.cat_Key_id
                    and c.cat_member_id = ".$iComId."
                    and s.Style_id='".$aArgs["Style_id"]."'
                    and s.Style_delete = 0";

        return $this->_oConn->getOne($sql);
    }

    function _itemExists($aArgs) {
 
         $sql = "SELECT 
                    count(1)
                FROM 
                    ".PREFIX."_catalog_items as i,
                    ".PREFIX."_catalog_styles as s,
                    ".PREFIX."_catalogs as c
                WHERE 
                    i.catItemUPC = '".$aArgs["Number"]."'
                    and i.catStyleKey_id = s.Style_Key_id
                    and c.cat_Key_id = s.cat_Key_id
                    and c.cat_member_id = ".$iComId."
                    and i.catItemdelete = 0";

        return $this->_oConn->getOne($sql);
    }

    function getCatLkupVals($sVal) {
    
    
        // get a list of all users

        $sql = "SELECT 
                    val, 
                    des 
                FROM 
                    ".PREFIX."_catalog_lookup_values
                WHERE
                    val_type = '".$sVal."'";


        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
		$return[$i]["val"] = $aRow["val"];
		$return[$i]["des"] = $aRow["des"];
		++$i;
        }
        return $return;
    }


    function getItemCnt($iStyId) {
        
        $sql = "SELECT count(*) 
                FROM 
                    ".PREFIX."_catalog_items 
                WHERE 
                    catStyleKey_id=".$iStyId."
                    and catItemdelete = 0";
        
        return $this->_oConn->getOne($sql);
    }


    function getItem($wItem, $wComId) {
    
    
        // get a list of all users

        $sql = "SELECT 
                    * 
                FROM 
                    ".PREFIX."_catalog_items
                where
                    catItemKey_id = ".$wItem;

        
        return $this->_oConn->getRow($sql);
    }


    function getItems($iCursor=10, $iRCnt, $wSty, $wComId) {
    
        // get a list of all items

        $sql = "select * from ( select top ".$iRCnt." * from (select TOP ".$iCursor."
                    * 
                from 
                    ".PREFIX."_catalog_items 
                where 
                    catStyleKey_id = ".$wSty."
                    and catItemdelete = 0 
                ORDER BY catItemUPC) as t1 ORDER BY catItemUPC DESC)as t2 order by catItemUPC";

        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
		$return[$i]["Id"] = $aRow["catItemKey_id"];
		$return[$i]["Name"] = $aRow["catItemUPC"] ." - ". $aRow["catItemVenSizeDesc"];
		$return[$i]["NFRCode"] = $aRow["catItemNFRCode"];
		$return[$i]["NFRColor"] = $aRow["catItemNRFColor"];
		$return[$i]["NFRSize"] = $aRow["catItemNRFSize"];
		$return[$i]["NFRSize1"] = $aRow["catItemNRFSize1"];
		$return[$i]["Status"] = $aRow["catItemStatus"];
		$return[$i]["Created"] = strtotime($aRow["catItemcreate_dt"]);
		++$i;
        }
        return $return;
    }


    function getStyle($wSty, $wComId) {
    
    
        // get a list of all users

        $sql = "select 
                    * 
                from 
                    ".PREFIX."_catalog_styles 
                where 
                    Style_Key_id = ".$wSty." 
                    and Style_Status = 1";

        
        return $this->_oConn->getRow($sql);
    }

    function getStyleCnt($wCat, $sSF='') {
        
        $sql = "SELECT count(*) 
                from 
                    ".PREFIX."_catalog_styles 
                where 
                    cat_Key_id = ".$wCat." 
                    ".$sSF."
                    and Style_delete = 0";
                    
        return $this->_oConn->getOne($sql);
    }


     function getStyles($iCursor=10, $iRCnt, $wCat, $wComId, $sSF='') {

        // get a list of all users

        $sql = "select * from ( select top ".$iRCnt." * from (select TOP ".$iCursor."
                    Style_Key_id, 
                    Style_id, 
                    Style_desc,
                    Style_Status,
                    Style_create_dt 
                from 
                    ".PREFIX."_catalog_styles 
                where 
                    cat_Key_id = ".$wCat." 
                    and Style_delete = 0
                    ".$sSF."
                ORDER BY
                    Style_id) as t1 ORDER BY Style_id DESC)as t2 order by Style_id";        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
		$return[$i]["Id"] = $aRow["Style_Key_id"];
		$return[$i]["Name"] = $aRow["Style_id"] ." - ". $aRow["Style_desc"];
		$return[$i]["Status"] = $aRow["Style_Status"];
		$return[$i]["Created"] = strtotime($aRow["Style_create_dt"]);
		++$i;
        }
        return $return;
    }    

    function getCatalog($wCat, $wComId) {
        
        // get a list of all users

        $sql = "SELECT 
            c.cat_Key_id,
            c.cat_num_id,
            c.cat_vernum_id,
            c.cat_desc,
            c.cat_status,
            c.cat_create_dt,
            c.cat_modified_dt
        FROM 
            ".PREFIX."_catalogs as c,
            ".PREFIX."_catalog_perms as cp,
            ".PREFIX."_members as m
        WHERE
            c.cat_Key_id=".$wCat."
            and cp.cat_perm = 1
            and cp.cat_Key_id = c.cat_Key_id
            and cp.cat_member_id = m.member_id
            and c.cat_delete=0
            and c.cat_status = 1
            and cp.cat_partner_id IN (SELECT 
                tp_id 
            from 
                ".PREFIX."_partners 
            where 
                tp_edi_code = (select 
                    member_login 
                from 
                    ".PREFIX."_members 
                where 
                    member_id = ".$wComId."))";


        $aRow = $this->_oConn->getRow($sql);
            
	$return["Cat Id"] = $aRow["cat_Key_id"];
	$return["Number"] = $aRow["cat_num_id"];
	$return["Version"] = $aRow["cat_vernum_id"];
	$return["Desc"] = $aRow["cat_desc"];
	$return["Status"] = $aRow["cat_status"];
	$return["Created Date"] = strtotime($aRow["cat_create_dt"]);
	$return["Modified Date"] = strtotime($aRow["cat_modified_dt"]);

        return $return;
    }    

    function getCatalogs($wMem) {
        
        // get a list of all catalogs

        $sql = "SELECT 
            c.cat_Key_id,
            c.cat_num_id,
            c.cat_vernum_id,
            c.cat_desc,
            c.cat_status,
            c.cat_create_dt
        FROM 
            edia_catalogs as c,
            edia_catalog_perms as cp,
            edia_members as m
        WHERE
            cp.cat_perm = 1
            and cp.cat_Key_id = c.cat_Key_id
            and cp.cat_member_id = m.member_id
            and c.cat_delete=0
            and c.cat_status = 1
            and cp.cat_partner_id IN (SELECT 
                tp_id 
            from 
                edia_partners 
            where 
                tp_edi_code = (select 
                    member_login 
                from 
                    edia_members 
                where 
                    member_id = ".$wMem."))";
         //       LIMIT ".$iPage.", ".ROWCOUNT;
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        $i = 0;
        while ($aRow = $stmt->fetch()) {
            
		$return[$i]["Id"] = $aRow["cat_Key_id"];
		$return[$i]["Name"] = $aRow["cat_num_id"] ."-". $aRow["cat_vernum_id"] ."-". $aRow["cat_desc"];
		$return[$i]["Company"] = $this->getCatalogMemName($aRow["cat_Key_id"]);
		$return[$i]["Status"] = $aRow["cat_status"];
		$return[$i]["Created Date"] = strtotime($aRow["cat_create_dt"]);
		++$i;
        }
        return $return;
    }    

    function getCatalogMemName($wCat) {
        
        // get a list of all users

        $sql = "Select 
			m.member_company_name 
		from 
			".PREFIX."_members as m,
			".PREFIX."_catalogs as c
		where
			m.member_id = cat_member_id
			and cat_Key_id =".$wCat;
        
        return $this->_oConn->getOne($sql);
    }    

    function exportCatalog($wMem, $wCat, $fi) {
        
        // get a list of all users

        $sql = "Select 
                    c.cat_num_id,    
                    c.cat_vernum_id,
                    c.cat_desc,
                    c.cat_date,
                    s.Style_id,
                    s.Style_desc,
                    s.stylePublishDate,
                    s.styleItemManufacturer,
                    s.styleItemFabric,
                    s.styleItemLeadTimeQual,
                    s.styleItemLeadTimeCode,
                    s.styleItemLeadTime,
                    s.styleItemAvailDate,
                    s.styleItemMinOrder,
                    s.styleItemMaxOrder,
                    s.styleItemMinMaxQual,
                    s.styleItemReOrderFlag,
                    s.styleItemSeasonFlag,
                    s.styleItemCountryOrig,
                    s.styleItemPointOrig,
                    s.styleItemShipLength,
                    s.styleItemShipHeight,
                    s.styleItemShipWidth,
                    s.styleItemShipQualifier,
                    s.styleItemWeight,
                    s.styleItemWeightQual,
                    s.styleItemHazMatQual,
                    s.styleItemHazMatClass,
                    s.styleItemHazMatDesc,
                    s.styleItemImageURL,
                    i.catItemUPC,
                    i.catItemPreUPC,
                    i.catItemALU,
                    i.catItemNRFColor,
                    i.catItemNRFSize,
                    i.catItemNRFSize1,
                    i.catItemNRFSize2,
                    i.catItemVenColor,
                    i.catItemVenColorDesc,
                    i.catItemVenSize,
                    i.catItemVenSizeDesc,
                    i.catItemAffective_dt,
                    i.catItemDiscontinue_dt,
                    i.catItemQTYOnHandQual,
                    i.catItemQTYOnHand,
                    i.catItemInnerUnitsPerPackQual,
                    i.catItemInnerUnitsPerPack 
                FROM 
                    edia_catalogs as c,
                    edia_catalog_styles as s,
                    edia_catalog_items as i
                WHERE
                    i.catStyleKey_id = s.Style_Key_id
                    and s.cat_Key_id = c.cat_Key_id
                    and c.cat_Key_id IN (SELECT 
                            c.cat_Key_id
                        FROM 
                            edia_catalogs as c,
                            edia_catalog_perms as cp,
                            edia_members as m
                        WHERE
                            cp.cat_perm = 1
                            and cp.cat_Key_id = c.cat_Key_id
                            and cp.cat_member_id = m.member_id
                            and c.cat_delete=0
                            and c.cat_status = 1
                            and cp.cat_partner_id IN (SELECT 
                                tp_id 
                            from 
                                edia_partners 
                            where 
                                tp_edi_code = (select 
                                    member_login 
                                from 
                                    edia_members 
                                where 
                                    member_id = ".$wMem.")))
                    and c.cat_Key_id = ".$wCat;
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        while ($aRow = $stmt->fetch()) {
            
		$return = $this->fputcsv($fi, $aRow);
        }
        return $return;
    }    

    function exportStyle($wMem, $wSty, $fi) {
        
        // get a list of all users

        $sql = "Select 
                    c.cat_num_id,    
                    c.cat_vernum_id,
                    c.cat_desc,
                    c.cat_date,
                    s.Style_id,
                    s.Style_desc,
                    s.stylePublishDate,
                    s.styleItemManufacturer,
                    s.styleItemFabric,
                    s.styleItemLeadTimeQual,
                    s.styleItemLeadTimeCode,
                    s.styleItemLeadTime,
                    s.styleItemAvailDate,
                    s.styleItemMinOrder,
                    s.styleItemMaxOrder,
                    s.styleItemMinMaxQual,
                    s.styleItemReOrderFlag,
                    s.styleItemSeasonFlag,
                    s.styleItemCountryOrig,
                    s.styleItemPointOrig,
                    s.styleItemShipLength,
                    s.styleItemShipHeight,
                    s.styleItemShipWidth,
                    s.styleItemShipQualifier,
                    s.styleItemWeight,
                    s.styleItemWeightQual,
                    s.styleItemHazMatQual,
                    s.styleItemHazMatClass,
                    s.styleItemHazMatDesc,
                    s.styleItemImageURL,
                    i.catItemUPC,
                    i.catItemPreUPC,
                    i.catItemALU,
                    i.catItemNRFColor,
                    i.catItemNRFSize,
                    i.catItemNRFSize1,
                    i.catItemNRFSize2,
                    i.catItemVenColor,
                    i.catItemVenColorDesc,
                    i.catItemVenSize,
                    i.catItemVenSizeDesc,
                    i.catItemAffective_dt,
                    i.catItemDiscontinue_dt,
                    i.catItemQTYOnHandQual,
                    i.catItemQTYOnHand,
                    i.catItemInnerUnitsPerPackQual,
                    i.catItemInnerUnitsPerPack 
                FROM 
                    edia_catalogs as c,
                    edia_catalog_styles as s,
                    edia_catalog_items as i
                WHERE
                    i.catStyleKey_id = s.Style_Key_id
                    and s.cat_Key_id = c.cat_Key_id
                    and c.cat_Key_id IN (SELECT 
                            c.cat_Key_id
                        FROM 
                            edia_catalogs as c,
                            edia_catalog_perms as cp,
                            edia_members as m
                        WHERE
                            cp.cat_perm = 1
                            and cp.cat_Key_id = c.cat_Key_id
                            and cp.cat_member_id = m.member_id
                            and c.cat_delete=0
                            and c.cat_status = 1
                            and cp.cat_partner_id IN (SELECT 
                                tp_id 
                            from 
                                edia_partners 
                            where 
                                tp_edi_code = (select 
                                    member_login 
                                from 
                                    edia_members 
                                where 
                                    member_id = ".$wMem.")))
                    and s.Style_Key_id = ".$wSty;
        
        $stmt = $this->_oConn->prepare($sql);
        $stmt->execute();
        
        // loop through result and build return array
        while ($aRow = $stmt->fetch()) {
            
		$return = $this->fputcsv($fi, $aRow);
        }
        return $return;
    }    


    function fputcsv ($fi, $array, $deliminator=",") { 
    // function fputcsv ($array, $deliminator=",") { 
    
        $line = ""; 
        // $fp = "testfile.csv";
        $fi=str_replace("/", "\\", $fi);
        $fp = fopen($fi,"a");
        foreach($array as $val) { 
            $val = str_replace("\r\n", "\n", $val); 
            
                if(ereg("[$deliminator\"\n\r]", $val)) { 
                    $val = '"'.str_replace('"', '""', $val).'"'; 
                }
            
            $line .= $val.$deliminator; 
        
        }
        
        $line = substr($line, 0, (strlen($deliminator) * -1)); 
        $line .= "\n"; 
        
        return fputs($fp, $line); 
    
    }

    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
   
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
}

?>
