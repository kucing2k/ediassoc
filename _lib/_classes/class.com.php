<?php

// File Location: /_lib/_classes/class.edia.php

require_once("DB.php");

/** 
 * handles transactions functions
 *
 * @author Stephen Case <src@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright EDI Associates, Inc.
 *
 */
class com { // open the class definition
    
    /**
	* @var EdiAssocPdo
	*/
	var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer $id [optional] advertisement identifier
     * @access public
     */
    function com() {
        
        // implement pear db object
        $this->_oConn = get_db();
        
         
    }
    
    // PRIVATE METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    /** 
     * get users count for paging
     *
     * @return integer record count
     * @access public
     */
     
     function getFTPInformation($wComId) {
        
        $sql = "SELECT * FROM pureftpd.ftpd WHERE User='".$wComId."'";
        
        return $this->_oConn->getRow($sql);
     }
     
     function getNewFTPPassword($wComId) {
     
        $sPasswordRemind = substr(md5(uniqid(rand(1, 100000))), 3, 10);
        
        $sql = "UPDATE pureftpd.ftpd SET `Password` = MD5('".$sPasswordRemind."') WHERE User = '".$wComId."'";
        $this->_oConn->doQuery($sql);

        $sql = "UPDATE pureftpd.ftpd SET `Reminder` = '".$sPasswordRemind."' WHERE User = '".$wComId."'";
        
        $this->_oConn->doQuery($sql);
     
     }

    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


} // close the class definition

?>