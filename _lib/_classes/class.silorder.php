<?php

class SilOrder {
    /**
     * @var EdiAssocPdo 
     */
    protected $db;
    
    public function __construct() {
        $this->db = get_db();
    }
    
    public function getAllShipments() {
        $stmt = $this->db->prepare('select 
            t1.EDIATransID,
            t1.TransNumber,
            t2.DetailReference,
            t3.Address_Name,
            t3.Country,
            t2.DetailRefDescription,
            t1.TransDate
            from HDR_INFO t1,
            HDR_REF_INFO t2,
            HDR_ADD_INFO t3
            where
            t1.DataKey = t2.ParentKey and
            t1.DataKey = t3.ParentKey
        ');
        $stmt->execute();
        return $stmt->fetchAll();
    }
}