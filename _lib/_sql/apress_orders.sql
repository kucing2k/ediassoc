CREATE TABLE apress_orders (
order_id int(10) NOT NULL auto_increment,
account_id int(10),
order_total_price float(10) NOT NULL default '0',
order_cc_number varchar(20),
order_cc_exp_dt varchar(5),
order_ship_dt datetime default NULL,
order_pfp_confirm int(1) NOT NULL default '0',
status int(1) NOT NULL default '0',
created_dt datetime NOT NULL default '0000-00-00 00:00:00',
PRIMARY KEY (order_id)
) TYPE=MyISAM;
