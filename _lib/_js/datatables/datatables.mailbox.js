$(document).ready(function() {
    /* Initialise datatables */
    var oTable = $('#dataTable').dataTable( {
        "aaSorting": [],
        "sPaginationType": "full_numbers",
        "bPaginate": false,
        "sDom": 'lrtip',
        "aoColumns": [
            {"bSortable": false, "bSearchable": false, "sType": "html"},
            {"sType": "string"},
            {"sType": "html"},
            {"sType": "html"},
            {"sType": "html"},
            {"sType": "html"},
            {"bSortable": false, "bSearchable": false, "sType": "html"}
        ] } );

    /* Add event listeners to the inputs */
    $("#searchICN, #searchPDId, #searchEndDate, #searchEndTime, #searchStartDate, #searchStartTime").keyup( function() {
        oTable.fnDraw();
    });

    $('#searchDirection').change( function() { oTable.fnDraw(); } );


    $('#advSearchButton').click(function() {
        $('#advSearchDiv').slideToggle('slow');
        if ($('#advSearchButton').val() == "Advanced Search"){
            $('#advSearchButton').val("Close Advanced Search");
        } else {
            $('#advSearchButton').val("Advanced Search");
        }
    });
} );

function getTextContent(html){
    div = document.createElement('div');
    div.innerHTML = html;
    if(document.body.textContent){
        return div.textContent;
    } else {
        return div.innerText;
    }
}

/* Custom filtering function which will filter data in column four between two values */
$.fn.dataTableExt.afnFiltering.push(
function(oSettings, aData, iDataIndex) {

    // filter by ICN
    tc = getTextContent(aData[2]);

    if (tc.indexOf($("#searchICN").val()) == -1){
        return false;
    }

    // filter by sender/receiver id
    if ($("#searchDirection").val() == "Sent"){
        tc = getTextContent(aData[3]);
    } else {
        tc = getTextContent(aData[4]);
    }

    if (tc.indexOf($("#searchPDId").val()) == -1){
        return false;
    }

    // filter by date
    rxDate = /\b\d{4}[\/-]\d{2}[\/-]\d{2}\b/;
    rxTime = /\b\d{2}[:]\d{2}[:]\d{2}\b/;

    tc = getTextContent(aData[5]);
    rowDate = tc.replace(" ", "T");

    // start date
    if (rxDate.test($("#searchStartDate").val()) && rxTime.test($("#searchStartTime").val())){
        seDate = $("#searchStartDate").val() + "T" + $("#searchStartTime").val();

        if (seDate > rowDate){
            return false;
        }
    }

    // end date
    if (rxDate.test($("#searchEndDate").val()) && rxTime.test($("#searchEndTime").val())){
        seDate = $("#searchEndDate").val() + "T" + $("#searchEndTime").val();
        if (seDate < rowDate){
            return false;
        }
    }

    // default
    return true;
}
);


