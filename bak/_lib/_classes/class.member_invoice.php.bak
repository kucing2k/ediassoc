<?php

// File Location: /_lib/_classes/class.members.php

// require PEAR objects
require_once("DB.php");
require_once("class.users.php");

// require USER objects
require_once("config.php");
require_once("funcs.php");

/** 
 * handles user functions
 *
 * @author Stephen Case <Stephen@ediassociates.com>
 * @version 1.0
 * @since 1.0
 * @access public
 * @copyright EDI Associates, Inc.
 *
 */
class mem { // open the class definition
    
    /** 
     * class member variables
     *
     * @var integer
     * @access private
     * @see setPartId()
     */
    var $_iMemId;
    
    /** 
     * PEAR db object
     *
     * @var object
     * @access private
     */
    var $_oConn;
    
    // CONSTRUCTOR ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * class constructor
     *
     * @param integer user id [optional]
     * @access public
     */
    function mem() {
        
        // Instanciate the database connection
        $this->_oConn =& DB::connect(DSN);
        
        // Check for DB class exceptions
        if (DB::isError($this->_oConn)) {
            
            // Report class exceptions if present
            catchExc($this->_oConn->getMessage());
        }
        
    }
    
    // PRIVATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::

     /** 
     * set user permissions
     *
     * @param array permissions
     * @return boolean
     * @access private
     */ 
    
    function _setMPerms($aPerms, $wComId) {
        
        // check perms array
        if (count($aPerms)) {
            
            $sql = "DELETE FROM 
                        ".PREFIX."_member_perms
                    WHERE 
                        member_id = ".$wComId;
            
            if (DB::isError($rsTmp = $this->_oConn->query($sql))) {
                
                catchExc($rsTmp->getMessage());
                return false;
            }
            
            // loop through permissions array
            while (list($key, $val) = each($aPerms)) {
                
                // add new permission record
                $sql = "INSERT INTO ".PREFIX."_member_perms (
                            user_app_id, 
                            member_id,
                            member_perm
                        ) values (
                            ".$key.",
                            ".$wComId.",
                            ".$val."
                        )";
                
                if (DB::isError($rsTmp = $this->_oConn->query($sql))) {
                    
                    catchExc($rsTmp->getMessage());
                    return false;
                }
            }
            
            return true;
        }
    }
    
     
    // PUBLIC METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    function setMemId($iMemId) {
        
        if (is_int($iMemId)) {
            
            $this->_iMemId = $iMemId;
        }
    }
    
    
    // SELECT METHODS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
    /** 
     * get total transcount count for invoiceing
     *
     * @return integer record count
     * @access public
     */
     function getMemberTransactionCount($mi, $sd, $ed) {
        
        $sql = "SELECT
            count(*) as transcnt 
        FROM 
            datagate.dbo.dg_interchanges 
        WHERE 
            (ISA_Sender IN (select member_receiver_id from ".PREFIX."_member_ids where member_id = ".$mi.") 
        OR 
            ISA_Receiver IN (select member_receiver_id from ".PREFIX."_member_ids where member_id = ".$mi.")) 
        AND 
            DateStamp >= '".$sd."' 
        AND 
            DateStamp <= '".$ed."'";
        
        if (DB::isError($iCnt = $this->_oConn->getOne($sql))) {
            
            catchExc($iCnt->getMessage());
            return false;
        }
        
        if (!$iCnt) {$iCnt = 0;}
        return $iCnt;
     }

    /** 
     * get total characters received for invoiceing
     *
     * @return integer record count
     * @access public
     */
     function getMemberCharacterRecFromMemCount($mi, $sd, $ed) {
        
        $sql = "SELECT
            sum(NbrBytes) as charcnt 
        FROM 
            datagate.dbo.dg_interchanges 
        WHERE 
            ISA_Receiver IN (select member_receiver_id from ".PREFIX."_member_ids where member_id = ".$mi.") 
        AND
            ISA_Sender IN (select member_receiver_id from ".PREFIX."_member_ids) 
        AND 
            DateStamp >= '".$sd."' 
        AND 
            DateStamp <= '".$ed."'";
        
        if (DB::isError($iCnt = $this->_oConn->getOne($sql))) {
            
            catchExc($iCnt->getMessage());
            return false;
        }
        if (!$iCnt) {$iCnt = 0;}
        return $iCnt;
     }

    /** 
     * get total characters sent for invoiceing
     *
     * @return integer record count
     * @access public
     */
     function getMemberCharacterSentToMemCount($mi, $sd, $ed) {
        
        $sql = "SELECT
            sum(NbrBytes) as charcnt 
        FROM 
            datagate.dbo.dg_interchanges 
        WHERE 
            ISA_Sender IN (select member_receiver_id from ".PREFIX."_member_ids where member_id = ".$mi.") 
        AND
            ISA_Receiver IN (select member_receiver_id from ".PREFIX."_member_ids) 
        AND 
            DateStamp >= '".$sd."' 
        AND 
            DateStamp <= '".$ed."'";
        
        if (DB::isError($iCnt = $this->_oConn->getOne($sql))) {
            
            catchExc($iCnt->getMessage());
            return false;
        }
        
        if (!$iCnt) {$iCnt = 0;}
        return $iCnt;
     }

    /** 
     * get total characters received for invoiceing
     *
     * @return integer record count
     * @access public
     */
     function getMemberCharacterRecFromNonMemCount($mi, $sd, $ed) {
        
        $sql = "SELECT
            sum(NbrBytes) as charcnt 
        FROM 
            datagate.dbo.dg_interchanges 
        WHERE 
            ISA_Receiver IN (select member_receiver_id from ".PREFIX."_member_ids where member_id = ".$mi.") 
        AND
            ISA_Sender NOT IN (select member_receiver_id from ".PREFIX."_member_ids) 
        AND 
            DateStamp >= '".$sd."' 
        AND 
            DateStamp <= '".$ed."'";
        
        if (DB::isError($iCnt = $this->_oConn->getOne($sql))) {
            
            catchExc($iCnt->getMessage());
            return false;
        }
        if (!$iCnt) {$iCnt = 0;}
        return $iCnt;
     }

    /** 
     * get total characters sent for invoiceing
     *
     * @return integer record count
     * @access public
     */
     function getMemberCharacterSentToNonMemCount($mi, $sd, $ed) {
        
        $sql = "SELECT
            sum(NbrBytes) as charcnt 
        FROM 
            datagate.dbo.dg_interchanges 
        WHERE 
            ISA_Sender IN (select member_receiver_id from ".PREFIX."_member_ids where member_id = ".$mi.") 
        AND
            ISA_Receiver NOT IN (select member_receiver_id from ".PREFIX."_member_ids) 
        AND 
            DateStamp >= '".$sd."' 
        AND 
            DateStamp <= '".$ed."'";
        
        if (DB::isError($iCnt = $this->_oConn->getOne($sql))) {
            
            catchExc($iCnt->getMessage());
            return false;
        }
        
        if (!$iCnt) {$iCnt = 0;}
        return $iCnt;
     }

    /** 
     * get total characters received for invoiceing
     *
     * @return integer record count
     * @access public
     */
     function getMemberCharacterRecCount($mi, $sd, $ed) {
        
        $sql = "SELECT
            sum(NbrBytes) as charcnt 
        FROM 
            datagate.dbo.dg_interchanges 
        WHERE 
            ISA_Receiver IN (select member_receiver_id from ".PREFIX."_member_ids where member_id = ".$mi.") 
        AND 
            DateStamp >= '".$sd."' 
        AND 
            DateStamp <= '".$ed."'";
        
        if (DB::isError($iCnt = $this->_oConn->getOne($sql))) {
            
            catchExc($iCnt->getMessage());
            return false;
        }
        if (!$iCnt) {$iCnt = 0;}
        return $iCnt;
     }

    /** 
     * get total characters sent for invoiceing
     *
     * @return integer record count
     * @access public
     */
     function getMemberCharacterSentCount($mi, $sd, $ed) {
        
        $sql = "SELECT
            sum(NbrBytes) as charcnt 
        FROM 
            datagate.dbo.dg_interchanges 
        WHERE 
            ISA_Sender IN (select member_receiver_id from ".PREFIX."_member_ids where member_id = ".$mi.") 
        AND 
            DateStamp >= '".$sd."' 
        AND 
            DateStamp <= '".$ed."'";
        
        if (DB::isError($iCnt = $this->_oConn->getOne($sql))) {
            
            catchExc($iCnt->getMessage());
            return false;
        }
        
        if (!$iCnt) {$iCnt = 0;}
        return $iCnt;
     }

    /** 
     * get trans sent count for invoiceing
     *
     * @return integer record count
     * @access public
     */
     function getMemberTransactionsSentCount($mi, $sd, $ed) {
        
        $sql = "SELECT
            count(*) as transcnt 
        FROM 
            datagate.dbo.dg_interchanges 
        WHERE 
            ISA_Sender IN (select member_receiver_id from ".PREFIX."_member_ids where member_id = ".$mi.") 
        AND 
            DateStamp >= '".$sd."' 
        AND 
            DateStamp <= '".$ed."'";
        
        if (DB::isError($iCnt = $this->_oConn->getOne($sql))) {
            
            catchExc($iCnt->getMessage());
            return false;
        }
        
        if (!$iCnt) {$iCnt = 0;}
        return $iCnt;
     }

    /** 
     * get trans received count for invoiceing
     *
     * @return integer record count
     * @access public
     */
     function getMemberTransactionsReceivedCount($mi, $sd, $ed) {
        
        $sql = "SELECT
            count(*) as transcnt 
        FROM 
            datagate.dbo.dg_interchanges 
        WHERE 
            ISA_Receiver IN (select member_receiver_id from ".PREFIX."_member_ids where member_id = ".$mi.") 
        AND 
            DateStamp >= '".$sd."' 
        AND 
            DateStamp <= '".$ed."'";
        
        if (DB::isError($iCnt = $this->_oConn->getOne($sql))) {
            
            catchExc($iCnt->getMessage());
            return false;
        }
        
        if (!$iCnt) {$iCnt = 0;}
        return $iCnt;
     }

    /** 
     * get partners using catalog count for invoiceing
     *
     * @return integer record count
     * @access public
     */
     function getMemberPartnersUsingCatalogCount($mi) {
        
        $sql = "select 
            count(cp.DataKey_id) as memcnt 
        from 
            edia_catalog_perms as cp, 
            edia_partners as p 
        where 
            cp.cat_partner_id = p.tp_id 
        and 
            p.tp_member_id = ".$mi."
        and 
            p.tp_status = 1
        and 
            cp.cat_perm = 1";
        
        if (DB::isError($iCnt = $this->_oConn->getOne($sql))) {
            
            catchExc($iCnt->getMessage());
            return false;
        }
        
        if (!$iCnt) {$iCnt = 0;}
        return $iCnt;
     }

     function str2no($number){
       $number = str_replace(".", "", $number);
       $number = str_replace(",", ".", $number);
       return $number;
     }
     
     function no2str($number){
       $number = number_format($number,2, ',', '.');
       return $number;
     }
     


    function exportInvoices($fi, $sd, $ed, $si) {
        
        // put together invoicing information
        
        $aLineT["TRNS"] = "!TRNS";	
        $aLineT["TRNSID"] = "TRNSID";
        $aLineT["TRNSTYPE"] = "TRNSTYPE";
        $aLineT["DATE"] = "DATE";
        $aLineT["ACCNT"] = "ACCNT";
        $aLineT["NAME"] = "NAME";
        $aLineT["CLASS"] = "CLASS";
        $aLineT["AMOUNT"] = "AMOUNT";
        $aLineT["DOCNUM"] = "DOCNUM";
        $aLineT["MEMO"] = "MEMO";
        $aLineT["CLEAR"] = "CLEAR";
        $aLineT["TOPRINT"] = "TOPRINT";
        $aLineT["ADDR1"] = "ADDR1";
        $aLineT["ADDR2"] = "ADDR2";
        $aLineT["ADDR3"] = "ADDR3";
        $aLineT["ADDR4"] = "ADDR4";
        $aLineT["ADDR5"] = "ADDR5";
        $aLineT["DUEDATE"] = "DUEDATE";
        $aLineT["TERMS"] = "TERMS";
        $aLineT["PAID"] = "PAID";
        $aLineT["SHIPDATE"] = "SHIPDATE";
        $aLineT["INVMEMO"] = "INVMEMO";
        $return = fputcsv($fi, $aLineT, "	");
        
        $aLineS["SPL"] = "!SPL";
        $aLineS["SPLID"] = "SPLID";
        $aLineS["TRNSTYPE"] = "TRNSTYPE";
        $aLineS["DATE"] = "DATE";
        $aLineS["ACCNT"] = "ACCNT";
        $aLineS["NAME"] = "NAME";
        $aLineS["CLASS"] = "CLASS";
        $aLineS["AMOUNT"] = "AMOUNT";
        $aLineS["DOCNUM"] = "DOCNUM";
        $aLineS["MEMO"] = "MEMO";
        $aLineS["CLEAR"] = "CLEAR";
        $aLineS["QNTY"] = "QNTY";
        $aLineS["PRICE"] = "PRICE";
        $aLineS["INVITEM"] = "INVITEM";
        $aLineS["PAYMETH"] = "PAYMETH";
        $aLineS["TAXABLE"] = "TAXABLE";
        $aLineS["REIMBEXP"] = "REIMBEXP";
        $aLineS["EXTRA"] = "EXTRA";
        $return = fputcsv($fi, $aLineS, "	");

        $aLineE["ENDTRNS"] = "!ENDTRNS";
        
        $return = fputcsv($fi, $aLineE, "	");

        $sql = "SELECT * FROM edia_members where member_status = 1 and billing_option > 1";
        
        if (DB::isError($rsTmp = $this->_oConn->query($sql))) {
            
            catchExc($rsTmp->getMessage());
            return false;
        }
        
        // loop through result and build return array
        $i = 1;
        while ($aRow = $rsTmp->fetchRow(DB_FETCHMODE_ASSOC)) {
            

            $sAdd = $aRow["billing_address_city"]." ".$aRow["billing_address_state"].", ".$aRow["billing_address_zip"];
            $tc = $this->getMemberTransactionCount($aRow["member_id"], $sd, $ed);
            $tsc = $this->getMemberTransactionsSentCount($aRow["member_id"], $sd, $ed);
            $trc = $this->getMemberTransactionsReceivedCount($aRow["member_id"], $sd, $ed);
            $tcsc = $this->getMemberCharacterSentCount($aRow["member_id"], $sd, $ed);
            $tcrc = $this->getMemberCharacterRecCount($aRow["member_id"], $sd, $ed);
            $tcsctm = $this->getMemberCharacterSentToMemCount($aRow["member_id"], $sd, $ed);
            $tcrcfm = $this->getMemberCharacterRecFromMemCount($aRow["member_id"], $sd, $ed);
            $tcsctnm = $this->getMemberCharacterSentToNonMemCount($aRow["member_id"], $sd, $ed);
            $tcrcfnm = $this->getMemberCharacterRecFromNonMemCount($aRow["member_id"], $sd, $ed);
            $mpucc = $this->getMemberPartnersUsingCatalogCount($aRow["member_id"]);

            $aLine["TRNS"] = "TRNS";
            $aLine["TRNSID"] = $i;
            $aLine["TRNSTYPE"] = "INVOICE";
            $aLine["DATE"] = date("n/j/Y", time());
            $aLine["ACCNT"] = "Accounts Receivable";
            $aLine["NAME"] = $aRow["member_login"];
            $aLine["CLASS"] = "Data Services";
            if ((!strcmp("2", $aRow["billing_option"]))||(!strcmp("5", $aRow["billing_option"]))) {
                if ($tc <= $aRow["billing_contract_qty"]) {
                    $aLine["AMOUNT"] = $aRow["billing_contract_amount"];
                    $oc = 0;
                } else {
                    $oc = $tc - $aRow["billing_contract_qty"];
                    $om = $oc * $aRow["billing_overage_amount"];
                    $aLine["AMOUNT"] = $om + $aRow["billing_contract_amount"];
                }
            } elseif ((!strcmp("3", $aRow["billing_option"]))||(!strcmp("6", $aRow["billing_option"]))) {
                $ot = 0;
                $ot += $aRow["billing_monthly_service_fee"];
                $ot += number_format(($aRow["billing_kilo_character_mem"]*(($tcrcfm) / 1000)), 2, '.', '');
                $ot += number_format(($aRow["billing_kilo_character_mem"]*(($tcsctm) / 1000)), 2, '.', '');
                $ot += number_format(($aRow["billing_kilo_character_nonmem"]*(($tcrcfnm) / 1000)), 2, '.', '');
                $ot += number_format(($aRow["billing_kilo_character_nonmem"]*(($tcsctnm) / 1000)), 2, '.', '');
                $aLine["AMOUNT"] = $ot;
            }
            if ((!strcmp("5", $aRow["billing_option"]))||(!strcmp("6", $aRow["billing_option"]))) {
                $aLine["AMOUNT"] += $aRow["billing_cat_subscription_fee"];
                $aLine["AMOUNT"] += ($aRow["billing_cat_per_cust_fee"] * $mpucc);
            }
            $aLine["AMOUNT"] = number_format($aLine["AMOUNT"], 2, '.', '');
            $aLine["DOCNUM"] = $si;
            $aLine["MEMO"] = "Period Covering ".date("n/j/Y", strtotime($sd))." - ".date("n/j/Y", strtotime($ed));
            $aLine["CLEAR"] = "N";
            $aLine["TOPRINT"] = "Y";
            $aLine["ADDR1"] = $aRow["member_company_name"];
            $aLine["ADDR2"] = $aRow["billing_address_1"];
            !strcmp("", $aRow["billing_address_2"]) ? $aLine["ADDR3"] = $sAdd : $aLine["ADDR3"] = $aRow["billing_address_2"];
            !strcmp($sAdd, $aLine["ADDR3"]) ? $aLine["ADDR4"] = "" : $aLine["ADDR4"] = $sAdd;
            $aLine["ADDR5"] = "";
            $aLine["DUEDATE"] = getNthWorkingDay(date("n/j/Y", time()),15);
            $aLine["TERMS"] = "NET15";
            $aLine["PAID"] = "N";
            $aLine["SHIPDATE"] = date("n/j/Y", time());
            $aLine["INVMEMO"] = "Thank you for your business.";
            $return = fputcsv($fi, $aLine, "	");
            ++$i;

            if ((!strcmp("2", $aRow["billing_option"]))||(!strcmp("5", $aRow["billing_option"]))) {

                ++$i;
                $aLineS["SPL"] = "SPL";
                $aLineS["SPLID"] = $i;
                $aLineS["TRNSTYPE"] = "INVOICE";
                $aLineS["DATE"] = date("n/j/Y", time());
                $aLineS["ACCNT"] = "Data Services";
                $aLineS["NAME"] = $aRow["member_login"];
                $aLineS["CLASS"] = "Data Services";
                $aLineS["AMOUNT"] = "-".$aRow["billing_contract_amount"];
                $aLineS["DOCNUM"] = "";
                $aLineS["MEMO"] = "Monthly Min ".currency($aRow["billing_contract_amount"])." - ".$aRow["billing_contract_qty"]." Transactions - Adjusted Invoice";
                $aLineS["CLEAR"] = "N";
                $aLineS["QNTY"] = "-1";
                $aLineS["PRICE"] = $aRow["billing_contract_amount"];
                $aLineS["INVITEM"] = "DATATransSvc";
                $aLineS["PAYMETH"] = "";
                $aLineS["TAXABLE"] = "N";
                $aLineS["REIMBEXP"] = "N";
                $aLineS["EXTRA"] = "";
                $return = fputcsv($fi, $aLineS, "	");
                
                if ($oc > 0) {
                    ++$i;
                    $aLineS["SPL"] = "SPL";
                    $aLineS["SPLID"] = $i;
                    $aLineS["TRNSTYPE"] = "INVOICE";
                    $aLineS["DATE"] = date("n/j/Y", time());
                    $aLineS["ACCNT"] = "Data Services";
                    $aLineS["NAME"] = $aRow["member_login"];
                    $aLineS["CLASS"] = "Data Services";
                    $aLineS["AMOUNT"] = "-".$om;
                    $aLineS["DOCNUM"] = "";
                    $aLineS["MEMO"] = "Transaction Overage - ".$oc." Transactions - ".currency($aRow["billing_overage_amount"])." Per Transaction";
                    $aLineS["CLEAR"] = "N";
                    $aLineS["QNTY"] = "-1";
                    $aLineS["PRICE"] = $om;
                    $aLineS["INVITEM"] = "DATATransSvc";
                    $aLineS["PAYMETH"] = "";
                    $aLineS["TAXABLE"] = "N";
                    $aLineS["REIMBEXP"] = "N";
                    $aLineS["EXTRA"] = "";
                    $return = fputcsv($fi, $aLineS, "	");
                }
            } elseif ((!strcmp("3", $aRow["billing_option"]))||(!strcmp("6", $aRow["billing_option"]))) {
                
                ++$i;
                $aLineS["SPL"] = "SPL";
                $aLineS["SPLID"] = $i;
                $aLineS["TRNSTYPE"] = "INVOICE";
                $aLineS["DATE"] = date("n/j/Y", time());
                $aLineS["ACCNT"] = "Data Services";
                $aLineS["NAME"] = $aRow["member_login"];
                $aLineS["CLASS"] = "Data Services";
                $ocms = $aRow["billing_monthly_service_fee"];
                $aLineS["AMOUNT"] = "-".$ocms;
                $aLineS["DOCNUM"] = "";
                $aLineS["MEMO"] = "Monthly Network Charge - Single Location";
                $aLineS["CLEAR"] = "N";
                $aLineS["QNTY"] = "-1";
                $aLineS["PRICE"] = $ocms;
                $aLineS["INVITEM"] = "DATATransSvc";
                $aLineS["PAYMETH"] = "";
                $aLineS["TAXABLE"] = "N";
                $aLineS["REIMBEXP"] = "N";
                $aLineS["EXTRA"] = "";
                $return = fputcsv($fi, $aLineS, "	");

                ++$i;
                $aLineS["SPL"] = "SPL";
                $aLineS["SPLID"] = $i;
                $aLineS["TRNSTYPE"] = "INVOICE";
                $aLineS["DATE"] = date("n/j/Y", time());
                $aLineS["ACCNT"] = "Data Services";
                $aLineS["NAME"] = $aRow["member_login"];
                $aLineS["CLASS"] = "Data Services";
                $ocms = ($aRow["billing_kilo_character_mem"]*(($tcrcfm) / 1000));
                $aLineS["AMOUNT"] = "-".number_format($ocms, 2, '.', '');
                $aLineS["DOCNUM"] = "";
                $aLineS["MEMO"] = number_format($tcrcfm)." Network Characters Received - ".$aRow["billing_kilo_character_mem"]." Per Thousand Character";
                $aLineS["CLEAR"] = "N";
                $aLineS["QNTY"] = "-1";
                $aLineS["PRICE"] = number_format($ocms, 2, '.', '');
                $aLineS["INVITEM"] = "DATATransSvc";
                $aLineS["PAYMETH"] = "";
                $aLineS["TAXABLE"] = "N";
                $aLineS["REIMBEXP"] = "N";
                $aLineS["EXTRA"] = "";
                $return = fputcsv($fi, $aLineS, "	");

                ++$i;
                $aLineS["SPL"] = "SPL";
                $aLineS["SPLID"] = $i;
                $aLineS["TRNSTYPE"] = "INVOICE";
                $aLineS["DATE"] = date("n/j/Y", time());
                $aLineS["ACCNT"] = "Data Services";
                $aLineS["NAME"] = $aRow["member_login"];
                $aLineS["CLASS"] = "Data Services";
                $ocms = ($aRow["billing_kilo_character_mem"]*(($tcsctm) / 1000));
                $aLineS["AMOUNT"] = "-".number_format($ocms, 2, '.', '');
                $aLineS["DOCNUM"] = "";
                $aLineS["MEMO"] = number_format($tcsctm)." Network Characters Sent - ".$aRow["billing_kilo_character_mem"]." Per Thousand Character";
                $aLineS["CLEAR"] = "N";
                $aLineS["QNTY"] = "-1";
                $aLineS["PRICE"] = number_format($ocms, 2, '.', '');
                $aLineS["INVITEM"] = "DATATransSvc";
                $aLineS["PAYMETH"] = "";
                $aLineS["TAXABLE"] = "N";
                $aLineS["REIMBEXP"] = "N";
                $aLineS["EXTRA"] = "";
                $return = fputcsv($fi, $aLineS, "	");

                ++$i;
                $aLineS["SPL"] = "SPL";
                $aLineS["SPLID"] = $i;
                $aLineS["TRNSTYPE"] = "INVOICE";
                $aLineS["DATE"] = date("n/j/Y", time());
                $aLineS["ACCNT"] = "Data Services";
                $aLineS["NAME"] = $aRow["member_login"];
                $aLineS["CLASS"] = "Data Services";
                $ocms = ($aRow["billing_kilo_character_nonmem"]*(($tcrcfnm) / 1000));
                $aLineS["AMOUNT"] = "-".number_format($ocms, 2, '.', '');
                $aLineS["DOCNUM"] = "";
                $aLineS["MEMO"] = number_format($tcrcfnm)." Interchanged Characters Received - ".$aRow["billing_kilo_character_nonmem"]." Per Thousand Character";
                $aLineS["CLEAR"] = "N";
                $aLineS["QNTY"] = "-1";
                $aLineS["PRICE"] = number_format($ocms, 2, '.', '');
                $aLineS["INVITEM"] = "DATATransSvc";
                $aLineS["PAYMETH"] = "";
                $aLineS["TAXABLE"] = "N";
                $aLineS["REIMBEXP"] = "N";
                $aLineS["EXTRA"] = "";
                $return = fputcsv($fi, $aLineS, "	");

                ++$i;
                $aLineS["SPL"] = "SPL";
                $aLineS["SPLID"] = $i;
                $aLineS["TRNSTYPE"] = "INVOICE";
                $aLineS["DATE"] = date("n/j/Y", time());
                $aLineS["ACCNT"] = "Data Services";
                $aLineS["NAME"] = $aRow["member_login"];
                $aLineS["CLASS"] = "Data Services";
                $ocms = ($aRow["billing_kilo_character_nonmem"]*(($tcsctnm) / 1000));
                $aLineS["AMOUNT"] = "-".number_format($ocms, 2, '.', '');
                $aLineS["DOCNUM"] = "";
                $aLineS["MEMO"] = number_format($tcsctnm)." Interchanged Characters Sent - ".$aRow["billing_kilo_character_nonmem"]." Per Thousand Character";
                $aLineS["CLEAR"] = "N";
                $aLineS["QNTY"] = "-1";
                $aLineS["PRICE"] = number_format($ocms, 2, '.', '');
                $aLineS["INVITEM"] = "DATATransSvc";
                $aLineS["PAYMETH"] = "";
                $aLineS["TAXABLE"] = "N";
                $aLineS["REIMBEXP"] = "N";
                $aLineS["EXTRA"] = "";
                $return = fputcsv($fi, $aLineS, "	");
            }

            ++$i;
            $aLineS["SPL"] = "SPL";
            $aLineS["SPLID"] = $i;
            $aLineS["TRNSTYPE"] = "INVOICE";
            $aLineS["DATE"] = date("n/j/Y", time());
            $aLineS["ACCNT"] = "";
            $aLineS["NAME"] = "";
            $aLineS["CLASS"] = "";
            $aLineS["AMOUNT"] = "";
            $aLineS["DOCNUM"] = "";
            $aLineS["MEMO"] = number_format(number_format($tsc)+number_format($trc))." Transactions Total - ".number_format($tsc)." Transaction Sent - ".number_format($trc)." Transactions Received";
            $aLineS["CLEAR"] = "N";
            $aLineS["QNTY"] = "";
            $aLineS["PRICE"] = "";
            $aLineS["INVITEM"] = "";
            $aLineS["PAYMETH"] = "";
            $aLineS["TAXABLE"] = "N";
            $aLineS["REIMBEXP"] = "N";
            $aLineS["EXTRA"] = "";
            $return = fputcsv($fi, $aLineS, "	");

            ++$i;
            $aLineS["SPL"] = "SPL";
            $aLineS["SPLID"] = $i;
            $aLineS["TRNSTYPE"] = "INVOICE";
            $aLineS["DATE"] = date("n/j/Y", time());
            $aLineS["ACCNT"] = "";
            $aLineS["NAME"] = "";
            $aLineS["CLASS"] = "";
            $aLineS["AMOUNT"] = "";
            $aLineS["DOCNUM"] = "";
            $aLineS["MEMO"] = number_format($tcsc+$tcrc)." Characters Total - ".number_format($tcsc)." Characters Sent - ".number_format($tcrc)." Characters Received";
            $aLineS["CLEAR"] = "N";
            $aLineS["QNTY"] = "";
            $aLineS["PRICE"] = "";
            $aLineS["INVITEM"] = "";
            $aLineS["PAYMETH"] = "";
            $aLineS["TAXABLE"] = "N";
            $aLineS["REIMBEXP"] = "N";
            $aLineS["EXTRA"] = "";
            $return = fputcsv($fi, $aLineS, "	");

            
            ++$i;
            $aLineS["SPL"] = "SPL";
            $aLineS["SPLID"] = $i;
            $aLineS["TRNSTYPE"] = "INVOICE";
            $aLineS["DATE"] = date("n/j/Y", time());
            $aLineS["ACCNT"] = "";
            $aLineS["NAME"] = "";
            $aLineS["CLASS"] = "";
            $aLineS["AMOUNT"] = "";
            $aLineS["DOCNUM"] = "";
            $aLineS["MEMO"] = "Period Covering ".date("n/j/Y", strtotime($sd))." - ".date("n/j/Y", strtotime($ed));
            $aLineS["CLEAR"] = "N";
            $aLineS["QNTY"] = "";
            $aLineS["PRICE"] = "";
            $aLineS["INVITEM"] = "";
            $aLineS["PAYMETH"] = "";
            $aLineS["TAXABLE"] = "N";
            $aLineS["REIMBEXP"] = "N";
            $aLineS["EXTRA"] = "";
            $return = fputcsv($fi, $aLineS, "	");
            
            if ((!strcmp("5", $aRow["billing_option"]))||(!strcmp("6", $aRow["billing_option"]))) {
                ++$i;
                $aLineS["SPL"] = "SPL";
                $aLineS["SPLID"] = $i;
                $aLineS["TRNSTYPE"] = "INVOICE";
                $aLineS["DATE"] = date("n/j/Y", time());
                $aLineS["ACCNT"] = "Data Services";
                $aLineS["NAME"] = $aRow["member_login"];
                $aLineS["CLASS"] = "Data Services";
                $aLineS["AMOUNT"] = "-".number_format($aRow["billing_cat_subscription_fee"], 2, '.', '');
                $aLineS["DOCNUM"] = "";
                $aLineS["MEMO"] = "Catalog Service Subscription";
                $aLineS["CLEAR"] = "N";
                $aLineS["QNTY"] = "-1";
                $aLineS["PRICE"] = number_format($aRow["billing_cat_subscription_fee"], 2, '.', '');
                $aLineS["INVITEM"] = "DATATransSvc";
                $aLineS["PAYMETH"] = "";
                $aLineS["TAXABLE"] = "N";
                $aLineS["REIMBEXP"] = "N";
                $aLineS["EXTRA"] = "";
                $return = fputcsv($fi, $aLineS, "	");

                ++$i;
                $aLineS["SPL"] = "SPL";
                $aLineS["SPLID"] = $i;
                $aLineS["TRNSTYPE"] = "INVOICE";
                $aLineS["DATE"] = date("n/j/Y", time());
                $aLineS["ACCNT"] = "Data Services";
                $aLineS["NAME"] = $aRow["member_login"];
                $aLineS["CLASS"] = "Data Services";
                $aLineS["AMOUNT"] = "-".number_format(($aRow["billing_cat_per_cust_fee"] * $mpucc), 2, '.', '');
                $aLineS["DOCNUM"] = "";
                $aLineS["MEMO"] = "Active Catalog Relationships";
                $aLineS["CLEAR"] = "N";
                $aLineS["QNTY"] = "-".$mpucc;
                $aLineS["PRICE"] = number_format($aRow["billing_cat_per_cust_fee"], 2, '.', '');
                $aLineS["INVITEM"] = "DATATransSvc";
                $aLineS["PAYMETH"] = "";
                $aLineS["TAXABLE"] = "N";
                $aLineS["REIMBEXP"] = "N";
                $aLineS["EXTRA"] = "";
                $return = fputcsv($fi, $aLineS, "	");

                ++$i;
                $aLine["AMOUNT"] += $aRow["billing_cat_subscription_fee"];
                $aLine["AMOUNT"] += ($aRow["billing_cat_per_cust_fee"] * $mpucc);
            
            }
            
            $aLineE["ENDTRNS"] = "ENDTRNS";
            $return = fputcsv($fi, $aLineE, "	");

            ++$si;
        }
        return $return;
    }    



    // INSERT METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    
    // UPDATE METHODS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    
}

?>
